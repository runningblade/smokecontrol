#ifndef SPACE_TIME_MESH_H
#define SPACE_TIME_MESH_H

#include "FluidSolver.h"
#include "CommonFile/ImplicitFuncInterface.h"
#include <boost/property_tree/ptree.hpp>

PRJ_BEGIN

class PDForce;
class SphereFunc : public ImplicitFunc<scalar>
{
public:
  SphereFunc(const Vec3& ctr,scalar rad,scalar srad);
  scalar operator()(const Vec3& pos) const;
private:
  Vec3 _ctr;
  scalar _rad,_srad;
};
class CubeFunc : public ImplicitFunc<scalar>
{
public:
  CubeFunc(const Vec3& ctr,const Vec3& ext,scalar srad);
  CubeFunc(const BBox<scalar>& bb,scalar srad);
  scalar operator()(const Vec3& pos) const;
private:
  BBox<scalar> _bb;
  scalar _srad;
};
class ConstantVelFunc : public VelFunc<scalar>
{
public:
  ConstantVelFunc(const ImplicitFunc<scalar>& func,const Vec3& vel);
  Vec3 operator()(const Vec3& pos) const;
private:
  const ImplicitFunc<scalar>& _func;
  const Vec3 _vel;
};
class SpaceTimeMesh : public Traits, public Serializable
{
public:
  typedef Eigen::Matrix<scalar,7,1> TRANS;
  enum MODE {
    NO_SOLVER,
    SPATIAL_SOLVE,
    SPATIAL_MG_SOLVE,
  };
  SpaceTimeMesh();
  SpaceTimeMesh(const SpaceTimeMesh& mesh);
  SpaceTimeMesh(const boost::property_tree::ptree& pt,bool initialize);
  SpaceTimeMesh(const SpaceTimeMesh& mesh,sizeType start,sizeType end);
  virtual ~SpaceTimeMesh();
  virtual SpaceTimeMesh& operator=(const SpaceTimeMesh& other);
  virtual boost::shared_ptr<Serializable> copy() const;
  virtual bool write(ostream& os,IOData* dat) const;
  virtual bool read(istream& is,IOData* dat);
  //getter
  const boost::property_tree::ptree& getPt() const;
  boost::property_tree::ptree& getPt();
  const ScalarField* keyFrame(sizeType id) const;
  const KeyFrameMap& keyFrameMap() const;
  boost::shared_ptr<FluidState> statePtr(sizeType id,sizeType maxId);
  const FluidState& state(sizeType id) const;
  FluidState& state(sizeType id);
  sizeType nrFrame() const;
  template <typename T=FluidSolver>
  T& solver() {
    T* sol=boost::dynamic_pointer_cast<T>(_sol).get();
    ASSERT_MSGV(sol,"Your solver doesn't match type: %s",typeid(T).name())
    return *sol;
  }
  //setter
  void initSolver();
  void init(const Vec3i& nr,const Vec3& len,sizeType N);
  void writeVTK(const string& path,sizeType start=-1,sizeType end=-1,bool vel=true,bool rho=true) const;
  void update();
  void updateAdvect(sizeType fromFrame=-1);
  void updateAdvect(Advector::ADVECTOR adv,bool secondOrder);
  void setFrameTaylorVortex(scalar a=0.12f,scalar sep=0.16f,scalar coef=1.0f);
  bool setFrameAutoFitArea(sizeType fid,const string& path,Vec4i loc=Vec4i::Zero(),const scalar* extMin=NULL,const scalar* extMax=NULL);
  bool setFrameAutoFitArea(sizeType fid,const string& path,Vec3 marginMin=Vec3::Zero(),Vec3 marginMax=Vec3::Zero());
  TRANS setFrame(sizeType fid,const string& path,const Vec3& marginMin=Vec3::Zero(),const Vec3& marginMax=Vec3::Zero(),const TRANS* T=NULL,bool lazy=true);
  void setFrame(sizeType fid,const string& path,const Vec4i& loc=Vec4i::Zero(),scalar frac=0,const scalar* extMin=NULL,const scalar* extMax=NULL);
  void setFrame(sizeType fid,const ImplicitFunc<scalar>& f,bool add=false);
  void setInitVel(const VelFunc<scalar>& f);
  void checkValidity() const;
  void continueAnimation(const string& path,sizeType N=-1);
  boost::shared_ptr<PDForce> _PDForce;
private:
  ScalarField* getTarget(sizeType fid);
  void writeKeyFrame(const string& path,sizeType fid,const ScalarField& f) const;
  void readKeyFrameSphere(const boost::property_tree::ptree& pt);
  void readKeyFrameCube(const boost::property_tree::ptree& pt);
  void readKeyFrameImage(const boost::property_tree::ptree& pt);
  void readKeyFrameMesh(const boost::property_tree::ptree& pt,TRANS& T);
  void readKeyFrameArticulated(const boost::property_tree::ptree& pt,TRANS& T);
  //data
  boost::property_tree::ptree _pt;
  vector<boost::shared_ptr<FluidState> > _state;
  boost::shared_ptr<FluidSolver> _sol;
  KeyFrameMap _keyFrame;
};

PRJ_END

#endif
