#include "FluidSolverSpatialMG.h"
#include "CommonFile/GridOp.h"
#include "SpaceTimeMesh.h"
#include "Optimizer.h"

#include <boost/property_tree/xml_parser.hpp>
#include <boost/filesystem/operations.hpp>
#include <boost/lexical_cast.hpp>

USE_PRJ_NAMESPACE

int main(int argc,char** argv)
{
  //debugInterp();
  //debugBoundaryCondition();
  //debugTridiagSolver();
  //debugFluidSolver(2,Vec3c(1,0,0));
  //debugFluidSolver(3,Vec3c(1,0,1));
  //debugFluidSolverSpatialMG(2,Vec3c(1,0,0));
  //debugFluidSolverSpatialMG(3,Vec3c(1,0,1));
  //debugPassiveOpt(2,Advector::IMPLICIT,Vec3c(1,0,0));
  //debugPassiveOpt(3,Advector::IMPLICIT,Vec3c(1,0,1));
  //debugActiveOpt(2,FluidSolver::MIDPOINT,Vec3c(0,0,0),false);
  //debugActiveOpt(3,FluidSolver::MIDPOINT,Vec3c(1,0,1));
  //debugGradHess(30,5);
  //debugSolveLM(30,5);
  //QuadraticTerm::solveProbLM("./prob.dat",false);
  //exit(0);

  if(endsWith(argv[1],".txt")) {
    string line;
    boost::filesystem::ifstream is(argv[1]);
    while(!is.eof() && std::getline(is,line).good()) {
      line=(boost::filesystem::path(argv[1]).parent_path()/line).string();
      if(!boost::filesystem::exists(line))
        continue;
      boost::property_tree::ptree pt;
      boost::property_tree::read_xml(line,pt);
      INFOV("Reading %s",line.c_str())

      boost::filesystem::path path(line);
      pt.put<string>("outputPath",path.parent_path().string()+"/result");
      SpaceTimeMesh mesh(pt,true);
      FluidOpt opt(mesh);
      opt.optimize();
    }
  } else if(endsWith(argv[1],".xml")) {
    for(sizeType i=1; i<argc; i++) {
      if(!boost::filesystem::exists(argv[i]))
        continue;
      boost::property_tree::ptree pt;
      boost::property_tree::read_xml(argv[i],pt);
      INFOV("Reading %s",argv[i])

      boost::filesystem::path path(argv[i]);
      pt.put<string>("outputPath",path.parent_path().string()+"/result");
      SpaceTimeMesh mesh(pt,true);
      if(mesh._PDForce)
        mesh.continueAnimation(pt.get<string>("outputPath"));
      else {
        FluidOpt opt(mesh);
        opt.optimize();
        //{
        //  IOData dat;
        //  boost::filesystem::ifstream is("./mesh.dat",ios::binary);
        //  mesh.read(is,&dat);
        //}
        //opt.optimizeActive();
      }
    }
  } else if(endsWith(argv[1],".dat") && argc == 4) {
    IOData dat;
    SpaceTimeMesh mesh;
    boost::filesystem::ifstream is(argv[1],ios::binary);
    mesh.read(is,&dat);

    scalar time=atof(argv[2]),step=atof(argv[3]);
    boost::filesystem::create_directory("./velocitySample");
    for(sizeType i=0; i<mesh.nrFrame(); i++) {
      if(!mesh.state(i).velPtr(FluidState::VELOCITY))
        continue;
      const MACVelocityField& vel=mesh.state(i).vel(FluidState::VELOCITY);
      const string path="./velocitySample/frm"+boost::lexical_cast<string>(i)+".vtk";
      GridOp<scalar,scalar>::write2DVelocityGridVTK(path,vel,time,Vec3::Constant(step),false);
    }
  } else if(endsWith(argv[1],".dat")) {
    ASSERT_MSG(argc == 5,"Usage: [exe] mesh.dat outputPath nrFrame ptTree")
    IOData dat;
    SpaceTimeMesh mesh;
    boost::filesystem::ifstream is(argv[1],ios::binary);
    boost::property_tree::read_xml(argv[4],mesh.getPt());
    mesh.read(is,&dat);
    mesh.continueAnimation(argv[2],atoi(argv[3]));
  }
  return 0;
}
