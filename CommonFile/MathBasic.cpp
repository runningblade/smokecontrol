#include "MathBasic.h"
#include <boost/random.hpp>

PRJ_BEGIN

scalarF ScalarUtil<scalarF>::scalar_max=FLT_MAX;
scalarF ScalarUtil<scalarF>::scalar_eps=1E-10f;
scalarF ScalarUtil<scalarF>::scalar_inf=numeric_limits<scalarF>::infinity();

scalarD ScalarUtil<scalarD>::scalar_max=DBL_MAX;
scalarD ScalarUtil<scalarD>::scalar_eps=1E-20f;
scalarD ScalarUtil<scalarD>::scalar_inf=numeric_limits<scalarF>::infinity();

sizeType ScalarUtil<sizeType>::scalar_max=numeric_limits<sizeType>::max();
sizeType ScalarUtil<sizeType>::scalar_eps=1;

bool compL(const Vec3i& a,const Vec3i& b)
{
  return a.x() < b.x() && a.y() < b.y() && a.z() < b.z();
}
bool compLE(const Vec3i& a,const Vec3i& b)
{
  return a.x() <= b.x() && a.y() <= b.y() && a.z() <= b.z();
}
bool compG(const Vec3i& a,const Vec3i& b)
{
  return a.x() > b.x() && a.y() > b.y() && a.z() > b.z();
}
bool compGE(const Vec3i& a,const Vec3i& b)
{
  return a.x() >= b.x() && a.y() >= b.y() && a.z() >= b.z();
}
bool compL(const Vec2i& a,const Vec2i& b)
{
  return a.x() < b.x() && a.y() < b.y();
}
bool compLE(const Vec2i& a,const Vec2i& b)
{
  return a.x() <= b.x() && a.y() <= b.y();
}
bool compG(const Vec2i& a,const Vec2i& b)
{
  return a.x() > b.x() && a.y() > b.y();
}
bool compGE(const Vec2i& a,const Vec2i& b)
{
  return a.x() >= b.x() && a.y() >= b.y();
}

bool compL(const Vec3f& a,const Vec3f& b)
{
  return a.x() < b.x() && a.y() < b.y() && a.z() < b.z();
}
bool compLE(const Vec3f& a,const Vec3f& b)
{
  return a.x() <= b.x() && a.y() <= b.y() && a.z() <= b.z();
}
bool compG(const Vec3f& a,const Vec3f& b)
{
  return a.x() > b.x() && a.y() > b.y() && a.z() > b.z();
}
bool compGE(const Vec3f& a,const Vec3f& b)
{
  return a.x() >= b.x() && a.y() >= b.y() && a.z() >= b.z();
}
bool compL(const Vec2f& a,const Vec2f& b)
{
  return a.x() < b.x() && a.y() < b.y();
}
bool compLE(const Vec2f& a,const Vec2f& b)
{
  return a.x() <= b.x() && a.y() <= b.y();
}
bool compG(const Vec2f& a,const Vec2f& b)
{
  return a.x() > b.x() && a.y() > b.y();
}
bool compGE(const Vec2f& a,const Vec2f& b)
{
  return a.x() >= b.x() && a.y() >= b.y();
}

bool compL(const Vec3d& a,const Vec3d& b)
{
  return a.x() < b.x() && a.y() < b.y() && a.z() < b.z();
}
bool compLE(const Vec3d& a,const Vec3d& b)
{
  return a.x() <= b.x() && a.y() <= b.y() && a.z() <= b.z();
}
bool compG(const Vec3d& a,const Vec3d& b)
{
  return a.x() > b.x() && a.y() > b.y() && a.z() > b.z();
}
bool compGE(const Vec3d& a,const Vec3d& b)
{
  return a.x() >= b.x() && a.y() >= b.y() && a.z() >= b.z();
}
bool compL(const Vec2d& a,const Vec2d& b)
{
  return a.x() < b.x() && a.y() < b.y();
}
bool compLE(const Vec2d& a,const Vec2d& b)
{
  return a.x() <= b.x() && a.y() <= b.y();
}
bool compG(const Vec2d& a,const Vec2d& b)
{
  return a.x() > b.x() && a.y() > b.y();
}
bool compGE(const Vec2d& a,const Vec2d& b)
{
  return a.x() >= b.x() && a.y() >= b.y();
}

scalarF compMin(const scalarF& a,const scalarF& b)
{
  return min(a,b);
}
scalarF compMax(const scalarF& a,const scalarF& b)
{
  return max(a,b);
}
scalarD compMin(const scalarD& a,const scalarD& b)
{
  return min(a,b);
}
scalarD compMax(const scalarD& a,const scalarD& b)
{
  return max(a,b);
}

Vec2i compMin(const Vec2i& a,const Vec2i& b)
{
  return Vec2i(min(a.x(),b.x()),min(a.y(),b.y()));
}
Vec3i compMin(const Vec3i& a,const Vec3i& b)
{
  return Vec3i(min(a.x(),b.x()),min(a.y(),b.y()),min(a.z(),b.z()));
}
Vec4i compMin(const Vec4i& a,const Vec4i& b)
{
  return Vec4i(min(a.x(),b.x()),min(a.y(),b.y()),min(a.z(),b.z()),min(a.w(),b.w()));
}

Vec2i compMax(const Vec2i& a,const Vec2i& b)
{
  return Vec2i(max(a.x(),b.x()),max(a.y(),b.y()));
}
Vec3i compMax(const Vec3i& a,const Vec3i& b)
{
  return Vec3i(max(a.x(),b.x()),max(a.y(),b.y()),max(a.z(),b.z()));
}
Vec4i compMax(const Vec4i& a,const Vec4i& b)
{
  return Vec4i(max(a.x(),b.x()),max(a.y(),b.y()),max(a.z(),b.z()),max(a.w(),b.w()));
}

Vec2f compMin(const Vec2f& a,const Vec2f& b)
{
  return Vec2f(min(a.x(),b.x()),min(a.y(),b.y()));
}
Vec3f compMin(const Vec3f& a,const Vec3f& b)
{
  return Vec3f(min(a.x(),b.x()),min(a.y(),b.y()),min(a.z(),b.z()));
}
Vec4f compMin(const Vec4f& a,const Vec4f& b)
{
  return Vec4f(min(a.x(),b.x()),min(a.y(),b.y()),min(a.z(),b.z()),min(a.w(),b.w()));
}

Vec2f compMax(const Vec2f& a,const Vec2f& b)
{
  return Vec2f(max(a.x(),b.x()),max(a.y(),b.y()));
}
Vec3f compMax(const Vec3f& a,const Vec3f& b)
{
  return Vec3f(max(a.x(),b.x()),max(a.y(),b.y()),max(a.z(),b.z()));
}
Vec4f compMax(const Vec4f& a,const Vec4f& b)
{
  return Vec4f(max(a.x(),b.x()),max(a.y(),b.y()),max(a.z(),b.z()),max(a.w(),b.w()));
}

Vec2uc compMin(const Vec2uc& a,const Vec2uc& b)
{
  return Vec2uc(min(a.x(),b.x()),min(a.y(),b.y()));
}
Vec3uc compMin(const Vec3uc& a,const Vec3uc& b)
{
  return Vec3uc(min(a.x(),b.x()),min(a.y(),b.y()),min(a.z(),b.z()));
}
Vec4uc compMin(const Vec4uc& a,const Vec4uc& b)
{
  return Vec4uc(min(a.x(),b.x()),min(a.y(),b.y()),min(a.z(),b.z()),min(a.w(),b.w()));
}

Vec2uc compMax(const Vec2uc& a,const Vec2uc& b)
{
  return Vec2uc(max(a.x(),b.x()),max(a.y(),b.y()));
}
Vec3uc compMax(const Vec3uc& a,const Vec3uc& b)
{
  return Vec3uc(max(a.x(),b.x()),max(a.y(),b.y()),max(a.z(),b.z()));
}
Vec4uc compMax(const Vec4uc& a,const Vec4uc& b)
{
  return Vec4uc(max(a.x(),b.x()),max(a.y(),b.y()),max(a.z(),b.z()),max(a.w(),b.w()));
}

Vec2c compMin(const Vec2c& a,const Vec2c& b)
{
  return Vec2c(min(a.x(),b.x()),min(a.y(),b.y()));
}
Vec3c compMin(const Vec3c& a,const Vec3c& b)
{
  return Vec3c(min(a.x(),b.x()),min(a.y(),b.y()),min(a.z(),b.z()));
}
Vec4c compMin(const Vec4c& a,const Vec4c& b)
{
  return Vec4c(min(a.x(),b.x()),min(a.y(),b.y()),min(a.z(),b.z()),min(a.w(),b.w()));
}

Vec2c compMax(const Vec2c& a,const Vec2c& b)
{
  return Vec2c(max(a.x(),b.x()),max(a.y(),b.y()));
}
Vec3c compMax(const Vec3c& a,const Vec3c& b)
{
  return Vec3c(max(a.x(),b.x()),max(a.y(),b.y()),max(a.z(),b.z()));
}
Vec4c compMax(const Vec4c& a,const Vec4c& b)
{
  return Vec4c(max(a.x(),b.x()),max(a.y(),b.y()),max(a.z(),b.z()),max(a.w(),b.w()));
}

Vec2i ceilV(const Vec2f& vec)
{
  return Vec2i(
           convert<sizeType>()(ceil(vec.x())),
           convert<sizeType>()(ceil(vec.y())));
}
Vec3i ceilV(const Vec3f& vec)
{
  return Vec3i(
           convert<sizeType>()(ceil(vec.x())),
           convert<sizeType>()(ceil(vec.y())),
           convert<sizeType>()(ceil(vec.z())));
}
Vec4i ceilV(const Vec4f& vec)
{
  return Vec4i(
           convert<sizeType>()(ceil(vec.x())),
           convert<sizeType>()(ceil(vec.y())),
           convert<sizeType>()(ceil(vec.z())),
           convert<sizeType>()(ceil(vec.w())));
}

Vec2i floorV(const Vec2f& vec)
{
  return Vec2i(
           convert<sizeType>()(floor(vec.x())),
           convert<sizeType>()(floor(vec.y())));
}
Vec3i floorV(const Vec3f& vec)
{
  return Vec3i(
           convert<sizeType>()(floor(vec.x())),
           convert<sizeType>()(floor(vec.y())),
           convert<sizeType>()(floor(vec.z())));
}
Vec4i floorV(const Vec4f& vec)
{
  return Vec4i(
           convert<sizeType>()(floor(vec.x())),
           convert<sizeType>()(floor(vec.y())),
           convert<sizeType>()(floor(vec.z())),
           convert<sizeType>()(floor(vec.w())));
}

Vec2i round(const Vec2f& vec)
{
  return Vec2i(
           convert<sizeType>()(floor(vec.x()+0.5f)),
           convert<sizeType>()(floor(vec.y()+0.5f)));
}
Vec3i round(const Vec3f& vec)
{
  return Vec3i(
           convert<sizeType>()(floor(vec.x()+0.5f)),
           convert<sizeType>()(floor(vec.y()+0.5f)),
           convert<sizeType>()(floor(vec.z()+0.5f)));
}
Vec4i round(const Vec4f& vec)
{
  return Vec4i(
           convert<sizeType>()(floor(vec.x()+0.5f)),
           convert<sizeType>()(floor(vec.y()+0.5f)),
           convert<sizeType>()(floor(vec.z()+0.5f)),
           convert<sizeType>()(floor(vec.w()+0.5f)));
}

Vec2d compMin(const Vec2d& a,const Vec2d& b)
{
  return Vec2d(min(a.x(),b.x()),min(a.y(),b.y()));
}
Vec3d compMin(const Vec3d& a,const Vec3d& b)
{
  return Vec3d(min(a.x(),b.x()),min(a.y(),b.y()),min(a.z(),b.z()));
}
Vec4d compMin(const Vec4d& a,const Vec4d& b)
{
  return Vec4d(min(a.x(),b.x()),min(a.y(),b.y()),min(a.z(),b.z()),min(a.w(),b.w()));
}

Vec2d compMax(const Vec2d& a,const Vec2d& b)
{
  return Vec2d(max(a.x(),b.x()),max(a.y(),b.y()));
}
Vec3d compMax(const Vec3d& a,const Vec3d& b)
{
  return Vec3d(max(a.x(),b.x()),max(a.y(),b.y()),max(a.z(),b.z()));
}
Vec4d compMax(const Vec4d& a,const Vec4d& b)
{
  return Vec4d(max(a.x(),b.x()),max(a.y(),b.y()),max(a.z(),b.z()),max(a.w(),b.w()));
}

Vec2i ceilV(const Vec2d& vec)
{
  return Vec2i(
           convert<sizeType>()(ceil(vec.x())),
           convert<sizeType>()(ceil(vec.y())));
}
Vec3i ceilV(const Vec3d& vec)
{
  return Vec3i(
           convert<sizeType>()(ceil(vec.x())),
           convert<sizeType>()(ceil(vec.y())),
           convert<sizeType>()(ceil(vec.z())));
}
Vec4i ceilV(const Vec4d& vec)
{
  return Vec4i(
           convert<sizeType>()(ceil(vec.x())),
           convert<sizeType>()(ceil(vec.y())),
           convert<sizeType>()(ceil(vec.z())),
           convert<sizeType>()(ceil(vec.w())));
}

Vec2i floorV(const Vec2d& vec)
{
  return Vec2i(
           convert<sizeType>()(floor(vec.x())),
           convert<sizeType>()(floor(vec.y())));
}
Vec3i floorV(const Vec3d& vec)
{
  return Vec3i(
           convert<sizeType>()(floor(vec.x())),
           convert<sizeType>()(floor(vec.y())),
           convert<sizeType>()(floor(vec.z())));
}
Vec4i floorV(const Vec4d& vec)
{
  return Vec4i(
           convert<sizeType>()(floor(vec.x())),
           convert<sizeType>()(floor(vec.y())),
           convert<sizeType>()(floor(vec.z())),
           convert<sizeType>()(floor(vec.w())));
}

Vec2i round(const Vec2d& vec)
{
  return Vec2i(
           convert<sizeType>()(floor(vec.x()+0.5)),
           convert<sizeType>()(floor(vec.y()+0.5)));
}
Vec3i round(const Vec3d& vec)
{
  return Vec3i(
           convert<sizeType>()(floor(vec.x()+0.5)),
           convert<sizeType>()(floor(vec.y()+0.5)),
           convert<sizeType>()(floor(vec.z()+0.5)));
}
Vec4i round(const Vec4d& vec)
{
  return Vec4i(
           convert<sizeType>()(floor(vec.x()+0.5)),
           convert<sizeType>()(floor(vec.y()+0.5)),
           convert<sizeType>()(floor(vec.z()+0.5)),
           convert<sizeType>()(floor(vec.w()+0.5)));
}

const scalarD ErfCalc::cof[28] = {
  -1.3026537197817094, 6.4196979235649026e-1,
  1.9476473204185836e-2,-9.561514786808631e-3,-9.46595344482036e-4,
  3.66839497852761e-4,4.2523324806907e-5,-2.0278578112534e-5,
  -1.624290004647e-6,1.303655835580e-6,1.5626441722e-8,-8.5238095915e-8,
  6.529054439e-9,5.059343495e-9,-9.91364156e-10,-2.27365122e-10,
  9.6467911e-11, 2.394038e-12,-6.886027e-12,8.94487e-13, 3.13092e-13,
  -1.12708e-13,3.81e-16,7.106e-15,-1.523e-15,-9.4e-17,1.21e-16,-2.8e-17
};
scalarD ErfCalc::erf(scalarD x) const {
  if (x >=0.0) return 1.0 - erfccheb(x);
  else return erfccheb(-x) - 1.0;
}
scalarD ErfCalc::erfc(scalarD x) const {
  if (x >= 0.0) return erfccheb(x);
  else return 2.0 - erfccheb(-x);
}
scalarD ErfCalc::erfccheb(scalarD z) const {
  sizeType j;
  scalarD t,ty,tmp,d=0.0,dd=0.0;
  if (z < 0.0)
    throw("erfccheb requires nonnegative argument");
  t = 2.0/(2.0+z);
  ty = 4.0*t - 2.0;
  for (j=ncof-1; j>0; j--) {
    tmp = d;
    d = ty*d - dd + cof[j];
    dd = tmp;
  }
  return t*exp(-z*z + 0.5*(cof[0] + ty*d) - dd);
}
scalarD ErfCalc::inverfc(scalarD p) const {
  scalarD x,err,t,pp;
  if (p >= 2.0) return -100.0;
  if (p <= 0.0) return 100.0;
  pp = (p < 1.0)? p : 2.0 - p;
  t = sqrt(-scalarD(2.0)*log(pp/scalarD(2.0)));
  x = -0.70711*((2.30753+t*0.27061)/(1.+t*(0.99229+t*0.04481)) - t);
  for (sizeType j=0; j<2; j++) {
    err = erfc(x) - pp;
    x += err/(1.12837916709551257*exp(-(x*x))-x*err);
  }
  return (p < 1.0? x : -x);
}
scalarD ErfCalc::inverf(scalarD p) const {
  return inverfc(1.-p);
}
scalarD erf(const scalarD& val);

boost::random::mt19937 vgen;
void RandEngine::srand(sizeType i)
{
  vgen.seed((uint32_t)i);
}
scalar RandEngine::randR01()
{
  return boost::uniform_real<scalar>(0,1)(vgen);
}
scalar RandEngine::randR(scalar l,scalar u)
{
  if(l > u)
    swap(l,u);
  return boost::uniform_real<scalar>(l,u)(vgen);
}
scalar RandEngine::randSR(sizeType seed,scalar l,scalar u)
{
  vgen.seed((uint32_t)seed);
  return randR(l,u);
}
sizeType RandEngine::randSI(sizeType seed)
{
  return randSI(seed,0,numeric_limits<int>::max());
}
sizeType RandEngine::randI(sizeType l,sizeType u)
{
  if(l > u)
    swap(l,u);
  return boost::uniform_int<sizeType>(l,u)(vgen);
}
sizeType RandEngine::randSI(sizeType seed,sizeType l,sizeType u)
{
  vgen.seed((uint32_t)seed);
  return randI(l,u);
}
scalar RandEngine::normal()
{
  return boost::normal_distribution<scalar>()(vgen);
}

char sgn(const scalarD& val)
{
  return val > 0.0f ? 1 : val < 0.0f ? -1 : 0;
}
char sgn(const scalarF& val)
{
  return val > 0.0f ? 1 : val < 0.0f ? -1 : 0;
}
bool isInf( const scalarD& x )
{
  return ( ( x - x ) != scalarD(0.0f) );
}
bool isInf( const scalarF& x )
{
  return ( ( x - x ) != 0.0f );
}
bool isNan( const scalarD& x )
{
  return ( ( ! ( x < 0.0f ) ) && ( ! ( x >= 0.0f ) ) );
}
bool isNan( const scalarF& x )
{
  return ( ( ! ( x < 0.0f ) ) && ( ! ( x >= 0.0f ) ) );
}
sizeType makePOT(sizeType val,sizeType decimate)
{
  ASSERT(decimate > 1)

  sizeType ret=1;
  while(ret < val)
    ret*=decimate;
  return ret;
}
sizeType mod(sizeType a,sizeType b,const scalar &bInv)
{
  a-=a/b*b;
  //sizeType n=sizeType(a*bInv);
  //a-=n*b;
  if(a<0)
    a+=b;
  return a;
}
scalarD minmod( const scalarD& a,const scalarD& b,const scalarD& c)
{
  if(a > 0.0f && b > 0.0f && c > 0.0f)
    return min<scalarD>(min<scalarD>(a,b),c);
  else if(a < 0.0f && b < 0.0f && c < 0.0f)
    return max<scalarD>(max<scalarD>(a,b),c);
  else
    return 0.0f;
}
scalarF minmod( const scalarF& a,const scalarF& b,const scalarF& c)
{
  if(a > 0.0f && b > 0.0f && c > 0.0f)
    return min<scalarF>(min<scalarF>(a,b),c);
  else if(a < 0.0f && b < 0.0f && c < 0.0f)
    return max<scalarF>(max<scalarF>(a,b),c);
  else
    return 0.0f;
}

Vec2i makePOT(const Vec2i& val,sizeType decimate)
{
  return Vec2i(makePOT(val.x(),decimate),makePOT(val.y(),decimate));
}
Vec3i makePOT(const Vec3i& val,sizeType decimate)
{
  return Vec3i(makePOT(val.x(),decimate),makePOT(val.y(),decimate),makePOT(val.z(),decimate));
}
Vec4i makePOT(const Vec4i& val,sizeType decimate)
{
  return Vec4i(makePOT(val.x(),decimate),makePOT(val.y(),decimate),makePOT(val.z(),decimate),makePOT(val.w(),decimate));
}

Vec2i operator>>(const Vec2i& in,const sizeType& nr)
{
  return Vec2i(in.x() >> nr,in.y() >> nr);
}
Vec3i operator>>(const Vec3i& in,const sizeType& nr)
{
  return Vec3i(in.x() >> nr,in.y() >> nr,in.z() >> nr);
}
Vec4i operator>>(const Vec4i& in,const sizeType& nr)
{
  return Vec4i(in.x() >> nr,in.y() >> nr,in.z() >> nr,in.w() >> nr);
}
Vec2i operator&(const Vec2i& in,const sizeType& nr)
{
  return Vec2i(in.x()&nr,in.y()&nr);
}
Vec3i operator&(const Vec3i& in,const sizeType& nr)
{
  return Vec3i(in.x()&nr,in.y()&nr,in.z()&nr);
}
Vec4i operator&(const Vec4i& in,const sizeType& nr)
{
  return Vec4i(in.x()&nr,in.y()&nr,in.z()&nr,in.w()&nr);
}

bool isInf( const Vec2f& x )
{
  return isInf(x.x()) || isInf(x.y());
}
bool isNan( const Vec2f& x )
{
  return isNan(x.x()) || isNan(x.y());
}
bool isInf( const Vec3f& x )
{
  return isInf(x.x()) || isInf(x.y()) || isInf(x.z());
}
bool isNan( const Vec3f& x )
{
  return isNan(x.x()) || isNan(x.y()) || isNan(x.z());
}
bool isInf( const Vec4f& x )
{
  return isInf(x.x()) || isInf(x.y()) || isInf(x.z()) || isInf(x.w());
}
bool isNan( const Vec4f& x )
{
  return isNan(x.x()) || isNan(x.y()) || isNan(x.z()) || isNan(x.w());
}
Vec2f minmodV(const Vec2f& a,const Vec2f& b,const Vec2f& c)
{
  return Vec2f(minmod(a.x(),b.x(),c.x()),
               minmod(a.y(),b.y(),c.y()));
}
Vec3f minmodV(const Vec3f& a,const Vec3f& b,const Vec3f& c)
{
  return Vec3f(minmod(a.x(),b.x(),c.x()),
               minmod(a.y(),b.y(),c.y()),
               minmod(a.z(),b.z(),c.z()));
}
Vec4f minmodV(const Vec4f& a,const Vec4f& b,const Vec4f& c)
{
  return Vec4f(minmod(a.x(),b.x(),c.x()),
               minmod(a.y(),b.y(),c.y()),
               minmod(a.z(),b.z(),c.z()),
               minmod(a.w(),b.w(),c.w()));
}

bool isInf( const Vec2d& x )
{
  return isInf(x.x()) || isInf(x.y());
}
bool isNan( const Vec2d& x )
{
  return isNan(x.x()) || isNan(x.y());
}
bool isInf( const Vec3d& x )
{
  return isInf(x.x()) || isInf(x.y()) || isInf(x.z());
}
bool isNan( const Vec3d& x )
{
  return isNan(x.x()) || isNan(x.y()) || isNan(x.z());
}
bool isInf( const Vec4d& x )
{
  return isInf(x.x()) || isInf(x.y()) || isInf(x.z()) || isInf(x.w());
}
bool isNan( const Vec4d& x )
{
  return isNan(x.x()) || isNan(x.y()) || isNan(x.z()) || isNan(x.w());
}
Vec2d minmodV(const Vec2d& a,const Vec2d& b,const Vec2d& c)
{
  return Vec2d(minmod(a.x(),b.x(),c.x()),
               minmod(a.y(),b.y(),c.y()));
}
Vec3d minmodV(const Vec3d& a,const Vec3d& b,const Vec3d& c)
{
  return Vec3d(minmod(a.x(),b.x(),c.x()),
               minmod(a.y(),b.y(),c.y()),
               minmod(a.z(),b.z(),c.z()));
}
Vec4d minmodV(const Vec4d& a,const Vec4d& b,const Vec4d& c)
{
  return Vec4d(minmod(a.x(),b.x(),c.x()),
               minmod(a.y(),b.y(),c.y()),
               minmod(a.z(),b.z(),c.z()),
               minmod(a.w(),b.w(),c.w()));
}

//OpenMP Settings
const OmpSettings& OmpSettings::getOmpSettings() {
  return _ompSettings;
}
OmpSettings& OmpSettings::getOmpSettingsNonConst() {
  return _ompSettings;
}
int OmpSettings::nrThreads() const {
  return _nrThreads;
}
void OmpSettings::setNrThreads(int nr) {
  nr=max<int>(nr,1);
  omp_set_num_threads(nr);
  _nrThreads=nr;
}
OmpSettings::OmpSettings():_nrThreads(std::max<int>(omp_get_num_procs(),2)*3/4) {}
OmpSettings OmpSettings::_ompSettings;

PRJ_END
