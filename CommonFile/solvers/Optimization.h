#ifndef OPTIMIZATION_H
#define OPTIMIZATION_H

#include "Objective.h"
#include <boost/property_tree/ptree.hpp>

PRJ_BEGIN

//The optimization framework tries to optimize:
//\E{argmin}_{X}&&\Sigma_i \|Term_i(X)\|^2
//\E{s.t.}&&\foreach j C_j(X)>=/==0

//Use class Term for Term_i(X)
//Use class GEQConstraint for C_j(X)>=0
//Use class EQConstraint for C_j(X)==0

//Message/Parameter passing is through boost::property_tree
//To choose optimizer, use: pt.put<string>("optimizer",TYPE)
//Here we support several types of optimizer:
//use TYPE=LBFGS to use buildin CommonFile LBFGS solver (just a port of commerical software)
//use TYPE=BLEIC to use alglib BLEIC solver (support linear/box constraints)
//use TYPE=AUGLAG to use augmented-lagrangian solver with alglib BLEIC as subproblem solver (support all constraints)
//use TYPE=LM to use alglib levenberg-marquardt solver (support box constraints)
//use TYPE=ELM to use interior point solver with L1-penalty support (levenberg-marquardt updation) (support linear/box constraints)
//use TYPE=TR to use simple trust region solver with naive trust region radius tunning
//use TYPE=SGD to use stochastic gradient that takes linear/box constraints with optional covariant-diff modification
//Checkout Optimizer.cpp to find more tunable parameters
//Checkout Optimizer::runExamples() to find usage

class Optimizer;
class QPSolver;
class Term
{
public:
  typedef scalarD T;
  typedef Objective<T>::Vec Vec;
  typedef Eigen::Matrix<T,-1,-1> Mat;
  //internal functions
  Term(const Optimizer& opt);
  virtual Vec val(const Vec& x);
  virtual void grad(const Vec& x,Mat& grad);
  virtual void addGrad(const Vec& x,Vec& grad,Vec alpha=Vec());
  virtual Vec valGrad(const Vec& x,Mat* grad=NULL);
  virtual void setReuse(bool reuse);
  //user implemented functions
  virtual sizeType values() const=0;
  //default to true
  virtual bool isLinear() const;
  //default to false
  virtual bool isRectified() const;
  //for constraint, default to exit(-1)
  virtual char sign() const;
  //this provide the option of treating this as energy
  bool _asEnergy;
protected:
  //just implement this: return the value of Term_i(X) or C_j(X)
  //and set grad=grad+\FPP{Term_i/C_j}{X}*alpha (not needed for gradient free optimization)
  virtual Vec valGradInner(const Vec& x,Mat& grad)=0;
  const boost::property_tree::ptree& _pt;
  bool _reuse;
  Vec _valCache;
private:
  Mat _gradCache;
};
class SparseTerm : public Term
{
public:
  using Term::T;
  using Term::Vec;
  typedef vector<Eigen::Triplet<T,sizeType> > SMatd;
  SparseTerm(const Optimizer& opt);
  virtual void addGrad(const Vec& x,Vec& grad,Vec alpha=Vec());
  virtual Vec valGrad(const Vec& x,Mat* grad=NULL);
  virtual Vec svalGrad(const Vec& x,SMatd* grad=NULL);
protected:
  virtual Vec valGradInner(const Vec& x,Mat& grad);
  virtual Vec valGradInner(const Vec& x,SMatd& grad)=0;
private:
  SMatd _sgradCache;
};
class GEQConstraint : public Term
{
public:
  GEQConstraint(const Optimizer& opt):Term(opt) {_asEnergy=false;}
  virtual bool isRectified() const;
  virtual char sign() const;
};
class EQConstraint : public Term
{
public:
  EQConstraint(const Optimizer& opt):Term(opt) {_asEnergy=false;}
  virtual char sign() const;
};
class SparseGEQConstraint : public SparseTerm
{
public:
  SparseGEQConstraint(const Optimizer& opt):SparseTerm(opt) {_asEnergy=false;}
  virtual bool isRectified() const;
  virtual char sign() const;
};
class SparseEQConstraint : public SparseTerm
{
public:
  SparseEQConstraint(const Optimizer& opt):SparseTerm(opt) {_asEnergy=false;}
  virtual char sign() const;
};
class Optimizer : public Objective<Term::T>
{
public:
  typedef Term::T T;
  typedef Term::T Scalar;
  typedef sizeType Index;
  typedef Term::Mat Mat;
  typedef Objective<T>::Vec InputType;
  typedef Objective<T>::Vec ValueType;
  typedef Eigen::SparseMatrix<Scalar,Eigen::RowMajor,int> JacobianType;
  //problem setup methods
  Optimizer();
  Optimizer(const boost::property_tree::ptree& pt);
  virtual T minimize(Vec& x);
  //virtual void checkKKT(const Vec &x);
  void addTerm(boost::shared_ptr<Term> term);
  void addConstraint(boost::shared_ptr<Term> term);
  void removeTermOrConstraint(boost::shared_ptr<Term> term);
  void clearTerm();
  void clearConstraint();
  template <typename T>
  boost::shared_ptr<const T> getTerm() const {
    boost::shared_ptr<const T> ret;
    for(sizeType i=0;i<(sizeType)_terms.size();i++)
      if(ret=boost::dynamic_pointer_cast<const T>(_terms[i]))
        return ret;
    return ret;
  }
  template <typename T>
  boost::shared_ptr<T> getTerm() {
    boost::shared_ptr<T> ret;
    for(sizeType i=0;i<(sizeType)_terms.size();i++)
      if(ret=boost::dynamic_pointer_cast<T>(_terms[i]))
        return ret;
    return ret;
  }
  const boost::property_tree::ptree& getProps() const;
  boost::property_tree::ptree& getProps();
  //covariant diff is used by SGD solver for parameter-independent performance
  //linear constraints are automatically encounted using MPRGP method
  void setCovariantDiff(boost::shared_ptr<JacobianType> metric);
  //overwritte Objective<T> for LBFGS Optimization
  virtual int operator()(const Vec& x,T& FX,Vec& DFDX,const T& step,bool wantGradient);
  //overwritte Objective<T> for LM Optimization
  virtual int operator()(const Vec& x,Vec& fvec);
  virtual int df(const Vec& x,Mat& fjac);
  virtual int df(const Vec& x,JacobianType& fjac);
  //objective functions for Lasso-penaltized optimization
  virtual int operator()(const Vec& x,Vec& fvec,Vec& fvecL1);
  virtual int df(const Vec& x,Mat& fjac,Mat& fjacL1,Vec* fvecL1);
  virtual int df(const Vec& x,JacobianType& fjac,JacobianType& fjacL1,Vec* fvecL1);
  //for Finite Difference Debug
  virtual void debugFD(T delta=1E-8f,bool cons=false,bool debugJac=false,bool debugGrad=false,const Vec *currX=NULL);
  //dimension Info
  virtual int inputs() const;
  virtual int values() const;
  virtual int constraints() const;
  virtual void profileLineSearch(const sizeType& k,const Vec& x,const Vec& d,const T& step);
protected:
  virtual void setDirty();
  virtual void setX(const Vec* x);
private:
  void reorganize();
  template <typename MAT>
  void extractConstraint(Vec* lb,Vec* ub,MAT* cL,Vec* cR,Coli* ct,vector<boost::shared_ptr<Term> >& nl) const;
  Eigen::DiagonalMatrix<T,-1,-1> getL1GradCoef(const Vec& val) const;
  Vec subtractBound(const Vec& b,const Vec& x) const;
  //solve QP subproblem: for ELM and DTR
  void solveQP(const JacobianType& LHS,const Vec& RHS, //objective
               const JacobianType& fjacL1,const Vec& fvecL1, //L1 term
               Vec& lb,Vec& ub,T trustRegion,  //box constraints
               const JacobianType& cL,const Vec& cR,const Coli& ct,  //linear constraints
               const Vec& x,Vec& dx,Vec& newL1) const;
  bool isQP() const;
  //data
  boost::property_tree::ptree _pt;
  vector<boost::shared_ptr<Term> > _terms,_cons;
  boost::shared_ptr<JacobianType> _metric;
  boost::shared_ptr<QPSolver> _solQP;
  Vec _lastX;
};
//quadratic programming subproblem solver
class QPSolver
{
public:
  typedef Optimizer::T T;
  typedef Optimizer::Scalar Scalar;
  typedef Optimizer::Index Index;
  typedef Optimizer::Vec Vec;
  typedef Optimizer::Mat Mat;
  typedef Optimizer::JacobianType JacobianType;
  QPSolver(const boost::property_tree::ptree& pt);
  bool solveQP(Vec& x,const JacobianType& A,const Vec& b,const Vec& lb,const Vec& ub,const Mat& C,const Vec& d,const Coli& type) const;
  bool solveQP(Vec& x,const JacobianType& A,const Vec& b,const Vec* lb,const Vec* ub,const Mat* C,const Vec* d,const Coli* type) const; //dense constraints
  bool solveQP(Vec& x,const JacobianType& A,const Vec& b,const Vec* lb,const Vec* ub,const JacobianType* C,const Vec* d,const Coli* type) const;  //sparse constraints
private:
  bool MPRGPSolve(Vec& x,const JacobianType& A,const Vec& b,const Vec* lb,const Vec* ub) const;
  bool BLEICSolve(Vec& x,const JacobianType& A,const Vec& b,const Vec* lb,const Vec* ub,const Mat* C,const Vec* d,const Coli* type) const;
  bool OOQPSolve(Vec& x,const JacobianType& A,const Vec& b,const Vec* lb,const Vec* ub,const JacobianType* C,const Vec* d,const Coli* type) const;
  template <typename MAT>
  void checkProb(Vec& x,const JacobianType& A,const Vec& b,const Vec* lb,const Vec* ub,const MAT* C,const Vec* d,const Coli* type) const;
  const boost::property_tree::ptree& _pt;
};
class OptimizerTest
{
public:
  typedef Optimizer::T T;
  typedef Optimizer::Vec Vec;
  typedef Optimizer::Mat Mat;
  typedef Optimizer::JacobianType JacobianType;
  //example1: 1D diffusion with constraint
  static Vec runExample1(sizeType N,bool cons,bool consTotal,bool consNorm, T SL1,const string& type,bool quiet=true,bool covariant=false);
  static void drawFunc(const Vec& x,const string& name);
  static void runExample1();
  //example2: 2D cloth with constraint
  static void runExample2(sizeType N,bool cons,bool consTwoPlane,bool consBall,const string& type);
  static void runExample2();
  //example3: test the set of QP solvers
  static void runExample3(sizeType N,bool BC,bool LC,const string& type);
  static void runExample3();
  //example4: stochastic gradient descendent
  static void runExample4();
};

PRJ_END

#endif
