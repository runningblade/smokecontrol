#ifndef OPTIMIZATION_UTILS_H
#define OPTIMIZATION_UTILS_H

#include "../MathBasic.h"

PRJ_BEGIN

//some matrix utility
template <typename VEC>
Eigen::Matrix<typename VEC::Scalar,3,3> cross(const VEC& w)
{
  Eigen::Matrix<typename VEC::Scalar,3,3> ret;
  ret.setZero();
  ret(0,1)=-w[2];
  ret(0,2)= w[1];
  ret(1,2)=-w[0];
  ret(1,0)= w[2];
  ret(2,0)=-w[1];
  ret(2,1)= w[0];
  return ret;
}
template <typename MAT>
Eigen::Matrix<typename MAT::Scalar,-1,1> flatten(const MAT& mat)
{
  return Eigen::Map<const Eigen::Matrix<typename MAT::Scalar,-1,1> >(mat.data(),mat.size());
}
template <typename MAT>
void addBlk(vector<Eigen::Triplet<typename MAT::Scalar,sizeType> >& grad,sizeType rr,sizeType cc,const MAT& mat)
{
  for(sizeType r=0; r<mat.rows(); r++)
    for(sizeType c=0; c<mat.cols(); c++)
      grad.push_back(Eigen::Triplet<typename MAT::Scalar,sizeType>(rr+r,cc+c,mat(r,c)));
}
template <typename T>
Eigen::Matrix<T,12,1> expand(const Eigen::Transform<T,3,2,0>& aff)
{
  Eigen::Matrix<T,12,1> ret;
  ret.template block<3,1>(0,0)=aff.translation();
  ret.template block<9,1>(3,0)=Eigen::Map<const Eigen::Matrix<T,9,1> >(aff.linear().eval().data());
  return ret;
}
template <typename T>
void extend(T& mat,sizeType r,sizeType c,typename T::Scalar value=0)
{
  T tmp=mat;
  mat.setConstant(mat.rows()+r,mat.cols()+c,value);
  mat.block(0,0,tmp.rows(),tmp.cols())=tmp;
}
template <typename T>
T extendRet(const T& mat,sizeType r,sizeType c,typename T::Scalar value=0)
{
  T ret=T::Constant(mat.rows()+r,mat.cols()+c,value);
  ret.block(0,0,mat.rows(),mat.cols())=mat;
  return ret;
}
template <typename T>
void mulL(const Eigen::DiagonalMatrix<T,-1,-1>& D,vector<Eigen::Triplet<T,sizeType> >& grad)
{
  OMP_PARALLEL_FOR_
  for(sizeType i=0;i<grad.size();i++)
    const_cast<T&>(grad[i].value())=D.diagonal()[grad[i].row()]*grad[i].value();
}
template <typename T>
struct MatrixBuilder
{
  MatrixBuilder(T& data,sizeType rows,sizeType cols)
    :_data(data){_data.setZero(rows,cols);}
  const T& get() const{return _data;}
  void addBlk(sizeType r,sizeType c,const Eigen::Matrix<typename T::Scalar,-1,-1>& other,typename T::Scalar coef=1)
  {
    _data.block(r,c,other.rows(),other.cols())+=other*coef;
  }
  template <typename TRIP>
  void addBlk(sizeType r,sizeType c,const vector<TRIP>& other,typename T::Scalar coef=1)
  {
    for(sizeType i=0;i<(sizeType)other.size();i++)
      _data(r+other[i].row(),c+other[i].col())+=other[i].value()*coef;
  }
private:
  T& _data;
};
template <typename T,int OP,typename INT_TYPE>
struct MatrixBuilder<Eigen::SparseMatrix<T,OP,INT_TYPE> >
{
  typedef Eigen::SparseMatrix<T,OP,INT_TYPE> ST;
  typedef Eigen::Triplet<T,INT_TYPE> TRIP;
  MatrixBuilder(ST& data,sizeType rows,sizeType cols):_data(data){_data.resize(rows,cols);}
  ~MatrixBuilder(){
    for(sizeType i=0;i<(sizeType)_trips.size();i++)
      ASSERT_MSGV(_trips[i].row() < _data.rows() && _trips[i].col() < _data.cols(),"Triplet %ld index out of bounds!",i)
    _data.setFromTriplets(_trips.begin(),_trips.end());
  }
  const ST& get() const{return _data;}
  void addBlk(sizeType r,sizeType c,const ST& other,T coef) {
    _trips.reserve(_trips.size()+other.nonZeros());
    for(int k=0;k<other.outerSize(); ++k)
      for(typename ST::InnerIterator it(other,k);it;++it)
        _trips.push_back(TRIP(r+it.row(),c+it.col(),it.value()*coef));
  }
  void addIdentity(sizeType r,sizeType c,sizeType nr)
  {
    for(sizeType off=0;off<nr;off++)
      _trips.push_back(TRIP(r+off,c+off,1));
  }
  void addBlk(sizeType r,sizeType c,const Eigen::Matrix<T,-1,-1>& other,T coef=1)
  {
    _trips.reserve(_trips.size()+other.size());
    for(sizeType R=0;R<other.rows();R++)
      for(sizeType C=0;C<other.cols();C++)
        _trips.push_back(TRIP(r+R,c+C,other(R,C)*coef));
  }
  template <typename TT>
  void addBlk(sizeType r,sizeType c,const vector<TT>& other,T coef=1)
  {
    _trips.reserve(_trips.size()+other.size());
    for(sizeType i=0;i<(sizeType)other.size();i++)
      _trips.push_back(TRIP(r+other[i].row(),c+other[i].col(),other[i].value()*coef));
  }
private:
  ST& _data;
  vector<TRIP> _trips;
};

PRJ_END

#endif
