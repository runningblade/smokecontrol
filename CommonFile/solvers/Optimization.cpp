#include "Optimization.h"
#include "OptimizationUtils.h"
//QPSolver
#include "QPSolver.h"
#ifdef OOQP_SUPPORT
#include "ooqp/cQpGenSparse.h"
#endif
//LBFGS
#include "Minimizer.h"
//EM Levenberg-Marquardt
#include "Eigen/SparseQR"
#include "Eigen/IterativeLinearSolvers"
//alglib
#include "alglib/optimization.h"

USE_PRJ_NAMESPACE

//Term interface
Term::Term(const Optimizer& opt):_asEnergy(true),_pt(opt.getProps()),_reuse(false) {}
Term::Vec Term::val(const Vec& x)
{
  return valGrad(x);
}
void Term::grad(const Vec& x,Mat& grad)
{
  valGrad(x,&grad);
}
void Term::addGrad(const Vec& x,Vec& grad,Vec alpha)
{
  valGrad(x,NULL);
  if(alpha.size() == 0)
    alpha.setOnes(values());
  grad+=_gradCache.transpose()*alpha;
}
Term::Vec Term::valGrad(const Vec& x,Mat* grad)
{
  if(!_reuse) {
    sizeType nrV=values();
    _gradCache.setZero(nrV,x.size());
    _valCache=valGradInner(x,_gradCache);
    ASSERT(nrV == _valCache.size())
  }
  if(grad)
    *grad=_gradCache;
  return _valCache;
}
void Term::setReuse(bool reuse)
{
  _reuse=reuse;
}
bool Term::isLinear() const
{
  return true;
}
bool Term::isRectified() const
{
  return false;
}
char Term::sign() const
{
  ASSERT_MSG(false,"Inherit (Sparse)(G)EQConstraint for constraints!")
  return 0;
}

//SparseTerm interface
SparseTerm::SparseTerm(const Optimizer& opt):Term(opt) {}
void SparseTerm::addGrad(const Vec& x,Vec& grad,Vec alpha)
{
  valGrad(x,NULL);
  if(alpha.size() == 0)
    alpha.setOnes(values());
  for(sizeType i=0; i<(sizeType)_sgradCache.size(); i++)
    grad[_sgradCache[i].col()]+=_sgradCache[i].value()*alpha[_sgradCache[i].row()];
}
Term::Vec SparseTerm::valGrad(const Vec& x,Mat* grad)
{
  Vec ret=svalGrad(x);
  if(grad) {
    grad->setZero(values(),x.size());
    for(sizeType i=0; i<(sizeType)_sgradCache.size(); i++)
      (*grad)(_sgradCache[i].row(),_sgradCache[i].col())+=_sgradCache[i].value();
  }
  return ret;
}
Term::Vec SparseTerm::svalGrad(const Vec& x,SMatd* grad)
{
  if(!_reuse) {
    sizeType nrV=values();
    _sgradCache.clear();
    _valCache=valGradInner(x,_sgradCache);
    ASSERT(nrV == _valCache.size())
  }
  if(grad)
    *grad=_sgradCache;
  return _valCache;
}
Term::Vec SparseTerm::valGradInner(const Vec& x,Mat& grad)
{
  ASSERT_MSG(false,"Use the sparse version for SparseTerm!")
  return Vec();
}

//constraint interface
char EQConstraint::sign() const
{
  return 0;
}
bool GEQConstraint::isRectified() const
{
  return true;
}
char GEQConstraint::sign() const
{
  return 1;
}
char SparseEQConstraint::sign() const
{
  return 0;
}
bool SparseGEQConstraint::isRectified() const
{
  return true;
}
char SparseGEQConstraint::sign() const
{
  return 1;
}

//term utility
sizeType COUNT_TERM(const vector<boost::shared_ptr<Term> >& terms)
{
  sizeType nrTerm=0;
  for(sizeType i=0; i<(sizeType)terms.size(); i++)
    nrTerm+=terms[i]->values();
  return nrTerm;
}
sizeType COUNT_TERM_L1(const vector<boost::shared_ptr<Term> >& terms)
{
  sizeType nrTerm=0;
  for(sizeType i=0; i<(sizeType)terms.size(); i++)
    if(terms[i]->isRectified())
      nrTerm+=terms[i]->values();
  return nrTerm;
}
#define LOOP_TERM(v) for(sizeType i=0,off=0;i<(sizeType)v.size();off+=v[i]->values(),i++)
#define LOOP_TERM_L1(v) for(sizeType i=0,off=0,offL1=0;i<(sizeType)v.size();(v[i]->isRectified() ? (offL1+=v[i]->values()) : (off+=v[i]->values())),i++)
bool IS_BOX_CONSTRAINT(Term::Vec& lb,Term::Vec& ub,SparseTerm& term)
{
  typedef Eigen::SparseMatrix<scalarD,Eigen::RowMajor,sizeType> SMat;
  SparseTerm::SMatd grad;
  Term::Vec val=term.svalGrad(Term::Vec::Zero(lb.size()),&grad);

  //the sparse matrix
  SMat gradM;
  gradM.resize(val.size(),lb.size());
  gradM.setFromTriplets(grad.begin(),grad.end());

  //detect
  Term::Vec coef=Term::Vec::Zero(val.size());
  Coli id=Coli::Constant(val.size(),-1);
  for(sizeType r=0; r<val.size(); r++) {
    for(SMat::InnerIterator it(gradM,r); it; ++it) {
      //simply ignore zero terms
      //user should call prune explicitly
      if(it.value() == 0)
        continue;
      else if(id[r] == -1) {
        coef[r]=it.value();
        id[r]=it.col();
      } else {
        id[r]=-1;
        break;
      }
    }
    if(id[r] == -1)
      return false;
  }

  //setup
  val.array()/=-coef.array();
  for(sizeType r=0; r<val.size(); r++) {
    if(term.sign() == 0) {
      lb[id[r]]=max(lb[id[r]],val[r]);
      ub[id[r]]=min(ub[id[r]],val[r]);
    } else if(coef[r] > 0)
      lb[id[r]]=max(lb[id[r]],val[r]);
    else ub[id[r]]=min(ub[id[r]],val[r]);
  }
  return true;
}

//conversion from/to alglib
Optimizer::Vec fromAlglib(const alglib::real_1d_array& xAlglib)
{
  Eigen::Map<const Cold> xMap(xAlglib.getcontent(),xAlglib.length());
  return xMap.cast<Optimizer::T>();
}
void toAlglib(alglib::real_1d_array& ret,const Optimizer::Vec& x)
{
  if(ret.length() != x.size())
    ret.setlength(x.size());
  memcpy(ret.getcontent(),x.data(),x.size()*sizeof(Cold::Scalar));
}
alglib::real_1d_array toAlglib(const Optimizer::Vec& x)
{
  alglib::real_1d_array ret;
  toAlglib(ret,x);
  return ret;
}
void toAlglib(alglib::real_2d_array& ret,const Optimizer::Mat& x)
{
  if(ret.rows() != x.rows() || ret.cols() != x.cols())
    ret.setlength(x.rows(),x.cols());
  Optimizer::Mat xt=x.transpose();
  for(sizeType i=0; i<ret.rows(); i++)
    memcpy(ret[i],xt.data()+i*ret.cols(),ret.cols()*sizeof(Optimizer::Mat::Scalar));
}
alglib::real_2d_array toAlglib(const Optimizer::Mat& x)
{
  alglib::real_2d_array ret;
  toAlglib(ret,x);
  return ret;
}
alglib::integer_1d_array toAlglib(const Coli& x)
{
  alglib::integer_1d_array ret;
  ret.setcontent(x.size(),x.cast<alglib::ae_int_t>().eval().data());
  return ret;
}
void gradAlglib(const alglib::real_1d_array& xAlglib,Optimizer::T& val,alglib::real_1d_array& grad,void* ptr)
{
  Optimizer* opt=(Optimizer*)ptr;
  Optimizer::Vec x=fromAlglib(xAlglib),gradEigen;
  (*opt)(x,val,gradEigen,1,true);
  toAlglib(grad,gradEigen);
}
void fvecAlglib(const alglib::real_1d_array& xAlglib,alglib::real_1d_array& fvec,void* ptr)
{
  Optimizer* opt=(Optimizer*)ptr;
  Optimizer::Vec x=fromAlglib(xAlglib);
  Optimizer::Vec fvecEigen;
  (*opt)(x,fvecEigen);
  if(fvec.length() != fvecEigen.size()) {
    WARNING("Number of terms changed in optimization, not allowed!")
    exit(EXIT_FAILURE);
  }
  toAlglib(fvec,fvecEigen);
}
void fjacAlglib(const alglib::real_1d_array& xAlglib,alglib::real_1d_array& fvec,alglib::real_2d_array& fjac,void* ptr)
{
  Optimizer* opt=(Optimizer*)ptr;
  Optimizer::Vec x=fromAlglib(xAlglib);
  Optimizer::Vec fvecEigen;
  Optimizer::Mat fjacEigen;
  (*opt)(x,fvecEigen);
  (*opt).df(x,fjacEigen);
  if(fvec.length() != fvecEigen.size() || fjac.rows() != fjacEigen.rows()) {
    WARNING("Number of terms changed in optimization, not allowed!")
    exit(EXIT_FAILURE);
  }
  toAlglib(fvec,fvecEigen);
  toAlglib(fjac,fjacEigen);
}
void toAlglib(alglib::sparsematrix& sparse,const Optimizer::JacobianType& s)
{
  alglib::sparsecreate(s.rows(),s.rows(),sparse);
  for(int k=0; k<s.outerSize(); ++k)
    for(Optimizer::JacobianType::InnerIterator it(s,k); it; ++it)
      alglib::sparseset(sparse,it.row(),it.col(),it.value());
}

//auglag interface
class AugLag
{
public:
  typedef Optimizer::T T;
  typedef Optimizer::Vec Vec;
  typedef Optimizer::Mat Mat;
  AugLag(vector<boost::shared_ptr<Term> >& terms,vector<boost::shared_ptr<Term> >& nl,Optimizer& inner)
    :_terms(terms),_nl(nl),_inner(inner) {}
  void minimize(alglib::minbleicstate& state,alglib::real_1d_array& x,scalarD epsg,scalarD epsf,scalarD epsx,sizeType nrIter) {
    _lambda=Vec::Zero(COUNT_TERM(_nl));
    _mu=1E3f;

    sizeType iter=0;
    for(; iter<nrIter;) {
      //inner loop
      alglib::minbleicsetcond(state,epsg,epsf,epsx,nrIter-iter);
      if(iter > 0)
        alglib::minbleicrestartfrom(state,x);
      alglib::minbleicoptimize(state,valGrad,NULL,this);

      //update lambda
      alglib::minbleicreport rep;
      alglib::minbleicresults(state,x,rep);
      const Vec xEigen=fromAlglib(x);
      LOOP_TERM(_nl)
      _lambda.segment(off,_nl[i]->values())=clampCons(_lambda.segment(off,_nl[i]->values())+_mu*_nl[i]->val(xEigen),_nl[i]->sign());

      //count number of iterations consumed
      if(rep.iterationscount == 0)break;
      iter+=rep.iterationscount;

      //fallback to BLEIC
      if(_nl.empty())
        break;
    }
  }
private:
  static void valGrad(const alglib::real_1d_array& xAlglib,scalarD& val,alglib::real_1d_array& grad,void* ptr) {
    //init
    AugLag* opt=(AugLag*)(ptr);
    const Vec& lambda=opt->_lambda;
    const T mu=opt->_mu;

    //toEigen
    const Vec xEigen=fromAlglib(xAlglib);
    Vec gradEigen=Vec::Zero(grad.length());

    //compute gradient
    T valT;
    opt->_inner(xEigen,valT,gradEigen,1,true);
    val=valT;
    if(lambda.size() != COUNT_TERM(opt->_nl)) {
      WARNING("Number of terms changed in optimization, not allowed!")
      exit(EXIT_FAILURE);
    }
    LOOP_TERM(opt->_nl) {
      Term& nl=*(opt->_nl[i]);
      nl.setReuse(false);
      Vec cValClamp=clampCons(nl.val(xEigen)+lambda.segment(off,opt->_nl[i]->values())/mu,nl.sign());
      val+=mu*0.5f*cValClamp.squaredNorm();
      nl.setReuse(true);
      nl.addGrad(xEigen,gradEigen,mu*cValClamp);
    }
    toAlglib(grad,gradEigen);
  }
  static Vec clampCons(Vec x,char sign) {
    return (sign<0) ? x.cwiseMax(Vec::Zero(x.size())) :
           (sign>0) ? x.cwiseMin(Vec::Zero(x.size())) : x;
  }
  vector<boost::shared_ptr<Term> >& _terms;
  vector<boost::shared_ptr<Term> >& _nl;
  Optimizer& _inner;
  Vec _lambda;
  T _mu;
};

//problem setup methods
Optimizer::Optimizer() {}
Optimizer::Optimizer(const boost::property_tree::ptree& pt):_pt(pt) {}
Optimizer::T Optimizer::minimize(Vec& x)
{
  //reorganize energy and constraints
  reorganize();

  //settings
  T gtol=_pt.get<T>("gtol",sqrt(numeric_limits<T>::epsilon()));
  T ftol=_pt.get<T>("ftol",sqrt(numeric_limits<T>::epsilon()));
  T xtol=_pt.get<T>("xtol",sqrt(numeric_limits<T>::epsilon()));
  int nrIter=_pt.get<int>("nrIter",numeric_limits<int>::max());

  //constraints
  Vec lb,ub;

  setDirty();
  string type=_pt.get<string>("type");
  if(type == "LBFGS") {
    ASSERT_MSG(_cons.empty(),"LBFGS doesn't support any constraints!")
    T fx=0;
    NoCallback<T,Kernel<T> > cb;
    LBFGSMinimizer<T> sol;
    sol.maxIterations()=nrIter;
    sol.nrCorrect()=_pt.get<int>("nrCorrect",10);
    sol.epsilon()=_pt.get<T>("gtol_over_xtol",1E-5f);
    sol.minimize(x,fx,*this,cb);
  } else if(type == "BLEIC" || type == "AUGLAG") {
    alglib::real_1d_array xAlglib=toAlglib(x);
    alglib::minbleicstate state;
    alglib::minbleicreport rep;
    alglib::minbleiccreate(xAlglib,state);
    vector<boost::shared_ptr<Term> > nl;
    {
      Mat c;
      Coli ct;
      extractConstraint(&lb,&ub,&c,NULL,&ct,nl);
      alglib::minbleicsetbc(state,toAlglib(lb),toAlglib(ub));
      if(ct.size() > 0)
        alglib::minbleicsetlc(state,toAlglib(c),toAlglib(ct));
      if(type != "AUGLAG") {
        ASSERT_MSG(nl.empty(),"alglib BLEIC doesn't support nonlinear constraints!")
      }
    }
    AugLag opt(_terms,nl,*this);
    opt.minimize(state,xAlglib,gtol,ftol,xtol,nrIter);
    alglib::minbleicresults(state,xAlglib,rep);
    x=fromAlglib(xAlglib);
  } else if(type == "LM") {
    alglib::real_1d_array xAlglib=toAlglib(x);
    alglib::minlmstate state;
    alglib::minlmreport rep;
    alglib::minlmcreatevj(values(),xAlglib,state);
    {
      Mat c;
      Coli ct;
      vector<boost::shared_ptr<Term> > nl;
      extractConstraint(&lb,&ub,&c,NULL,&ct,nl);
      alglib::minlmsetbc(state,toAlglib(lb),toAlglib(ub));
      ASSERT_MSG(ct.size() == 0,"alglib LM doesn't support general linear constraints!")
      ASSERT_MSG(nl.empty(),"alglib LM doesn't support nonlinear constraints!")
    }
    alglib::minlmsetcond(state,gtol,ftol,xtol,nrIter);
    alglib::minlmoptimize(state,fvecAlglib,fjacAlglib,NULL,this);
    alglib::minlmresults(state,xAlglib,rep);
    x=fromAlglib(xAlglib);
  } else if(type == "ELM" || type == "TR") {
    //the support of general constraints
    Coli ct;
    Vec cR;
    JacobianType cL;
    if(!_cons.empty()) {
      vector<boost::shared_ptr<Term> > nl;
      extractConstraint(&lb,&ub,&cL,&cR,&ct,nl);
      ASSERT_MSG(nl.empty(),"Eigen LM doesn't support nonlinear constraints!")
    }

    //QPSolver
    boost::property_tree::ptree ptQP;
#ifdef OOQP_SUPPORT
    ptQP.put<string>("type",_pt.get<string>("QPType","OOQP"));
#else
    ptQP.put<string>("type",_pt.get<string>("QPType","BLEIC"));
#endif
    ptQP.put<T>("gtol",_pt.get<T>("QPGtol",0.0f));
    ptQP.put<T>("ftol",_pt.get<T>("QPFtol",0.0f));
    ptQP.put<T>("xtol",_pt.get<T>("QPXtol",0.0f));
    ptQP.put<int>("nrIter",_pt.get<int>("QPNrIter",100000));
    _solQP.reset(new QPSolver(ptQP));

    //main loop
    JacobianType fjac,fjacL1,LHS;
    Vec fvec,fvecL1,fvecNew,fvecNewL1,RHS,dx,newL1,lastX;
    T nu=2.0f,eps=0.1f,lambda=0,trustRegion=ScalarUtil<T>::scalar_max;
    T trustRegionExpandCoef=_pt.get<T>("trustRegionExpandCoef",1.5f);
    T trustRegionShrinkCoef=_pt.get<T>("trustRegionShrinkCoef",0.1f);
    for(int it=0; it<nrIter; it++) {
      //compute function value
      if(operator()(x,fvec,fvecL1) < 0)
        break;
      T F=fvec.squaredNorm()+fvecL1.cwiseMax(0).sum();
      //exit if function value too small
      if(F < ftol)
        break;

      //compute local function expansion
      df(x,fjac,fjacL1,NULL);
      LHS=fjac.transpose()*fjac;
      RHS=fjac.transpose()*fvec;
      //diagonal adjust for ELM
      if(type == "ELM") {
        if(it == 0 && !isQP()) {
          //make LM initialization less sensitive
          lambda=LHS.diagonal().cwiseAbs().maxCoeff();
        }
        if(lambda != 0)
          for(sizeType i=0; i<LHS.rows(); i++)
            LHS.coeffRef(i,i)+=lambda;
      } else if(type == "TR") {
        if(it == 0 && !isQP()) {
          //make TR initialization less sensitive
          trustRegion=RHS.cwiseAbs().maxCoeff()/LHS.diagonal().cwiseAbs().maxCoeff();
          trustRegion=max<T>(trustRegion,1)*_pt.get<T>("initTrustRegion",0.001f);
        }
      } else ASSERT("Strange error, this optimization loop is for ELM and TR only!")

      //save last and solve subproblem
      lastX=x;
      solveQP(LHS,RHS,fjacL1,fvecL1,
              lb,ub,trustRegion,
              cL,cR,ct,
              x,dx,newL1);
      //exit if updation too small
      const Vec xNonTrivial=x.cwiseAbs().cwiseMax(Vec::Constant(ScalarUtil<T>::scalar_eps,x.size()));
      if((dx.array()/xNonTrivial.array()).matrix().cwiseAbs().maxCoeff() < xtol)
        break;
      if(dx.cwiseAbs().maxCoeff() < gtol)
        break;

      //prepare parameter updation
      x+=dx;
      if(operator()(x,fvecNew,fvecNewL1) < 0)
        break;
      const T FNew=fvecNew.squaredNorm()+fvecNewL1.cwiseMax(0).sum();
      const T FNewPredicate=(fvec+fjac*dx).squaredNorm()+newL1.sum();
      const T denom=(F-FNewPredicate);
      const T rho=(F-FNew)/denom;

      //parameter updation
      if(denom <= ScalarUtil<T>::scalar_eps) {
        WARNINGV("QP Solver Fail, function value increased by %f after solve!",denom)
        x=lastX;
        break;
      } else if(type == "ELM") {
        if(it == 0) {
          //we don't do parameter adjustment in the first iteration
          //this is because user might input an initial value violating the constraints
          continue;
        } else if(rho > eps) {
          //accept update
          lambda*=max<T>(1/3.0f,1-pow(2*rho-1,3));
          nu=2;
        } else if(rho <= 0) {
          //don't accept update
          x=lastX;
        }
        lambda=lambda*nu;
        nu=min<T>(2*nu,64.0f);
      } else if(type == "TR") {
        if(rho > eps)
          trustRegion*=trustRegionExpandCoef;
        else {
          x=lastX;
          trustRegion*=trustRegionShrinkCoef;
        }
      } else ASSERT("Strange error, this optimization loop is for ELM and TR only!")
    }
    _solQP.reset((QPSolver*)NULL);
  } else if(type == "SGD") {
    Vec dx,lastDx,lbL,ubL;

    //check bound
    if(!_cons.empty()) {
      Mat c;
      Coli ct;
      vector<boost::shared_ptr<Term> > nl;
      extractConstraint(&lb,&ub,&c,NULL,&ct,nl);
      ASSERT_MSG(ct.size() == 0,"SGD doesn't support general linear constraints!")
      ASSERT_MSG(nl.empty(),"SGD doesn't support nonlinear constraints!")
    }

    //parameter setup
    T learningRateInit=_pt.get<T>("SGDStepSize",0.01f);
    T f=0,norm=0,learningRate=learningRateInit;
    T decay=_pt.get<T>("decay",0.1f);
    int interval;
    if(_pt.get_optional<int>("interval").get_ptr() != NULL)
      interval=*(_pt.get_optional<int>("interval"));
    else if(_pt.get_optional<int>("stage").get_ptr() != NULL)
      interval=nrIter/max<int>(1,*(_pt.get_optional<int>("stage")));
    else interval=nrIter/2;
    if(!_pt.get<bool>("quiet",true))
      INFOV("Starting SGD with decay=%f, decay-interval=%d, nrIter=%d!",decay,interval,nrIter)

      //covariant-diff
      alglib::minqpreport rep;
    alglib::sparsematrix sparse;
    alglib::real_1d_array xAlglib;
    boost::shared_ptr<alglib::minqpstate> covariantSolver;
    boost::shared_ptr<Eigen::SparseQR<JacobianType,Eigen::COLAMDOrdering<int> > > QRSolver;
    if(_metric) {
      if(lb.size() == 0) {
        QRSolver.reset(new Eigen::SparseQR<JacobianType,Eigen::COLAMDOrdering<int> >);
        QRSolver->compute(*_metric);
      } else {
        covariantSolver.reset(new alglib::minqpstate());
        alglib::minqpcreate(x.size(),*covariantSolver);
        alglib::minqpsetalgobleic(*covariantSolver,0,0,0,1000000);
        toAlglib(sparse,*_metric);
        alglib::minqpsetquadratictermsparse(*covariantSolver,sparse,false);
      }
    }

    //SGD loop
    for(int it=0; it<nrIter; it++) {
      //compute gradient, termination check
      if(operator()(x,f,dx,1,true) < 0)
        break;
      //function value condition is invalid for SGD because constraint violation is not considered!
      //if(f < ftol)
      //  break;
      if(dx.cwiseAbs().maxCoeff() < gtol)
        break;
      if((dx.array()/x.array()).matrix().cwiseAbs().maxCoeff() < xtol)
        break;

      //update trust region
      if(it > 0 && (it%interval) == 0)
        learningRate*=decay;

      //go in that direction
      if((norm=dx.norm()) > 1)
        dx/=norm;
      dx*=learningRate;

      //account for covariant diff
      if(covariantSolver) {
        lbL=subtractBound(lb,x);
        ubL=subtractBound(ub,x);
        //start optimization
        alglib::minqpsetbc(*covariantSolver,toAlglib(lbL),toAlglib(ubL));
        alglib::minqpsetlinearterm(*covariantSolver,toAlglib(dx));
        dx.setZero(x.size());
        alglib::minqpsetstartingpoint(*covariantSolver,toAlglib(lastDx.size() == dx.size() ? lastDx : dx));
        alglib::minqpoptimize(*covariantSolver);
        alglib::minqpresults(*covariantSolver,xAlglib,rep);
        dx=fromAlglib(xAlglib);
      } else if(QRSolver) {
        dx=-QRSolver->solve(dx).eval();
      } else dx*=-1;
      x+=dx;
      lastDx=dx;

      //project
      if(lb.size() > 0) {
        x=x.cwiseMax(lb);
        x=x.cwiseMin(ub);
      }
    }
    if(covariantSolver)
      alglib::sparsefree(sparse);
  } else {
    ASSERT_MSG(false,"Unknown Optimizer Type!")
  }

  Vec dfdx=x;
  T fx;
  operator()(x,fx,dfdx,1,false);
  return fx;
}
/*void Optimizer::checkKKT(const Vec& x)
{
  //local linear approximation for constraints
  Coli types;
  Vec valCons;
  JacobianType gradCons;
  sizeType nrCons=COUNT_TERM(_cons);
  {
    types.setZero(nrCons);
    valCons.setZero(nrCons);
    MatrixBuilder<JacobianType> builder(gradCons,nrCons,x.size());
    LOOP_TERM(_cons) {
      //constraint gradient
      if(boost::dynamic_pointer_cast<SparseTerm>(_cons[i])) {
        SparseTerm::SMatd mat;
        valCons.block(off,0,_cons[i]->values(),1)=boost::dynamic_pointer_cast<SparseTerm>(_cons[i])->svalGrad(x,&mat);
        builder.addBlk(off,0,mat);
      } else {
        Mat mat;
        valCons.block(off,0,_cons[i]->values(),1)=_cons[i]->valGrad(x,&mat);
        builder.addBlk(off,0,mat);
      }
      //constraint
      int sgn=1;
      if(boost::dynamic_pointer_cast<EQConstraint>(_cons[i]))sgn=0;
      if(boost::dynamic_pointer_cast<SparseEQConstraint>(_cons[i]))sgn=0;
      types.block(off,0,_cons[i]->values(),1).setConstant(sgn);
    }
  }

  T fx=0;
  Vec dfdx=Vec::Zero(x.size());
  //local linear approximation for function gradient
  operator()(x,fx,dfdx,1,true);

  //now check kkt condition, solve for multiplier
  Vec lambda=Eigen::SparseQR<JacobianType,Eigen::COLAMDOrdering<int> >(gradCons*gradCons.transpose()).solve(gradCons*dfdx);
  Vec kktErr=dfdx-gradCons.transpose()*lambda;

  //check max constraint violation
  T maxConsVio=0,maxLambdaVio=0;
  for(sizeType i=0;i<nrCons;i++) {
    if(types[i] == 0)
      maxConsVio=max<T>(maxConsVio,abs(valCons[i]));
    else {
      maxConsVio=max<T>(maxConsVio,max<T>(0,-valCons[i]));
      if(valCons[i] > EPS)
        maxLambdaVio=max<T>(maxLambdaVio,abs(lambda[i]));
      else {
        //only in this case can lambda be non-zero
      }
    }
  }
  INFOV("Max constraint violation: %f, Max multiplier violation: %f, KKT violation: %f",maxConsVio,maxLambdaVio,kktErr.norm())
}*/
void Optimizer::addTerm(boost::shared_ptr<Term> term)
{
  _terms.push_back(term);
}
void Optimizer::addConstraint(boost::shared_ptr<Term> term)
{
  _cons.push_back(term);
}
void Optimizer::removeTermOrConstraint(boost::shared_ptr<Term> term)
{
  for(sizeType i=0; i<(sizeType)_terms.size(); i++)
    if(_terms[i] == term) {
      _terms.erase(_terms.begin()+i);
      break;
    }
  for(sizeType i=0; i<(sizeType)_cons.size(); i++)
    if(_cons[i] == term) {
      _cons.erase(_cons.begin()+i);
      break;
    }
}
void Optimizer::clearTerm()
{
  _terms.clear();
}
void Optimizer::clearConstraint()
{
  _cons.clear();
}
const boost::property_tree::ptree& Optimizer::getProps() const
{
  return _pt;
}
boost::property_tree::ptree& Optimizer::getProps()
{
  return _pt;
}
//covariant diff is used by SGD solver for parameter-independent performance
//linear constraints are automatically encounted using MPRGP method
void Optimizer::setCovariantDiff(boost::shared_ptr<JacobianType> metric)
{
  _metric=metric;
}
//overwritte Objective<T> for LBFGS Optimization
int Optimizer::operator()(const Vec& x,T& FX,Vec& DFDX,const T& step,bool wantGradient)
{
  bool reuse=_lastX.size() == x.size() && _lastX == x;
  if(!reuse)
    setX(&x);
  FX=0;
  DFDX.setZero(inputs());
  LOOP_TERM(_terms) {
    _terms[i]->setReuse(reuse);
    Vec val=_terms[i]->val(x);
    if(_terms[i]->isRectified()) {
      FX+=val.cwiseMax(0).sum();
      for(sizeType i=0; i<val.size(); i++)
        val[i]=val[i] > 0 ? 1 : 0;
    } else {
      FX+=val.squaredNorm();
      val*=2.0f;
    }
    if(wantGradient)
      _terms[i]->addGrad(x,DFDX,val);
  }
  if(_pt.get<bool>("debugOutputFunc",false)) {
    if(wantGradient) {
      INFOV("Function Value=%f, Gradient Norm=%f!",FX,DFDX.norm())
    } else {
      INFOV("Function Value=%f!",FX)
    }
  }
  _lastX=x;
  return 0;
}
//overwritte Objective<T> for LM Optimization
int Optimizer::operator()(const Vec& x,Vec& fvec)
{
  Vec fvecInner,fvecL1;
  int ret=operator()(x,fvecInner,fvecL1);
  fvec.resize(fvecInner.rows()+fvecL1.rows());
  fvec.block(0,0,fvecInner.size(),1)=fvecInner;
  fvec.block(fvecInner.size(),0,fvecL1.size(),1)=fvecL1.cwiseMax(0).cwiseSqrt();
  return ret;
}
int Optimizer::df(const Vec& x,Mat& fjac)
{
  Vec fvecL1;
  Mat fjacInner,fjacL1;
  int ret=df(x,fjacInner,fjacL1,&fvecL1);
  fjacL1=(getL1GradCoef(fvecL1)*fjacL1).eval();

  MatrixBuilder<Mat> builder(fjac,fjacInner.rows()+fjacL1.rows(),inputs());
  builder.addBlk(0,0,fjacInner);
  builder.addBlk(fjacInner.rows(),0,fjacL1);
  return ret;
}
int Optimizer::df(const Vec& x,JacobianType& fjac)
{
  Vec fvecL1;
  JacobianType fjacInner,fjacL1;
  int ret=df(x,fjacInner,fjacL1,&fvecL1);
  fjacL1=(getL1GradCoef(fvecL1)*fjacL1).eval();

  MatrixBuilder<JacobianType> builder(fjac,fjacInner.rows()+fjacL1.rows(),inputs());
  builder.addBlk(0,0,fjacInner);
  builder.addBlk(fjacInner.rows(),0,fjacL1);
  return ret;
}
//objective functions for Lasso-penaltized optimization
int Optimizer::operator()(const Vec& x,Vec& fvec,Vec& fvecL1)
{
  bool reuse=_lastX.size() == x.size() && _lastX == x;
  if(!reuse)
    setX(&x);
  fvecL1.setZero(COUNT_TERM_L1(_terms));
  fvec.setZero(values()-fvecL1.size());
  LOOP_TERM_L1(_terms) {
    _terms[i]->setReuse(reuse);
    if(_terms[i]->isRectified())
      fvecL1.segment(offL1,_terms[i]->values())=_terms[i]->val(x);
    else fvec.segment(off,_terms[i]->values())=_terms[i]->val(x);
  }
  if(_pt.get<bool>("debugOutputFunc",false))
    INFOV("Function Value=%f!",fvec.squaredNorm()+fvecL1.cwiseMax(0).sum())
    _lastX=x;
  return 0;
}
int Optimizer::df(const Vec& x,Mat& fjac,Mat& fjacL1,Vec* fvecL1)
{
  bool reuse=_lastX.size() == x.size() && _lastX == x;
  if(!reuse)
    setX(&x);
  Mat df;
  fjacL1.setZero(COUNT_TERM_L1(_terms),inputs());
  fjac.setZero(values()-fjacL1.rows(),inputs());
  if(fvecL1)
    fvecL1->resize(fjacL1.rows());
  LOOP_TERM_L1(_terms) {
    _terms[i]->setReuse(reuse);
    df.setZero(_terms[i]->values(),inputs());
    if(fvecL1 && _terms[i]->isRectified())
      fvecL1->block(offL1,0,df.rows(),1)=_terms[i]->valGrad(x,&df);
    else _terms[i]->grad(x,df);
    if(_terms[i]->isRectified())
      fjacL1.block(offL1,0,df.rows(),df.cols())=df;
    else fjac.block(off,0,df.rows(),df.cols())=df;
  }
  _lastX=x;
  return 0;
}
int Optimizer::df(const Vec& x,JacobianType& fjac,JacobianType& fjacL1,Vec* fvecL1)
{
  bool reuse=_lastX.size() == x.size() && _lastX == x;
  if(!reuse)
    setX(&x);
  Mat df;
  SparseTerm::SMatd currTrips;
  MatrixBuilder<JacobianType> builderJacL1(fjacL1,COUNT_TERM_L1(_terms),inputs());
  MatrixBuilder<JacobianType> builderJac(fjac,values()-builderJacL1.get().rows(),inputs());
  if(fvecL1)
    fvecL1->resize(fjacL1.rows());
  LOOP_TERM_L1(_terms) {
    _terms[i]->setReuse(reuse);
    if(boost::dynamic_pointer_cast<SparseTerm>(_terms[i])) {
      currTrips.clear();
      if(fvecL1 && _terms[i]->isRectified())
        fvecL1->block(offL1,0,_terms[i]->values(),1)=boost::dynamic_pointer_cast<SparseTerm>(_terms[i])->svalGrad(x,&currTrips);
      else boost::dynamic_pointer_cast<SparseTerm>(_terms[i])->svalGrad(x,&currTrips);
      if(_terms[i]->isRectified())
        builderJacL1.addBlk(offL1,0,currTrips);
      else builderJac.addBlk(off,0,currTrips);
    } else {
      df.setZero(_terms[i]->values(),inputs());
      if(fvecL1 && _terms[i]->isRectified())
        fvecL1->block(offL1,0,df.rows(),1)=_terms[i]->valGrad(x,&df);
      else _terms[i]->grad(x,df);
      if(_terms[i]->isRectified())
        builderJacL1.addBlk(offL1,0,df);
      else builderJac.addBlk(off,0,df);
    }
  }
  _lastX=x;
  return 0;
}
//For Finite Difference Debug
void Optimizer::debugFD(T delta,bool cons,bool debugJac,bool debugGrad,const Vec* currX)
{
  //reorganize energy and constraints
  reorganize();

  Mat fjac;
  JacobianType fjacS;
  Vec x=currX ? *currX : Vec::Random(inputs()),fvec,fvec2;

  //add all constraints to terms
  vector<boost::shared_ptr<Term> > terms=_terms;
  if(cons)
    _terms.insert(_terms.end(),_cons.begin(),_cons.end());

  //pass 1
  T maxErr=0,maxRelErr=0;
  if(debugJac) {
    setDirty();
    operator()(x,fvec);
    df(x,fjac);
    df(x,fjacS);
    for(sizeType i=0; i<inputs(); i++) {
      Vec tmp=x;
      tmp[i]+=delta;
      operator()(tmp,fvec2);

      T jacAbs=fjac.col(i).norm();
      T jacErr=(fjac.col(i)-(fvec2-fvec)/delta).norm();
      if(_pt.get<bool>("printFDError",false))
        INFOV("fjac-Norm: %f Err: %f!",jacAbs,jacErr);
      if(jacAbs > ScalarUtil<T>::scalar_eps) {
        maxErr=max(maxErr,jacErr);
        maxRelErr=max(maxRelErr,jacErr/jacAbs);
      }
    }
    ASSERT_MSG(maxErr < 1E-4f || maxRelErr < 1E-2f,"Too big relative maxErr!")
    INFOV("Max Jacobian Error: %f Rel: %f, Sparse-Dense Error: %f!",maxErr,maxRelErr,(fjac-fjacS.toDense()).norm())
  }

  //pass 2
  maxErr=0,maxRelErr=0;
  if(debugGrad) {
    setDirty();
    T fx,fx2;
    Vec grad,grad2;
    operator()(x,fvec);
    operator()(x,fx,grad,1,true);
    for(sizeType i=0; i<inputs(); i++) {
      Vec tmp=x;
      tmp[i]+=delta;
      operator()(tmp,fx2,grad2,1,false);

      T gradErr=(grad[i]-(fx2-fx)/delta);
      if(_pt.get<bool>("printFDError",false))
        INFOV("fjac-Norm: %f Err: %f!",grad[i],gradErr);
      if(abs(grad[i]) > ScalarUtil<T>::scalar_eps) {
        maxErr=max(maxErr,gradErr);
        maxRelErr=max(maxRelErr,gradErr/grad[i]);
      }
    }
    ASSERT_MSG(maxErr < 1E-4f || maxRelErr < 1E-2f,"Too big relative maxErr!")
    INFOV("Max Grad Error: %f Rel: %f, fvec-FX Error: %f!",maxErr,maxRelErr,(fx-fvec.squaredNorm()))
  }

  //restore
  _terms=terms;
}
//Dimension Info
int Optimizer::inputs() const
{
  return _pt.get<int>("inputs",-1);
}
int Optimizer::values() const
{
  return COUNT_TERM(_terms);
}
int Optimizer::constraints() const
{
  return COUNT_TERM(_cons);
}
void Optimizer::profileLineSearch(const sizeType& k,const Vec& x,const Vec& d,const T& step)
{
  if(!_pt.get<bool>("debug",false))
    return;
  INFOV("Line Search Step Size=%f!",step)
}
void Optimizer::setDirty()
{
  _lastX.setZero(0);
}
void Optimizer::setX(const Vec* x) {}
void Optimizer::reorganize()
{
  //rearrange
  vector<boost::shared_ptr<Term> > toCons,toTerm;
  LOOP_TERM(_terms)
  if(!_terms[i]->_asEnergy) {
    INFOV("Moving term %s to constraint set!",typeid(*(_terms[i])).name())
    toCons.push_back(_terms[i]);
  }
  LOOP_TERM(_cons)
  if(_cons[i]->_asEnergy) {
    INFOV("Moving constraint %s to energy set!",typeid(*(_cons[i])).name())
    toTerm.push_back(_cons[i]);
  }
  LOOP_TERM(toCons) {
    removeTermOrConstraint(toCons[i]);
    addConstraint(toCons[i]);
  }
  LOOP_TERM(toTerm) {
    removeTermOrConstraint(toTerm[i]);
    addTerm(toTerm[i]);
  }
  //remove duplicate
  {
    sort(_terms.begin(),_terms.end());
    vector<boost::shared_ptr<Term> >::iterator it=unique(_terms.begin(),_terms.end());
    _terms.resize(distance(_terms.begin(),it));
  }
  //remove duplicate
  {
    sort(_cons.begin(),_cons.end());
    vector<boost::shared_ptr<Term> >::iterator it=unique(_cons.begin(),_cons.end());
    _cons.resize(distance(_cons.begin(),it));
  }
}
template <typename MAT>
void Optimizer::extractConstraint(Vec* lb,Vec* ub,MAT* cL,Vec* cR,Coli* ct,vector<boost::shared_ptr<Term> >& nl) const
{
  Vec x=Vec::Zero(inputs());
  vector<boost::shared_ptr<Term> > gc;
  //pass 1: build box constraints/collect number of general linear constraints
  if(lb && ub) {
    lb->setConstant(inputs(),-ScalarUtil<T>::scalar_max);
    ub->setConstant(inputs(),ScalarUtil<T>::scalar_max);
    sizeType nrBox=0;
    for(sizeType i=0; i<(sizeType)_cons.size(); i++) {
      if(!_cons[i]->isLinear()) {
        nl.push_back(_cons[i]);
        continue;
      }
      //you must use SparseTerm for box constraint
      boost::shared_ptr<SparseTerm> term=boost::dynamic_pointer_cast<SparseTerm>(_cons[i]);
      if(!term) {
        gc.push_back(_cons[i]);
        continue;
      }
      if(!IS_BOX_CONSTRAINT(*lb,*ub,*term))
        gc.push_back(term);
      else nrBox+=term->values();
    }
    if(!_pt.get<bool>("quiet",true))
      INFOV("Found %ld box constraints!",nrBox);
  }

  //pass 2: build general linear constraints
  if(cL && ct) {
    if(cR)cR->setZero(COUNT_TERM(gc));
    ct->setZero(COUNT_TERM(gc));
    MatrixBuilder<MAT> cLBuilder(*cL,COUNT_TERM(gc),x.size()+(cR ? 0 : 1));
    LOOP_TERM(gc) {
      //set LHS
      Vec val;
      boost::shared_ptr<SparseTerm> st=boost::dynamic_pointer_cast<SparseTerm>(gc[i]);
      if(st) {
        SparseTerm::SMatd mat;
        val=st->svalGrad(x,&mat);
        cLBuilder.addBlk(off,0,mat);
      } else {
        Mat mat;
        val=gc[i]->valGrad(x,&mat);
        cLBuilder.addBlk(off,0,mat);
      }
      //set RHS
      if(cR)cR->block(off,0,gc[i]->values(),1)=-val;
      else cLBuilder.addBlk(off,inputs(),-val);
      //set type
      ct->segment(off,gc[i]->values()).setConstant(gc[i]->sign());
    }
    if(!_pt.get<bool>("quiet",true))
      INFOV("Found %ld linear constraints, %ld nonlinear constraints!",(sizeType)cL->rows(),COUNT_TERM(nl))
    }
}
Eigen::DiagonalMatrix<Optimizer::T,-1,-1> Optimizer::getL1GradCoef(const Vec& val) const
{
  Eigen::DiagonalMatrix<T,-1,-1> ret(val.size());
  ret.diagonal().setZero();
  for(sizeType i=0; i<val.size(); i++)
    if(val[i] > ScalarUtil<T>::scalar_eps)
      ret.diagonal()[i]=0.5f/sqrt(val[i]);
  return ret;
}
Optimizer::Vec Optimizer::subtractBound(const Vec& b,const Vec& x) const
{
  Vec ret=b;
  OMP_PARALLEL_FOR_
  for(sizeType i=0; i<ret.size(); i++)
    if(ret[i] != ScalarUtil<T>::scalar_max &&
       ret[i] != -ScalarUtil<T>::scalar_max)
      ret[i]-=x[i];
  return ret;
}
void Optimizer::solveQP(const JacobianType& LHS,const Vec& RHS, //objective
                        const JacobianType& fjacL1,const Vec& fvecL1, //L1 term
                        Vec& lb,Vec& ub,T trustRegion,  //box constraints
                        const JacobianType& cL,const Vec& cR,const Coli& ct,  //linear constraints
                        const Vec& x,Vec& dx,Vec& newL1) const   //output
{
  Vec lbL,ubL;
  sizeType nl1=COUNT_TERM_L1(_terms);
  if(lb.size() == 0)
    //need local bounds for trust region
    //need local bounds if we have L1 constraints
    if(trustRegion < ScalarUtil<T>::scalar_max || nl1 > 0) {
      lb.setConstant(inputs(),-ScalarUtil<T>::scalar_max);
      ub.setConstant(inputs(),ScalarUtil<T>::scalar_max);
    }
  //setup local bounds
  if(lb.size() > 0) {
    lbL=subtractBound(lb,x);
    ubL=subtractBound(ub,x);
    lbL=lbL.cwiseMax(-trustRegion);
    ubL=ubL.cwiseMin(trustRegion);
    extend<Vec>(lbL,nl1,0,0);
    extend<Vec>(ubL,nl1,0,ScalarUtil<T>::scalar_max);
  }
  //local linear constraints
  Vec cRL;
  JacobianType cLL;
  Coli ctL=extendRet<Coli>(ct,nl1,0,1);
  if(cL.rows() > 0 || nl1 > 0) {
    MatrixBuilder<JacobianType> cLLBuilder(cLL,cL.rows()+nl1,x.size()+nl1);
    cLLBuilder.addBlk(0,0,cL);
    cRL=extendRet<Vec>(cR,nl1,0,0);
    if(cL.rows() > 0)
      cRL.block(0,0,cL.rows(),1)-=cL*x;
    cRL.block(cL.rows(),0,nl1,1)=fvecL1;
    cLLBuilder.addBlk(cL.rows(),0,fjacL1,-1);
    cLLBuilder.addIdentity(cL.rows(),x.size(),nl1);
  }
  //local problem
  JacobianType AL;
  {
    MatrixBuilder<JacobianType> ALBuilder(AL,x.size()+nl1,x.size()+nl1);
    ALBuilder.addBlk(0,0,LHS);
  }
  Vec bL=extendRet<Vec>(RHS,nl1,0,1);
  _solQP->solveQP(dx=Vec::Zero(bL.size()),AL,bL,lbL,ubL,cLL,cRL,ctL);
  //if(!solQP.solveQP(dx=Vec::Zero(bL.size()),AL,bL,lbL,ubL,cLL,cRL,ctL)) {WARNING("QP Solver Fail!")break;}
  newL1=dx.block(x.size(),0,nl1,1);
  dx=dx.block(0,0,inputs(),1).eval();
}
bool Optimizer::isQP() const
{
  const string type=_pt.get<string>("type","");
  LOOP_TERM(_terms) {
    if(!_terms[i]->isLinear())
      return false;
    if(_terms[i]->isRectified() && type != "ELM")
      return false;
  }
  LOOP_TERM(_cons)
  if(!_cons[i]->isLinear())
    return false;
  return true;
}

//quadratic programming subproblem solver
QPSolver::QPSolver(const boost::property_tree::ptree& pt):_pt(pt) {}
bool QPSolver::solveQP(Vec& x,const JacobianType& A,const Vec& b,const Vec& lb,const Vec& ub,const Mat& C,const Vec& d,const Coli& type) const
{
  bool ret=solveQP(x,A,b,
                   lb.size() == 0 ? NULL : &lb,
                   ub.size() == 0 ? NULL : &ub,
                   C.size() == 0 ? NULL : &C,
                   d.size() == 0 ? NULL : &d,
                   type.size() == 0 ? NULL : &type);
  return ret;
}
bool QPSolver::solveQP(Vec& x,const JacobianType& A,const Vec& b,const Vec* lb,const Vec* ub,const Mat* C,const Vec* d,const Coli* type) const
{
  checkProb(x,A,b,lb,ub,C,d,type);
  string stype=_pt.get<string>("type");
  if(stype == "MPRGP") {
    ASSERT_MSG(!C || C->rows() == 0,"MPRGP doesn't support general linear constraints!")
    return MPRGPSolve(x,A,b,lb,ub);
  } else if(stype == "BLEIC") {
    return BLEICSolve(x,A,b,lb,ub,C,d,type);
  } else if(stype == "OOQP") {
    JacobianType Cs;
    if(C) {
      Cs.resize(C->rows(),C->cols());
      for(sizeType r=0; r<C->rows(); r++)
        for(sizeType c=0; c<C->cols(); c++)
          Cs.coeffRef(r,c)=C->operator()(r,c);
    }
    return OOQPSolve(x,A,b,lb,ub,C ? &Cs : NULL,d,type);
  } else ASSERT_MSG(false,"Unknown QP Solver Type!")
    return false;
}
bool QPSolver::solveQP(Vec& x,const JacobianType& A,const Vec& b,const Vec* lb,const Vec* ub,const JacobianType* C,const Vec* d,const Coli* type) const
{
  string stype=_pt.get<string>("type");
  if(stype == "MPRGP") {
    ASSERT_MSG(!C || C->rows() == 0,"MPRGP doesn't support general linear constraints!")
    return MPRGPSolve(x,A,b,lb,ub);
  } else if(stype == "BLEIC") {
    if(C) {
      Mat Cd=C->toDense();
      return BLEICSolve(x,A,b,lb,ub,&Cd,d,type);
    } else return BLEICSolve(x,A,b,lb,ub,NULL,NULL,NULL);
  } else if(stype == "OOQP") {
    return OOQPSolve(x,A,b,lb,ub,C,d,type);
  } else ASSERT_MSG(false,"Unknown QP Solver Type!")
    return false;
}
bool QPSolver::MPRGPSolve(Vec& x,const JacobianType& A,const Vec& b,const Vec* lb,const Vec* ub) const
{
  T gtol=_pt.get<T>("gtol",0);
  int nrIter=_pt.get<int>("nrIter",numeric_limits<int>::max());

  Vec lbC=lb ? *lb : Vec::Constant(b.size(),-ScalarUtil<T>::scalar_max);
  Vec ubC=ub ? *ub : Vec::Constant(b.size(),ScalarUtil<T>::scalar_max);
  MPRGPQPSolver<T> sol;
  sol.reset(lbC,ubC);
  boost::shared_ptr<DefaultEigenKrylovMatrix<T,Kernel<T>,JacobianType::Options,JacobianType::Index> >
  m(new DefaultEigenKrylovMatrix<T,Kernel<T>,JacobianType::Options,JacobianType::Index>(A));
  boost::shared_ptr<DiagonalInFacePreconSolver<T> > pre(new DiagonalInFacePreconSolver<T>(A));
  sol.setKrylovMatrix(m);
  sol.setPre(pre);
  sol.setSolverParameters(gtol,nrIter);
  return sol.solve(-b,x) == MPRGPQPSolver<T>::SUCCESSFUL;
}
bool QPSolver::BLEICSolve(Vec& x,const JacobianType& A,const Vec& b,const Vec* lb,const Vec* ub,const Mat* C,const Vec* d,const Coli* type) const
{
  T gtol=_pt.get<T>("gtol",0);
  T ftol=_pt.get<T>("ftol",sqrt(numeric_limits<T>::epsilon()));
  T xtol=_pt.get<T>("xtol",sqrt(numeric_limits<T>::epsilon()));
  int nrIter=_pt.get<int>("nrIter",numeric_limits<int>::max());

  //need general QP solver
  alglib::real_1d_array xAlglib;
  alglib::minqpstate state;
  alglib::minqpreport rep;
  alglib::sparsematrix sparse;
  //count L1 terms, create solver
  alglib::minqpcreate(b.size(),state);
  alglib::minqpsetalgobleic(state,gtol,ftol,xtol,nrIter);
  //INFOV("Bleic QP Solve with gtol=%f, ftol=%f, xtol=%f!",gtol,ftol,xtol)
  //set linear, quadratic terms
  toAlglib(sparse,A);
  alglib::minqpsetquadratictermsparse(state,sparse,false);
  alglib::minqpsetlinearterm(state,toAlglib(b)); //use value 1 to extend RHS
  //set bound constraints
  if(lb && ub)
    alglib::minqpsetbc(state,toAlglib(*lb),toAlglib(*ub));
  //set general linear constraints
  if(C && d && type && C->rows() > 0) {
    Mat Cd=Mat::Zero(C->rows(),C->cols()+1);
    Cd.block(0,0,C->rows(),C->cols())=*C;
    Cd.block(0,C->cols(),C->rows(),1)=*d;
    alglib::minqpsetlc(state,toAlglib(Cd),toAlglib(*type));
  }
  //start optimization
  alglib::minqpsetstartingpoint(state,toAlglib(x));
  alglib::minqpoptimize(state);
  alglib::minqpresults(state,xAlglib,rep);
  alglib::sparsefree(sparse);
  x=fromAlglib(xAlglib);
  //if(rep.terminationtype < 1 || rep.terminationtype > 4)
  //  WARNINGV("Bleic QP Solver failed with termination type: %ld!",rep.terminationtype)
  return rep.terminationtype >= 1 && rep.terminationtype <= 4;
}
bool QPSolver::OOQPSolve(Vec& x,const JacobianType& A,const Vec& b,const Vec* lb,const Vec* ub,const JacobianType* C,const Vec* d,const Coli* type) const
{
  ASSERT_MSG(JacobianType::Options&Eigen::RowMajor,"OOQP only support row major matrix!")
  typedef Eigen::Matrix<double,-1,1> DVec;
  typedef Eigen::Matrix<char,-1,1> CVec;
  const double BEG=ScalarUtil<T>::scalar_max;
  const double NBEG=-ScalarUtil<T>::scalar_max;

  //build lb ub
  DVec lbV=DVec::Zero(x.size());
  DVec ubV=DVec::Zero(x.size());
  CVec lbT=CVec::Zero(x.size());
  CVec ubT=CVec::Zero(x.size());
  if(lb && ub)
    for(sizeType i=0; i<x.size(); i++) {
      if((*lb)[i] > NBEG) {
        lbV[i]=lb->operator[](i);
        lbT[i]=1;
      }
      if((*ub)[i] < BEG) {
        ubV[i]=ub->operator[](i);
        ubT[i]=1;
      }
    }

  //build b
  DVec bV=b.cast<double>();

  //build A
  int nnzA=0;
  int* irowA=NULL;
  int* jcolA=NULL;
  double* valA=NULL;
  {
    //count NNZ
    for(int k=0; k<A.outerSize(); ++k)
      for(typename JacobianType::InnerIterator it(A,k); it; ++it)
        if(it.row() >= it.col())
          nnzA++;
    //fill elements
    if(nnzA > 0) {
      irowA=new int[nnzA];
      jcolA=new int[nnzA];
      valA=new double[nnzA];
      nnzA=0;
      for(int k=0; k<A.outerSize(); ++k)
        for(typename JacobianType::InnerIterator it(A,k); it; ++it)
          if(it.row() >= it.col()) {
            irowA[nnzA]=it.row();
            jcolA[nnzA]=it.col();
            valA[nnzA++]=it.value();
          }
      //make row major
      //doubleLexSort(irowA,nnzA,jcolA,valA);
    }
  }

  //build C,d,type
  int nrCE=0,nrCNE=0;
  int nnzCE=0,nnzCNE=0;
  int *irowCE=NULL,*irowCNE=NULL;
  int *jcolCE=NULL,*jcolCNE=NULL;
  double *valCE=NULL,*valCNE=NULL;
  DVec bCE,LCNE,UCNE;
  CVec LCNEI,UCNEI;
  if(C && d && type) {
    //count nr constraints
    for(sizeType i=0; i<type->size(); i++)
      if((*type)[i] == 0)
        nrCE++;
      else nrCNE++;
    //allocate
    bCE.setZero(nrCE);
    LCNE.setZero(nrCNE);
    UCNE.setZero(nrCNE);
    LCNEI.setZero(nrCNE);
    UCNEI.setZero(nrCNE);
    //assign RHS
    nrCE=nrCNE=0;
    for(sizeType i=0; i<type->size(); i++)
      if((*type)[i] == 0) {
        bCE[nrCE++]=(*d)[i];
      } else if((*type)[i] > 0) {
        LCNE[nrCNE]=(*d)[i];
        LCNEI[nrCNE++]=1;
      } else {
        UCNE[nrCNE]=(*d)[i];
        UCNEI[nrCNE++]=1;
      }
    //count NNZ
    for(int k=0; k<C->outerSize(); ++k)
      for(typename JacobianType::InnerIterator it(*C,k); it; ++it)
        if((*type)[it.row()] == 0)
          nnzCE++;
        else nnzCNE++;
    if(nnzCE > 0) {
      irowCE=new int[nnzCE];
      jcolCE=new int[nnzCE];
      valCE=new double[nnzCE];
    }
    if(nnzCNE > 0) {
      irowCNE=new int[nnzCNE];
      jcolCNE=new int[nnzCNE];
      valCNE=new double[nnzCNE];
    }
    //fill elements
    nrCE=0;
    nrCNE=0;
    nnzCE=0;
    nnzCNE=0;
    for(int k=0; k<C->outerSize(); ++k)
      if((*type)[k] == 0) {
        for(typename JacobianType::InnerIterator it(*C,k); it; ++it) {
          irowCE[nnzCE]=nrCE;
          jcolCE[nnzCE]=it.col();
          valCE[nnzCE++]=it.value();
        }
        nrCE++;
      } else {
        for(typename JacobianType::InnerIterator it(*C,k); it; ++it) {
          irowCNE[nnzCNE]=nrCNE;
          jcolCNE[nnzCNE]=it.col();
          valCNE[nnzCNE++]=it.value();
        }
        nrCNE++;
      }
    ASSERT(nrCE+nrCNE == (C?C->rows():0))
    //make row major
    //if(nnzCE > 0)
    //  doubleLexSort(irowCE,nnzCE,jcolCE,valCE);
    //if(nnzCNE > 0)
    //  doubleLexSort(irowCNE,nnzCNE,jcolCNE,valCNE);
  }

  //solve
#ifdef OOQP_SUPPORT
  double FX;
  int status;
  DVec xV=x.cast<double>();
  DVec gamma,phi,y,z,lambda,pi;
  gamma.resize(x.size());
  phi.resize(x.size());
  y.resize(nrCE);
  z.resize(nrCNE);
  lambda.resize(nrCNE);
  pi.resize(nrCNE);
  qpsolvesp(bV.data(),b.size(),
            irowA,nnzA,jcolA,valA,
            lbV.data(),lbT.data(),
            ubV.data(),ubT.data(),
            irowCE,nnzCE,jcolCE,valCE,bCE.data(),nrCE,
            irowCNE,nnzCNE,jcolCNE,valCNE,LCNE.data(),nrCNE,LCNEI.data(),UCNE.data(),UCNEI.data(),
            xV.data(),gamma.data(),phi.data(),y.data(),z.data(),lambda.data(),pi.data(),&FX,0,&status);
  x=xV.cast<T>();

  //remove A
  if(irowA)delete[]irowA;
  if(jcolA)delete[]jcolA;
  if(valA)delete[]valA;

  //remove CE
  if(irowCE)delete[]irowCE;
  if(jcolCE)delete[]jcolCE;
  if(valCE)delete[]valCE;

  //remove CNE
  if(irowCNE)delete[]irowCNE;
  if(jcolCNE)delete[]jcolCNE;
  if(valCNE)delete[]valCNE;
  return status == 0;
#else
  ASSERT_MSG(false,"OOQP not supported!")
  return false;
#endif
}
template <typename MAT>
void QPSolver::checkProb(Vec& x,const JacobianType& A,const Vec& b,const Vec* lb,const Vec* ub,const MAT* C,const Vec* d,const Coli* type) const
{
  ASSERT_MSG((lb && ub) || (!lb && !ub),"You need to provide both bounds!")
  if(lb && ub)
    ASSERT_MSG(lb->size() == x.size() && ub->size() == x.size(),"Bounds size mismatch!")
    ASSERT_MSG((C && d && type) || (!C && !d && !type),"You need to provide both LHS RHS type of linear constraints!")
    if(C && d && type)
      ASSERT_MSG(C->cols() == x.size() && C->rows() == d->size() && C->rows() == type->size(),"Linear constraints size mismatch!")
      ASSERT_MSG(A.rows() == A.cols() && A.rows() == b.size() && x.size() == b.size(),"Problem size mismatch!")
    }

//example1: 1D Diffusion with constraint
class DirichletTerm : public SparseTerm
{
public:
  DirichletTerm(const Optimizer& opt,sizeType i,sizeType j):SparseTerm(opt),_i(i),_j(j) {}
  virtual sizeType values() const {
    return 1;
  }
protected:
  virtual Vec valGradInner(const Vec& x,SMatd& grad) {
    grad.clear();
    grad.push_back(Eigen::Triplet<T,sizeType>(0,_i,1));
    grad.push_back(Eigen::Triplet<T,sizeType>(0,_j,-1));

    Vec ret=Vec::Zero(1);
    ret[0]=x[_i]-x[_j];
    return ret;
  }
  sizeType _i,_j;
};
class FixTerm : public SparseEQConstraint
{
public:
  FixTerm(const Optimizer& opt):SparseEQConstraint(opt),_rectified(false) {}
  virtual sizeType values() const {
    return (sizeType)_goalStrength.size();
  }
  virtual bool isRectified() const {
    return _rectified;
  }
  void addTerm(sizeType i,T goal,T strength) {
    _id.push_back(i);
    _goalStrength.push_back(make_pair(goal,strength));
  }
  bool _rectified;
protected:
  virtual Vec valGradInner(const Vec& x,SMatd& grad) {
    grad.clear();
    Vec ret=Vec::Zero(values());
    for(sizeType i=0; i<values(); i++) {
      grad.push_back(Eigen::Triplet<T,sizeType>(i,_id[i],_goalStrength[i].second));
      ret[i]=(x[_id[i]]-_goalStrength[i].first)*_goalStrength[i].second;
    }
    return ret;
  }
  vector<sizeType> _id;
  vector<pair<T,T> > _goalStrength;
};
class FixTermDense : public EQConstraint
{
public:
  FixTermDense(const Optimizer& opt):EQConstraint(opt),_rectified(false) {}
  virtual sizeType values() const {
    return (sizeType)_goalStrength.size();
  }
  virtual bool isRectified() const {
    return _rectified;
  }
  void addTerm(sizeType i,T goal,T strength) {
    _id.push_back(i);
    _goalStrength.push_back(make_pair(goal,strength));
  }
  bool _rectified;
protected:
  virtual Vec valGradInner(const Vec& x,Mat& grad) {
    grad.setZero(values(),x.size());
    Vec ret=Vec::Zero(values());
    for(sizeType i=0; i<values(); i++) {
      grad(i,_id[i])=_goalStrength[i].second;
      ret[i]=(x[_id[i]]-_goalStrength[i].first)*_goalStrength[i].second;
    }
    return ret;
  }
  vector<sizeType> _id;
  vector<pair<T,T> > _goalStrength;
};
class TotalTerm : public EQConstraint
{
public:
  TotalTerm(const Optimizer& opt,T goal):EQConstraint(opt),_goal(goal) {}
  virtual sizeType values() const {
    return 1;
  }
protected:
  virtual Vec valGradInner(const Vec& x,Mat& grad) {
    grad.setConstant(1,x.size(),-1);
    Vec ret=Vec::Zero(1);
    ret[0]=_goal-x.sum();
    return ret;
  }
  T _goal;
};
class NormTerm : public GEQConstraint
{
public:
  NormTerm(const Optimizer& opt,T goal):GEQConstraint(opt),_goal(goal) {}
  virtual sizeType values() const {
    return 1;
  }
  bool isLinear() const {
    return false;
  }
protected:
  virtual Vec valGradInner(const Vec& x,Mat& grad) {
    grad.resize(1,x.size());
    grad.row(0)=-2*x;
    Vec ret=Vec::Zero(1);
    ret[0]=_goal-x.squaredNorm();
    return ret;
  }
  T _goal;
};
OptimizerTest::Vec OptimizerTest::runExample1(sizeType N,bool cons,bool consTotal,bool consNorm,T SL1,const string& type,bool quiet,bool covariant)
{
  Optimizer opt;
  opt.getProps().put<int>("inputs",N);
  opt.getProps().put<string>("type",type);
  for(sizeType i=1; i<N; i++)
    opt.addTerm(boost::shared_ptr<DirichletTerm>(new DirichletTerm(opt,i-1,i)));
  if(covariant) {
    Optimizer::JacobianType G;
    opt.df(Vec::Zero(opt.inputs()),G);
    boost::shared_ptr<Optimizer::JacobianType> GTG(new Optimizer::JacobianType);
    *GTG=G.transpose()*G;
    opt.setCovariantDiff(GTG);
  }
  boost::shared_ptr<FixTerm> fix(new FixTerm(opt));
  fix->addTerm(0,0,10.0f);
  fix->addTerm(N-1,1,10.0f);
  if(cons) { //add soft constraint
    fix->_asEnergy=false;
    opt.addConstraint(fix);
  } else { //add box hard constraint
    fix->_asEnergy=true;
    opt.addTerm(fix);
  }
  if(consTotal) //add dense hard constraint
    opt.addConstraint(boost::shared_ptr<TotalTerm>(new TotalTerm(opt,100.0f)));
  if(consNorm)  //add nonlinear constraint
    opt.addConstraint(boost::shared_ptr<NormTerm>(new NormTerm(opt,1.5f)));
  if(SL1 > 0) {
    boost::shared_ptr<FixTerm> L(new FixTerm(opt));
    L->addTerm(N/3,-0.1f,SL1);
    L->addTerm(N/2,0.5f,SL1);
    L->_asEnergy=true;
    L->_rectified=true;
    opt.addTerm(L);

    boost::shared_ptr<FixTermDense> LD;
    LD.reset(new FixTermDense(opt));
    LD->addTerm(N*2/3,-0.1f,SL1);
    LD->addTerm(N*4/5,2.0f,SL1);
    LD->_asEnergy=true;
    LD->_rectified=true;
    opt.addTerm(LD);
  }
  opt.getProps().put<bool>("debugOutputFunc",!quiet);
  opt.getProps().put<sizeType>("nrIter",10000);
  Vec x=Vec::Zero(N);
  T fx=opt.minimize(x);
  x=Vec::Random(N);
  T fx2=opt.minimize(x);
  //opt.getProps().put<bool>("printFDError",true);
  opt.debugFD(1E-8f,false,true,true);
  INFOV("Energy=%f (Err from Random: %f), Total=%f, Norm=%f, using %s!",fx,(fx2-fx),x.sum(),x.squaredNorm(),type.c_str())
  ostringstream ss;
  ss << "diffuse_" << (cons ? "cons_" : "") << (consTotal ? "total_" : "") << (consNorm ? "norm_" : "") << type << (covariant ? "_cov" : "");
  if(SL1 > 0)
    ss << "_L" << (int)(SL1*1000);
  ss << ".vtk";
  drawFunc(x,ss.str());
  return x;
}
void OptimizerTest::runExample1()
{
  INFO("Unconstrained")
  runExample1(128,false,false,false,false,"LBFGS");
  runExample1(128,false,false,false,false,"BLEIC");
  runExample1(128,false,false,false,false,"AUGLAG");
  runExample1(128,false,false,false,false,"LM");
  runExample1(128,false,false,false,false,"ELM");

  INFO("Box Constraint")
  runExample1(128,true,false,false,false,"BLEIC");
  runExample1(128,true,false,false,false,"AUGLAG");
  runExample1(128,true,false,false,false,"LM");
  runExample1(128,true,false,false,false,"ELM");

  INFO("Linear Constraint")
  runExample1(128,true,true,false,false,"BLEIC");
  runExample1(128,true,true,false,false,"AUGLAG");
  runExample1(128,true,true,false,false,"ELM");

  INFO("Nonlinear Constraint")
  runExample1(128,true,false,true,false,"AUGLAG");

  INFO("L1")
  runExample1(128,true,true,false,100.0f,"BLEIC");  //usual solver cannot handle these lasso-regressed problems
  for(T SL1=0.001f; SL1<=0.03f; SL1+=0.001f) {
    //runExample1(128,true,false,false,SL1,"LM");
    runExample1(128,true,false,false,SL1,"ELM");
  }
  runExample1(128,true,true,false,100.0f,"ELM");
}
void OptimizerTest::drawFunc(const Vec& x,const string& name)
{
  VTKWriter<T> wrt("diffuse",name,true);
  vector<Vec,Eigen::aligned_allocator<Vec> > vss;
  for(sizeType i=0; i<x.size(); i++)
    vss.push_back(Eigen::Matrix<T,3,1>((T)i/(T)x.size(),x[i],0));
  wrt.appendPoints(vss.begin(),vss.end());
  wrt.appendCells(VTKWriter<T>::IteratorIndex<Vec3i>(0,0,1),
                  VTKWriter<T>::IteratorIndex<Vec3i>(vss.size()-1,0,1),
                  VTKWriter<T>::LINE);
}
//example2: 2D cloth with constraint
#define ID(X,Y) (Y)*_N+(X)
#define IDV(X,Y) ((Y)*_N+(X))*3
class SpringTerm : public SparseTerm
{
public:
  SpringTerm(const Optimizer& opt,sizeType N,T K):SparseTerm(opt),_N(N),_K(K) {}
  virtual sizeType values() const {
    return _N*(_N-1)*2;
  }
protected:
  virtual Vec valGradInner(const Vec& x,SMatd& grad) {
    grad.clear();
    Vec ret=Vec::Zero(values());
    sizeType off=0;
    for(sizeType i=0; i<_N; i++)
      for(sizeType j=0; j<_N; j++) {
        if(i>0) {
          ret[off]=(x.segment<3>(IDV(i,j))-x.segment<3>(IDV(i-1,j))).squaredNorm()-1;
          addBlk(grad,off,IDV(i,j),2*(x.segment<3>(IDV(i,j))-x.segment<3>(IDV(i-1,j))).transpose()*_K);
          addBlk(grad,off,IDV(i-1,j),2*(x.segment<3>(IDV(i-1,j))-x.segment<3>(IDV(i,j))).transpose()*_K);
          off++;
        }
        if(j>0) {
          ret[off]=(x.segment<3>(IDV(i,j))-x.segment<3>(IDV(i,j-1))).squaredNorm()-1;
          addBlk(grad,off,IDV(i,j),2*(x.segment<3>(IDV(i,j))-x.segment<3>(IDV(i,j-1))).transpose()*_K);
          addBlk(grad,off,IDV(i,j-1),2*(x.segment<3>(IDV(i,j-1))-x.segment<3>(IDV(i,j))).transpose()*_K);
          off++;
        }
      }
    return ret*_K;
  }
  sizeType _N;
  T _K;
};
class GravityTerm : public SparseTerm
{
public:
  GravityTerm(const Optimizer& opt,sizeType N,T g):SparseTerm(opt),_N(N),_g(g) {}
  virtual sizeType values() const {
    return _N*_N;
  }
protected:
  virtual Vec valGradInner(const Vec& x,SMatd& grad) {
    grad.clear();
    T BIG_NUMBER=1E3f;
    Vec ret=Vec::Zero(values());
    for(sizeType i=0; i<_N; i++)
      for(sizeType j=0; j<_N; j++) {
        ret[ID(i,j)]=sqrt(BIG_NUMBER-x.segment<3>(IDV(i,j))[2]*_g);
        grad.push_back(Eigen::Triplet<T,sizeType>(ID(i,j),IDV(i,j)+2,-_g/ret[ID(i,j)]/2));
      }
    return ret;
  }
  sizeType _N;
  T _g;
};
class FixPointTerm : public SparseEQConstraint
{
public:
  FixPointTerm(const Optimizer& opt,sizeType N,T strength)
    :SparseEQConstraint(opt),_N(N),_strength(strength) {}
  virtual sizeType values() const {
    return 6;
  }
protected:
  virtual Vec valGradInner(const Vec& x,SMatd& grad) {
    grad.clear();
    addBlk(grad,0,IDV(0,0),Mat3d::Identity().cast<T>()*_strength);
    addBlk(grad,3,IDV(_N-1,0),Mat3d::Identity().cast<T>()*_strength);

    Vec ret=Vec::Zero(6);
    ret.segment<3>(0)=(x.segment<3>(IDV(0,0))-Vec3d(0,0,0).cast<T>())*_strength;
    ret.segment<3>(3)=(x.segment<3>(IDV(_N-1,0))-Vec3d(_N-1,0,0).cast<T>())*_strength;
    return ret;
  }
  sizeType _N;
  T _strength;
};
class TwoPlaneTerm : public SparseGEQConstraint
{
public:
  TwoPlaneTerm(const Optimizer& opt,sizeType N)
    :SparseGEQConstraint(opt),_N(N) {}
  virtual sizeType values() const {
    return 2;
  }
protected:
  virtual Vec valGradInner(const Vec& x,SMatd& grad) {
    grad.clear();
    addBlk(grad,0,IDV(_N-1,_N-1),Vec3d(1,0,1).transpose().cast<T>());
    addBlk(grad,1,IDV(0,_N-1),Vec3d(-1,0,1).transpose().cast<T>());

    Vec3d x0=Vec3d(7.5f,0,-2.0f);
    Vec ret=Vec::Zero(2);
    ret[0]=(x.segment<3>(IDV(_N-1,_N-1))-x0.cast<T>()).dot(Vec3d(1,0,1).cast<T>());
    ret[1]=(x.segment<3>(IDV(0,_N-1))-x0.cast<T>()).dot(Vec3d(-1,0,1).cast<T>());
    return ret;
  }
  sizeType _N;
};
class InBallTerm : public SparseGEQConstraint
{
public:
  InBallTerm(const Optimizer& opt,sizeType N):SparseGEQConstraint(opt),_N(N) {}
  virtual sizeType values() const {
    return 2;
  }
  bool isLinear() const {
    return false;
  }
protected:
  virtual Vec valGradInner(const Vec& x,SMatd& grad) {
    grad.clear();
    addBlk(grad,0,IDV(_N-1,_N-1),-2*(x.segment<3>(IDV(_N-1,_N-1))-Vec3d(_N-1,_N-1,0).cast<T>()).transpose());
    addBlk(grad,1,IDV(0,_N-1),-2*(x.segment<3>(IDV(0,_N-1))-Vec3d(0,_N-1,0).cast<T>()).transpose());

    Vec ret=Vec::Zero(2);
    ret[0]=0.1f-(x.segment<3>(IDV(_N-1,_N-1))-Vec3d(_N-1,_N-1,0).cast<T>()).squaredNorm();
    ret[1]=0.1f-(x.segment<3>(IDV(0,_N-1))-Vec3d(0,_N-1,0).cast<T>()).squaredNorm();
    return ret;
  }
  sizeType _N;
};
void OptimizerTest::runExample2(sizeType N,bool cons,bool consTwoPlane,bool consBall,const string& type)
{
  Optimizer opt;
  opt.getProps().put<int>("inputs",N*N*3);
  opt.getProps().put<string>("type",type);
  opt.addTerm(boost::shared_ptr<SpringTerm>(new SpringTerm(opt,N,100.0f)));
  opt.addTerm(boost::shared_ptr<GravityTerm>(new GravityTerm(opt,N,-9.81f)));
  boost::shared_ptr<FixPointTerm> fixPt(new FixPointTerm(opt,N,10.0f));
  if(cons) {  //add soft constraint
    fixPt->_asEnergy=false;
    opt.addConstraint(fixPt);
  } else {  //add box hard constraint
    fixPt->_asEnergy=true;
    opt.addTerm(fixPt);
  }
  if(consTwoPlane) //constraint above two plane points
    opt.addConstraint(boost::shared_ptr<TwoPlaneTerm>(new TwoPlaneTerm(opt,N)));
  if(consBall)  //constraint for points in a ball
    opt.addConstraint(boost::shared_ptr<InBallTerm>(new InBallTerm(opt,N)));
#undef ID
#undef IDV
#define ID(X,Y) (Y)*N+(X)
#define IDV(X,Y) ((Y)*N+(X))*3
  Vec x=Vec::Zero(N*N*3);
  for(sizeType i=0; i<N; i++)
    for(sizeType j=0; j<N; j++)
      x.segment<3>(IDV(i,j))=Vec3d(i,j,0).cast<T>();
  opt.getProps().put<bool>("debugOutputFunc",false);
  opt.getProps().put<sizeType>("nrIter",100000);
  opt.minimize(x);
  opt.getProps().put<bool>("printFDError",false);
  opt.debugFD(1E-8f,false,true,true);

  //output
  ostringstream ss;
  ss << "cloth_"  << (cons ? "cons_" : "") << (consTwoPlane ? "twoPlane_" : "") << (consBall ? "ball_" : "") << type << ".vtk";
  VTKWriter<T> wrt("cloth",ss.str(),true);
  vector<Vec,Eigen::aligned_allocator<Vec> > vss;
  vector<Vec2i,Eigen::aligned_allocator<Vec2i> > iss;
  for(sizeType i=0; i<N; i++)
    for(sizeType j=0; j<N; j++) {
      if(i>0)iss.push_back(Vec2i(ID(i,j),ID(i-1,j)));
      if(j>0)iss.push_back(Vec2i(ID(i,j),ID(i,j-1)));
      vss.push_back(x.segment<3>(IDV(i,j)));
    }
  wrt.appendPoints(vss.begin(),vss.end());
  wrt.appendCells(iss.begin(),iss.end(),VTKWriter<T>::LINE);
}
#undef ID
#undef IDV
void OptimizerTest::runExample2()
{
  INFO("Unconstrained")
  runExample2(16,false,false,false,"LBFGS");
  runExample2(16,false,false,false,"BLEIC");
  runExample2(16,false,false,false,"AUGLAG");
  //These two guys doesn't work
  //runExample2(16,false,false,false,"LM");
  //runExample2(16,false,false,false,"ELM");

  INFO("Box Constraint")
  runExample2(16,true,false,false,"BLEIC");
  runExample2(16,true,false,false,"AUGLAG");
  runExample2(16,true,false,false,"ELM");

  INFO("Linear Constraint")
  runExample2(16,true,true,false,"BLEIC");
  runExample2(16,true,true,false,"AUGLAG");
  runExample2(16,true,true,false,"ELM");

  INFO("Nonlinear Constraint")
  //runExample2(16,true,false,true,"BLEIC");
  runExample2(16,true,false,true,"AUGLAG");
}
//example3: test the set of QP solvers
void OptimizerTest::runExample3(sizeType N,bool BC,bool LC,const string& type)
{
  //QP Prob
  Vec b=Vec::Zero(N);
  Mat A=Mat::Zero(N,N);
  for(sizeType i=0; i<N; i++) {
    A(i,i)=2;
    if(i > 0)
      A(i,i-1)=-1;
    if(i < N-1)
      A(i,i+1)=-1;
  }
  //bound constraint
  Vec lb,ub;
  if(BC) {
    lb=Vec::Constant(N,-ScalarUtil<T>::scalar_max);
    ub=Vec::Constant(N,ScalarUtil<T>::scalar_max);
    lb[0]=0;
    ub[0]=0.00001f;
    lb[N-1]=1;
    ub[N-1]=1.00001f;
  } else {
    A(0,0)+=100;
    A(N-1,N-1)+=100;
    b[0]=0;
    b[N-1]=-100;
  }
  //linear constraints
  Mat C=Mat::Zero(0,0);
  Vec d=Vec::Zero(0);
  Coli ctype=Coli::Zero(0);
  if(LC) {
    C.setZero(3,N);
    d.resize(3);
    ctype.resize(3);

    C(0,N/3)=1;
    d[0]=1.5f;
    ctype[0]=1;

    C.row(1).setOnes();
    d[1]=70;
    ctype[1]=0;

    C(2,N*2/3)=1;
    d[2]=-1.5f;
    ctype[2]=-1;
  }
  //setup solver
  boost::property_tree::ptree pt;
  QPSolver sol(pt);
  pt.put<string>("type",type);
  //covert to sparse matrix
  JacobianType As;
  As.resize(A.rows(),A.cols());
  for(sizeType r=0; r<A.rows(); r++)
    for(sizeType c=0; c<A.cols(); c++)
      As.coeffRef(r,c)=A(r,c);
  //solve QP
  Vec x=Vec::Zero(N);
  ASSERT(sol.solveQP(x,As,b,lb,ub,C,d,ctype));
  INFOV("Energy: %f, using %s",x.dot(A*x)/2+b.dot(x),type.c_str())
  ostringstream ss;
  ss << "QP_" << (BC ? "bounds_" : "") << (LC ? "linear_" : "") << type << ".vtk";
  drawFunc(x,ss.str());
}
void OptimizerTest::runExample3()
{
  INFO("No Constraints")
  runExample3(100,false,false,"MPRGP");
  runExample3(100,false,false,"BLEIC");
  runExample3(100,false,false,"OOQP");

  INFO("Box Constraints")
  runExample3(100,true,false,"MPRGP");
  runExample3(100,true,false,"BLEIC");
  runExample3(100,true,false,"OOQP");

  INFO("General")
  runExample3(100,true,true,"BLEIC");
  runExample3(100,true,true,"OOQP");
}
//example4: stochastic gradient descendent
void OptimizerTest::runExample4()
{
  INFO("No Covariant Diff, Energy Formulation")
  runExample1(128,false,false,false,false,"SGD",false);
  sleep(10);
  INFO("No Covariant Diff, Constraint Formulation")
  runExample1(128,true,false,false,false,"SGD",false);
  sleep(10);
  INFO("Covariant Diff, Constraint Formulation")
  runExample1(128,true,false,false,false,"SGD",false,true);
  sleep(10);
}
