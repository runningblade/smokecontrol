#ifndef CALLBACK_H
#define CALLBACK_H

#include "../MathBasic.h"

PRJ_BEGIN

//============================================================================
// Iteration callback
template <typename T,typename KERNEL_TYPE>
struct Callback {
public:
  typedef typename KERNEL_TYPE::Vec Vec;
  typedef typename KERNEL_TYPE::Mat Mat;
  virtual ~Callback() {}
  virtual void reset() {
    //INFOV("Start")
  }
  virtual sizeType operator()(const Vec& x,const T& residueNorm,const sizeType& n) {
    INFOV("ITER %d: r=%f",(int)n,convert<double>()(residueNorm))
    return 0;
  }
  virtual sizeType operator()(const Vec& x,const Vec& g,const T& fx,const T& xnorm,const T& gnorm,const T& step,const sizeType& k,const sizeType& ls) {
    INFOV("ITER %d: f=%f, x=%f, g=%f, s=%f",(int)k,convert<double>()(fx),convert<double>()(xnorm),convert<double>()(gnorm),convert<double>()(step))
    return 0;
  }
  virtual sizeType operator()(sizeType iter,const Vec& vals,const Mat& points) {
    INFOV("ITER %d: minF=%f, maxF=%f, populationSize=%d",(int)iter,convert<double>()(vals.minCoeff()),convert<double>()(vals.maxCoeff()),(int)points.cols())
    return 0;
  }
};

template <typename T,typename KERNEL_TYPE>
struct NoCallback : public Callback<T,KERNEL_TYPE> {
public:
  typedef typename KERNEL_TYPE::Vec Vec;
  virtual ~NoCallback() {}
  virtual void reset() {}
  virtual sizeType operator()(const Vec& x,const T& residueNorm,const sizeType& n) {
    return 0;
  }
  virtual sizeType operator()(const Vec& x,const Vec& g,const T& fx,const T& xnorm,const T& gnorm,const T& step,const sizeType& k,const sizeType& ls) {
    return 0;
  }
};

template <typename T,typename KERNEL_TYPE>
struct CallbackCounter : public Callback<T,KERNEL_TYPE> {
public:
  typedef typename Callback<T,KERNEL_TYPE>::Vec Vec;
  CallbackCounter():_n(0) {}
  virtual void reset() {
    INFOV("Start %d",_n++)
  }
  virtual sizeType operator()(const Vec& x,const T& residueNorm,const sizeType& n) {
    return 0;
  }
  virtual sizeType operator()(const Vec& x,const Vec& g,const T& fx,const T& xnorm,const T& gnorm,const T& step,const sizeType& k,const sizeType& ls) {
    return 0;
  }
  sizeType _n;
};

template <typename T,typename KERNEL_TYPE>
struct ConvergencyCounter : public Callback<T,KERNEL_TYPE> {
public:
  struct Conv {
    T _time;
    T _residual;
    sizeType _it;
    Conv(){}
    Conv(T time,T residual,sizeType it)
      :_time(time),_residual(residual),_it(it){}
  };
  typedef typename Callback<T,KERNEL_TYPE>::Vec Vec;
  ConvergencyCounter(const std::string& path):_os(path.c_str(),std::ios::out) {
    reset();
  }
  ~ConvergencyCounter() {
    __t.stop();
    for(sizeType i=0; i<(sizeType)_conv.size(); i++)
      _os << i+1 << " " << _conv[i]._time << " " << _conv[i]._residual << std::endl;
  }
  virtual void reset() {
    __t.start();
    _ind=0;
  }
  virtual sizeType operator()(const Vec& x,const T& residueNorm,const sizeType& n) {
    if(_ind == 0) {
      _initRes=residueNorm;
    } else {
      T elapsed=(T)__t.elapsed().wall/1000000000.0L;
      if((sizeType)_conv.size() < _ind)
        _conv.push_back(Conv(elapsed,residueNorm/_initRes,1));
      else {
        _conv[_ind-1]._time=elapsed;
        _conv[_ind-1]._residual+=residueNorm/_initRes;
        _conv[_ind-1]._it+=1;
      }
    }
    _ind++;
    return 0;
  }
  virtual sizeType operator()(const Vec& x,const Vec& g,const T& fx,const T& xnorm,const T& gnorm,const T& step,const sizeType& k,const sizeType& ls) {
    Callback<T,KERNEL_TYPE>::operator()(x,g,fx,xnorm,gnorm,step,k,ls);
    return operator()(x,gnorm,k);
  }
protected:
  std::ofstream _os;
  boost::timer::cpu_timer __t;
  vector<Conv> _conv;
  sizeType _ind;
  T _initRes;
};

PRJ_END

#endif
