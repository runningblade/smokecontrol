#ifndef BVH_NODE_H
#define BVH_NODE_H

#include "../MathBasic.h"

PRJ_BEGIN

template <typename T,typename BBOX=BBox<scalar> >
struct Node : public Serializable {
  Node():Serializable(typeid(Node<T>).name()),_l(-1),_r(-1),_parent(-1),_nrCell(-1) {}
  bool read(std::istream& is,IOData* dat) {
    readBinaryData(_bb,is);
    readBinaryData(_cell,is,dat);
    readBinaryData(_l,is);
    readBinaryData(_r,is);
    readBinaryData(_parent,is);
    readBinaryData(_nrCell,is);
    return is.good();
  }
  bool write(std::ostream& os,IOData* dat) const {
    writeBinaryData(_bb,os);
    writeBinaryData(_cell,os,dat);
    writeBinaryData(_l,os);
    writeBinaryData(_r,os);
    writeBinaryData(_parent,os);
    writeBinaryData(_nrCell,os);
    return os.good();
  }
  boost::shared_ptr<Serializable> copy() const {
    return boost::shared_ptr<Serializable>(new Node<T,BBOX>);
  }
  BBOX _bb;
  T _cell;
  sizeType _l,_r,_parent,_nrCell;
};

PRJ_END

#endif
