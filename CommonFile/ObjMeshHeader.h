#ifdef OBJMESH

class OBJMESH
{
public:
  typedef SCALAR_NAME T;
  typedef Eigen::Matrix<T,2,1> PT2;
  typedef Eigen::Matrix<T,3,1> PT3;
  typedef Eigen::Matrix<T,4,1> PT4;
  typedef Eigen::Matrix<T,3,3> MAT3;
  typedef Eigen::Matrix<double,2,1> PT2D;
  typedef Eigen::Matrix<double,3,1> PT3D;
  struct Edge {
    //subdivision data
    //location of this edge in the
    //original mesh:
    //case 1: _subdId.second == -1
    //	if this edge is inside a triangle,
    //	in this case _subdId.first is the
    //	offset in the original _iss list
    //case 2: _subdId.second != -1
    //	if this edge is a subedge of the
    //	original mesh in this case _subdId
    //	store the edge-end vertex offset in original vss
    std::pair<int,int> _subdId;
    //offset in the subdivided _vss of
    //the vertex created for that edge
    int _interNode;
    //info data
    PT3 _nor;
    vector<int> _tris;
    Edge();
  };
  struct EdgeMap {
    friend class OBJMESH;
    struct LSS {
      bool operator()(const pair<int,int>& a,const pair<int,int>& b) const;
    };
    map<pair<int,int>,Edge,LSS> _ess;	//edge
  };
  struct PovTexture {
    virtual int nr() const;
    virtual int operator[](int fid) const;
    virtual std::string operator()(int tid) const;
    virtual PT2 uv(int vid) const;
    virtual bool hasUV() const;
  };
  OBJMESH();
  OBJMESH(const int& id);
  void setDim(int dim);
  virtual ~OBJMESH();
  template <typename T2>
  void cast(typename ObjMeshTraits<T2>::Type& other) const {
    other.getI()=getI();
    other.getV().resize(_vss.size());
    for(sizeType i=0; i<(sizeType)_vss.size(); i++)
      other.getV()[i]=_vss[i].cast<T2>();
    other.smooth();
  }
  bool read(istream &is,
            bool move=true,
            bool scale=true,
            int assertFaceType=-1,
            int assertTriangle=-1,
            bool doCheckCCW=true);
  bool readBinary(istream& is);
  bool write(ostream &os) const;
  bool write(const boost::filesystem::path& path) const;
  bool writePov(const boost::filesystem::path& path,bool normal=false,const PovTexture* tex=NULL) const;
  bool writePov(ostream &os,bool normal=false,const PovTexture* tex=NULL) const;
  bool writePBRT(ostream &os) const;
  bool writeVTK(const boost::filesystem::path& path,bool binary,bool normal=false,bool vertexNormal=false,const PT4* color=NULL) const;
  bool writeVTK(VTKWriter<T>& os,bool normal=false,bool vertexNormal=false,const PT4* color=NULL) const;
  bool writeVTK3D(VTKWriter<T>& os,bool normal=false,bool vertexNormal=false,const PT4* color=NULL) const;
  bool writeVTK2D(VTKWriter<T>& os,bool normal=false,bool vertexNormal=false,const PT4* color=NULL) const;
  bool writeBinary(ostream &os) const;
  bool write(ostream &vs,ostream &fs,int &index) const;
  void writeCsv(const boost::filesystem::path& path) const;
  void addMesh(const OBJMESH& mesh,const std::string& g);
  PT3 getTC(int i) const;
  const PT3& getV(int i) const;
  const Vec3i& getI(int i) const;
  const int& getIG(int i) const;
  const PT3& getN(int i) const;
  const PT3& getTN(int i) const;
  map<string,int>& getGS();
  map<int,string>& getIGS();
  PT3& getV(int i);
  Vec3i& getI(int i);
  int& getIG(int i);
  PT3& getN(int i);
  PT3& getTN(int i);
  T getArea(int i) const;
  const vector<PT3,Eigen::aligned_allocator<PT3> >& getV() const;
  const vector<Vec3i,Eigen::aligned_allocator<Vec3i> >& getI() const;
  const vector<int>& getIG() const;
  const vector<PT3,Eigen::aligned_allocator<PT3> >& getN() const;
  const vector<PT3,Eigen::aligned_allocator<PT3> >& getFN() const;
  const vector<PT3,Eigen::aligned_allocator<PT3> >& getTN() const;
  const map<string,int>& getGS() const;
  const map<int,string>& getIGS() const;
  string getTG(int i) const;
  int getGId(const string& name) const;
  vector<PT3,Eigen::aligned_allocator<PT3> >& getV();
  vector<Vec3i,Eigen::aligned_allocator<Vec3i> >& getI();
  vector<int>& getIG();
  vector<PT3,Eigen::aligned_allocator<PT3> >& getN();
  vector<PT3,Eigen::aligned_allocator<PT3> >& getFN();
  vector<PT3,Eigen::aligned_allocator<PT3> >& getTN();
  const MAT3& getT() const;
  MAT3& getT();
  const PT3& getPos() const;
  PT3& getPos();
  const T& getScale() const;
  T& getScale();
  void applyTrans();
  void applyTrans(const PT3& customCtr);
  BBox<T> getBB() const;
  const int& getId() const;
  //physics properties
  T getVolume() const;
  PT3 getCentroid() const;
  PT3 getVolumeCentroid() const;
  PT3& centroidOffset();
  const PT3& centroidOffset() const;
  //simple utility
  T getMass(const T& dens) const;
  void subdivide(const int& nrIter,vector<std::pair<int,int> >* vssInfo=NULL);
  void subdivideSingle3D(EdgeMap& eMap,int mod,vector<std::pair<int,int> >* vssInfo=NULL) ;
  void subdivideSingle2D();
  void marchingCube(const std::vector<T>& MCVal,OBJMESH& mesh);
  //smooth
  void makeUnique();
  void makeUniform();
  void makeUniform(int i,int j,int v0,int v1);
  void smooth();
  void insideOut();
  //topology
  const Edge& getE(int a,int b,const EdgeMap& eMap) const;
  void buildEdge(EdgeMap& eMap) const;
  void buildKRingV(vector<map<int,int> >& KRing,int r) const;
  void buildKRing(vector<map<int,int> >& KRing,int r) const;
  void findInsertV(map<int,int>& Ring,const int& v,int currR) const;
  //dimension
  const int& getDim() const;
  int& getDim();
protected:
  void addEdge(int a,int b,int tri,EdgeMap& eMap) const;
  void checkCCW(const PT3& v1,const PT3& n1,
                const PT3& v2,const PT3& n2,
                const PT3& v3,const PT3& n3,Vec3i& i);
  T estimateNormalLength() const;
protected:
  vector<PT3,Eigen::aligned_allocator<PT3> > _vss;	//vert
  vector<PT3,Eigen::aligned_allocator<PT3> > _nss;	//normal
  vector<PT3,Eigen::aligned_allocator<PT3> > _fnss;	//tree normals per face
  vector<PT3,Eigen::aligned_allocator<PT3> > _tnss;	//tri_normal
  vector<Vec3i,Eigen::aligned_allocator<Vec3i> > _iss;//index
  vector<int> _issg;
  map<string,int> _gss;//groups
  map<int,string> _igss;//inverse groups
  MAT3 _trans;
  int _id;
  PT3 _pos;
  T _scale;
  PT3 _ctrOff;
  int _dim;
};
template <>
struct ObjMeshTraits<SCALAR_NAME>
{
  typedef OBJMESH Type;
};

#endif
