#ifndef MATH_BASIC_H
#define MATH_BASIC_H

#include "Config.h"
#include <stdlib.h>
#include <vector>
#include <cmath>
#include <omp.h>
#include <float.h>
#include <Eigen/Eigen>
#include <boost/timer/timer.hpp>
#include <boost/pool/pool_alloc.hpp>

//convert function for IO.h
namespace std
{
template <typename T>
struct convert {
  template <typename T2>
  FORCE_INLINE T operator()(T2 d) const {
    return (T)d;
  }
};
}
typedef double scalarD;
typedef float scalarF;

typedef Eigen::Matrix<scalarD,2,2> Mat2d;
typedef Eigen::Matrix<scalarD,3,3> Mat3d;
typedef Eigen::Matrix<scalarD,4,4> Mat4d;
typedef Eigen::Matrix<scalarD,6,6> Mat6d;
typedef Eigen::Quaternion<scalarD> Quatd;
typedef Eigen::Translation<scalarD,3> Transd;
typedef Eigen::Transform<scalarD,3,Eigen::Affine> Affined;

typedef Eigen::Matrix<scalarD,-1, 1> Cold;
typedef Eigen::Matrix<scalarD, 1,-1> Rowd;
typedef Eigen::Matrix<scalarD,-1,-1> Matd;

typedef Eigen::Matrix<scalarF,2,2> Mat2f;
typedef Eigen::Matrix<scalarF,3,3> Mat3f;
typedef Eigen::Matrix<scalarF,4,4> Mat4f;
typedef Eigen::Matrix<scalarF,6,6> Mat6f;
typedef Eigen::Quaternion<scalarF> Quatf;
typedef Eigen::Translation<scalarF,3> Transf;
typedef Eigen::Transform<scalarF,3,Eigen::Affine> Affinef;

typedef Eigen::Matrix<scalarF,-1, 1> Colf;
typedef Eigen::Matrix<scalarF, 1,-1> Rowf;
typedef Eigen::Matrix<scalarF,-1,-1> Matf;

typedef Eigen::Matrix<scalarD,6,1> Vec6d;
typedef Eigen::Matrix<scalarD,4,1> Vec4d;
typedef Eigen::Matrix<scalarD,3,1> Vec3d;
typedef Eigen::Matrix<scalarD,2,1> Vec2d;

typedef Eigen::Matrix<scalarF,6,1> Vec6f;
typedef Eigen::Matrix<scalarF,4,1> Vec4f;
typedef Eigen::Matrix<scalarF,3,1> Vec3f;
typedef Eigen::Matrix<scalarF,2,1> Vec2f;

typedef Eigen::Matrix<sizeType, 4, 1> Vec4i;
typedef Eigen::Matrix<sizeType, 3, 1> Vec3i;
typedef Eigen::Matrix<sizeType, 2, 1> Vec2i;

typedef Eigen::Matrix<sizeType,-1, 1> Coli;
typedef Eigen::Matrix<sizeType, 1,-1> Rowi;
typedef Eigen::Matrix<sizeType,-1,-1> Mati;

typedef Eigen::Matrix<unsigned char, 4, 1> Vec4uc;
typedef Eigen::Matrix<unsigned char, 3, 1> Vec3uc;
typedef Eigen::Matrix<unsigned char, 2, 1> Vec2uc;

typedef Eigen::Matrix<char, 4, 1> Vec4c;
typedef Eigen::Matrix<char, 3, 1> Vec3c;
typedef Eigen::Matrix<char, 2, 1> Vec2c;

#ifdef min
#undef min
#endif
#ifdef max
#undef max
#endif
#ifdef M_PI
#undef M_PI
#endif

PRJ_BEGIN

using namespace std;

#ifndef DOUBLE_PRECISION
#define SCALAR_MAX FLT_MAX
#define SCALAR_EPS FLT_EPSILON
#define M_PI 3.14159265358979323846f
#define EPS 1E-5f
typedef float scalar;
typedef Vec4f Vec4;
typedef Vec3f Vec3;
typedef Vec2f Vec2;
typedef Colf Vecx;
typedef Mat2f Mat2;
typedef Mat3f Mat3;
typedef Mat4f Mat4;
typedef Matf Matx;
typedef Quatf Quat;
typedef Transf Trans;
typedef Affinef Affine;
#else
#define SCALAR_MAX DBL_MAX
#define SCALAR_EPS DBL_EPSILON
#define M_PI scalarD(3.14159265358979323846)
#define EPS scalarD(1E-9)
typedef scalarD scalar;
typedef Vec4d Vec4;
typedef Vec3d Vec3;
typedef Vec2d Vec2;
typedef Cold Vecx;
typedef Mat2d Mat2;
typedef Mat3d Mat3;
typedef Mat4d Mat4;
typedef Matd Matx;
typedef Quatd Quat;
typedef Transd Trans;
typedef Affined Affine;
#endif

template <typename T>
struct ScalarUtil;
template <>
struct ScalarUtil<scalarF> {
  typedef Vec4f ScalarVec4;
  typedef Vec3f ScalarVec3;
  typedef Vec2f ScalarVec2;
  typedef Colf ScalarVecx;

  typedef Mat2f ScalarMat2;
  typedef Mat3f ScalarMat3;
  typedef Mat4f ScalarMat4;
  typedef Matf ScalarMatx;
  typedef Quatf ScalarQuat;

  static scalarF scalar_max;
  static scalarF scalar_eps;
  static scalarF scalar_inf;
};
template <>
struct ScalarUtil<scalarD> {
  typedef Vec4d ScalarVec4;
  typedef Vec3d ScalarVec3;
  typedef Vec2d ScalarVec2;
  typedef Cold ScalarVecx;

  typedef Mat2d ScalarMat2;
  typedef Mat3d ScalarMat3;
  typedef Mat4d ScalarMat4;
  typedef Matd ScalarMatx;
  typedef Quatd ScalarQuat;

  static scalarD scalar_max;
  static scalarD scalar_eps;
  static scalarD scalar_inf;
};
template <>
struct ScalarUtil<sizeType> {
  typedef Eigen::Matrix<sizeType,4,1> ScalarVec4;
  typedef Eigen::Matrix<sizeType,3,1> ScalarVec3;
  typedef Eigen::Matrix<sizeType,2,1> ScalarVec2;
  typedef Eigen::Matrix<sizeType,-1,1> ScalarVecx;

  typedef Eigen::Matrix<sizeType,2,2> ScalarMat2;
  typedef Eigen::Matrix<sizeType,3,3> ScalarMat3;
  typedef Eigen::Matrix<sizeType,4,4> ScalarMat4;
  typedef Eigen::Matrix<sizeType,-1,-1> ScalarMatx;

  static sizeType scalar_max;
  static sizeType scalar_eps;
};
template<typename T> sizeType getEigenStride()
{
  vector<T,Eigen::aligned_allocator<T> > tester(2);
  return (sizeType)(((char*)&(tester[1]))-((char*)&(tester[0])));
}

bool compL(const Vec3i& a,const Vec3i& b);
bool compLE(const Vec3i& a,const Vec3i& b);
bool compG(const Vec3i& a,const Vec3i& b);
bool compGE(const Vec3i& a,const Vec3i& b);
bool compL(const Vec2i& a,const Vec2i& b);
bool compLE(const Vec2i& a,const Vec2i& b);
bool compG(const Vec2i& a,const Vec2i& b);
bool compGE(const Vec2i& a,const Vec2i& b);

bool compL(const Vec3f& a,const Vec3f& b);
bool compLE(const Vec3f& a,const Vec3f& b);
bool compG(const Vec3f& a,const Vec3f& b);
bool compGE(const Vec3f& a,const Vec3f& b);
bool compL(const Vec2f& a,const Vec2f& b);
bool compLE(const Vec2f& a,const Vec2f& b);
bool compG(const Vec2f& a,const Vec2f& b);
bool compGE(const Vec2f& a,const Vec2f& b);

bool compL(const Vec3d& a,const Vec3d& b);
bool compLE(const Vec3d& a,const Vec3d& b);
bool compG(const Vec3d& a,const Vec3d& b);
bool compGE(const Vec3d& a,const Vec3d& b);
bool compL(const Vec2d& a,const Vec2d& b);
bool compLE(const Vec2d& a,const Vec2d& b);
bool compG(const Vec2d& a,const Vec2d& b);
bool compGE(const Vec2d& a,const Vec2d& b);

scalarF compMin(const scalarF& a,const scalarF& b);
scalarF compMax(const scalarF& a,const scalarF& b);
scalarD compMin(const scalarD& a,const scalarD& b);
scalarD compMax(const scalarD& a,const scalarD& b);

Vec2i compMin(const Vec2i& a,const Vec2i& b);
Vec3i compMin(const Vec3i& a,const Vec3i& b);
Vec4i compMin(const Vec4i& a,const Vec4i& b);

Vec2i compMax(const Vec2i& a,const Vec2i& b);
Vec3i compMax(const Vec3i& a,const Vec3i& b);
Vec4i compMax(const Vec4i& a,const Vec4i& b);

Vec2f compMin(const Vec2f& a,const Vec2f& b);
Vec3f compMin(const Vec3f& a,const Vec3f& b);
Vec4f compMin(const Vec4f& a,const Vec4f& b);

Vec2f compMax(const Vec2f& a,const Vec2f& b);
Vec3f compMax(const Vec3f& a,const Vec3f& b);
Vec4f compMax(const Vec4f& a,const Vec4f& b);

Vec2uc compMin(const Vec2uc& a,const Vec2uc& b);
Vec3uc compMin(const Vec3uc& a,const Vec3uc& b);
Vec4uc compMin(const Vec4uc& a,const Vec4uc& b);

Vec2uc compMax(const Vec2uc& a,const Vec2uc& b);
Vec3uc compMax(const Vec3uc& a,const Vec3uc& b);
Vec4uc compMax(const Vec4uc& a,const Vec4uc& b);

Vec2c compMin(const Vec2c& a,const Vec2c& b);
Vec3c compMin(const Vec3c& a,const Vec3c& b);
Vec4c compMin(const Vec4c& a,const Vec4c& b);

Vec2c compMax(const Vec2c& a,const Vec2c& b);
Vec3c compMax(const Vec3c& a,const Vec3c& b);
Vec4c compMax(const Vec4c& a,const Vec4c& b);

Vec2i ceilV(const Vec2f& vec);
Vec3i ceilV(const Vec3f& vec);
Vec4i ceilV(const Vec4f& vec);

Vec2i floorV(const Vec2f& vec);
Vec3i floorV(const Vec3f& vec);
Vec4i floorV(const Vec4f& vec);

Vec2i round(const Vec2f& vec);
Vec3i round(const Vec3f& vec);
Vec4i round(const Vec4f& vec);

Vec2d compMin(const Vec2d& a,const Vec2d& b);
Vec3d compMin(const Vec3d& a,const Vec3d& b);
Vec4d compMin(const Vec4d& a,const Vec4d& b);

Vec2d compMax(const Vec2d& a,const Vec2d& b);
Vec3d compMax(const Vec3d& a,const Vec3d& b);
Vec4d compMax(const Vec4d& a,const Vec4d& b);

Vec2i ceilV(const Vec2d& vec);
Vec3i ceilV(const Vec3d& vec);
Vec4i ceilV(const Vec4d& vec);

Vec2i floorV(const Vec2d& vec);
Vec3i floorV(const Vec3d& vec);
Vec4i floorV(const Vec4d& vec);

Vec2i round(const Vec2d& vec);
Vec3i round(const Vec3d& vec);
Vec4i round(const Vec4d& vec);

template <typename T,int DIM=3>
struct BBox {
  typedef Eigen::Matrix<T,DIM,1> PT;
  typedef Eigen::Matrix<T,2,1> PT2;
  BBox() {
    reset();
  }
  virtual ~BBox() {}
  BBox(const PT& p):_minC(p),_maxC(p) {}
  BBox(const PT& minC,const PT& maxC):_minC(minC),_maxC(maxC) {}
  template <typename T2>
  BBox(const BBox<T2,DIM>& other) {
    copy(other);
  }
  template <typename T2>
  BBox& operator=(const BBox<T2,DIM>& other) {
    copy(other);
    return *this;
  }
  static BBox createMM(const PT& minC,const PT& maxC) {
    return BBox(minC,maxC);
  }
  static BBox createME(const PT& minC,const PT& extent) {
    return BBox(minC,minC+extent);
  }
  static BBox createCE(const PT& center,const PT& extent) {
    return BBox(center-extent,center+extent);
  }
  BBox getIntersect(const BBox& other) const {
    return createMM(compMax(_minC,other._minC),compMin(_maxC,other._maxC));
  }
  BBox getUnion(const BBox& other) const {
    return createMM(compMin(_minC,other._minC),compMax(_maxC,other._maxC));
  }
  BBox getUnion(const PT& point) const {
    return createMM(compMin(_minC,point),compMax(_maxC,point));
  }
  BBox getUnion(const PT& ctr,const T& rad) const {
    return createMM(compMin(_minC,ctr-PT::Constant(rad)),compMax(_maxC,ctr+PT::Constant(rad)));
  }
  void setIntersect(const BBox& other) {
    _minC=compMax(_minC,other._minC);
    _maxC=compMin(_maxC,other._maxC);
  }
  void setUnion(const BBox& other) {
    _minC=compMin(_minC,other._minC);
    _maxC=compMax(_maxC,other._maxC);
  }
  void setUnion(const PT& point) {
    _minC=compMin(_minC,point);
    _maxC=compMax(_maxC,point);
  }
  void setUnion(const PT& ctr,const T& rad) {
    _minC=compMin(_minC,ctr-PT::Constant(rad));
    _maxC=compMax(_maxC,ctr+PT::Constant(rad));
  }
  void setPoints(const PT& a,const PT& b,const PT& c) {
    _minC=compMin(compMin(a,b),c);
    _maxC=compMax(compMax(a,b),c);
  }
  PT minCorner() const {
    return _minC;
  }
  PT maxCorner() const {
    return _maxC;
  }
  void enlargedEps(T eps) {
    PT d=(_maxC-_minC)*(eps*0.5f);
    _minC-=d;
    _maxC+=d;
  }
  BBox enlargeEps(T eps) const {
    PT d=(_maxC-_minC)*(eps*0.5f);
    return createMM(_minC-d,_maxC+d);
  }
  void enlarged(T len,const sizeType d=DIM) {
    for(sizeType i=0; i<d; i++) {
      _minC[i]-=len;
      _maxC[i]+=len;
    }
  }
  BBox enlarge(T len,const sizeType d=DIM) const {
    BBox ret=createMM(_minC,_maxC);
    ret.enlarged(len,d);
    return ret;
  }
  PT lerp(const PT& frac) const {
    return _maxC*frac-_minC*(frac-1.0f);
  }
  bool empty() const {
    return !compL(_minC,_maxC);
  }
  template <int DIM2>
  bool containDim(const PT& point) const {
    for(int i=0; i<DIM2; i++)
      if(_minC[i] > point[i] || _maxC[i] < point[i])
        return false;
    return true;
  }
  bool contain(const BBox& other,const sizeType d=DIM) const {
    for(int i=0; i<d; i++)
      if(_minC[i] > other._minC[i] || _maxC[i] < other._maxC[i])
        return false;
    return true;
  }
  bool contain(const PT& point,const sizeType d=DIM) const {
    for(int i=0; i<d; i++)
      if(_minC[i] > point[i] || _maxC[i] < point[i])
        return false;
    return true;
  }
  bool contain(const PT& point,const T& rad,const sizeType d=DIM) const {
    for(int i=0; i<d; i++)
      if(_minC[i]+rad > point[i] || _maxC[i]-rad < point[i])
        return false;
    return true;
  }
  void reset() {
    _minC=PT::Constant( ScalarUtil<T>::scalar_max);
    _maxC=PT::Constant(-ScalarUtil<T>::scalar_max);
  }
  PT getExtent() const {
    return _maxC-_minC;
  }
  T distTo(const BBox& other,const sizeType d=DIM) const {
    PT dist=PT::Zero();
    for(sizeType i=0; i<d; i++) {
      if (other._maxC[i] < _minC[i])
        dist[i] = other._maxC[i] - _minC[i];
      else if (other._minC[i] > _maxC[i])
        dist[i] = other._minC[i] - _maxC[i];
    }
    return dist.norm();
  }
  T distTo(const PT& pt,const sizeType d=DIM) const {
    return sqrt(distToSqr(pt,d));
  }
  T distToSqr(const PT& pt,const sizeType d=DIM) const {
    PT dist=PT::Zero();
    for(sizeType i=0; i<d; i++) {
      if (pt[i] < _minC[i])
        dist[i] = pt[i] - _minC[i];
      else if (pt[i] > _maxC[i])
        dist[i] = pt[i] - _maxC[i];
    }
    return dist.squaredNorm();
  }
  PT closestTo(const PT& pt,const sizeType d=DIM) const {
    PT dist(pt);
    for(sizeType i=0; i<d; i++) {
      if (pt[i] < _minC[i])
        dist[i] = _minC[i];
      else if (pt[i] > _maxC[i])
        dist[i] = _maxC[i];
    }
    return dist;
  }
  bool intersect(const PT& p,const PT& q,const sizeType d=DIM) const {
    const T lo=1-5*ScalarUtil<T>::scalar_eps;
    const T hi=1+5*ScalarUtil<T>::scalar_eps;

    T s=0, t=1;
    for(sizeType i=0; i<d; ++i) {
      if(p[i]<q[i]) {
        T d=q[i]-p[i];
        T s0=(_minC[i]-p[i])/d, t0=(_maxC[i]-p[i])/d;
        if(s0>s) s=s0;
        if(t0<t) t=t0;
      } else if(p[i]>q[i]) {
        T d=q[i]-p[i];
        T s0=lo*(_maxC[i]-p[i])/d, t0=hi*(_minC[i]-p[i])/d;
        if(s0>s) s=s0;
        if(t0<t) t=t0;
      } else {
        if(p[i]<_minC[i] || p[i]>_maxC[i])
          return false;
      }

      if(s>t)
        return false;
    }
    return true;
  }
  bool intersect(const PT& p,const PT& q,T& s,T& t,const sizeType d=DIM) const {
    const T lo=1-5*ScalarUtil<T>::scalar_eps;
    const T hi=1+5*ScalarUtil<T>::scalar_eps;

    s=0;
    t=1;
    for(sizeType i=0; i<d; ++i) {
      if(p[i]<q[i]) {
        T d=q[i]-p[i];
        T s0=lo*(_minC[i]-p[i])/d, t0=hi*(_maxC[i]-p[i])/d;
        if(s0>s) s=s0;
        if(t0<t) t=t0;
      } else if(p[i]>q[i]) {
        T d=q[i]-p[i];
        T s0=lo*(_maxC[i]-p[i])/d, t0=hi*(_minC[i]-p[i])/d;
        if(s0>s) s=s0;
        if(t0<t) t=t0;
      } else {
        if(p[i]<_minC[i] || p[i]>_maxC[i])
          return false;
      }

      if(s>t)
        return false;
    }
    return true;
  }
  bool intersect(const BBox& other,const sizeType& d=DIM) const {
    for(sizeType i=0; i<d; i++) {
      if(_maxC[i] < other._minC[i] || other._maxC[i] < _minC[i])
        return false;
    }
    return true;
    //return compLE(_minC,other._maxC) && compLE(other._minC,_maxC);
  }
  PT2 project(const PT& a,const sizeType d=DIM) const {
    PT ctr=(_minC+_maxC)*0.5f;
    T ctrD=a.dot(ctr);
    T delta=0.0f;
    ctr=_maxC-ctr;
    for(sizeType i=0; i<d; i++)
      delta+=abs(ctr[i]*a[i]);
    return PT2(ctrD-delta,ctrD+delta);
  }
  template<typename T2>
  BBox& copy(const BBox<T2,DIM>& other) {
    for(sizeType i=0; i<DIM; i++) {
      _minC[i]=convert<T>()(other._minC[i]);
      _maxC[i]=convert<T>()(other._maxC[i]);
    }
    return *this;
  }
  T perimeter(const sizeType d=DIM) const {
    PT ext=getExtent();
    if(d <= 2)
      return ext.sum()*2.0f;
    else {
      ASSERT(d == 3);
      return (ext[0]*ext[1]+ext[1]*ext[2]+ext[0]*ext[2])*2.0f;
    }
  }
  PT _minC;
  PT _maxC;
};
typedef BBox<scalarF> BBoxf;
typedef BBox<scalarD> BBoxd;

template <typename T> T mmin(const T& a,const T& b)
{
  return a < b ? a : b;
}
template <typename T> T mmax(const T& a,const T& b)
{
  return a > b ? a : b;
}

struct ErfCalc {
  static const sizeType ncof=28;
  static const scalarD cof[28];
  scalarD erf(scalarD x) const;
  scalarD erfc(scalarD x) const;
  scalarD erfccheb(scalarD z) const;
  scalarD inverfc(scalarD p) const;
  scalarD inverf(scalarD p) const;
};
scalarD erf(const scalarD& val);
class RandEngine
{
public:
  static void srand(sizeType i);
  //real
  static scalar randR01();
  static scalar randR(scalar l,scalar u);
  static scalar randSR(sizeType seed,scalar l,scalar u);
  //int
  static sizeType randSI(sizeType seed);
  static sizeType randI(sizeType l,sizeType u);
  static sizeType randSI(sizeType seed,sizeType l,sizeType u);
  //normal
  static scalar normal();
};
template <typename T>
typename ScalarUtil<T>::ScalarVec3 randBary()
{
  typename ScalarUtil<T>::ScalarVec3 ret;

  ret.x()=(T)RandEngine::randR(0,1.0f);
  ret.y()=(T)RandEngine::randR(0,1.0f);
  ret.y()*=(1.0f-ret.x());
  ret.z()=1.0f-ret.x()-ret.y();

  return ret;
}
char sgn(const scalarD& val);
char sgn(const scalarF& val);
bool isInf( const scalarD& x );
bool isInf( const scalarF& x );
bool isNan( const scalarD& x );
bool isNan( const scalarF& x );
sizeType makePOT(sizeType val,sizeType decimate);
sizeType mod(sizeType a,sizeType b,const scalar &bInv);
scalarD minmod( const scalarD& a,const scalarD& b,const scalarD& c);
scalarF minmod( const scalarF& a,const scalarF& b,const scalarF& c);

template<typename RET>
RET countBits(const unsigned char& v)
{
  static const unsigned char BitsSetTable256[256] = {
#   define B2(n) n,     n+1,     n+1,     n+2
#   define B4(n) B2(n), B2(n+1), B2(n+1), B2(n+2)
#   define B6(n) B4(n), B4(n+1), B4(n+1), B4(n+2)
    B6(0), B6(1), B6(1), B6(2)
  };
  return BitsSetTable256[v];
}
template<typename RET>
RET countBits(const sizeType& v)
{
  RET ret=0;
  unsigned char* vc=(unsigned char*)&v;
  for(sizeType i=0,sz=sizeof(sizeType); i<sz; i++)
    ret+=countBits<RET>(vc[i]);
  return ret;
}

Vec2i makePOT(const Vec2i& val,sizeType decimate);
Vec3i makePOT(const Vec3i& val,sizeType decimate);
Vec4i makePOT(const Vec4i& val,sizeType decimate);

Vec2i operator>>(const Vec2i& in,const sizeType& nr);
Vec3i operator>>(const Vec3i& in,const sizeType& nr);
Vec4i operator>>(const Vec4i& in,const sizeType& nr);
Vec2i operator&(const Vec2i& in,const sizeType& nr);
Vec3i operator&(const Vec3i& in,const sizeType& nr);
Vec4i operator&(const Vec4i& in,const sizeType& nr);

bool isInf( const Vec2f& x );
bool isNan( const Vec2f& x );
bool isInf( const Vec3f& x );
bool isNan( const Vec3f& x );
bool isInf( const Vec4f& x );
bool isNan( const Vec4f& x );
Vec2f minmodV(const Vec2f& a,const Vec2f& b,const Vec2f& c);
Vec3f minmodV(const Vec3f& a,const Vec3f& b,const Vec3f& c);
Vec4f minmodV(const Vec4f& a,const Vec4f& b,const Vec4f& c);

bool isInf( const Vec2d& x );
bool isNan( const Vec2d& x );
bool isInf( const Vec3d& x );
bool isNan( const Vec3d& x );
bool isInf( const Vec4d& x );
bool isNan( const Vec4d& x );
Vec2d minmodV(const Vec2d& a,const Vec2d& b,const Vec2d& c);
Vec3d minmodV(const Vec3d& a,const Vec3d& b,const Vec3d& c);
Vec4d minmodV(const Vec4d& a,const Vec4d& b,const Vec4d& c);

template<typename T>
T getAngle3D(const typename ScalarUtil<T>::ScalarVec3& a,const typename ScalarUtil<T>::ScalarVec3& b)
{
  T denom=std::max<T>(a.norm()*b.norm(),ScalarUtil<T>::scalar_eps);
  return acos(max<T>(min<T>(a.dot(b)/denom,(T)(1.0f-ScalarUtil<T>::scalar_eps)),(T)(-1.0f+ScalarUtil<T>::scalar_eps)));
}
template<typename T>
T getAngle2D(const typename ScalarUtil<T>::ScalarVec2& a,const typename ScalarUtil<T>::ScalarVec2& b)
{
  T denom=std::max<T>(a.norm()*b.norm(),ScalarUtil<T>::scalar_eps);
  return acos(max<T>(min<T>(a.dot(b)/denom,(T)(1.0f-ScalarUtil<T>::scalar_eps)),(T)(-1.0f+ScalarUtil<T>::scalar_eps)));
}

template<typename T>
typename ScalarUtil<T>::ScalarMat3 makeRotation(const typename ScalarUtil<T>::ScalarVec3& rotation)
{
  typename ScalarUtil<T>::ScalarMat3 ret;//=(typename ScalarUtil<T>::ScalarMat3)::Identity();

  typename ScalarUtil<T>::ScalarVec3 negRot=rotation*-1.0f;
  const T cr = cos( negRot.x() );
  const T sr = sin( negRot.x() );
  const T cp = cos( negRot.y() );
  const T sp = sin( negRot.y() );
  const T cy = cos( negRot.z() );
  const T sy = sin( negRot.z() );

  ret(0,0) = ( cp*cy );
  ret(0,1) = ( cp*sy );
  ret(0,2) = ( -sp );

  const T srsp = sr*sp;
  const T crsp = cr*sp;

  ret(1,0) = ( srsp*cy-cr*sy );
  ret(1,1) = ( srsp*sy+cr*cy );
  ret(1,2) = ( sr*cp );

  ret(2,0) = ( crsp*cy+sr*sy );
  ret(2,1) = ( crsp*sy-sr*cy );
  ret(2,2) = ( cr*cp );

  return ret;
}
template<typename T>
typename ScalarUtil<T>::ScalarQuat toAngleAxis(const typename ScalarUtil<T>::ScalarVec3& q)
{
  T angle=q.norm();
  if(angle < ScalarUtil<T>::scalar_eps)
    return ScalarUtil<T>::ScalarQuat::Identity();
  else return (typename ScalarUtil<T>::ScalarQuat)Eigen::AngleAxis<T>(angle,q/angle);
}
template<typename T>
typename ScalarUtil<T>::ScalarVec3 getRotation(const typename ScalarUtil<T>::ScalarMat3& m,bool wrap=true)
{
  const typename ScalarUtil<T>::ScalarVec3 scale(m.block(0,0,1,3).norm(),
      m.block(1,0,1,3).norm(),
      m.block(2,0,1,3).norm());
  const typename ScalarUtil<T>::ScalarVec3 invScale(1.0f/scale.x(),
      1.0f/scale.y(),
      1.0f/scale.z());

  T Y = -asin(m(0,2)*invScale.x());
  const T C = cos(Y);

  T rotx, roty, X, Z;

  if (abs(C) >= ScalarUtil<T>::scalar_eps) {
    const T invC = 1.0f/C;
    rotx = m(2,2) * invC * invScale.z();
    roty = m(1,2) * invC * invScale.y();
    X = atan2( roty, rotx ) ;
    rotx = m(0,0) * invC * invScale.x();
    roty = m(0,1) * invC * invScale.y();
    Z = atan2( roty, rotx ) ;
  } else {
    X = 0.0f;
    rotx = m(1,1) * invScale.y();
    roty = -m(1,0) * invScale.y();
    Z = atan2( roty, rotx ) ;
  }

  X*=-1.0f;
  Y*=-1.0f;
  Z*=-1.0f;

  if(wrap) {
    // fix values that get below zero
    // before it would set (!) values to 360
    // that were above 360:
    if (X < 0.0f) X += ((T)M_PI)*2.0f;
    if (Y < 0.0f) Y += ((T)M_PI)*2.0f;
    if (Z < 0.0f) Z += ((T)M_PI)*2.0f;
  }

  return typename ScalarUtil<T>::ScalarVec3(X,Y,Z);
}

template<typename T>
bool getProjectionMatrixFrustum(const typename ScalarUtil<T>::ScalarMat4& prj,
                                T& left,T& right,T& bottom,T& top,T& zNear,T& zFar)
{
  if (prj(3,0)!=0.0f || prj(3,1)!=0.0f || prj(3,2)!=-1.0f || prj(3,3)!=0.0f)
    return false;

  // note: near and far must be used inside this method instead of zNear and zFar
  // because zNear and zFar are references and they may point to the same variable.
  T temp_near = prj(2,3) / (prj(2,2)-1.0f);
  T temp_far = prj(2,3) / (1.0f+prj(2,2));

  left = temp_near * (prj(0,2)-1.0f) / prj(0,0);
  right = temp_near * (1.0f+prj(0,2)) / prj(0,0);

  top = temp_near * (1.0f+prj(1,2)) / prj(1,1);
  bottom = temp_near * (prj(1,2)-1.0f) / prj(1,1);

  zNear = temp_near;
  zFar = temp_far;

  return true;
}
template<typename T>
void getViewMatrixFrame(const typename ScalarUtil<T>::ScalarMat4& m,
                        typename ScalarUtil<T>::ScalarVec3& c,
                        typename ScalarUtil<T>::ScalarVec3& x,
                        typename ScalarUtil<T>::ScalarVec3& y,
                        typename ScalarUtil<T>::ScalarVec3& z)
{
  typename ScalarUtil<T>::ScalarMat4 mInv=m.inverse();

  typename ScalarUtil<T>::ScalarVec4 cH=mInv*typename ScalarUtil<T>::ScalarVec4(0.0f,0.0f,0.0f,1.0f);
  c=(typename ScalarUtil<T>::ScalarVec3(cH.x(),cH.y(),cH.z())*(1.0f/cH.w()));

  typename ScalarUtil<T>::ScalarVec4 xH=mInv*typename ScalarUtil<T>::ScalarVec4(1.0f,0.0f,0.0f,1.0f);
  x=(typename ScalarUtil<T>::ScalarVec3(xH.x(),xH.y(),xH.z())*(1.0f/xH.w())-c).normalized();

  typename ScalarUtil<T>::ScalarVec4 yH=mInv*typename ScalarUtil<T>::ScalarVec4(0.0f,1.0f,0.0f,1.0f);
  y=(typename ScalarUtil<T>::ScalarVec3(yH.x(),yH.y(),yH.z())*(1.0f/yH.w())-c).normalized();

  typename ScalarUtil<T>::ScalarVec4 zH=mInv*typename ScalarUtil<T>::ScalarVec4(0.0f,0.0f,-1.0f,1.0f);
  z=(typename ScalarUtil<T>::ScalarVec3(zH.x(),zH.y(),zH.z())*(1.0f/zH.w())-c).normalized();
}
template<typename T>
bool getViewMatrixFrame(const typename ScalarUtil<T>::ScalarMat4& m,
                        const typename ScalarUtil<T>::ScalarMat4& prj,
                        typename ScalarUtil<T>::ScalarVec3& c,
                        typename ScalarUtil<T>::ScalarVec3& x,
                        typename ScalarUtil<T>::ScalarVec3& y,
                        typename ScalarUtil<T>::ScalarVec3& z)
{
  T left,right,bottom,top,zNear,zFar;
  if(!getProjectionMatrixFrustum<T>(prj,left,right,bottom,top,zNear,zFar))
    return false;

  typename ScalarUtil<T>::ScalarMat4 mInv=m.inverse();
  typename ScalarUtil<T>::ScalarMat3 mRotInv=(m.block(0,0,3,3)).inverse();

  typename ScalarUtil<T>::ScalarVec4 cH=mInv*typename ScalarUtil<T>::ScalarVec4(0.0f,0.0f,0.0f,1.0f);
  c=(typename ScalarUtil<T>::ScalarVec3(cH.x(),cH.y(),cH.z())*(1.0f/cH.w()));

  x=mRotInv*(typename ScalarUtil<T>::ScalarVec3(1.0f,0.0f,0.0f));
  x=x.normalized()*right;

  y=mRotInv*(typename ScalarUtil<T>::ScalarVec3(0.0f,1.0f,0.0f));
  y=y.normalized()*top;

  z=mRotInv*(typename ScalarUtil<T>::ScalarVec3(0.0f,0.0f,-1.0f));
  z=z.normalized()*zNear;

  return true;
}
template<typename T>
typename ScalarUtil<T>::ScalarVec3
transformHomo(const typename ScalarUtil<T>::ScalarMat4& m,
              const typename ScalarUtil<T>::ScalarVec3& p)
{
  return m.block(0,0,3,3)*p+m.block(0,3,3,1);
}
template<typename T>
typename ScalarUtil<T>::ScalarVec3
transformHomoInv(const typename ScalarUtil<T>::ScalarMat4& m,
                 const typename ScalarUtil<T>::ScalarVec3& p)
{
  return m.block(0,0,3,3).inverse()*(p-m.block(0,3,3,1));
}
template<typename T>
typename ScalarUtil<T>::ScalarMat4
getOrtho2D(const typename ScalarUtil<T>::ScalarVec3& minC,
           const typename ScalarUtil<T>::ScalarVec3& maxC)
{
  typename ScalarUtil<T>::ScalarMat4 mt=typename ScalarUtil<T>::ScalarMat4::Zero();
  mt(0,0)=2.0f/(maxC.x()-minC.x());
  mt(0,3)=(minC.x()+maxC.x())/(minC.x()-maxC.x());

  mt(1,1)=2.0f/(maxC.y()-minC.y());
  mt(1,3)=(minC.y()+maxC.y())/(minC.y()-maxC.y());

  mt(2,2)=1.0f;
  mt(3,3)=1.0f;
  return mt;
}

#include "Interp.h"

#ifdef _MSC_VER
#define STRINGIFY_OMP_OMP(X) X
#define PRAGMA __pragma
#else
#define STRINGIFY_OMP(X) #X
#define PRAGMA _Pragma
#endif

#define OMP_PARALLEL_FOR_ PRAGMA(STRINGIFY_OMP(omp parallel for num_threads(OmpSettings::getOmpSettings().nrThreads()) schedule(static)))
#define OMP_PARALLEL_FOR_I(...) PRAGMA(STRINGIFY_OMP(omp parallel for num_threads(OmpSettings::getOmpSettings().nrThreads()) schedule(static) __VA_ARGS__))
#define OMP_PARALLEL_FOR_X(X) PRAGMA(STRINGIFY_OMP(omp parallel for num_threads(X) schedule(static)))
#define OMP_PARALLEL_FOR_XI(X,...) PRAGMA(STRINGIFY_OMP(omp parallel for num_threads(X) schedule(static) __VA_ARGS__))
#define OMP_ADD(...) reduction(+: __VA_ARGS__)
#define OMP_PRI(...) private(__VA_ARGS__)
#define OMP_FPRI(...) firstprivate(__VA_ARGS__)
#define OMP_ATOMIC_	PRAGMA(STRINGIFY_OMP(omp atomic))
#define OMP_ATOMIC_CAPTURE_	PRAGMA(STRINGIFY_OMP(omp atomic capture))
#define OMP_CRITICAL_ PRAGMA(STRINGIFY_OMP(omp critical))
#define OMP_FLUSH_(X) PRAGMA(STRINGIFY_OMP(omp flush(X)))
struct OmpSettings {
public:
  static const OmpSettings& getOmpSettings();
  static OmpSettings& getOmpSettingsNonConst();
  int nrThreads() const;
  void setNrThreads(int nr);
private:
  OmpSettings();
  int _nrThreads;
  static OmpSettings _ompSettings;
};

#define TIME_PERFORMANCE
#ifndef TIME_PERFORMANCE
#define TBEG(STR) {
#define TEND }
#define TENDC }

#define TBEGT {
#define TENDT(STR,tree) }
#else
#define TBEG(STR) {std::string __name=STR;boost::timer::cpu_timer __t;__t.start();
#define TEND __t.stop();std::cout << __name << ": " << __t.format() << std::endl;}
#define TENDC(COND) __t.stop();if(COND)std::cout << __name << ": " << __t.format() << std::endl;}

#define TBEGT {boost::timer::cpu_timer __t;__t.start();
#define TENDT(STR,tree) __t.stop();tree.put<scalarD>(STR,__t.elapsed().wall/1000000000.0L);}
#endif

PRJ_END

#endif
