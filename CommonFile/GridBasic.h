#ifndef GRID_BASIC_H
#define GRID_BASIC_H

#include "Config.h"
#include "MathBasic.h"
#include "IO.h"

PRJ_BEGIN

template <typename T,typename TI,typename TG=vector<T,Eigen::aligned_allocator<T> > >
struct Grid : public HasMagic, public Serializable {
  friend class DataStructureCL;
public:
  typedef T value_type;
  typedef typename Eigen::Matrix<T,3,1> ValueType;
  typedef typename Eigen::Matrix<TI,3,1> IndexType;
  typedef typename Eigen::Matrix<T,3,3> MatrixType;
  Grid():HasMagic(0xFCFCCFCFFCFCCFCF),Serializable(typeid(Grid).name()),_szPoint(0,0,0) {}
  virtual ~Grid() {}
  virtual boost::shared_ptr<Serializable> copy() const {
    return boost::shared_ptr<Serializable>(new Grid());
  }
  virtual bool write(ostream &os,IOData* dat) const {
    if(!HasMagic::writeMagic(os))
      return false;

    if(!writeBinaryData(_off,os).good())
      return false;
    if(!writeBinaryData(_szCell,os).good())
      return false;
    if(!writeBinaryData(_invSzCell,os).good())
      return false;
    if(!writeBinaryData(_szPoint,os).good())
      return false;
    if(!writeBinaryData(_bb,os).good())
      return false;
    if(!writeBinaryData(_szPointAligned,os).good())
      return false;
    if(!writeBinaryData(_stride,os).good())
      return false;
    if(!writeBinaryData(_align,os).good())
      return false;
    if(!writeVector(_grid,os).good())
      return false;
    if(!writeBinaryData(_dim,os).good())
      return false;

    return true;
  }
  virtual bool read(istream &is,IOData* dat) {
    if(!HasMagic::readMagic(is))
      return false;

    if(!readBinaryData(_off,is).good())
      return false;
    if(!readBinaryData(_szCell,is).good())
      return false;
    if(!readBinaryData(_invSzCell,is).good())
      return false;
    if(!readBinaryData(_szPoint,is).good())
      return false;
    if(!readBinaryData(_bb,is).good())
      return false;
    if(!readBinaryData(_szPointAligned,is).good())
      return false;
    if(!readBinaryData(_stride,is).good())
      return false;
    if(!readBinaryData(_align,is).good())
      return false;
    if(!readVector(_grid,is).good())
      return false;
    if(!readBinaryData(_dim,is).good())
      return false;

    return true;
  }
  virtual bool write(ostream &os) const {
    return write(os,NULL);
  }
  virtual bool read(istream& is) {
    return read(is,NULL);
  }
  bool writeASCII1D(ostream& os) const {
    os << getNrPoint().x() << std::endl;
    for(sizeType x=0; x<getNrPoint().x(); x++)
      os << (T)get(Vec3i(x,0,0)) << " "  << std::endl;
    return os.good();
  }
  bool writeASCII2D(ostream& os) const {
    //y
    //|
    //|
    //|
    //0--------x
    os << getNrPoint().x() << " " << getNrPoint().y() << std::endl;
    for(sizeType y=getNrPoint().y()-1; y>=0; y--) {
      for(sizeType x=0; x<getNrPoint().x(); x++)
        os << (T)get(Vec3i(x,y,0)) << " ";
      os << std::endl;
    }
    return os.good();
  }
  void reset(const Vec3i& nrCell,const BBox<TI>& bb,const T& val,bool center=true,sizeType align=4,bool shadow=false,bool ZFirst=true) {
    const IndexType extent=bb.getExtent();
    _dim=3;
    if(extent.z() == 0.0f)_dim--;
    if(extent.y() == 0.0f)_dim--;

    _bb=bb;
    _szPoint=center ? nrCell : (nrCell+Vec3i::Constant(1));
    //fill alignment
    _szPointAligned=_szPoint;
    if(_dim == 3)
      _szPointAligned[2]=((_szPoint[2]+align-1)/align)*align;
    else if(_dim == 2)
      _szPointAligned[1]=((_szPoint[1]+align-1)/align)*align;
    else
      _szPointAligned[0]=((_szPoint[0]+align-1)/align)*align;
    //set all pending dimension to 1
    for(sizeType j=_dim; j<3; j++)
      _szPoint(j)=_szPointAligned(j)=1;
    if((_ZFirst=ZFirst))
      _stride=Vec3i(_szPointAligned.y()*_szPointAligned.z(),_szPointAligned.z(),1);
    else
      _stride=Vec3i(1,_szPointAligned.x(),_szPointAligned.x()*_szPointAligned.y());
    _align=align;

    _off=center ? 0.5f : 0.0f;
    _szCell=IndexType(extent.x()/TI(nrCell.x()),
                      (_dim < 2) ? 0.0f : extent.y()/TI(nrCell.y()),
                      (_dim < 3) ? 0.0f : extent.z()/TI(nrCell.z()));
    _invSzCell=IndexType(1.0f/_szCell.x(),
                         (_dim < 2) ? ScalarUtil<TI>::scalar_max : 1.0f/_szCell.y(),
                         (_dim < 3) ? ScalarUtil<TI>::scalar_max : 1.0f/_szCell.z());
    if(!shadow)
      init(val);
    else {
      TG tmp;
      _grid.swap(tmp);
    }
  }
  template <typename T2,typename TI2>
  void makeSameGeometry(const Grid<T2,TI2>& other,bool shadow=false,bool ZFirst=true,sizeType align=-1) {
    BBox<TI> bb;
    bb.copy(other.getBB());
    if(align == -1)
      align=other.getAlign();
    reset(other.getNrCell(),bb,T(),other.isCenter(),align,shadow,ZFirst);
  }
  void expand(const Vec3i& nr,const T& val) {
    IndexType nrD(_szCell.x()*(scalar)nr.x(),
                  _szCell.y()*(scalar)nr.y(),
                  _szCell.y()*(scalar)nr.z());

    //expand cell size
    Vec3i nrCell=getNrCell()+nr*2;

    //expand bounding box size
    BBox<scalar> bb=getBB();
    bb._minC-=nrD;
    bb._maxC+=nrD;
    for(sizeType j=2; j>=_dim; j--)
      bb._maxC[j]=bb._minC[j];

    reset(nrCell,bb,val,isCenter(),getAlign(),false);//,_ZFirst,getMove());
  }
  void decimate(const Grid<T,TI>& child,const sizeType& decimate,const T& val,bool uniformScale,sizeType align=4) {
    _align=align;

    //we only support node grid decimate
    //ASSERT(child._off == 0.0f);
    ASSERT(decimate > 1);

    if(uniformScale) {
      //cell size
      IndexType cellSz=child.getCellSize()*(TI)decimate;
      Vec3i nrCell=ceilV((IndexType)((child._bb.getExtent().array()/cellSz.array()).matrix()));
      nrCell=compMax(nrCell,Vec3i(2,2,2));	//ensure correct interpolation
      ASSERT(nrCell.x()*nrCell.y()*nrCell.z() > 1)

      BBox<TI> bb;
      bb._minC=child._bb._minC;
      bb._maxC=(cellSz.array()*IndexType((TI)nrCell.x(),(TI)nrCell.y(),(TI)nrCell.z()).array()).matrix()+bb._minC;
      for(sizeType j=child.getDim(); j<3; j++)
        bb._maxC(j)=bb._minC(j);
      reset(nrCell,bb,val,child.isCenter(),align,child.getMove());
    } else {
      Vec3i nrCell=child.getNrCell()/decimate;
      nrCell=compMax(nrCell,Vec3i(2,2,2));
      ASSERT(nrCell.x()*nrCell.y()*nrCell.z() > 1)

      reset(nrCell,child._bb,val,child.isCenter(),align,child.getMove());
    }
  }
public:
  const sizeType getAlign() const {
    return _align;
  }
  const sizeType getIndexNoAlign(const Vec3i& index) const {
    return Vec3i(_szPoint.y()*_szPoint.z(),_szPoint.z(),1).dot(index);
  }
  const sizeType getIndex(Vec3i index,const Vec3i* warpMode=NULL) const {
    if(warpMode)
      for(sizeType d=0; d<_dim; d++)
        if((*warpMode)[d] > 0) {
          while(index[d] < 0)
            index[d]+=(*warpMode)[d];
          while(index[d] >= (*warpMode)[d])
            index[d]-=(*warpMode)[d];
        }
    return _stride.dot(index);
  }
  const sizeType getIndexSafe(Vec3i index,const Vec3i* warpMode=NULL) const {
    if(warpMode)
      for(sizeType d=0; d<_dim; d++)
        if((*warpMode)[d] > 0) {
          while(index[d] < 0)
            index[d]+=(*warpMode)[d];
          while(index[d] >= (*warpMode)[d])
            index[d]-=(*warpMode)[d];
        }
    return _stride.dot(compMax(compMin(index,getNrPoint()-Vec3i::Ones()),Vec3i(0,0,0)));
  }
  const Vec3i getIndex(const sizeType& index) const {
    return Vec3i(index/_stride.x(),(index%_stride.x())/_stride.y(),index%_stride.y());
  }
  FORCE_INLINE const T& operator[](const Vec3i& index) const {
    return get(index);
  }
  FORCE_INLINE const T& operator[](const sizeType& index) const {
    return _grid[index];
  }
  FORCE_INLINE T& operator[](const Vec3i& index) {
    return get(index);
  }
  FORCE_INLINE T& operator[](const sizeType& index) {
    return _grid[index];
  }
  const T& get(const Vec3i& index,const Vec3i* warpMode=NULL) const {
    return _grid[getIndex(index,warpMode)];
  }
  const T& getSafe(const Vec3i& index,const Vec3i* warpMode=NULL) const {
    return _grid[getIndexSafe(index,warpMode)];
  }
  T& get(const Vec3i& index,const Vec3i* warpMode=NULL) {
    return _grid[getIndex(index,warpMode)];
  }
  T& getSafe(const Vec3i& index,const Vec3i* warpMode=NULL) {
    return _grid[getIndexSafe(index,warpMode)];
  }
  bool isSafeIndex(const Vec3i& id) const {
    return compGE(id,Vec3i(0,0,0)) && compLE(id,getNrPoint()-Vec3i::Ones());
  }
  const Vec3i& getNrPoint() const {
    return _szPoint;
  }
  Vec3i getNrCell() const {
    if(_dim == 1)
      return (_off == 0.5f) ? _szPoint : _szPoint-Vec3i(1,0,0);
    else if(_dim == 2)
      return (_off == 0.5f) ? _szPoint : _szPoint-Vec3i(1,1,0);
    else
      return (_off == 0.5f) ? _szPoint : _szPoint-Vec3i::Constant(1);
  }
  const BBox<TI>& getBB() const {
    return _bb;
  }
  const IndexType& getInvCellSize() const {
    return _invSzCell;
  }
  const IndexType& getCellSize() const {
    return _szCell;
  }
  IndexType getIndexFrac(const IndexType& pos,const Vec3i* warpMode=NULL) const {
    IndexType ret=((pos-_bb._minC).array()*getInvCellSize().array()).matrix()-IndexType::Constant(_off);
    if(_dim < 2)
      ret.y()=0.0f;
    if(_dim < 3)
      ret.z()=0.0f;
    return ret;
  }
  IndexType getIndexFracSafe(const IndexType& pos,const Vec3i* warpMode=NULL) const {
    IndexType ret=getIndexFrac(pos);
    if(warpMode) {
      for(sizeType d=0; d<_dim; d++)
        if((*warpMode)[d] <= 0)
          ret[d]=std::max<TI>(std::min<TI>(ret[d],(TI)(_szPoint[d]-1-1E-4f)),0);
    } else {
      for(sizeType d=0; d<_dim; d++)
        ret[d]=std::max<TI>(std::min<TI>(ret[d],(TI)(_szPoint[d]-1-1E-4f)),0);
    }
    return ret;
  }
  IndexType getPt(const Vec3i& cell) const {
    return _bb._minC+(getCellSize().array()*IndexType(TI(cell.x())+_off,TI(cell.y())+_off,TI(cell.z())+_off).array()).matrix();
  }
  IndexType getCellCtr(const Vec3i& cell) const {
    return _bb._minC+(getCellSize().array()*IndexType(TI(cell.x())+0.5f,TI(cell.y())+0.5f,TI(cell.z())+0.5f).array()).matrix();
  }
  IndexType getCellCorner(const Vec3i& cell) const {
    return _bb._minC+(getCellSize().array()*IndexType(TI(cell.x())     ,TI(cell.y())     ,TI(cell.z())).array()).matrix();
  }
  const sizeType getSzLinear() const {
    return _szPointAligned.prod();
  }
  const sizeType getSzLinearNoAlign() const {
    return _szPoint.x()*_szPoint.y()*_szPoint.z();
  }
//determine 3D interpolation value
#define DETERMINE_VALUE3D(FRAC_FUNC)  \
ASSERT(_dim == 3) \
ASSERT(_szPoint.x() > 1)  \
ASSERT(_szPoint.y() > 1)  \
ASSERT(_szPoint.z() > 1); \
IndexType frac=FRAC_FUNC(pos,warpMode); \
const Vec3i base=floorV(frac);  \
frac-=IndexType((TI)base.x(),(TI)base.y(),(TI)base.z());  \
const sizeType id000=getIndex(base+Vec3i(0,0,0),warpMode);  \
const sizeType id100=getIndex(base+Vec3i(1,0,0),warpMode);  \
const sizeType id010=getIndex(base+Vec3i(0,1,0),warpMode);  \
const sizeType id110=getIndex(base+Vec3i(1,1,0),warpMode);  \
const sizeType id001=getIndex(base+Vec3i(0,0,1),warpMode);  \
const sizeType id101=getIndex(base+Vec3i(1,0,1),warpMode);  \
const sizeType id011=getIndex(base+Vec3i(0,1,1),warpMode);  \
const sizeType id111=getIndex(base+Vec3i(1,1,1),warpMode);
//determine 2D interpolation value
#define DETERMINE_VALUE2D(FRAC_FUNC)  \
ASSERT(_dim == 2) \
ASSERT(_szPoint.x() > 1)  \
ASSERT(_szPoint.y() > 1); \
IndexType frac=FRAC_FUNC(pos,warpMode); \
const Vec3i base=floorV(frac);  \
frac-=IndexType((TI)base.x(),(TI)base.y(),0.0f);  \
const sizeType id000=getIndex(base+Vec3i(0,0,0),warpMode);  \
const sizeType id100=getIndex(base+Vec3i(1,0,0),warpMode);  \
const sizeType id010=getIndex(base+Vec3i(0,1,0),warpMode);  \
const sizeType id110=getIndex(base+Vec3i(1,1,0),warpMode);
//determine 1D interpolation value
#define DETERMINE_VALUE1D(FRAC_FUNC)  \
ASSERT(_dim == 1) \
ASSERT(_szPoint.x() > 1); \
IndexType frac=FRAC_FUNC(pos,warpMode); \
const Vec3i base=floorV(frac);  \
frac-=IndexType((TI)base.x(),0.0f,0.0f);  \
const sizeType id000=getIndex(base+Vec3i(0,0,0),warpMode);  \
const sizeType id100=getIndex(base+Vec3i(1,0,0),warpMode);
#define DETERMINE_VALUE3D_CUBIC(FRAC_FUNC)  \
ASSERT(_dim == 3) \
ASSERT(_szPoint.x() > 1)  \
ASSERT(_szPoint.y() > 1)  \
IndexType frac=FRAC_FUNC(pos,warpMode); \
const Vec3i base=floorV(frac);  \
frac-=IndexType((TI)base.x(),(TI)base.y(),(TI)base.z());  \
for(int i=-1;i<=2;i++)  \
  for(int j=-1;j<=2;j++)  \
    for(int k=-1;k<=2;k++)  \
      index[i+1][j+1][k+1]=getIndexSafe(base+Vec3i(k,j,i),warpMode);
#define DETERMINE_VALUE2D_CUBIC(FRAC_FUNC)  \
ASSERT(_dim == 2) \
ASSERT(_szPoint.x() > 1)  \
ASSERT(_szPoint.y() > 1)  \
IndexType frac=FRAC_FUNC(pos,warpMode); \
const Vec3i base=floorV(frac);  \
frac-=IndexType((TI)base.x(),(TI)base.y(),0.0f);  \
for(int j=-1;j<=2;j++)  \
  for(int k=-1;k<=2;k++)  \
    index[j+1][k+1]=getIndexSafe(base+Vec3i(k,j,0),warpMode);
#define DETERMINE_VALUE1D_CUBIC(FRAC_FUNC)  \
ASSERT(_dim == 1) \
ASSERT(_szPoint.x() > 1)  \
IndexType frac=FRAC_FUNC(pos,warpMode); \
const Vec3i base=floorV(frac);  \
frac-=IndexType((TI)base.x(),0.0f,0.0f);  \
for(int k=-1;k<=2;k++)  \
  index[k+1]=getIndexSafe(base+Vec3i(k,0,0),warpMode);
  //sample value
  T sampleSafe(const IndexType& pos,const Vec3i* warpMode=NULL) const {
    return _dim == 1 ? sampleSafe1D(pos,warpMode) :
           _dim == 2 ? sampleSafe2D(pos,warpMode) :
           sampleSafe3D(pos,warpMode);
  }
  T sampleSafe3D(const IndexType& pos,const Vec3i* warpMode=NULL) const {
    DETERMINE_VALUE3D(getIndexFracSafe)
    return interp3D(_grid[id000],_grid[id100],
                    _grid[id010],_grid[id110],	 //lower z plane
                    _grid[id001],_grid[id101],
                    _grid[id011],_grid[id111],
                    frac.x(),frac.y(),frac.z());//higher z plane
  }
  T sampleSafe2D(const IndexType& pos,const Vec3i* warpMode=NULL) const {
    DETERMINE_VALUE2D(getIndexFracSafe)
    return interp2D(_grid[id000],_grid[id100],
                    _grid[id010],_grid[id110],
                    frac.x(),frac.y());
  }
  T sampleSafe1D(const IndexType& pos,const Vec3i* warpMode=NULL) const {
    DETERMINE_VALUE1D(getIndexFracSafe)
    return interp1D(_grid[id000],_grid[id100],frac.x());
  }
  //sample stencil
  sizeType getSampleStencilSafe(const IndexType& pos,TI* coefs,Vec3i* pts=NULL,sizeType* offS=NULL,const Vec3i* warpMode=NULL) const {
    return _dim == 1 ? getSampleStencilSafe1D(pos,coefs,pts,offS,warpMode) :
           _dim == 2 ? getSampleStencilSafe2D(pos,coefs,pts,offS,warpMode) :
           getSampleStencilSafe3D(pos,coefs,pts,offS,warpMode);
  }
  sizeType getSampleStencilSafe3D(const IndexType& pos,TI* coefs,Vec3i* pts=NULL,sizeType* offS=NULL,const Vec3i* warpMode=NULL) const {
    DETERMINE_VALUE3D(getIndexFracSafe)
    if(pts) {
      pts[0]=base+Vec3i(0,0,0);
      pts[1]=base+Vec3i(1,0,0);
      pts[2]=base+Vec3i(0,1,0);
      pts[3]=base+Vec3i(1,1,0);
      pts[4]=base+Vec3i(0,0,1);
      pts[5]=base+Vec3i(1,0,1);
      pts[6]=base+Vec3i(0,1,1);
      pts[7]=base+Vec3i(1,1,1);
    }
    if(offS) {
      offS[0]=id000;
      offS[1]=id100;
      offS[2]=id010;
      offS[3]=id110;
      offS[4]=id001;
      offS[5]=id101;
      offS[6]=id011;
      offS[7]=id111;
    }
    return stencil3D(coefs,frac.x(),frac.y(),frac.z());//higher z plane
  }
  sizeType getSampleStencilSafe2D(const IndexType& pos,TI* coefs,Vec3i* pts=NULL,sizeType* offS=NULL,const Vec3i* warpMode=NULL) const {
    DETERMINE_VALUE2D(getIndexFracSafe)
    if(pts) {
      pts[0]=base+Vec3i(0,0,0);
      pts[1]=base+Vec3i(1,0,0);
      pts[2]=base+Vec3i(0,1,0);
      pts[3]=base+Vec3i(1,1,0);
    }
    if(offS) {
      offS[0]=id000;
      offS[1]=id100;
      offS[2]=id010;
      offS[3]=id110;
    }
    return stencil2D(coefs,frac.x(),frac.y());//higher z plane
  }
  sizeType getSampleStencilSafe1D(const IndexType& pos,TI* coefs,Vec3i* pts=NULL,sizeType* offS=NULL,const Vec3i* warpMode=NULL) const {
    DETERMINE_VALUE1D(getIndexFracSafe)
    if(pts) {
      pts[0]=base+Vec3i(0,0,0);
      pts[1]=base+Vec3i(1,0,0);
    }
    if(offS) {
      offS[0]=id000;
      offS[1]=id100;
    }
    return stencil1D(coefs,frac.x());//higher z plane
  }
  //sample value with minmax
  T sampleSafe(const IndexType& pos,T& minV,T& maxV,const Vec3i* warpMode=NULL) const {
    return _dim == 1 ? sampleSafe1D(pos,minV,maxV,warpMode) :
           _dim == 2 ? sampleSafe2D(pos,minV,maxV,warpMode) :
           sampleSafe3D(pos,minV,maxV,warpMode);
  }
  T sampleSafe3D(const IndexType& pos,T& minV,T& maxV,const Vec3i* warpMode=NULL) const {
    DETERMINE_VALUE3D(getIndexFracSafe)
    return interp3D(_grid[id000],_grid[id100],
                    _grid[id010],_grid[id110],	 //lower z plane
                    _grid[id001],_grid[id101],
                    _grid[id011],_grid[id111],
                    frac.x(),frac.y(),frac.z(),minV,maxV);//higher z plane
  }
  T sampleSafe2D(const IndexType& pos,T& minV,T& maxV,const Vec3i* warpMode=NULL) const {
    DETERMINE_VALUE2D(getIndexFracSafe)
    return interp2D(_grid[id000],_grid[id100],
                    _grid[id010],_grid[id110],
                    frac.x(),frac.y(),minV,maxV);
  }
  T sampleSafe1D(const IndexType& pos,T& minV,T& maxV,const Vec3i* warpMode=NULL) const {
    DETERMINE_VALUE1D(getIndexFracSafe)
    return interp1D(_grid[id000],_grid[id100],frac.x(),minV,maxV);
  }
  //sample value with default
  T sampleSafe(const IndexType& pos,const Grid<unsigned char,TI>& valid,const T& def,const Vec3i* warpMode=NULL) const {
    return _dim == 1 ? sampleSafe1D(pos,valid,def,warpMode) :
           _dim == 2 ? sampleSafe2D(pos,valid,def,warpMode) :
           sampleSafe3D(pos,valid,def,warpMode);
  }
  T sampleSafe3D(const IndexType& pos,const Grid<unsigned char,TI>& valid,const T& def,const Vec3i* warpMode=NULL) const {
    DETERMINE_VALUE3D(getIndexFracSafe)
    if(valid[id000] == 0 || valid[id100] == 0 ||
       valid[id010] == 0 || valid[id110] == 0 ||
       valid[id001] == 0 || valid[id101] == 0 ||
       valid[id011] == 0 || valid[id111] == 0)
      return def;

    return interp3D(_grid[id000],_grid[id100],
                    _grid[id010],_grid[id110],	 //lower z plane
                    _grid[id001],_grid[id101],
                    _grid[id011],_grid[id111],
                    frac.x(),frac.y(),frac.z());//higher z plane
  }
  T sampleSafe2D(const IndexType& pos,const Grid<unsigned char,TI>& valid,const T& def,const Vec3i* warpMode=NULL) const {
    DETERMINE_VALUE2D(getIndexFracSafe)
    if(valid[id000] == 0 || valid[id100] == 0 ||
       valid[id010] == 0 || valid[id110] == 0)
      return def;

    return interp2D(_grid[id000],_grid[id100],
                    _grid[id010],_grid[id110],
                    frac.x(),frac.y());
  }
  T sampleSafe1D(const IndexType& pos,const Grid<unsigned char,TI>& valid,const T& def,const Vec3i* warpMode=NULL) const {
    DETERMINE_VALUE1D(getIndexFracSafe)
    if(valid[id000] == 0 || valid[id100] == 0)
      return def;

    return interp1D(_grid[id000],_grid[id100],frac.x());
  }
  //sample gradient
  ValueType sampleSafeGrad(const IndexType& pos,const Vec3i* warpMode=NULL) const {
    return _dim == 1 ? sampleSafe1DGrad(pos,warpMode) :
           _dim == 2 ? sampleSafe2DGrad(pos,warpMode) :
           sampleSafe3DGrad(pos,warpMode);
  }
  ValueType sampleSafe3DGrad(const IndexType& pos,const Vec3i* warpMode=NULL) const {
    DETERMINE_VALUE3D(getIndexFracSafe)
    return
      ValueType(interp2D(_grid[id100]-_grid[id000],
                         _grid[id110]-_grid[id010],
                         _grid[id101]-_grid[id001],
                         _grid[id111]-_grid[id011],
                         frac.y(),frac.z())*_invSzCell.x(),
                interp2D(_grid[id010]-_grid[id000],
                         _grid[id110]-_grid[id100],
                         _grid[id011]-_grid[id001],
                         _grid[id111]-_grid[id101],
                         frac.x(),frac.z())*_invSzCell.y(),
                interp2D(_grid[id001]-_grid[id000],
                         _grid[id101]-_grid[id100],
                         _grid[id011]-_grid[id010],
                         _grid[id111]-_grid[id110],
                         frac.x(),frac.y())*_invSzCell.z());
  }
  ValueType sampleSafe2DGrad(const IndexType& pos,const Vec3i* warpMode=NULL) const {
    DETERMINE_VALUE2D(getIndexFracSafe)
    return ValueType(interp1D(_grid[id100]-_grid[id000],_grid[id110]-_grid[id010],frac.y())*_invSzCell.x(),
                     interp1D(_grid[id010]-_grid[id000],_grid[id110]-_grid[id100],frac.x())*_invSzCell.y(),0.0f);
  }
  ValueType sampleSafe1DGrad(const IndexType& pos,const Vec3i* warpMode=NULL) const {
    DETERMINE_VALUE1D(getIndexFracSafe)
    return ValueType((_grid[id100]-_grid[id000])*_invSzCell.x(),0.0f,0.0f);
  }
  //sample laplace
  void sampleLaplaceSafe3D(const IndexType& pos,MatrixType& lap,const Vec3i* warpMode=NULL) const {
    DETERMINE_VALUE3D(getIndexFracSafe)
    lap.row(0)=ValueType(0.0f,
                         interp1D((_grid[id110]-_grid[id010])-(_grid[id100]-_grid[id000]),
                                  (_grid[id111]-_grid[id011])-(_grid[id101]-_grid[id001]),frac.z()),
                         interp1D((_grid[id101]-_grid[id001])-(_grid[id100]-_grid[id000]),
                                  (_grid[id111]-_grid[id011])-(_grid[id110]-_grid[id010]),frac.y()))*
               (_invSzCell.y()*_invSzCell.z());

    lap.row(1)=ValueType(interp1D((_grid[id110]-_grid[id100])-(_grid[id010]-_grid[id000]),
                                  (_grid[id111]-_grid[id101])-(_grid[id011]-_grid[id001]),frac.z()),
                         0.0f,
                         interp1D((_grid[id011]-_grid[id001])-(_grid[id010]-_grid[id000]),
                                  (_grid[id111]-_grid[id101])-(_grid[id110]-_grid[id100]),frac.x()))*
               (_invSzCell.x()*_invSzCell.z());

    lap.row(2)=ValueType(interp1D((_grid[id101]-_grid[id100])-(_grid[id001]-_grid[id000]),
                                  (_grid[id111]-_grid[id110])-(_grid[id011]-_grid[id010]),frac.y()),
                         interp1D((_grid[id011]-_grid[id010])-(_grid[id001]-_grid[id000]),
                                  (_grid[id111]-_grid[id110])-(_grid[id101]-_grid[id100]),frac.x()),
                         0.0f)*
               (_invSzCell.x()*_invSzCell.y());
  }
  //sample value cubic
  T sampleSafeCubic(const IndexType& pos,const Vec3i* warpMode=NULL) const {
    return _dim == 1 ? sampleSafe1DCubic(pos,warpMode) :
           _dim == 2 ? sampleSafe2DCubic(pos,warpMode) :
           sampleSafe3DCubic(pos,warpMode);
  }
  T sampleSafe3DCubic(const IndexType& pos,const Vec3i* warpMode=NULL) const {
    T valLinear,minV=ScalarUtil<T>::scalar_max,maxV=-minV;
    sizeType index[4][4][4];
    DETERMINE_VALUE3D_CUBIC(getIndexFracSafe)
    T val=interp3DCubic<T,TI,TG>(index,_grid,frac.x(),frac.y(),frac.z(),&valLinear,&minV,&maxV);
    if(val >= minV && val <= maxV)
      return val;
    else return valLinear;
  }
  T sampleSafe2DCubic(const IndexType& pos,const Vec3i* warpMode=NULL) const {
    T valLinear,minV=ScalarUtil<T>::scalar_max,maxV=-minV;
    sizeType index[4][4];
    DETERMINE_VALUE2D_CUBIC(getIndexFracSafe)
    T val=interp2DCubic<T,TI,TG>(index,_grid,frac.x(),frac.y(),&valLinear,&minV,&maxV);
    if(val >= minV && val <= maxV)
      return val;
    else return valLinear;
  }
  T sampleSafe1DCubic(const IndexType& pos,const Vec3i* warpMode=NULL) const {
    T valLinear,minV=ScalarUtil<T>::scalar_max,maxV=-minV;
    sizeType index[4];
    DETERMINE_VALUE1D_CUBIC(getIndexFracSafe)
    T val=interp1DCubic<T,TI,TG>(index,_grid,frac.x(),&valLinear,&minV,&maxV);
    if(val >= minV && val <= maxV)
      return val;
    else return valLinear;
  }
  //sample stencil cubic
  void getSampleStencilSafeCubic(const IndexType& pos,TI coefs[4][4][4],sizeType offS[4][4][4],const Vec3i* warpMode=NULL) const {
    _dim == 1 ? getSampleStencilSafe1DCubic(pos,coefs[0][0],offS[0][0],warpMode) :
    _dim == 2 ? getSampleStencilSafe2DCubic(pos,coefs[0],offS[0],warpMode) :
    getSampleStencilSafe3DCubic(pos,coefs,offS,warpMode);
  }
  void getSampleStencilSafe3DCubic(const IndexType& pos,TI coefs[4][4][4],sizeType index[4][4][4],const Vec3i* warpMode=NULL) const {
    T valLinear,minV=ScalarUtil<T>::scalar_max,maxV=-minV;
    DETERMINE_VALUE3D_CUBIC(getIndexFracSafe)
    T val=interp3DCubic<T,TI,TG>(index,_grid,frac.x(),frac.y(),frac.z(),&valLinear,&minV,&maxV);
    stencil3DCubic<TI>(coefs,frac.x(),frac.y(),frac.z(),!(val >= minV && val <= maxV));
  }
  void getSampleStencilSafe2DCubic(const IndexType& pos,TI coefs[4][4],sizeType index[4][4],const Vec3i* warpMode=NULL) const {
    T valLinear,minV=ScalarUtil<T>::scalar_max,maxV=-minV;
    DETERMINE_VALUE2D_CUBIC(getIndexFracSafe)
    T val=interp2DCubic<T,TI,TG>(index,_grid,frac.x(),frac.y(),&valLinear,&minV,&maxV);
    stencil2DCubic<TI>(coefs,frac.x(),frac.y(),!(val >= minV && val <= maxV));
  }
  void getSampleStencilSafe1DCubic(const IndexType& pos,TI coefs[4],sizeType index[4],const Vec3i* warpMode=NULL) const {
    T valLinear,minV=ScalarUtil<T>::scalar_max,maxV=-minV;
    DETERMINE_VALUE1D_CUBIC(getIndexFracSafe)
    T val=interp1DCubic<T,TI,TG>(index,_grid,frac.x(),&valLinear,&minV,&maxV);
    stencil1DCubic<TI>(coefs,frac.x(),!(val >= minV && val <= maxV));
  }
  //sample gradient cubic
  ValueType sampleSafeGradCubic(const IndexType& pos,const Vec3i* warpMode=NULL) const {
    return _dim == 1 ? sampleSafe1DGradCubic(pos,warpMode) :
           _dim == 2 ? sampleSafe2DGradCubic(pos,warpMode) :
           sampleSafe3DGradCubic(pos,warpMode);
  }
  ValueType sampleSafe3DGradCubic(const IndexType& pos,const Vec3i* warpMode=NULL) const {
    sizeType index[4][4][4];
    ValueType ret=ValueType::Zero();
    T valLinear,minV=ScalarUtil<T>::scalar_max,maxV=-minV;
    DETERMINE_VALUE3D_CUBIC(getIndexFracSafe)
    T val=interp3DCubic<T,TI,TG>(index,_grid,frac.x(),frac.y(),frac.z(),&valLinear,&minV,&maxV);
    ret=interp3DGradCubic<T,TI,TG>(index,_grid,frac.x(),frac.y(),frac.z(),!(val >= minV && val <= maxV));
    return (ret.array()*_invSzCell.array()).matrix();
  }
  ValueType sampleSafe2DGradCubic(const IndexType& pos,const Vec3i* warpMode=NULL) const {
    sizeType index[4][4];
    ValueType ret=ValueType::Zero();
    T valLinear,minV=ScalarUtil<T>::scalar_max,maxV=-minV;
    DETERMINE_VALUE2D_CUBIC(getIndexFracSafe)
    T val=interp2DCubic<T,TI,TG>(index,_grid,frac.x(),frac.y(),&valLinear,&minV,&maxV);
    ret.template block<2,1>(0,0)=interp2DGradCubic<T,TI,TG>(index,_grid,frac.x(),frac.y(),!(val >= minV && val <= maxV));
    return (ret.array()*_invSzCell.array()).matrix();
  }
  ValueType sampleSafe1DGradCubic(const IndexType& pos,const Vec3i* warpMode=NULL) const {
    sizeType index[4];
    ValueType ret=ValueType::Zero();
    T valLinear,minV=ScalarUtil<T>::scalar_max,maxV=-minV;
    DETERMINE_VALUE1D_CUBIC(getIndexFracSafe)
    T val=interp1DCubic<T,TI,TG>(index,_grid,frac.x(),&valLinear,&minV,&maxV);
    ret.template block<1,1>(0,0)=interp1DGradCubic<T,TI,TG>(index,_grid,frac.x(),!(val >= minV && val <= maxV));
    return (ret.array()*_invSzCell.array()).matrix();
  }
#undef DETERMINE_VALUE3D
#undef DETERMINE_VALUE2D
#undef DETERMINE_VALUE1D
#undef DETERMINE_VALUE3D_CUBIC
#undef DETERMINE_VALUE2D_CUBIC
#undef DETERMINE_VALUE1D_CUBIC
  //simple operation
  T dot(const Grid& other) const {
    const sizeType n=(sizeType)_grid.size();
    T ret=0.0f;
    for(sizeType i=0; i<n; i++)
      ret+=_grid[i]*other._grid[i];
    return ret;
  }
  T getAbsMax() const {
    const sizeType n=(sizeType)_grid.size();
    T maxV=-ScalarUtil<T>::scalar_max;
    for(sizeType i=0; i<n; i++) {
      const T& val=_grid[i];
      if(abs(val) > maxV)
        maxV=abs(val);
    }
    return maxV;
  }
  T getAbsMaxVec3() const {
    const sizeType n=(sizeType)_grid.size();
    T maxV=T::Constant(-ScalarUtil<typename T::Scalar>::scalar_max);
    for(sizeType i=0; i<n; i++) {
      const T& val=_grid[i];
      if(abs(val[0]) > maxV[0])
        maxV[0]=abs(val[0]);
      if(abs(val[1]) > maxV[1])
        maxV[1]=abs(val[1]);
      if(abs(val[2]) > maxV[2])
        maxV[2]=abs(val[2]);
    }
    return maxV;
  }
  T sum() const {
    T ret=0.0f;
    const sizeType n=(sizeType)_grid.size();
    OMP_PARALLEL_FOR_I(OMP_ADD(ret))
    for(sizeType i=0; i<n; i++)
      ret+=_grid[i];
    return ret;
  }
  T squaredDistTo(const Grid& other) const {
    T ret=0.0f;
    const TG& otherG=other._grid;
    const sizeType n=(sizeType)_grid.size();
    OMP_PARALLEL_FOR_I(OMP_ADD(ret))
    for(sizeType i=0; i<n; i++)
      ret+=(_grid[i]-otherG[i])*(_grid[i]-otherG[i]);
    return ret;
  }
  T squaredDistToCoef(const Grid& other,T coef) const {
    T ret=0.0f,tmp;
    const TG& otherG=other._grid;
    const sizeType n=(sizeType)_grid.size();
    OMP_PARALLEL_FOR_I(OMP_PRI(tmp) OMP_ADD(ret))
    for(sizeType i=0; i<n; i++) {
      tmp=(_grid[i]-otherG[i]*coef);
      ret+=tmp*tmp;
    }
    return ret;
  }
  T squaredNorm() const {
    T ret=0.0f;
    const sizeType n=(sizeType)_grid.size();
    OMP_PARALLEL_FOR_I(OMP_ADD(ret))
    for(sizeType i=0; i<n; i++)
      ret+=_grid[i]*_grid[i];
    return ret;
  }
  void minMax(T& minV,T& maxV) const {
    const sizeType n=(sizeType)_grid.size();
    minV=ScalarUtil<T>::scalar_max;
    maxV=-minV;
    for(sizeType i=0; i<n; i++) {
      const T& val=_grid[i];
      minV=mmin<T>(val,minV);
      maxV=mmax<T>(val,maxV);
    }
  }
  Grid& add(const Grid& other) {
    const sizeType n=(sizeType)_grid.size();
    for(sizeType i=0; i<n; i++)
      _grid[i]+=other._grid[i];
    return *this;
  }
  Grid& add(const T& coef) {
    const sizeType n=(sizeType)_grid.size();
    for(sizeType i=0; i<n; i++)
      _grid[i]+=coef;
    return *this;
  }
  Grid& sub(const Grid& other) {
    const sizeType n=(sizeType)_grid.size();
    for(sizeType i=0; i<n; i++)
      _grid[i]-=other._grid[i];
    return *this;
  }
  Grid& min(const Grid& other) {
    const sizeType n=(sizeType)_grid.size();
    for(sizeType i=0; i<n; i++)
      _grid[i]=std::min(_grid[i],other._grid[i]);
    return *this;
  }
  Grid& sub(const T& coef) {
    const sizeType n=(sizeType)_grid.size();
    for(sizeType i=0; i<n; i++)
      _grid[i]-=coef;
    return *this;
  }
  Grid& addScaled(const Grid& other,const T& coef) {
    const sizeType n=(sizeType)_grid.size();
    for(sizeType i=0; i<n; i++)
      _grid[i]+=other._grid[i]*coef;
    return *this;
  }
  Grid& mul(const T& coef) {
    const sizeType n=(sizeType)_grid.size();
    for(sizeType i=0; i<n; i++)
      _grid[i]*=coef;
    return *this;
  }
  Grid& clamp(const T& maxVal) {
    const sizeType n=(sizeType)_grid.size();
    for(sizeType i=0; i<n; i++) {
      T val=abs(_grid[i]);
      if(val > maxVal)
        _grid[i]*=maxVal/val;
    }
    return *this;
  }
  //dimension reduction
  void getSlice(Grid<T,TI>& slice,const sizeType& dim0,const sizeType& dim1,const sizeType& dim2,const T& x) const {
    Vec3i nrCell(getNrCell()[dim0],getNrCell()[dim1],0);
    BBox<TI> bb;
    bb._minC.x()=_bb._minC[dim0];
    bb._maxC.x()=_bb._maxC[dim0];
    bb._minC.y()=_bb._minC[dim1];
    bb._maxC.y()=_bb._maxC[dim1];
    bb._minC.z()=0.0f;
    bb._maxC.z()=0.0f;
    slice.reset(nrCell,bb,get(Vec3i(0,0,0)),isCenter(),getAlign(),false);

    sizeType sliceId=convert<sizeType>()(floor((x-_bb._minC[dim2])/getCellSize()[dim2]));

    Vec3i nrPoint=slice.getNrPoint();
    for(sizeType x=0; x<nrPoint.x(); x++)
      for(sizeType y=0; y<nrPoint.y(); y++) {
        Vec3i id;
        id[dim0]=x;
        id[dim1]=y;
        id[dim2]=sliceId;
        slice.get(Vec3i(x,y,0))=get(id);
      }
  }
  void getSliceYZ(Grid<T,TI>& slice,const T& x) const {
    getSlice(slice,1,2,0,x);
  }
  void getSliceXZ(Grid<T,TI>& slice,const T& y) const {
    getSlice(slice,0,2,1,y);
  }
  void getSliceXY(Grid<T,TI>& slice,const T& z) const {
    getSlice(slice,0,1,2,z);
  }
  Grid<T,TI> getSliceYZ(const T& x) const {
    Grid<T,TI> ret;
    getSlice(ret,1,2,0,x);
    return ret;
  }
  Grid<T,TI> getSliceXZ(const T& y) const {
    Grid<T,TI> ret;
    getSlice(ret,0,2,1,y);
    return ret;
  }
  Grid<T,TI> getSliceXY(const T& z) const {
    Grid<T,TI> ret;
    getSlice(ret,0,1,2,z);
    return ret;
  }
  //dangerous methods
  const TG& data() const {
    return _grid;
  }
  void setData(const Grid<T,TI>& other) {
    for(sizeType x=0; x<_szPoint.x(); x++)
      for(sizeType y=0; y<_szPoint.y(); y++)
        for(sizeType z=0; z<_szPoint.z(); z++)
          get(Vec3i(x,y,z))=other.get(Vec3i(x,y,z));
  }
  const T& get(const sizeType& index) const {
    return _grid[index];
  }
  T& get(const sizeType& index) {
    return _grid[index];
  }
  const Vec3i& getStride() const {
    return _stride;
  }
  bool isCenter() const {
    return _off == 0.5f;
  }
  void init(const T& val) {
    {
      TG tmp;
      _grid.swap(tmp);
    }
    _grid.assign(_szPointAligned.x()*_szPointAligned.y()*_szPointAligned.z(),val);
  }
  const sizeType& getDim() const {
    return _dim;
  }
  bool getZFirst() const {
    return _ZFirst;
  }
  void swap(Grid& other) {
    ASSERT(_grid.size() == other._grid.size())
    std::swap(_off,other._off);
    std::swap(_szCell,other._szCell);
    std::swap(_invSzCell,other._invSzCell);
    std::swap(_szPoint,other._szPoint);
    std::swap(_bb,other._bb);
    std::swap(_szPointAligned,other._szPointAligned);
    std::swap(_stride,other._stride);
    std::swap(_align,other._align);
    _grid.swap(other._grid);
    std::swap(_dim,other._dim);
  }
  void setBB(const BBox<TI>& bb) {
    ASSERT((bb.getExtent()-_bb.getExtent()).norm() < ScalarUtil<TI>::scalar_eps);
    _bb._minC=bb._minC;
    _bb._maxC=bb._maxC;
  }
protected:
  //param
  TI _off;
  IndexType _szCell;
  IndexType _invSzCell;
  Vec3i _szPoint;
  BBox<TI> _bb;
  //memory
  Vec3i _szPointAligned;
  Vec3i _stride;
  sizeType _align;
  TG _grid;
  sizeType _dim;
  bool _ZFirst;
};

template <typename T,typename TI,typename TG=vector<T,Eigen::aligned_allocator<T> > >
struct MACGrid : public HasMagic, public Serializable {
  friend class dataStructureCL;
public:
  typedef typename Eigen::Matrix<T,3,1> ValueType;
  typedef typename Eigen::Matrix<TI,3,1> IndexType;
  MACGrid():HasMagic(0xAAAABBBBAAAABBBB),Serializable(typeid(MACGrid).name()) {}
  ~MACGrid() {}
  virtual boost::shared_ptr<Serializable> copy() const {
    return boost::shared_ptr<Serializable>(new MACGrid());
  }
  virtual bool write(ostream &os,IOData* dat) const {
    if(!HasMagic::writeMagic(os))
      return false;

    if(!writeBinaryData(_bb,os).good())
      return false;
    if(!writeBinaryData(_cellSz,os).good())
      return false;
    if(!writeBinaryData(_nrCell,os).good())
      return false;
    if(!writeBinaryData(_dim,os).good())
      return false;

    bool ret=true;
    if(_dim >= 1 && ret)
      ret=ret && _g[0].write(os);
    if(_dim >= 2 && ret)
      ret=ret && _g[1].write(os);
    if(_dim >= 3 && ret)
      ret=ret && _g[2].write(os);
    return ret;
  }
  virtual bool read(istream &is,IOData* dat) {
    if(!HasMagic::readMagic(is))
      return false;

    if(!readBinaryData(_bb,is).good())
      return false;
    if(!readBinaryData(_cellSz,is).good())
      return false;
    if(!readBinaryData(_nrCell,is).good())
      return false;
    if(!readBinaryData(_dim,is).good())
      return false;

    bool ret=true;
    if(_dim >= 1 && ret)
      ret=ret && _g[0].read(is);
    if(_dim >= 2 && ret)
      ret=ret && _g[1].read(is);
    if(_dim >= 3 && ret)
      ret=ret && _g[2].read(is);
    return ret;
  }
  virtual bool write(ostream &os) const {
    return write(os,NULL);
  }
  virtual bool read(istream &is) {
    return read(is,NULL);
  }
  template <typename T2,typename TI2,typename TG2>
  void reset(const Grid<T2,TI2,TG2>& ref,bool shadow=false,bool edge=false) {
    //ASSERT(ref.isCenter())
    _cellSz=IndexType((TI)ref.getCellSize().x(),(TI)ref.getCellSize().y(),(TI)ref.getCellSize().z());
    _nrCell=ref.getNrCell();

    _dim=ref.getDim();
    _bb.copy(ref.getBB());

    if(ref.getDim() >= 1)
      resetGu(ref,shadow,edge);
    if(ref.getDim() >= 2)
      resetGv(ref,shadow,edge);
    if(ref.getDim() >= 3)
      resetGw(ref,shadow,edge);
  }
  template <typename T2,typename TI2,typename TG2>
  void makeSameGeometry(const MACGrid<T2,TI2,TG2>& other) {
    _bb.copy(other.getBB());

    typename MACGrid<T2,TI2,TG2>::IndexType cellSz=other.getCellSize();
    _cellSz=IndexType(cellSz.x(),cellSz.y(),cellSz.z());
    _nrCell=other.getNrCell();
    _dim=other.getDim();

    if(_dim >= 1)
      getGu().makeSameGeometry(other.getGu());
    if(_dim >= 2)
      getGv().makeSameGeometry(other.getGv());
    if(_dim >= 3)
      getGw().makeSameGeometry(other.getGw());
  }
public:
  const sizeType getAlign() const {
    return _g[0].getAlign();
  }
  const Vec3i& getNrCell() const {
    return _nrCell;
  }
  const BBox<TI>& getBB() const {
    return _bb;
  }
  const IndexType& getInvCellSize() const {
    return _g[0].getInvCellSize();
  }
  const IndexType& getCellSize() const {
    return _g[0].getCellSize();
  }
  ValueType get(const Vec3i& index) const {
    if(getDim() == 1)
      return get1D(index);
    else if(getDim() == 2)
      return get2D(index);
    else
      return get3D(index);
  }
  ValueType getSafe(const Vec3i& index) const {
    if(getDim() == 1)
      return getSafe1D(index);
    else if(getDim() == 2)
      return getSafe2D(index);
    else
      return getSafe3D(index);
  }
  ValueType get1D(const Vec3i& index) const {
    ASSERT(getDim() == 1)
    return ValueType((_g[0].get(index)+_g[0].get(index+Vec3i(1,0,0)))*0.5f,
                     0.0f,0.0f);
  }
  ValueType getSafe1D(const Vec3i& index) const {
    return get1D(compMin(compMax(index,Vec3i::Zero()),getNrCell()-Vec3i::Ones()));
  }
  ValueType get2D(const Vec3i& index) const {
    ASSERT(getDim() == 2)
    return ValueType((_g[0].get(index)+_g[0].get(index+Vec3i(1,0,0)))*0.5f,
                     (_g[1].get(index)+_g[1].get(index+Vec3i(0,1,0)))*0.5f,
                     0.0f);
  }
  ValueType getSafe2D(const Vec3i& index) const {
    return get2D(compMin(compMax(index,Vec3i::Zero()),getNrCell()-Vec3i::Ones()));
  }
  ValueType get3D(const Vec3i& index) const {
    ASSERT(getDim() == 3)
    return ValueType((_g[0].get(index)+_g[0].get(index+Vec3i(1,0,0)))*0.5f,
                     (_g[1].get(index)+_g[1].get(index+Vec3i(0,1,0)))*0.5f,
                     (_g[2].get(index)+_g[2].get(index+Vec3i(0,0,1)))*0.5f);
  }
  ValueType getSafe3D(const Vec3i& index) const {
    return get3D(compMin(compMax(index,Vec3i::Zero()),getNrCell()-Vec3i::Ones()));
  }
  //safe version
  ValueType sample(const IndexType& pos,const Vec3c* warpMode=NULL) const {
    if(getDim() == 1)
      return sample1D(pos,warpMode);
    else if(getDim() == 2)
      return sample2D(pos,warpMode);
    else return sample3D(pos,warpMode);
  }
  ValueType sampleSafe(const IndexType& pos,const Vec3c* warpMode=NULL) const {
    if(getDim() == 1)
      return sampleSafe1D(pos,warpMode);
    else if(getDim() == 2)
      return sampleSafe2D(pos,warpMode);
    else return sampleSafe3D(pos,warpMode);
  }
#define SAMPLE(D,DIM) \
ValueType sample##DIM(const IndexType& pos,const Vec3c* warpMode=NULL) const {  \
  ASSERT(getDim() == D);  \
  ValueType ret=ValueType::Zero();  \
  if(warpMode) {  \
    Vec3i nrCell=Vec3i::Zero(); \
    for(sizeType d=0; d<D; d++) \
      if((*warpMode)[d])  \
        nrCell[d]=_nrCell[d]; \
    for(sizeType d=0; d<D; d++) \
      ret[d]=_g[d].sample##DIM(pos,&nrCell);  \
  } else {  \
    for(sizeType d=0; d<D; d++) \
      ret[d]=_g[d].sample##DIM(pos);  \
  } \
  return ret; \
} \
ValueType sampleSafe##DIM(const IndexType& pos,const Vec3c* warpMode=NULL) const  \
{ \
  ASSERT(getDim() == D);  \
  ValueType ret=ValueType::Zero();  \
  if(warpMode) {  \
    Vec3i nrCell=Vec3i::Zero(); \
    for(sizeType d=0; d<D; d++) \
      if((*warpMode)[d])  \
        nrCell[d]=_nrCell[d]; \
    for(sizeType d=0; d<D; d++) \
      ret[d]=_g[d].sampleSafe##DIM(pos,&nrCell);  \
  } else {  \
    for(sizeType d=0; d<D; d++) \
      ret[d]=_g[d].sampleSafe##DIM(pos);  \
  } \
  return ret; \
}
  SAMPLE(1,1D)
  SAMPLE(2,2D)
  SAMPLE(3,3D)
#undef SAMPLE
//safe version with default value
#define SAMPLE(D,DIM) \
ValueType sample##DIM(const IndexType& pos,const Grid<unsigned char,TI>& valid,const T& def,const Vec3c* warpMode=NULL) const {  \
  ASSERT(getDim() == D);  \
  ValueType ret=ValueType::Zero();  \
  if(warpMode) {  \
    Vec3i nrCell=Vec3i::Zero(); \
    for(sizeType d=0; d<D; d++) \
      if((*warpMode)[d])  \
        nrCell[d]=_nrCell[d]; \
    for(sizeType d=0; d<D; d++) \
      ret[d]=_g[d].sample##DIM(pos,valid,def,&nrCell);  \
  } else {  \
    for(sizeType d=0; d<D; d++) \
      ret[d]=_g[d].sample##DIM(pos,valid,def);  \
  } \
  return ret; \
} \
ValueType sampleSafe##DIM(const IndexType& pos,const Grid<unsigned char,TI>& valid,const T& def,const Vec3c* warpMode=NULL) const  \
{ \
  ASSERT(getDim() == D);  \
  ValueType ret=ValueType::Zero();  \
  if(warpMode) {  \
    Vec3i nrCell=Vec3i::Zero(); \
    for(sizeType d=0; d<D; d++) \
      if((*warpMode)[d])  \
        nrCell[d]=_nrCell[d]; \
    for(sizeType d=0; d<D; d++) \
      ret[d]=_g[d].sampleSafe##DIM(pos,valid,def,&nrCell);  \
  } else {  \
    for(sizeType d=0; d<D; d++) \
      ret[d]=_g[d].sampleSafe##DIM(pos,valid,def);  \
  } \
  return ret; \
}
  SAMPLE(1,1D)
  SAMPLE(2,2D)
  SAMPLE(3,3D)
#undef SAMPLE
//simple operation
  T dot(const MACGrid& other) const {
    T ret=0.0f;
    if(getDim() >= 1)
      ret+=_g[0].dot(other._g[0]);
    if(getDim() >= 2)
      ret+=_g[1].dot(other._g[1]);
    if(getDim() >= 3)
      ret+=_g[2].dot(other._g[2]);
    return ret;
  }
  T squaredDistTo(const MACGrid& other) const {
    T ret=0;
    for(sizeType d=0; d<_dim; d++)
      ret+=_g[d].squaredDistTo(other._g[d]);
    return ret;
  }
  T squaredDistToCoef(const MACGrid& other,T coef) const {
    T ret=0;
    for(sizeType d=0; d<_dim; d++)
      ret+=_g[d].squaredDistToCoef(other._g[d],coef);
    return ret;
  }
  T squaredNorm() const {
    T ret=0;
    for(sizeType d=0; d<_dim; d++)
      ret+=_g[d].squaredNorm();
    return ret;
  }
  ValueType getAbsMax() const {
    ValueType ret=ValueType::Zero();
    if(getDim() >= 1)
      ret.x()=_g[0].getAbsMax();
    if(getDim() >= 2)
      ret.y()=_g[1].getAbsMax();
    if(getDim() >= 3)
      ret.z()=_g[2].getAbsMax();
    return ret;
  }
  void minMax(ValueType& minV,ValueType& maxV) const {
    minV=maxV=ValueType::Zero();
    if(getDim() >= 1)
      _g[0].minMax(minV.x(),maxV.x());
    if(getDim() >= 2)
      _g[1].minMax(minV.y(),maxV.y());
    if(getDim() >= 3)
      _g[2].minMax(minV.z(),maxV.z());
  }
  MACGrid& add(const MACGrid& other) {
    if(getDim() >= 1)
      _g[0].add(other._g[0]);
    if(getDim() >= 2)
      _g[1].add(other._g[1]);
    if(getDim() >= 3)
      _g[2].add(other._g[2]);
    return *this;
  }
  MACGrid& sub(const MACGrid& other) {
    if(getDim() >= 1)
      _g[0].sub(other._g[0]);
    if(getDim() >= 2)
      _g[1].sub(other._g[1]);
    if(getDim() >= 3)
      _g[2].sub(other._g[2]);
    return *this;
  }
  MACGrid& min(const MACGrid& other) {
    if(getDim() >= 1)
      _g[0].min(other._g[0]);
    if(getDim() >= 2)
      _g[1].min(other._g[1]);
    if(getDim() >= 3)
      _g[2].min(other._g[2]);
    return *this;
  }
  MACGrid& mul(const T& other) {
    if(getDim() >= 1)
      _g[0].mul(other);
    if(getDim() >= 2)
      _g[1].mul(other);
    if(getDim() >= 3)
      _g[2].mul(other);
    return *this;
  }
  MACGrid& addScaled(const MACGrid& other,const T& coef) {
    if(getDim() >= 1)
      _g[0].addScaled(other._g[0],coef);
    if(getDim() >= 2)
      _g[1].addScaled(other._g[1],coef);
    if(getDim() >= 3)
      _g[2].addScaled(other._g[2],coef);
    return *this;
  }
  MACGrid& clamp(const T& maxVal) {
    if(getDim() >= 1)
      _g[0].clamp(maxVal);
    if(getDim() >= 2)
      _g[1].clamp(maxVal);
    if(getDim() >= 3)
      _g[2].clamp(maxVal);
    return *this;
  }
//dangerous methods
  void setData(const MACGrid<T,TI>& other) {
    if(_dim>=1)getGu().setData(other.getGu());
    if(_dim>=2)getGv().setData(other.getGv());
    if(_dim>=2)getGw().setData(other.getGw());
  }
  void init(const ValueType& val) {
    if(getDim() >= 1)
      _g[0].init(val.x());
    if(getDim() >= 2)
      _g[1].init(val.y());
    if(getDim() >= 3)
      _g[2].init(val.z());
  }
  const sizeType& getDim() const {
    return _dim;
  }
  Grid<T,TI,TG>& getGu() {
    return _g[0];
  }
  Grid<T,TI,TG>& getGv() {
    return _g[1];
  }
  Grid<T,TI,TG>& getGw() {
    return _g[2];
  }
  Grid<T,TI,TG>& getComp(const sizeType& i) {
    return _g[i];
  }
  const Grid<T,TI,TG>& getGu() const {
    return _g[0];
  }
  const Grid<T,TI,TG>& getGv() const {
    return _g[1];
  }
  const Grid<T,TI,TG>& getGw() const {
    return _g[2];
  }
  const Grid<T,TI,TG>& getComp(const sizeType& i) const {
    return _g[i];
  }
  void swap(MACGrid& other) {
    std::swap(_bb,other._bb);
    std::swap(_cellSz,other._cellSz);
    std::swap(_nrCell,other._nrCell);
    std::swap(_dim,other._dim);
    _g[0].swap(other._g[0]);
    _g[1].swap(other._g[1]);
    _g[2].swap(other._g[2]);
  }
protected:
  template <typename T2,typename TI2,typename TG2>
  void resetGu(const Grid<T2,TI2,TG2>& ref,bool shadow,bool edge) {
    Vec3i nrCell=ref.getNrCell();
    BBox<TI> bb=ref.getBB();
    if(edge) {
      ASSERT(ref.getDim() > 1)
      if(ref.getDim() == 3) {
        bb._minC.x()+=_cellSz.x()*0.5f;
        bb._maxC.x()-=_cellSz.x()*0.5f;
        nrCell.x()--;
      }
    } else {
      if(ref.getDim() >= 2) {
        bb._minC.y()+=_cellSz.y()*0.5f;
        bb._maxC.y()-=_cellSz.y()*0.5f;
        nrCell.y()--;
      }
      if(ref.getDim() >= 3) {
        bb._minC.z()+=_cellSz.z()*0.5f;
        bb._maxC.z()-=_cellSz.z()*0.5f;
        nrCell.z()--;
      }
    }

    _g[0].reset(nrCell,bb,(T)0.0f,false,ref.getAlign(),shadow,true);
  }
  template <typename T2,typename TI2,typename TG2>
  void resetGv(const Grid<T2,TI2,TG2>& ref,bool shadow,bool edge) {
    Vec3i nrCell=ref.getNrCell();
    BBox<TI> bb=ref.getBB();
    if(edge) {
      ASSERT(ref.getDim() > 1)
      if(ref.getDim() == 3) {
        bb._minC.y()+=_cellSz.y()*0.5f;
        bb._maxC.y()-=_cellSz.y()*0.5f;
        nrCell.y()--;
      }
    } else {
      if(ref.getDim() >= 1) {
        bb._minC.x()+=_cellSz.x()*0.5f;
        bb._maxC.x()-=_cellSz.x()*0.5f;
        nrCell.x()--;
      }
      if(ref.getDim() >= 3) {
        bb._minC.z()+=_cellSz.z()*0.5f;
        bb._maxC.z()-=_cellSz.z()*0.5f;
        nrCell.z()--;
      }
    }

    _g[1].reset(nrCell,bb,(T)0.0f,false,ref.getAlign(),shadow,true);
  }
  template <typename T2,typename TI2,typename TG2>
  void resetGw(const Grid<T2,TI2,TG2>& ref,bool shadow,bool edge) {
    Vec3i nrCell=ref.getNrCell();
    BBox<TI> bb=ref.getBB();
    if(edge) {
      ASSERT(ref.getDim() > 1)
      bb._minC.z()+=_cellSz.z()*0.5f;
      bb._maxC.z()-=_cellSz.z()*0.5f;
      nrCell.z()--;
    } else {
      if(ref.getDim() >= 1) {
        bb._minC.x()+=_cellSz.x()*0.5f;
        bb._maxC.x()-=_cellSz.x()*0.5f;
        nrCell.x()--;
      }
      if(ref.getDim() >= 2) {
        bb._minC.y()+=_cellSz.y()*0.5f;
        bb._maxC.y()-=_cellSz.y()*0.5f;
        nrCell.y()--;
      }
    }

    _g[2].reset(nrCell,bb,(T)0.0f,false,ref.getAlign(),shadow,true);
  }
//params
  BBox<TI> _bb;
  IndexType _cellSz;
  Vec3i _nrCell;
  sizeType _dim;
  Grid<T,TI,TG> _g[3];
};

typedef Grid<unsigned char,scalarF> TagFieldF;
typedef Grid<scalarF,scalarF> ScalarFieldF;
typedef Grid<Vec3f,scalarF> VectorFieldF;
typedef MACGrid<unsigned char,scalarF> MACTagFieldF;
typedef MACGrid<scalarF,scalarF> MACVelocityFieldF;

typedef Grid<unsigned char,scalarD> TagFieldD;
typedef Grid<scalarD,scalarD> ScalarFieldD;
typedef Grid<Vec3d,scalarD> VectorFieldD;
typedef MACGrid<unsigned char,scalarD> MACTagFieldD;
typedef MACGrid<scalarD,scalarD> MACVelocityFieldD;

typedef Grid<unsigned char,scalar> TagField;
typedef Grid<scalar,scalar> ScalarField;
typedef Grid<Vec3,scalar> VectorField;
typedef MACGrid<unsigned char,scalar> MACTagField;
typedef MACGrid<scalar,scalar> MACVelocityField;

PRJ_END

#endif
