#ifndef ADVECTOR_H
#define ADVECTOR_H

#include "FluidState.h"
#include <boost/property_tree/ptree_fwd.hpp>

PRJ_BEGIN

class Advector : public Traits
{
public:
  enum ADVECTOR {
    SEMI_LAGRANGIAN,
    SEMI_LAGRANGIAN_CUBIC,
    EXPLICIT,
    IMPLICIT,
  };
  Advector(const boost::property_tree::ptree& pt);
  void reset(const FluidState& s);
  void advect(const ScalarField& from,ScalarField& to,const MACVelocityField& v) const;
  void advectT(ScalarField& lambda,const ScalarField& rho,const MACVelocityField& v) const;
  void advectV(VecD& DFDX,const ScalarField& lambda,const ScalarField& rho,const MACVelocityField& v) const;
  ScalarField& cacheA() const;
  ScalarField& cacheB() const;
private:
  //our first version use semi-lagrangian, not a good choice
  void advectSemiLagrangian(const ScalarField& from,ScalarField& to,const MACVelocityField& v) const;
  void advectTSemiLagrangian(ScalarField& lambda,const ScalarField& rho,const MACVelocityField& v) const;
  void advectVSemiLagrangian(VecD& DFDX,const ScalarField& lambda,const ScalarField& rho,const MACVelocityField& v) const;
  //our first version use semi-lagrangian, but use cubic interpolation
  void advectSemiLagrangianCubic(const ScalarField& from,ScalarField& to,const MACVelocityField& v) const;
  void advectTSemiLagrangianCubic(ScalarField& lambda,const ScalarField& rho,const MACVelocityField& v) const;
  void advectVSemiLagrangianCubic(VecD& DFDX,const ScalarField& lambda,const ScalarField& rho,const MACVelocityField& v) const;
  //our next version, explicit first order advection operator
  void advectExplicit(const ScalarField& from,ScalarField& to,const MACVelocityField& v) const;
  void advectTExplicit(ScalarField& lambda,const ScalarField& rho,const MACVelocityField& v) const;
  void advectVExplicit(VecD& DFDX,const ScalarField& lambda,const ScalarField& rho,const MACVelocityField& v) const;
  //our final version, high order truncated implicit method
  void advectImplicit(const ScalarField& from,ScalarField& to,const MACVelocityField& v) const;
  void advectTImplicit(ScalarField& lambda,const ScalarField& rho,const MACVelocityField& v) const;
  void advectVImplicit(VecD& DFDX,const ScalarField& lambda,const ScalarField& rho,const MACVelocityField& v) const;
  //data
  Vec3 getStencil() const;
  void checkVariableOrder(sizeType& order) const;
  const boost::property_tree::ptree& _pt;
  boost::shared_ptr<ScalarField> _A,_B;
  vector<ScalarField> _LCache;
  Vec3c _periodic;
  Vec3i _periodicS;
};

PRJ_END

#endif
