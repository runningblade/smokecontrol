#ifndef OPTIMIZER_H
#define OPTIMIZER_H

#include "SpaceTimeMesh.h"
#include "CommonFile/solvers/Objective.h"

PRJ_BEGIN

class FluidOptObjective : public Objective<scalarD>, public Traits
{
public:
  //using Traits::Vec;
  FluidOptObjective(SpaceTimeMesh& mesh,const std::set<sizeType>* fixed=NULL);
  virtual int inputs() const;
  int inputsVG() const;
  void assembleV(Vec& x) const;
  void assignV(const Vec& x);
  void assembleVG(Vec& x) const;
  void assignVG(const Vec& x);
protected:
  bool isFixed(sizeType i) const;
  void filter(Vec& DFDX) const;
  string outputFileName(const string& method) const;
  const boost::property_tree::ptree& _pt;
  const std::set<sizeType>* _fixed;
  SpaceTimeMesh& _mesh;
};
//we want to search for the time dependent velocity fields to minimize
//argmin \frac{1}{2} \sum_{k=1}^K\|\rho_{i_k}-\rho_{i_k}^*\|^2 +
//       \frac{_regK}{2} \sum_{i=1}^N\|v_i-v_i^*\|^2
//s.t.   \rho_{i+1}=\rho_i+A[\rho]*dt*v_i
class PassiveOpt : public FluidOptObjective
{
public:
  enum GRADIENT {
    HIERARCHICAL,
    GAUSSIAN,
    GAUSSIAN_HIERARCHICAL,
  };
  //using Traits::Vec;
  PassiveOpt(SpaceTimeMesh& mesh,const std::set<sizeType>* fixed=NULL);
  virtual void DEDVel(scalarD& FX,Vec* DFDX) const;
  virtual void DEDRho(scalarD& FX,ScalarField* DEDRho) const;
  virtual int operator()(const Vec& x,scalarD& FX,Vec& DFDX,const scalarD& step,bool wantGradient);
  scalarD optimizeGD();
  scalarD optimizeKKT();
  scalarD optimizeLBFGS();
private:
  //for optimizer that require only gradient
  scalarD updateGradient(sizeType i,Vec* DFDX,const ScalarField* DEDRho,scalarD regK,bool KKT) const;
  scalarD backPropGradient(sizeType i,ScalarField* DEDRho) const;
  scalarD addGradient(ScalarField* DEDRho,const ScalarField& rho,const ScalarField& rhoStar,ScalarField& restrictPool,OP op=&add) const;
  scalarD addGradientHierarchical(ScalarField* DEDRho,const ScalarField& rho,const ScalarField& rhoStar,ScalarField& restrictPool,OP op) const;
  scalarD addGradientGaussian(ScalarField* DEDRho,const ScalarField& rho,const ScalarField& rhoStar,ScalarField& restrictPool,OP op) const;
  scalarD addGradientGaussianHierarchical(ScalarField* DEDRho,const ScalarField& rho,const ScalarField& rhoStar,ScalarField& restrictPool,OP op) const;
  scalar _stepSz;
};
//we want to search for the time dependent velocity fields to minimize
//argmin \frac{_regU}{2} \sum_{i=1}^N\|u_i\|^2 +
//       \frac{_regK}{2} \sum_{i=1}^N\|v_i-v_i^*\|^2
//s.t.   \v_{i+1}-\v_{i}+Adv[v_{i+1}]*dt=u_i
class ActiveOpt : public FluidOptObjective
{
public:
  //using Traits::Vec;
  ActiveOpt(SpaceTimeMesh& mesh);
  virtual void DEDVel(scalarD& FX,Vec* DFDX);
  virtual int operator()(const Vec& x,scalarD& FX,Vec& DFDX,const scalarD& step,bool wantGradient);
  const vector<boost::shared_ptr<SpaceTimeMesh> >& levels() const;
  void optimizeGD();
  void optimizeKKT();
  void optimizeLBFGS();
  void optimizeSTMG();
  void initializeSTMG();
  void beginRHS();
  void endRHS();
  //spacetime multigrid VCycle
  scalarD smoothST(SpaceTimeMesh& mesh,sizeType nrF);
  void restrictST(const SpaceTimeMesh& currLv,SpaceTimeMesh& nextLv,OP op);
  void interpolateST(SpaceTimeMesh& currLv,const SpaceTimeMesh& nextLv,OP op);
  scalarD computeFASRhsST(SpaceTimeMesh& currLv,SpaceTimeMesh& nextLv,bool forResidual);
  //forth order polynomial coefficient build
  VecD findPolyCoef(const Vec& x0,const Vec& dir,const scalarD* FX0);
  VecD findPolyCoef(Vec VG0,const Vec& dir);
private:
  //helper
  void saveSolutionToFile();
  void loadSolutionFromFile();
  scalarD STMGVCycle(sizeType lvCurr,sizeType nrPre,sizeType nrPost,sizeType nrFinal);
  vector<boost::shared_ptr<SpaceTimeMesh> > _levelST;
  scalar _stepSz;
};
//combine passive and active formulation
class FluidOpt : public Traits
{
public:
  enum MODE {
    LBFGS,
    GD,
    KKT,
    STMG,
  };
  FluidOpt(SpaceTimeMesh& mesh);
  void fixFrame(sizeType frm,bool fix);
  void updateRefVelocity(bool updateLambda,scalar maxLambdaFrac);
  scalarD optimizePassive();
  void optimizeActive();
  void optimize();
protected:
  void addLambda(scalar coef);
  void clearLambda();
  bool checkStop();
  std::set<sizeType> _fixed;
  const boost::property_tree::ptree& _pt;
  SpaceTimeMesh& _mesh;
  PassiveOpt _PO;
  ActiveOpt _AO;
private:
  scalar _regK;
  sizeType _nrPassive;
  sizeType _nrActive;
  sizeType _passiveMode;
  vector<ScalarField> _lastRho;
};
void debugPassiveOpt(sizeType dim,sizeType advector,Vec3c periodic);
void debugActiveOpt(sizeType dim,FluidSolver::TIME_INTEGRATOR integrator,Vec3c periodic,bool useTridiag);

PRJ_END

#endif
