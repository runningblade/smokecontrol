#include "FluidState.h"
#include "CommonFile/GridOp.h"

USE_PRJ_NAMESPACE

//CurlElem
void CurlElem::evalDerivKKTBlk(scalarD coef,sizeType nrPrimal,vector<QuadraticTerm>& terms,sizeType offRowGF,sizeType offRowV,
                               MACVelocityField** vCurr,MACVelocityField** vOther,MACVelocityField** gf)
{
#define ADD_QUAD(VA,VB)  \
if(_coefV[comp] < nrPrimal) \
terms.push_back(QuadraticTerm(offRowGF+_coefV[comp],QuadraticIndex(VA,_coefV[r]),QuadraticIndex(VB,_coefV[c]),W));  \
if(gf && _coefV[r] < nrPrimal && VA == vCurr)  \
terms.push_back(QuadraticTerm(offRowV+_coefV[r],QuadraticIndex(gf,_coefV[comp]),QuadraticIndex(VB,_coefV[c]),W));  \
if(gf && _coefV[c] < nrPrimal && VB == vCurr)  \
terms.push_back(QuadraticTerm(offRowV+_coefV[c],QuadraticIndex(gf,_coefV[comp]),QuadraticIndex(VA,_coefV[r]),W));
  Vec4d coefW(_coefW[0],-_coefW[0],_coefW[1],-_coefW[1]);
  for(sizeType r=0; r<4; r++)
    for(sizeType c=0; c<4; c++)
      for(sizeType comp=0; comp<4; comp++) {
        Vec4d coefAdv=(comp < 2) ? Vec4d(0,0,coef,coef) : Vec4d(-coef,-coef,0,0);
        scalarD W=coefW[r]*coefAdv[c];
        ADD_QUAD(vCurr,vCurr)
        if(vOther) {
          ADD_QUAD(vCurr,vOther)
          ADD_QUAD(vOther,vCurr)
          ADD_QUAD(vOther,vOther)
        }
      }
#undef ADD_QUAD
}
void CurlElem::evalVel(const Vec4i& offS,const vector<VelRef>& VRefs,const MACVelocityField& v,Vec4d& vel) const
{
  for(sizeType i=0; i<4; i++) {
    const VelRef& ref=VRefs[_coefV[i]];
    vel[i]+=FETCHVELREF(v)
  }
}
//FluidState
FluidState::FluidState():Serializable(typeid(FluidState).name()) {}
FluidState::FluidState(const FluidState& other):Serializable(typeid(FluidState).name())
{
  operator=(other);
}
FluidState::~FluidState() {}
FluidState& FluidState::operator=(const FluidState& other)
{
  _pre.resize(other._pre.size());
  _v.resize(other._v.size());
  for(sizeType i=1; i<(sizeType)other._pre.size(); i*=2)
    if(other._pre[i])
      _pre[i].reset(new ScalarField(*(other._pre[i])));
  for(sizeType i=1; i<(sizeType)other._v.size(); i*=2)
    if(other._v[i])
      _v[i].reset(new MACVelocityField(*(other._v[i])));
  if(other._rho)
    _rho.reset(new ScalarField(*(other._rho)));
  else _rho.reset((ScalarField*)NULL);
  _periodic=other._periodic;
  return *this;
}
boost::shared_ptr<Serializable> FluidState::copy() const
{
  return boost::shared_ptr<Serializable>(new FluidState());
}
bool FluidState::write(ostream& os,IOData* dat) const
{
  dat->registerType<ScalarField>();
  dat->registerType<MACVelocityField>();
  writeBinaryData(_rho,os,dat);
  writeVector(_pre.getVector(),os,dat);
  writeVector(_v.getVector(),os,dat);
  writeBinaryData(_periodic,os,dat);
  return os.good();
}
bool FluidState::read(istream& is,IOData* dat)
{
  dat->registerType<ScalarField>();
  dat->registerType<MACVelocityField>();
  readBinaryData(_rho,is,dat);
  readVector(_pre.getVector(),is,dat);
  readVector(_v.getVector(),is,dat);
  readBinaryData(_periodic,is,dat);
  return is.good();
}
//setter
void FluidState::init(Vec3i nr,Vec3 len,sizeType SF,sizeType VF,bool shadow,Vec3c periodic)
{
#define INITS(S)  \
if(SF&S) {  \
if((sizeType)_pre.size() < S+1)_pre.resize(S+1); \
if(!_pre[S])_pre[S].reset(new ScalarField); \
_pre[S]->reset(nr,bb,0,true,1,shadow);}

#define INITV(V)  \
if(VF&V)  { \
if((sizeType)_v.size() < V+1)_v.resize(V+1); \
if(!_v[V])_v[V].reset(new MACVelocityField); \
_v[V]->reset(*(_pre[PRESSURE]),shadow);}

  if(nr[2] == 0 || len[2] == 0) {
    //you want two dimensional
    nr[2]=0;
    len[2]=0;
  }
  BBox<scalar> bb(Vec3::Zero(),len);
  _periodic=periodic;
  INITS(PRESSURE)
  INITS(RHS_PRESSURE)
  INITS(GHOST_FORCE_PRESSURE)
  INITS(RHS_GHOST_FORCE_PRESSURE)
  INITV(VELOCITY)
  INITV(RHS_VELOCITY)
  INITV(GHOST_FORCE)
  INITV(RHS_GHOST_FORCE)
  INITV(VELOCITY_REFERENCE)
  INITV(VELOCITY_LAMBDA)
  for(sizeType d=0; d<scal(PRESSURE).getDim(); d++)
    ASSERT((vel(VELOCITY).getComp(d).getCellSize()-scal(PRESSURE).getCellSize()).norm() < EPS)

#undef INITS
#undef INITV
  }
void FluidState::addField(sizeType SF,sizeType VF,bool shadow)
{
#define ADDS(S)  \
if(SF&S) {  \
if((sizeType)_pre.size() < S+1)_pre.resize(S+1); \
_pre[S].reset(new ScalarField); \
_pre[S]->makeSameGeometry(scal(PRESSURE),shadow);}

#define ADDV(V)  \
if(VF&V)  { \
if((sizeType)_v.size() < V+1)_v.resize(V+1); \
if(!_v[V])_v[V].reset(new MACVelocityField); \
_v[V]->reset(scal(PRESSURE),shadow);}

  ADDS(RHS_PRESSURE)
  ADDS(GHOST_FORCE_PRESSURE)
  ADDS(RHS_GHOST_FORCE_PRESSURE)
  ADDV(VELOCITY)
  ADDV(RHS_VELOCITY)
  ADDV(GHOST_FORCE)
  ADDV(RHS_GHOST_FORCE)
  ADDV(VELOCITY_REFERENCE)
  ADDV(VELOCITY_LAMBDA)

#undef INITS
#undef INITV
}
void FluidState::removeField(sizeType SF,sizeType VF)
{
#define RMOVS(S)  \
if((SF&S) && (sizeType)_pre.size() > S) \
_pre[S].reset((ScalarField*)NULL);

#define RMOVV(V)  \
if((VF&V) && (sizeType)_v.size() > V) \
_v[V].reset((MACVelocityField*)NULL);

  RMOVS(PRESSURE)
  RMOVS(RHS_PRESSURE)
  RMOVS(GHOST_FORCE_PRESSURE)
  RMOVS(RHS_GHOST_FORCE_PRESSURE)
  RMOVV(VELOCITY)
  RMOVV(RHS_VELOCITY)
  RMOVV(GHOST_FORCE)
  RMOVV(RHS_GHOST_FORCE)
  RMOVV(VELOCITY_REFERENCE)
  RMOVV(VELOCITY_LAMBDA)

#undef RMOVS
#undef RMOVV
}
void FluidState::clearField()
{
  _pre.clear();
  _v.clear();
}
void FluidState::writeVTK(const string& path,bool writeVel,bool writeRho) const
{
  if(!_rho)
    return;
  if(_rho->getDim() == 2) {
    if(writeRho)
      GridOp<scalar,scalar>::write2DScalarGridVTK(path+"rho.vtk",*_rho,false);
    if(writeVel && velPtr(VELOCITY))
      GridOp<scalar,scalar>::write2DVelocityGridVTK(path+"vel.vtk",vel(VELOCITY));
  } else if(writeRho) {
    GridOp<scalar,scalar>::write3DScalarGridVTK(path+"rho.vtk",*_rho);
  }
}
sizeType FluidState::nrCell() const
{
  return scal(PRESSURE).getNrPoint().prod();
}
sizeType FluidState::nrVelComp() const
{
  sizeType ret=0;
  for(sizeType d=0; d<vel(VELOCITY).getDim(); d++)
    ret+=vel(VELOCITY).getComp(d).getNrPoint().prod();
  return ret;
}
Traits::Vec6c FluidState::isVelValid(Vec3i curr) const
{
  Vec6c ret=Vec6c::Constant(false);
  if(!clampBoundary(curr))
    return ret;
  const MACVelocityField& v=vel(VELOCITY);
  const Vec3i& nrCell=v.getNrCell();
  for(sizeType d=0,j=0; d<v.getDim(); d++)
    if(_periodic[d]) {
      ret[j++]=true;
      ret[j++]=true;
    } else {
      ret[j++]=curr[d] > 0;
      ret[j++]=curr[d] < nrCell[d]-1;
    }
  return ret;
}
Traits::Vec6i FluidState::velComp(Vec3i curr) const
{
  Vec6i ret=Vec6i::Constant(-1);
  if(!clampBoundary(curr))
    return ret;
  sizeType off=0;
  Vec3i periodicS=getPeriodicS();
  Vec6c valid=isVelValid(curr);
  const MACVelocityField& v=vel(VELOCITY);
  for(sizeType d=0,j=0; d<v.getDim(); d++) {
    if(valid[j])
      ret[j]=v.getComp(d).getIndex(curr)+off;
    j++;
    if(valid[j])
      ret[j]=v.getComp(d).getIndex(curr+Vec3i::Unit(d),&periodicS)+off;
    j++;
    off+=v.getComp(d).getNrPoint().prod();
  }
  return ret;
}
pair<Vec3i,sizeType> FluidState::velComp(sizeType vid) const
{
  const MACVelocityField& vf=vel(VELOCITY);
  for(sizeType d=0,off=0,delta=0; d<vf.getDim(); d++) {
    delta=vf.getComp(d).getNrPoint().prod();
    if(vid < off+delta)
      return make_pair(vf.getComp(d).getIndex(vid-off),d);
    off+=delta;
  }
  ASSERT(false)
  return make_pair(Vec3i(),0);
}
bool FluidState::isBoundary(sizeType vid) const
{
  pair<Vec3i,sizeType> v=velComp(vid);
  const MACVelocityField& vf=vel(VELOCITY);
  if(v.first[v.second] == vf.getComp(v.second).getNrPoint()[v.second]-1)
    return true;
  return v.first[v.second] == 0 && !_periodic[v.second];
}
const Vec3c& FluidState::getPeriodic() const
{
  return _periodic;
}
Vec3i FluidState::getPeriodicS() const
{
  Vec3i periodicS=Vec3i::Zero();
  for(sizeType i=0; i<scal(PRESSURE).getDim(); i++)
    if(_periodic[i])
      periodicS[i]=scal(PRESSURE).getNrCell()[i];
  return periodicS;
}
Traits::Vec6 FluidState::divStencil() const
{
  Vec6 ret;
  Vec3 invCellSz=scal(PRESSURE).getInvCellSize();
  ret[0]=-invCellSz[0];
  ret[1]=invCellSz[0];
  ret[2]=-invCellSz[1];
  ret[3]=invCellSz[1];
  ret[4]=-invCellSz[2];
  ret[5]=invCellSz[2];
  return ret;
}
Traits::Vec6 FluidState::lapStencil() const
{
  Vec6 ret;
  Vec3 invCellSz=scal(PRESSURE).getInvCellSize();
  ret[0]=ret[1]=-pow(invCellSz[0],2);
  ret[2]=ret[3]=-pow(invCellSz[1],2);
  ret[4]=ret[5]=-pow(invCellSz[2],2);
  return ret;
}
//velocity
const MACVelocityField& FluidState::vel(FIELD v) const
{
  ASSERT_MSGV((sizeType)_v.size() > v && _v[v],"We cannot find VelocityField %ld",(sizeType)v)
  return *(_v[v]);
}
MACVelocityField& FluidState::velNonConst(FIELD v)
{
  ASSERT_MSGV((sizeType)_v.size() > v && _v[v],"We cannot find VelocityField %ld",(sizeType)v)
  return *(_v[v]);
}
boost::shared_ptr<MACVelocityField>& FluidState::velPtr(FIELD v)
{
  if((sizeType)_v.size() < v+1)
    _v.resize(v+1);
  return _v[v];
}
const boost::shared_ptr<MACVelocityField>& FluidState::velPtr(FIELD v) const
{
  if((sizeType)_v.size() < v+1)
    _v.resizeForce(v+1);
  return _v[v];
}
//pre
const ScalarField& FluidState::scal(FIELD s) const
{
  ASSERT_MSGV((sizeType)_pre.size() > s && _pre[s],"We cannot ScalarField %ld",(sizeType)s)
  return *(_pre[s]);
}
ScalarField& FluidState::scalNonConst(FIELD s)
{
  ASSERT_MSGV((sizeType)_pre.size() > s && _pre[s],"We cannot ScalarField %ld",(sizeType)s)
  return *(_pre[s]);
}
boost::shared_ptr<ScalarField>& FluidState::scalPtr(FIELD s)
{
  if((sizeType)_pre.size() < s+1)
    _pre.resize(s+1);
  return _pre[s];
}
const boost::shared_ptr<ScalarField>& FluidState::scalPtr(FIELD s) const
{
  if((sizeType)_pre.size() < s+1)
    _pre.resizeForce(s+1);
  return _pre[s];
}
//helper
void FluidState::setZero(sizeType f)
{
  for(sizeType i=1; i<(sizeType)_pre.size(); i*=2)
    if(_pre[i] && (f&i))
      _pre[i]->init(0);
  for(sizeType i=1; i<(sizeType)_v.size(); i*=2)
    if(_v[i] && (f&i))
      _v[i]->init(Vec3::Zero());
}
void FluidState::limitCFL(FIELD f,scalar dt,scalar CFL)
{
  if(isInf(CFL))
    return;
  MACVelocityField& v=velNonConst(f);
  Vec3 maxVel=v.getCellSize()*CFL/dt;
  for(sizeType d=0; d<v.getDim(); d++)
    v.getComp(d).clamp(maxVel[d]);
}
void FluidState::toVec(FIELD f,VecD& ret,OPD op,sizeType off,scalarD coef) const
{
  if(off == -1)
    off=0;
  const MACVelocityField& v=vel(f);
  ASSERT(v.getNrCell() == scal(PRESSURE).getNrCell())
  if(ret.size() < nrVelComp()+off)
    ret.setZero(nrVelComp()+off);
  for(sizeType d=0; d<v.getDim(); d++) {
    ITERSP(v.getComp(d),Vec3i::Zero(),1)
    op(ret[offS+off],v.getComp(d).get(offS)*coef);
    ITERSPEND
    off+=v.getComp(d).getNrPoint().prod();
  }
}
void FluidState::fromVec(FIELD f,const VecD& vec,OP op,sizeType off,scalarD coef)
{
  if(off == -1)
    off=0;
  MACVelocityField& v=velNonConst(f);
  ASSERT(v.getNrCell() == scal(PRESSURE).getNrCell())
  for(sizeType d=0; d<v.getDim(); d++) {
    ITERSP_ALL(v.getComp(d))
    op(v.getComp(d).get(offS),(scalar)(vec[offS+off]*coef));
    ITERSPEND
    ITERBDLSET(v.getComp(d),d,_periodic)
    ITERBDRSET(v.getComp(d),d)
    off+=v.getComp(d).getNrPoint().prod();
  }
}
void FluidState::toVecScal(FIELD f,VecD& ret,OPD op,sizeType off,scalarD coef) const
{
  if(off == -1)
    off=0;
  const ScalarField& s=scal(f);
  ASSERT(s.getNrCell() == scal(PRESSURE).getNrCell())
  if(ret.size() < s.getNrPoint().prod()+off)
    ret.setZero(s.getNrPoint().prod()+off);
  ITERSP_ALL(scal(PRESSURE))
  op(ret[offS+off],s.get(offS)*coef);
  ITERSPEND
}
void FluidState::fromVecScal(FIELD f,const VecD& vec,OP op,sizeType off,scalarD coef)
{
  if(off == -1)
    off=0;
  ScalarField& s=scalNonConst(f);
  ASSERT(s.getNrCell() == scal(PRESSURE).getNrCell())
  ITERSP_ALL(scal(PRESSURE))
  op(s.get(offS),(scalar)(vec[off+offS]*coef));
  ITERSPEND
}
void FluidState::save()
{
  _preSave=_pre;
  _vSave=_v;
}
void FluidState::load()
{
  _pre=_preSave;
  _v=_vSave;
  _preSave.clear();
  _vSave.clear();
}
sizeType FluidState::search(const Vec6i& vid,sizeType v)
{
  for(sizeType i=0; i<6; i++)
    if(vid[i] == v)
      return i;
  return -1;
}
//interpolate/restrict for multigrid
void FluidState::interpolateV(FluidState& finer,FIELD vCurr,FIELD vFiner,OP f,scalar coef) const
{
  ASSERT(f != &set<scalar> && _periodic == finer._periodic)
  Vec3i periodicS=getPeriodicS();
  const MACVelocityField& v=vel(vCurr);
  MACVelocityField& fv=finer.velNonConst(vFiner);
  const ScalarField& fp=finer.scal(PRESSURE);
  sizeType dim=fv.getDim();
  for(sizeType d=0; d<dim; d++) {
    ITERSP_PERIODIC(fp,_periodic)
    if(curr[d]%2 == 1)
      f(fv.getComp(d).get(curr),(v.getComp(d).get(curr/2)+v.getComp(d).get(curr/2+Vec3i::Unit(d),&periodicS))*coef/2);
    else f(fv.getComp(d).get(curr),v.getComp(d).get(curr/2)*coef);
    ITERSPEND
    //care about boundary value
    ITERBDLSET(fv.getComp(d),d,_periodic)
    ITERBDRSET(fv.getComp(d),d)
  }
}
void FluidState::interpolateS(FluidState& finer,FIELD sCurr,FIELD sFiner,OP f,scalar coef) const
{
  ASSERT(f != &set<scalar> && _periodic == finer._periodic)
  //use averaging for pressure
  const ScalarField& s=sCurr == 0 ? *_rho : scal(sCurr);
  ScalarField& fs=sFiner == 0 ? *(finer._rho) : finer.scalNonConst(sFiner);
  ITERSP_ALL(fs)
  f(fs.get(offS),s.get(curr/2)*coef);
  ITERSPEND
}
void FluidState::restrictV(const FluidState& finer,FIELD vCurr,FIELD vFiner,OP f,scalar coef)
{
  ASSERT(f != &set<scalar> && _periodic == finer._periodic)
  Vec3i periodicSC=getPeriodicS();
  MACVelocityField& v=velNonConst(vCurr);
  const MACVelocityField& fv=finer.vel(vFiner);
  const ScalarField& fp=finer.scal(PRESSURE);
  sizeType dim=fv.getDim();
  coef/=(scalar)pow(2,dim);
  for(sizeType d=0; d<dim; d++) {
    ITERSP_PERIODIC(fp,_periodic)
    restrict(v,curr,d,fv.getComp(d).get(curr)*coef,f,periodicSC);
    ITERSPEND
    //care about boundary value
    ITERBDLSET(v.getComp(d),d,_periodic)
    ITERBDRSET(v.getComp(d),d)
  }
}
void FluidState::restrictS(const FluidState& finer,FIELD sCurr,FIELD sFiner,OP f,scalar coef)
{
  ASSERT(f != &set<scalar> && _periodic == finer._periodic)
  //use averaging for pressure
  ScalarField& s=scalNonConst(sCurr);
  const ScalarField& fs=finer.scal(sFiner);
  sizeType dim=fs.getDim();
  coef/=(scalar)pow(2,dim);
  ITERSP_ALL(fs)
  restrict(s,curr,fs.get(offS)*coef,f);
  ITERSPEND
}
bool FluidState::clampBoundary(Vec3i& curr) const
{
  const Vec3i& nrCell=scal(PRESSURE).getNrPoint();
  for(sizeType d=0; d<scal(PRESSURE).getDim(); d++) {
    if(_periodic[d]) {
      while(curr[d] < 0)
        curr[d]+=nrCell[d];
      while(curr[d] >= nrCell[d])
        curr[d]-=nrCell[d];
    }
    if(!(curr[d] >= 0 && curr[d] < nrCell[d]))
      return false;
  }
  return true;
}
