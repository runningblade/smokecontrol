#include "SpaceTimeMesh.h"
#include "FluidSolverSpatialMG.h"
#include "Optimizer.h"
#include "CommonFile/GridOp.h"
#include "CommonFile/solvers/KrylovMatrix.h"
#include <iomanip>

PRJ_BEGIN

#define NTENSOR 1000
#define NCC 2
#define NC 15
#define NCMG 16
#define OTHERCELL ((j%2 == 1) ? (Vec3i)Vec3i::Unit(j/2) : (Vec3i)-Vec3i::Unit(j/2))
#define OFFFACE ((j%2 == 1) ? (Vec3i)Vec3i::Unit(j/2) : (Vec3i)Vec3i::Zero())

#define RANDOMIZEV(F) \
mesh.state(i).addField(0,F); \
mesh.state(i).toVec(F,v);  \
v=Traits::VecD::Random(v.size())*0.1f; \
mesh.state(i).fromVec(F,v);  \
mesh.solver<FluidSolver>().enforceDivFree(mesh.state(i),F); \
mesh.state(i).toVec(F,v);

#define RANDOMIZEV_ALL(F) \
mesh.state(i).addField(0,F); \
mesh.state(i).toVec(F,v);  \
v=Traits::VecD::Random(v.size())*0.1f; \
mesh.state(i).fromVec(F,v);

#define RANDOMIZES(F) \
mesh.state(i).addField(F,0); \
mesh.state(i).toVecScal(F,v);  \
v=Traits::VecD::Random(v.size())*0.1f; \
mesh.state(i).fromVecScal(F,v);

//check boundary condition
class SineFunc : public ImplicitFunc<scalar>
{
public:
  virtual scalar operator()(const Vec3& pos) const {
    return sin((pos[0]+0.125f)*M_PI*4)*cos(pos[1]*M_PI*4);
  }
};
class SineVFunc : public VelFunc<scalar>
{
public:
  virtual Vec3 operator()(const Vec3& pos) const {
    return Vec3(sin((pos[0]+0.125f)*M_PI*4),cos(pos[1]*M_PI*4),0.0f)*0.01f;
  }
};
class RandomFunc : public ImplicitFunc<scalar>
{
public:
  virtual scalar operator()(const Vec3& pos) const {
    scalar ret;
    OMP_CRITICAL_
    ret=RandEngine::randR(-1,1);
    return ret;
  }
};
//check interpolation
void debugInterp(sizeType dim,Vec3i warpMode)
{
  ScalarField val;
  BBox<scalar> bb(Vec3(-1,-1,-1),Vec3(dim >= 1 ? 1 : -1,dim >= 2 ? 1 : -1,dim >= 3 ? 1 : -1));
  val.reset(Vec3i(4,4,4),bb,0);
  for(sizeType x=0; x<val.getNrPoint()[0]; x++)
    for(sizeType y=0; y<val.getNrPoint()[1]; y++)
      for(sizeType z=0; z<val.getNrPoint()[2]; z++)
        val.get(Vec3i(x,y,z))=RandEngine::randR(-1,1);
  for(sizeType i=0; i<100; i++) {
    Vec3 pos=Vec3::Zero();
    for(sizeType d=0; d<val.getDim(); d++)
      if(warpMode[d] > 0)
        pos[d]=RandEngine::randR(-10.0f,10.0f);
      else pos[d]=RandEngine::randR(-0.5f,0.5f);
    {
      scalar coefs[8]= {0,0,0,0,0,0,0,0};
      sizeType pts[8]= {0,0,0,0,0,0,0,0};
      scalar valI=val.sampleSafe(pos,&warpMode),valIRef=0;
      sizeType nr=val.getSampleStencilSafe(pos,coefs,NULL,pts,&warpMode);
      for(sizeType i=0; i<nr; i++)
        valIRef+=coefs[i]*val.get(pts[i]);
      ASSERT(abs(valI-valIRef) < EPS)
    }
    {
      scalar coefs[4][4][4]= {
        0,0,0,0,0,0,0,0,
        0,0,0,0,0,0,0,0,
        0,0,0,0,0,0,0,0,
        0,0,0,0,0,0,0,0,
        0,0,0,0,0,0,0,0,
        0,0,0,0,0,0,0,0,
        0,0,0,0,0,0,0,0,
        0,0,0,0,0,0,0,0,
      };
      sizeType pts[4][4][4]= {
        -1,-1,-1,-1,-1,-1,-1,-1,
        -1,-1,-1,-1,-1,-1,-1,-1,
        -1,-1,-1,-1,-1,-1,-1,-1,
        -1,-1,-1,-1,-1,-1,-1,-1,
        -1,-1,-1,-1,-1,-1,-1,-1,
        -1,-1,-1,-1,-1,-1,-1,-1,
        -1,-1,-1,-1,-1,-1,-1,-1,
        -1,-1,-1,-1,-1,-1,-1,-1,
      };
      scalar valI=val.sampleSafeCubic(pos,&warpMode),valIRef=0;
      val.getSampleStencilSafeCubic(pos,coefs,pts,&warpMode);
      for(sizeType k=0; k<pow(4,val.getDim()); k++)
        valIRef+=coefs[0][0][k]*val.get(pts[0][0][k]);
      ASSERT(abs(valI-valIRef) < EPS)
#define DELTA 1E-8f
      Vec3 valG=val.sampleSafeGradCubic(pos,&warpMode),valGRef=Vec3::Zero();
      for(sizeType d=0; d<val.getDim(); d++)
        valGRef[d]=(val.sampleSafeCubic(pos+Vec3::Unit(d)*DELTA,&warpMode)-valI)/DELTA;
      ASSERT((valG-valGRef).norm() < 1E-5f)
#undef DELTA
    }
  }
}
void debugInterp()
{
  for(sizeType x=0; x<=4; x+=4) {
    debugInterp(1,Vec3i(x,0,0));
    for(sizeType y=0; y<=4; y+=4) {
      debugInterp(2,Vec3i(x,y,0));
      for(sizeType z=0; z<=4; z+=4)
        debugInterp(3,Vec3i(x,y,z));
    }
  }
}
//check boundary condition
void debugBoundaryCondition()
{
  //scalar
  Vec3i warpMode;
  ScalarField s,s2;
  s.reset(Vec3i(100,100,0),BBox<scalar>(Vec3(0,0,0),Vec3(1,1,0)),0);
  s2.reset(Vec3i(300,300,0),BBox<scalar>(Vec3(-1,-1,0),Vec3(2,2,0)),0);
  GridOp<scalar,scalar>::copyFromImplictFunc(s,SineFunc());
  GridOp<scalar,scalar>::write2DScalarGridVTK("./ScalarField.vtk",s,false);
  for(sizeType x=0; x<2; x++)
    for(sizeType y=0; y<2; y++) {
      ostringstream oss;
      warpMode[0]=(x==0) ? s.getNrPoint()[0] : 0;
      warpMode[1]=(y==0) ? s.getNrPoint()[1] : 0;
      oss << "./ScalarField<" << (warpMode[0]?"x":"-") << "," << (warpMode[1]?"y":"-") << ">.vtk";
      GridOp<scalar,scalar>::copyFromOtherGrid(s2,s,&warpMode);
      GridOp<scalar,scalar>::write2DScalarGridVTK(oss.str(),s2,false);
    }
  //velocity
  Vec3c warpModeV;
  MACVelocityField v,v2;
  v.reset(s);
  v2.reset(s2);
  GridOp<scalar,scalar>::copyVelFromFunc(v,SineVFunc());
  GridOp<scalar,scalar>::write2DVelocityGridVTK("./VelocityField.vtk",v);
  for(sizeType x=0; x<2; x++)
    for(sizeType y=0; y<2; y++) {
      ostringstream oss;
      warpModeV[0]=(x==0);
      warpModeV[1]=(y==0);
      oss << "./VelocityField<" << (warpModeV[0]?"x":"-") << "," << (warpModeV[1]?"y":"-") << ">.vtk";
      GridOp<scalar,scalar>::copyVelFromOtherGrid(v2,v,&warpModeV);
      GridOp<scalar,scalar>::write2DVelocityGridVTK(oss.str(),v2);
    }
}
//check tridiagonal solver
void debugTridiagSolver()
{
  sizeType n=8,nrB=17;
  scalarD diagP=RandEngine::randR(-1,1);
  scalarD diagD=RandEngine::randR(-1,1);
  Traits::VecD div=Traits::VecD::Random(n);
  Traits::MatdArr off(nrB);
  Traits::VecDArr rhs(nrB+1),sol(nrB+1),sol2(nrB+1);
  for(sizeType i=0; i<nrB+1; i++) {
    if(i<nrB)
      off[i].setRandom(n,n);
    rhs[i].setRandom(n+1);
  }
  Traits::triDiagKKT(diagP,diagD,div,off,rhs,sol);
  Traits::triDiagKKTReorder(diagP,diagD,div,off,rhs,sol2);
  for(sizeType i=0; i<nrB; i++)
    ASSERT_MSGV((sol2[i]-sol[i]).norm() < 1E-5f,"Error out of bound: %f",(sol2[i]-sol[i]).norm())
  }
//check iterator
void debugIterator(sizeType dim)
{
  FluidState s;
  s.init(Vec3i(NC,NC*NCC,dim == 3 ? NC*NCC*NCC : 0),Vec3::Ones(),FluidState::PRESSURE,FluidState::VELOCITY);
  const ScalarField& P=s.scal(FluidState::PRESSURE);
  const MACVelocityField& V=s.vel(FluidState::VELOCITY);
  for(sizeType d=0; d<dim; d++) {
    ITERBDL(P,d)
    ASSERT(offS == P.getIndex(curr))
    ITERBDLEND

    ITERBDR(P,d)
    ASSERT(offS == P.getIndex(curr))
    ITERBDREND

    ITERSP(P,Vec3i(5,2,3),3)
    ASSERT(offS == P.getIndex(curr))
    ITERSPEND

    ITERSVP(P,V,Vec3i(5,2,3),3)
    ASSERT(offS[0] == P.getIndex(curr))
    ASSERT(offS[1] == V.getComp(0).getIndex(curr))
    ASSERT(offS[2] == V.getComp(1).getIndex(curr))
    ASSERT(offS[3] == V.getComp(2).getIndex(curr))
    ITERSVPEND
  }
}
//check basic fluid solver
void debugFluidState(sizeType dim,Vec3c periodic)
{
  FluidState s;
  s.init(Vec3i(NC,NC*NCC,dim == 3 ? NC*NCC : 0),Vec3::Ones(),FluidState::PRESSURE,FluidState::VELOCITY,false,periodic);
  //set random
  Traits::VecD vel;
  s.toVec(FluidState::VELOCITY,vel);
  vel.setRandom();
  s.fromVec(FluidState::VELOCITY,vel);
  s.toVec(FluidState::VELOCITY,vel);
  //test
  for(sizeType i=0; i<100; i++) {
    const MACVelocityField& vf=s.vel(FluidState::VELOCITY);
    Vec3i id(RandEngine::randI(0,NC-1),RandEngine::randI(0,NC*NCC-1),dim == 3 ? RandEngine::randI(0,NC*NCC-1) : 0);
    Traits::Vec6i vid=s.velComp(id);
    for(sizeType j=0; j<dim*2; j++)
      if(vid[j] >= 0)  {
        pair<Vec3i,sizeType> v=s.velComp(vid[j]);
        //ASSERT(abs(vf.getComp(j/2)[id+OFFFACE]-vel[vid[j]]) < 1E-5f)  //this doesn't hold in periodic boundary condition
        ASSERT(abs(vf.getComp(v.second)[v.first]-vel[vid[j]]) < 1E-5f)
      } else {
        ASSERT(!s.getPeriodic()[j/2])
        if(j%2 == 0) {
          ASSERT(id[j/2] == 0)
        } else {
          ASSERT(id[j/2] == vf.getNrCell()[j/2]-1)
        }
      }
  }
}
scalarD evalAdvection(const FluidState& s,const Traits::VecD& v,const Vec3i& id,sizeType d,Vec3c periodic)
{
  scalarD ret=0;
  sizeType dim=s.scal(FluidState::PRESSURE).getDim();
  Vec3 cellSz=s.scal(FluidState::PRESSURE).getCellSize();
  Vec3 pt=s.vel(FluidState::VELOCITY).getComp(d).getPt(id);
  for(sizeType i=0; i<dim; i++) {
    Vec3 vP=s.vel(FluidState::VELOCITY).sampleSafe(pt+Vec3::Unit(i)*cellSz[i]/2,&periodic);
    Vec3 vN=s.vel(FluidState::VELOCITY).sampleSafe(pt-Vec3::Unit(i)*cellSz[i]/2,&periodic);
    ret+=(vP[d]*vP[i]-vN[d]*vN[i])/cellSz[i];
  }
  Vec3 idV=s.scal(FluidState::PRESSURE).getPt(id);
  Vec3 idVN=s.scal(FluidState::PRESSURE).getPt(id-Vec3i::Unit(d));
  for(sizeType i=0; i<dim*2; i++) {
    Vec3 pt=idV+Vec3::Unit(i/2)*(i%2 == 0 ? -0.5f : 0.5f)*cellSz[i/2];
    scalar vIdV=s.vel(FluidState::VELOCITY).sampleSafe(pt,&periodic)[i/2];

    Vec3 ptN=idVN+Vec3::Unit(i/2)*(i%2 == 0 ? -0.5f : 0.5f)*cellSz[i/2];
    scalar vIdVN=s.vel(FluidState::VELOCITY).sampleSafe(ptN,&periodic)[i/2];

    ret-=(vIdV*vIdV-vIdVN*vIdVN)/cellSz[d]/4;
  }
  return ret;
}
void debugAdvection(sizeType dim,sizeType advector,Vec3c periodic,bool doAssert)
{
  //mesh
  boost::property_tree::ptree pt;
  pt.put<scalar>("dt",0.1f);
  pt.put<scalar>("regK",0);
  pt.put<sizeType>("advector",advector);
  pt.put<sizeType>("advectionOrder",3);
  pt.put<bool>("periodicX",periodic[0]);
  pt.put<bool>("periodicY",periodic[1]);
  pt.put<bool>("periodicZ",periodic[2]);
  SpaceTimeMesh mesh(pt,false);
  mesh.init(Vec3i(32,32,dim == 3 ? 64*NCC : 0),Vec3(1,1,dim == 3 ? 1 : 0),3);
  mesh.setFrame(-1,SphereFunc(Vec3(0.5f,0.25f,0),0.1f,0.1f),true);
  //mesh.setFrame(0,SphereFunc(Vec3(0.5f,0.5f,0),0.1f,0.1f),true);
  //mesh.setFrame(1,SphereFunc(Vec3(0.5f,0.5f,0),0.1f,0.1f),true);
  mesh.setFrame(2,SphereFunc(Vec3(0.5f,0.75f,0),0.1f,0.1f),true);

  //randomize velocity
  Traits::VecD v;
  for(sizeType i=0; i<mesh.nrFrame()-1; i++) {
    mesh.state(i).toVec(FluidState::VELOCITY,v);
    v=Traits::VecD::Random(v.size())*0.1f;
    mesh.state(i).fromVec(FluidState::VELOCITY,v);
  }

#define DELTA 1E-6f
  scalarD FX;
  Traits::VecD DFDX;
  ScalarField DEDRho;
  //debug advectT/advectV
  {
    ScalarField A=*(mesh.state(0)._rho);
    ScalarField B=*(mesh.state(0)._rho);
    DEDRho=A;
    ITERSS_ALL(DEDRho)
    A.get(curr)=RandEngine::randR(-1,1);
    B.get(curr)=RandEngine::randR(-1,1);
    ITERSSEND
    MACVelocityField vel=mesh.state(0).vel(FluidState::VELOCITY);
    for(sizeType d=0; d<vel.getDim(); d++)
      ITERSS_PERIODIC(DEDRho,periodic)
      vel.getComp(d).get(curr)=RandEngine::randR(-1,1);
    ITERSSEND

    const Advector& adv=mesh.solver().advector();
    INFO("Debugging AdvectT!")
    //method A
    FX=0;
    adv.advect(A,DEDRho,vel);
    ITERSP_ALL(DEDRho,OMP_ADD(FX))
    FX+=DEDRho.get(offS)*B.get(offS);
    ITERSPEND
    //method B
    scalarD FX2=0;
    adv.advectT(B,A,vel);
    ITERSP_ALL(DEDRho,OMP_ADD(FX2))
    FX2+=A.get(offS)*B.get(offS);
    ITERSPEND
    if(doAssert)
      ASSERT(abs(FX2-FX) < 1E-3f*abs(FX))
      else INFOV("BTAdvA: %f, Err: %f",FX,(FX2-FX))

        //debug advectV
        mesh.state(0).toVec(FluidState::VELOCITY,DFDX);
    DFDX.setZero();
    adv.advectV(DFDX,B,A,vel);
    for(sizeType d=0,off=0; d<vel.getDim(); d++) {
      INFOV("Debugging AdvectV component %ld!",d)
      ITERSS_ALL(DEDRho)
      FX=0;
      adv.advect(A,DEDRho,vel);
      ITERSP_ALL(DEDRho,OMP_ADD(FX))
      FX+=DEDRho.get(offS)*B.get(offS);
      ITERSPEND

      scalar tmp=vel.getComp(d).get(curr);
      vel.getComp(d).get(curr)+=DELTA;
      FX2=0;
      adv.advect(A,DEDRho,vel);
      ITERSP_ALL(DEDRho,OMP_ADD(FX2))
      FX2+=DEDRho.get(offS)*B.get(offS);
      ITERSPEND
      scalarD grad=(FX2-FX)/DELTA,DEDV=DFDX[off+vel.getComp(d).getIndex(curr)];
      if(abs(grad) > sqrt(DELTA) || abs(grad) > sqrt(DELTA)) {
        if(doAssert)
          ASSERT(abs(grad-DEDV) < 1E-3f*abs(grad))
          else INFOV("Grad: %f, Err: %f",grad,abs(grad-DEDV))
          }
      vel.getComp(d).get(curr)=tmp;
      ITERSSEND
      off+=vel.getComp(d).getNrPoint().prod();
    }
  }
#undef DELTA
}
void debugFluidSolver(sizeType dim,Vec3c periodic)
{
  //test indexing
  debugIterator(dim);
  debugFluidState(dim,periodic);

  //construct a shadow grid
  FluidState s;
  s.init(Vec3i(NC,NC*NCC,dim == 3 ? NC*NCC : 0),Vec3::Ones(),FluidState::PRESSURE,FluidState::VELOCITY,false,periodic);

  //build advection
  Traits::Curl adv=FluidSolver::buildAdvStencil(s);
  //check that tensors should related no boundary values
  for(Traits::Curl::const_iterator beg=adv.begin(),end=adv.end(); beg!=end; beg++)
    for(sizeType i=0; i<4; i++) {
      ASSERT(!s.isBoundary(beg->VID(i)))
    }

  //construct advection stencil for an arbitrary interior cell
  Traits::VecD v;
  s.toVec(FluidState::VELOCITY,v);
  {
    v.setRandom();
    Traits::TRIPS trips;
    FluidSolver::buildDivergenceFree(s,trips);
    //check that triplets should related no boundary values
    for(Traits::TRIPS::const_iterator beg=trips.begin(),end=trips.end(); beg!=end; beg++) {
      if(beg->row() < s.nrVelComp())
        ASSERT(!s.isBoundary(beg->row()))
        if(beg->col() < s.nrVelComp())
          ASSERT(!s.isBoundary(beg->col()))
        }
    //enforce divergence free first, this is essential for the two advection operator to coincide
    Traits::VecD rhs=Traits::VecD::Zero(s.nrVelComp()+s.nrCell());
    rhs.block(0,0,v.size(),1)=v;
    v=FluidSolver::solve(s,trips,rhs);
    s.fromVec(FluidState::VELOCITY,v);
    //check divergence
    Traits::Vec6 div=s.divStencil();
    ITERSP(s.scal(FluidState::PRESSURE),Vec3i::Zero(),1)
    scalar divVal=0;
    FluidState::Vec6i vid=s.velComp(curr);
    for(sizeType j=0; j<s.scal(FluidState::PRESSURE).getDim()*2; j++)
      if(vid[j] >= 0) {
        pair<Vec3i,sizeType> v=s.velComp(vid[j]);
        divVal+=s.vel(FluidState::VELOCITY).getComp(v.second).get(v.first)*div[j];
      }
    ASSERT(abs(divVal) < EPS)
    ITERSPEND
  }

  //check equivalent of two advection operators
  Traits::VecD advV=Traits::VecD::Zero(v.size());
  for(Traits::Curl::const_iterator beg=adv.begin(),end=adv.end(); beg!=end; beg++)
    beg->eval(v,advV);
  const MACVelocityField& vel=s.vel(FluidState::VELOCITY);
  for(sizeType i=0; i<NTENSOR; i++) {
    Vec3i id(RandEngine::randI(0,NC-2),
             RandEngine::randI(0,NC*NCC-2),
             dim == 3 ? RandEngine::randI(0,NC*NCC-2) : 0);
    for(sizeType d=0,off=0; d<dim; off+=vel.getComp(d).getNrPoint().prod(),d++)
      ASSERT_MSGV(abs(evalAdvection(s,v,id,d,periodic)-advV[off+vel.getComp(d).getIndex(id)]) < 1E-3f,
                  "A: %f, B: %f",evalAdvection(s,v,id,d,periodic),advV[off+vel.getComp(d).getIndex(id)])
    }

  //debug advection
  //debugAdvection(dim,Advector::SEMI_LAGRANGIAN,periodic,false);
  debugAdvection(dim,Advector::SEMI_LAGRANGIAN_CUBIC,periodic,false);
  //debugAdvection(dim,Advector::EXPLICIT,periodic,false);
  //debugAdvection(dim,Advector::IMPLICIT,periodic,false);
}
//check spatial multigrid
scalar calcLapRhs(const FluidState& s)
{
  const ScalarField& P=s.scal(FluidState::PRESSURE);
  const ScalarField& PR=s.scal(FluidState::RHS_PRESSURE);
  Vec3i periodicS=s.getPeriodicS();

  Traits::Vec6c validVelComp;
  Traits::Vec6 lap=s.lapStencil();
  sizeType dim=P.getDim(),nrPrimal=dim*2;
  scalar errNorm=0,err=0,diag=0;
  ITERSS_ALL(P)
  diag=0;
  err=PR.get(offS);
  validVelComp=s.isVelValid(curr);
  for(sizeType j=0; j<nrPrimal; j++)
    if(validVelComp[j]) {
      err+=lap[j]*P.get(curr+OTHERCELL,&periodicS);
      diag-=lap[j];
    }
  err+=diag*P.get(offS);
  errNorm+=err*err;
  ITERSSEND
  return sqrt(errNorm);
}
void debugTransfer(FluidSolverSpatialMG& sol,sizeType dim)
{
  Traits::VecD tmp;
  Vec3 velVal=Vec3::Random();
  scalar pressureVal=RandEngine::randR(-1,1);
  FluidStateMG coarser,finer;
  for(sizeType i=1; i<(sizeType)sol.levels().size(); i++) {
    coarser=sol.levels()[i];
    finer=sol.levels()[i-1];

    //test restrict
    coarser.setZero();
    finer.velNonConst(FluidState::RHS_VELOCITY).init(velVal);
    finer.toVec(FluidState::RHS_VELOCITY,tmp);
    finer.fromVec(FluidState::RHS_VELOCITY,tmp);
    finer.scalNonConst(FluidState::RHS_PRESSURE).init(pressureVal);
    finer.toVecScal(FluidState::RHS_PRESSURE,tmp);
    finer.fromVecScal(FluidState::RHS_PRESSURE,tmp);
    coarser.restrictV(finer,FluidState::VELOCITY,FluidState::RHS_VELOCITY,&Traits::add);
    coarser.restrictS(finer,FluidState::PRESSURE,FluidState::RHS_PRESSURE,&Traits::add);
    ITERSS_ALL(coarser.scal(FluidState::PRESSURE))
    ASSERT(abs(coarser.scal(FluidState::PRESSURE).get(curr)-pressureVal) < EPS)
    ITERSSEND
    for(sizeType d=0; d<dim; d++) {
      ITERSS_ALL(coarser.vel(FluidState::VELOCITY).getComp(d))
      if(curr[d] == coarser.scal(FluidState::PRESSURE).getNrCell()[d]) {
        ASSERT(coarser.vel(FluidState::VELOCITY).getComp(d).get(curr) == 0)
      } else if(curr[d] == 0 && !sol.levels()[0].getPeriodic()[d]) {
        ASSERT(coarser.vel(FluidState::VELOCITY).getComp(d).get(curr) == 0)
      } else {
        ASSERT(abs(coarser.vel(FluidState::VELOCITY).getComp(d).get(curr)-velVal[d]) < EPS)
      }
      ITERSSEND
    }

    //test interpolate
    finer.setZero();
    coarser.velNonConst(FluidState::RHS_VELOCITY).init(velVal);
    coarser.toVec(FluidState::RHS_VELOCITY,tmp);
    coarser.fromVec(FluidState::RHS_VELOCITY,tmp);
    coarser.scalNonConst(FluidState::RHS_PRESSURE).init(pressureVal);
    coarser.toVecScal(FluidState::RHS_PRESSURE,tmp);
    coarser.fromVecScal(FluidState::RHS_PRESSURE,tmp);
    coarser.interpolateV(finer,FluidState::RHS_VELOCITY,FluidState::VELOCITY,&Traits::add);
    coarser.interpolateS(finer,FluidState::RHS_PRESSURE,FluidState::PRESSURE,&Traits::add);
    ITERSS_ALL(finer.scal(FluidState::PRESSURE))
    ASSERT(abs(finer.scal(FluidState::PRESSURE).get(curr)-pressureVal) < EPS)
    ITERSSEND
    for(sizeType d=0; d<dim; d++) {
      ITERSS_ALL(finer.vel(FluidState::VELOCITY).getComp(d))
      if(curr[d] == finer.scal(FluidState::PRESSURE).getNrCell()[d]) {
        ASSERT(finer.vel(FluidState::VELOCITY).getComp(d).get(curr) == 0)
      } else if(curr[d] == 0 && !sol.levels()[0].getPeriodic()[d]) {
        ASSERT(finer.vel(FluidState::VELOCITY).getComp(d).get(curr) == 0)
      } else if((curr[d] == 1 || curr[d] == finer.scal(FluidState::PRESSURE).getNrCell()[d]-1) && !sol.levels()[0].getPeriodic()[d]) {
        ASSERT(abs(finer.vel(FluidState::VELOCITY).getComp(d).get(curr)-velVal[d]/2) < EPS)
      } else {
        ASSERT(abs(finer.vel(FluidState::VELOCITY).getComp(d).get(curr)-velVal[d]) < EPS)
      }
      ITERSSEND
    }

    //test divergence
    coarser.calcDivergence(FluidState::PRESSURE,FluidState::VELOCITY);
    Traits::VecD DIV;
    coarser.toVecScal(FluidState::PRESSURE,DIV);
    //reference divergence
    Traits::SMat m;
    Traits::TRIPS trips;
    FluidSolver::buildDivergenceFree(coarser,trips,false);
    m.resize(coarser.nrCell(),coarser.nrVelComp());
    m.setFromTriplets(trips.begin(),trips.end());
    Traits::VecD DIVRef;
    coarser.toVec(FluidState::VELOCITY,DIVRef);
    DIVRef=(m*DIVRef).eval();
    ASSERT((DIV-DIVRef).cwiseAbs().maxCoeff() < EPS)
  }
}
void debugSmoothing(FluidSolverSpatialMG& sol,sizeType dim)
{
  Traits::VecD lhsV,rhsV;
  FluidStateMG lhs;
  for(sizeType i=0; i<(sizeType)sol.levels().size(); i++) {
    lhs=sol.levels()[i];
    sizeType nrV=lhs.nrVelComp();
    sizeType nrC=lhs.nrCell();
    //randomize RHS
    rhsV.setZero(nrV+nrC);
    rhsV.block(0,0,nrV,1).setRandom();
    lhs.fromVec(FluidState::RHS_VELOCITY,rhsV);
    lhs.toVec(FluidState::RHS_VELOCITY,rhsV);
    lhs.scalNonConst(FluidState::RHS_PRESSURE).init(0);
    //smooth reference
    lhs.velNonConst(FluidState::VELOCITY)=lhs.vel(FluidState::RHS_VELOCITY);
    lhs.scalNonConst(FluidState::PRESSURE).init(0);
    INFO("Testing SCGS Newton")
    scalar dt=0.1f;
    while(true) {
      lhs.smoothSCGS(dt/2,false);
      lhsV.setZero(nrV+nrC);
      lhs.toVec(FluidState::VELOCITY,lhsV);
      lhs.toVecScal(FluidState::PRESSURE,lhsV,&Traits::set,nrV);
      if(FluidSolver::checkConvergence(lhs,lhsV,rhsV,dt,true) < 1E-4f)
        break;
    }
    lhs.velNonConst(FluidState::VELOCITY)=lhs.vel(FluidState::RHS_VELOCITY);
    lhs.scalNonConst(FluidState::PRESSURE).init(0);
    INFO("Testing SCGS FAS")
    while(true) {
      lhs.smoothSCGS(dt,true);
      lhsV.setZero(nrV+nrC);
      lhs.toVec(FluidState::VELOCITY,lhsV);
      lhs.toVecScal(FluidState::PRESSURE,lhsV,&Traits::set,nrV);
      if(FluidSolver::checkConvergence(lhs,lhsV,rhsV,dt,true) < 1E-4f)
        break;
    }
    INFO("Testing RBGS")
    lhsV.setZero(nrC);
    lhs.setZero(FluidState::PRESSURE);
    lhs.toVecScal(FluidState::RHS_PRESSURE,lhsV);
    lhsV.setRandom();
    lhsV.array()-=lhsV.sum()/lhsV.size();
    lhs.fromVecScal(FluidState::RHS_PRESSURE,lhsV);
    while(true) {
      lhs.smoothRBGS();
      scalar err=calcLapRhs(lhs);
      INFOV("RBGS Error: %f!",err)
      if(err < 1E-4f)
        break;
    }
  }
}
Traits::VecD calcLhs(const FluidState& s,scalar coefTensor)
{
  sizeType nrC=s.nrCell();
  sizeType nrV=s.nrVelComp();
  Traits::VecD x=Traits::VecD::Zero(nrC+nrV);
  s.toVec(FluidState::VELOCITY,x);
  s.toVecScal(FluidState::PRESSURE,x,&Traits::set,nrV);

  Traits::TRIPS trips;
  Traits::VecD lhs=Traits::VecD::Zero(nrC+nrV);
  FluidSolver::buildDivergenceFree(s,trips);
  for(Traits::TRIPS::const_iterator beg=trips.begin(),end=trips.end(); beg!=end; beg++)
    lhs[beg->row()]+=x[beg->col()]*beg->value();

  FluidSolver::Curl AVCoef=FluidSolver::buildAdvStencil(s);
  for(FluidSolver::Curl::const_iterator beg=AVCoef.begin(),end=AVCoef.end(); beg!=end; beg++)
    beg->eval(x,lhs,coefTensor);
  return lhs;
}
void debugFASRhs(FluidSolverSpatialMG& sol,sizeType dim)
{
  FluidStateMG finer=sol.levels()[0];
  FluidStateMG coarser=sol.levels()[1];
  Traits::VecD tmp,tmp2;

  //randomize finer
  finer.toVec(FluidState::VELOCITY,tmp);
  tmp.setRandom();
  finer.fromVec(FluidState::VELOCITY,tmp);
  finer.toVecScal(FluidState::PRESSURE,tmp);
  tmp.setRandom();
  finer.fromVecScal(FluidState::PRESSURE,tmp);
  finer.toVec(FluidState::RHS_VELOCITY,tmp);
  tmp.setRandom();
  finer.fromVec(FluidState::RHS_VELOCITY,tmp);
  finer.toVecScal(FluidState::RHS_PRESSURE,tmp);
  tmp.setRandom();
  finer.fromVecScal(FluidState::RHS_PRESSURE,tmp);
  //randomize coarser
  coarser.toVec(FluidState::VELOCITY,tmp);
  tmp.setRandom();
  coarser.fromVec(FluidState::VELOCITY,tmp);
  coarser.toVecScal(FluidState::PRESSURE,tmp);
  tmp.setRandom();
  coarser.fromVecScal(FluidState::PRESSURE,tmp);
  coarser.toVec(FluidState::RHS_VELOCITY,tmp);
  tmp.setRandom();
  coarser.fromVec(FluidState::RHS_VELOCITY,tmp);
  coarser.toVecScal(FluidState::RHS_PRESSURE,tmp);
  tmp.setRandom();
  coarser.fromVecScal(FluidState::RHS_PRESSURE,tmp);
  //compute rhs
  scalar dt=0.1f;
  sizeType nrC=coarser.nrCell();
  sizeType nrV=coarser.nrVelComp();
  coarser.calcFASRhs(finer,dt/4);
  tmp.setZero(nrC+nrV);
  coarser.toVec(FluidState::RHS_VELOCITY,tmp);
  coarser.toVecScal(FluidState::RHS_PRESSURE,tmp,&Traits::set,nrV);
  //reference
  tmp2=calcLhs(coarser,dt/4);

  nrC=finer.nrCell();
  nrV=finer.nrVelComp();
  Traits::VecD lhsF=calcLhs(finer,dt/4);
  finer.velNonConst(FluidState::VELOCITY)=finer.vel(FluidState::RHS_VELOCITY);
  finer.fromVec(FluidState::VELOCITY,lhsF,&Traits::sub);
  finer.scalNonConst(FluidState::PRESSURE)=finer.scal(FluidState::RHS_PRESSURE);
  finer.fromVecScal(FluidState::PRESSURE,lhsF,&Traits::sub,nrV);

  nrC=coarser.nrCell();
  nrV=coarser.nrVelComp();
  coarser.setZero();
  coarser.restrictV(finer,FluidState::VELOCITY,FluidState::VELOCITY,&Traits::add);
  coarser.restrictS(finer,FluidState::PRESSURE,FluidState::PRESSURE,&Traits::add);
  coarser.toVec(FluidState::VELOCITY,tmp2,&Traits::add);
  coarser.toVecScal(FluidState::PRESSURE,tmp2,&Traits::add,nrV);
  ASSERT((tmp-tmp2).cwiseAbs().maxCoeff() < EPS)
}
Traits::VecD debugEnforceDivFree(sizeType dim,SpaceTimeMesh::MODE mode,Vec3c periodic)
{
  boost::property_tree::ptree pt;
  pt.put<scalar>("dt",0.1f);
  pt.put<scalar>("regK",1);
  pt.put<scalar>("regU",10);
  pt.put<sizeType>("solverType",mode);
  pt.put<bool>("periodicX",periodic[0]);
  pt.put<bool>("periodicY",periodic[1]);
  pt.put<bool>("periodicZ",periodic[2]);
  SpaceTimeMesh mesh(pt,false);
  mesh.init(Vec3i(16,16,dim == 3 ? 16 : 0),Vec3(1,1,dim == 3 ? 1 : 0),1);

  srand(1000);
  Traits::VecD v0,v1;
  mesh.state(0).toVec(FluidState::VELOCITY,v0);
  v0.setRandom();
  mesh.state(0).fromVec(FluidState::VELOCITY,v0);
  mesh.state(0).toVec(FluidState::VELOCITY,v0);

  mesh.solver<FluidSolver>().enforceDivFree(mesh.state(0),FluidState::VELOCITY);
  mesh.state(0).toVec(FluidState::VELOCITY,v1);
  mesh.solver<FluidSolver>().enforceDivFree(mesh.state(0),FluidState::VELOCITY,&v0);
  ASSERT_MSGV((v0-v1).cwiseAbs().maxCoeff() < 1E-8f,"Max Error: %f",(v0-v1).cwiseAbs().maxCoeff())
  return v0;
}
void debugFluidSolverSpatialMG(sizeType dim,Vec3c periodic)
{
  Traits::VecD v1=debugEnforceDivFree(dim,SpaceTimeMesh::SPATIAL_SOLVE,periodic);
  Traits::VecD v2=debugEnforceDivFree(dim,SpaceTimeMesh::SPATIAL_MG_SOLVE,periodic);
  INFOV("EnforceDiv Err: %f!",(v1-v2).norm())

  //setup
  FluidStateMG s;
  boost::property_tree::ptree pt;
  s.init(Vec3i(NCMG,NCMG*NCC,dim == 3 ? NCMG*NCC : 0),Vec3(3,6,dim == 3 ? 9 : 0),false,periodic);
  FluidSolverSpatialMG sol(pt);
  sol.reset(s);
  //test interpolation/restriction on each level
  debugTransfer(sol,dim);
  //test RHS
  debugFASRhs(sol,dim);
  //test smoothing
  debugSmoothing(sol,dim);
  //we need a uniform grid for the next test
  s.init(Vec3i(NCMG,NCMG,dim == 3 ? NCMG : 0),Vec3(3,3,dim == 3 ? 3 : 0),false,periodic);
  sol.reset(s);
  //test divergence projections
  Traits::VecD v;
  s.toVec(FluidState::VELOCITY,v);
  v.setRandom();
  s.fromVec(FluidState::VELOCITY,v);
  sol.enforceDivFree(s);
  s.calcDivergence(FluidState::PRESSURE,FluidState::VELOCITY);
  scalar maxErr=s.scal(FluidState::PRESSURE).getAbsMax();
  ASSERT(maxErr < 1E-3f)
}
//check active/passive opt
void debugSTFASRhs(ActiveOpt& opt,bool primal,bool dual)
{
  opt.beginRHS();
  const vector<boost::shared_ptr<SpaceTimeMesh> > mesh=opt.levels();
  /*for(sizeType f=0; f<mesh[0]->nrFrame(); f++) {
    ASSERT(!mesh[0]->state(f).scalPtr(FluidState::RHS_PRESSURE))
    ASSERT(!mesh[0]->state(f).scalPtr(FluidState::RHS_GHOST_FORCE_PRESSURE))
    ASSERT(!mesh[0]->state(f).velPtr(FluidState::RHS_GHOST_FORCE))
    ASSERT(!mesh[0]->state(f).velPtr(FluidState::RHS_VELOCITY))
  }*/
  for(sizeType i=1; i<(sizeType)mesh.size(); i++) {
    INFOV("Testing ST-FAS Coarsen Level: %ld",i)
    SpaceTimeMesh& curr=*(mesh[i-1]);
    SpaceTimeMesh& next=*(mesh[i]);
    for(sizeType f=0; f<next.nrFrame(); f++)
      next.state(f).setZero(FluidState::VELOCITY|FluidState::GHOST_FORCE| //lhs stuff
                            FluidState::PRESSURE|FluidState::GHOST_FORCE_PRESSURE|
                            FluidState::RHS_VELOCITY|FluidState::RHS_GHOST_FORCE| //rhs stuff
                            FluidState::PRESSURE|FluidState::RHS_GHOST_FORCE_PRESSURE);
    opt.restrictST(curr,next,&Traits::add);
    opt.computeFASRhsST(curr,next,false);
    if(primal) {
      INFO("Primal")
      for(sizeType f=0; f<next.nrFrame()-1; f++) {
        INFOV("\tResidual RHS_VELOCITY at frame %ld: %f",f,sqrt(next.state(f).vel(FluidState::RHS_VELOCITY).squaredNorm()))
        INFOV("\tResidual RHS_GHOST_FORCE_PRESSURE at frame %ld: %f",f,sqrt(next.state(f).scal(FluidState::RHS_GHOST_FORCE_PRESSURE).squaredNorm()))
      }
    }
    if(dual) {
      INFO("Dual")
      for(sizeType f=0; f<next.nrFrame()-1; f++) {
        INFOV("\tResidual RHS_GHOST_FORCE at frame %ld: %f",f,sqrt(next.state(f).vel(FluidState::RHS_GHOST_FORCE).squaredNorm()))
        INFOV("\tResidual RHS_PRESSURE at frame %ld: %f",f,sqrt(next.state(f).scal(FluidState::RHS_PRESSURE).squaredNorm()))
      }
    }
    ASSERT(!next.state(next.nrFrame()-1).scalPtr(FluidState::RHS_PRESSURE))
    ASSERT(!next.state(next.nrFrame()-1).velPtr(FluidState::RHS_GHOST_FORCE))
  }
  opt.endRHS();
}
void findJacobianBlock(const FluidState& s,const Vec3i& cellId,const KrylovMatrix<scalarD>& LHS,Traits::VecD X,Traits::VecD RHS)
{
  const ScalarField& p=s.scal(FluidState::PRESSURE);
  const MACVelocityField& v=s.vel(FluidState::VELOCITY);
  sizeType nrV=s.nrVelComp(),nrC=p.getNrPoint().prod();

  vector<sizeType> cols;
  sizeType dim=p.getDim(),nrPrimal=dim*2;
  sizeType nrPrimalBlk=RHS.size()/(nrV+nrC);
  for(sizeType d=0,off=0; d<dim; off+=v.getComp(d).getNrPoint().prod(),d++)
    for(sizeType j=0; j<2; j++) {
      Vec3i cid=cellId;
      if(j == 1)
        cid+=Vec3i::Unit(d);
      cols.push_back(off+v.getComp(d).getIndex(cid));
    }

  for(sizeType k=1; k<nrPrimalBlk; k++)
    for(sizeType j=0; j<nrPrimal; j++)
      cols.push_back(cols[j]+k*nrV);

  sizeType cid=p.getIndex(cellId);
  for(sizeType k=0; k<nrPrimalBlk; k++)
    cols.push_back(nrPrimalBlk*nrV+k*nrC+cid);

  for(sizeType r=0; r<(sizeType)cols.size(); r++) {
    Traits::VecD LHSr=Traits::VecD::Zero(X.size());
    LHSr[cols[r]]=1;
    LHSr=LHS*LHSr;
    for(sizeType c=0; c<(sizeType)cols.size(); c++)
      cout << setw(12) << LHSr[cols[c]] << " ";
    cout << endl;
    //X[cols[r]]=0;
  }
  RHS-=LHS*X;
  for(sizeType r=0; r<(sizeType)cols.size(); r++)
    cout << RHS[cols[r]] << endl;
}
void debugPassiveOpt(sizeType dim,sizeType advector,Vec3c periodic)
{
  //mesh
  boost::property_tree::ptree pt;
  pt.put<scalar>("dt",0.1f);
  pt.put<scalar>("regK",0);
  pt.put<sizeType>("advector",advector);
  pt.put<sizeType>("advectionOrder",3);
  pt.put<bool>("periodicX",periodic[0]);
  pt.put<bool>("periodicY",periodic[1]);
  pt.put<bool>("periodicZ",periodic[2]);
  pt.put<scalar>("gaussianSigma",0.2f);
  pt.put<scalar>("gaussianRange",0.25f);
  SpaceTimeMesh mesh(pt,false);
  mesh.init(Vec3i(64,64,dim == 3 ? 64 : 0),Vec3(1,1,dim == 3 ? 1 : 0),3);
  //mesh.setFrame(-1,SphereFunc(Vec3(0.5f,0.25f,0),0.1f,0.1f),true);
  //mesh.setFrame(0,SphereFunc(Vec3(0.5f,0.5f,0),0.1f,0.1f),true);
  //mesh.setFrame(1,SphereFunc(Vec3(0.5f,0.5f,0),0.1f,0.1f),true);
  //mesh.setFrame(2,SphereFunc(Vec3(0.5f,0.75f,0),0.1f,0.1f),true);
  mesh.setFrame(-1,RandomFunc(),true);
  mesh.setFrame(2,RandomFunc(),true);

  //randomize velocity
  Traits::VecD v;
  for(sizeType i=0; i<mesh.nrFrame()-1; i++) {
    mesh.state(i).toVec(FluidState::VELOCITY,v);
    v=Traits::VecD::Random(v.size())*0.1f;
    mesh.state(i).fromVec(FluidState::VELOCITY,v);
  }

#define DELTA 1E-6f
  scalarD FX;
  Traits::VecD DFDX;
  ScalarField DEDRho;
  //debug DEDRho
  PassiveOpt opt(mesh);
  opt.DEDRho(FX,&DEDRho);
  {
    ScalarField& rho0=*(mesh.state(0)._rho);
    INFO("Debugging DEDRho!")
    ITERSS_ALL(DEDRho)
    scalar tmp=rho0.get(offS);
    rho0.get(offS)+=DELTA;
    scalarD FX2;
    opt.DEDRho(FX2,NULL);
    scalarD grad=(FX2-FX)/DELTA,DEDRHO=DEDRho.get(offS);
    if(abs(grad) > sqrt(DELTA) || abs(DEDRHO) > sqrt(DELTA))
      INFOV("Grad: %f, Err: %f",grad,abs(grad-DEDRHO))
      rho0.get(offS)=tmp;
    ITERSSEND
  }
  //debug DEDV
  /*{
    opt.DEDVel(FX,&DFDX);
    for(sizeType i=0,offF=0; i<mesh.nrFrame()-1; i++) {
      MACVelocityField& vel=mesh.state(i).velNonConst(FluidState::VELOCITY);
      for(sizeType d=0,off=offF; d<vel.getDim(); d++) {
        INFOV("Debugging DEDV at frame %ld component %ld!",i,d)
        ITERSS_PERIODIC(DEDRho,mesh.state(i).getPeriodic())
        scalar tmp=vel.getComp(d).get(curr);
        vel.getComp(d).get(curr)+=DELTA;
        scalarD FX2;
        opt.DEDVel(FX2,NULL);
        scalarD grad=(FX2-FX)/DELTA,DEDV=DFDX[off+vel.getComp(d).getIndex(curr)];
        if(abs(grad) > sqrt(DELTA) || abs(DEDV) > sqrt(DELTA))
          INFOV("Grad: %f, Err: %f",grad,abs(grad-DEDV))
          vel.getComp(d).get(curr)=tmp;
        ITERSSEND
        off+=vel.getComp(d).getNrPoint().prod();
      }
      offF+=v.size();
    }
  }*/
#undef DELTA
}
void debugActivePoly(ActiveOpt& opt)
{
  Traits::VecD x,dir,DFDX;
  x.setRandom(opt.inputs());
  dir.setRandom(opt.inputs());
  opt.assembleV(dir);
  opt.assignV(dir);

  scalarD fRef=0,f=0;
  opt.assignV(x);
  opt.assembleVG(x);
  opt.operator()(x,fRef,DFDX,1,false);
  Traits::VecD coef=opt.findPolyCoef(x,dir);
  //Traits::VecD coef=opt.findPolyCoef(x.block(0,0,dir.size(),1),dir,(scalarD*)NULL);
  for(sizeType i=0; i<100; i++) {
    scalarD len=RandEngine::randR01();
    opt(x.block(0,0,dir.size(),1)+dir*len,fRef,DFDX,1,false);
    f=(((coef[0]*len+coef[1])*len+coef[2])*len+coef[3])*len+coef[4];
    INFOV("Reference function value: %f, err: %f",f,f-fRef)
  }
}
void debugActiveOpt(sizeType dim,FluidSolver::TIME_INTEGRATOR integrator,Vec3c periodic,bool useTridiag)
{
#define DELTA 1E-6f
  boost::property_tree::ptree pt;
  pt.put<scalar>("dt",0.1f);
  pt.put<scalar>("regK",1);
  pt.put<scalar>("regU",10);
  pt.put<sizeType>("timeIntegrator",integrator);
  pt.put<sizeType>("solverType",SpaceTimeMesh::SPATIAL_SOLVE);
  pt.put<bool>("periodicX",periodic[0]);
  pt.put<bool>("periodicY",periodic[1]);
  pt.put<bool>("periodicZ",periodic[2]);
  SpaceTimeMesh mesh(pt,false);
  mesh.init(Vec3i(16,16,dim == 3 ? 16*NCC : 0),Vec3(1,1,dim == 3 ? 1 : 0),10);

  scalarD FX;
  Traits::VecD DFDX,v,x;
  Traits::VecD rhs,rhs2,err;
  ActiveOpt opt(mesh);

  //randomize velocity
  for(sizeType i=0; i<mesh.nrFrame()-1; i++) {
    RANDOMIZEV(FluidState::VELOCITY_REFERENCE)
    RANDOMIZEV(FluidState::GHOST_FORCE)
    RANDOMIZEV(FluidState::VELOCITY)
  }

  //debug DEDVel
  opt.DEDVel(FX,&DFDX);
//#define DEBUG_DEDV
#ifdef DEBUG_DEDV
  for(sizeType f=0; f<mesh.nrFrame()-1; f++) {
    MACVelocityField& vel=mesh.state(f).velNonConst(FluidState::VELOCITY);
    for(sizeType d=0,off=mesh.state(0).nrVelComp()*f; d<vel.getDim(); d++) {
      INFOV("Debugging DEDV at frame %ld component %ld!",f,d)
      ITERSS(mesh.state(0).scal(FluidState::PRESSURE),Vec3i::Unit(d),1)
      scalar tmp=vel.getComp(d).get(curr);
      vel.getComp(d).get(curr)+=DELTA;
      scalarD FX2;
      opt.DEDVel(FX2,NULL);
      scalarD grad=(FX2-FX)/DELTA,DEDV=DFDX[off+vel.getComp(d).getIndex(curr)];
      if(abs(grad) > sqrt(DELTA) || abs(DEDV) > sqrt(DELTA))
        INFOV("Grad: %f, Err: %f",grad,abs(grad-DEDV))
        vel.getComp(d).get(curr)=tmp;
      ITERSSEND
      off+=vel.getComp(d).getNrPoint().prod();
    }
  }
#endif

  //debug optimization by smoothing
  {
    //setup problem
    mesh.getPt().put<scalar>("regK",100);
    mesh.getPt().put<scalar>("epsilonLBFGS",1E-10f);
    mesh.getPt().put<sizeType>("solverType",SpaceTimeMesh::SPATIAL_MG_SOLVE);
    mesh.getPt().put<bool>("useExactProjector",true);
    mesh.init(Vec3i(8,16,dim == 3 ? 8*NCC : 0),Vec3(2,2,dim == 3 ? 2 : 0),20);
    mesh.setFrameTaylorVortex();

    FluidSolverSpatialMG& sol=mesh.solver<FluidSolverSpatialMG>();
    sol.enforceDivFree(mesh.state(0));
    for(sizeType i=0; i<mesh.nrFrame()-1; i++) {
      RANDOMIZEV(FluidState::VELOCITY_REFERENCE)
      mesh.state(i).addField(FluidState::GHOST_FORCE_PRESSURE,0);
    }
    for(sizeType i=0; i<mesh.nrFrame()-2; i++)
      mesh.state(i).addField(0,FluidState::GHOST_FORCE);

    //assemble states for later use
    vector<boost::shared_ptr<FluidState> > states;
    for(sizeType i=0; i<mesh.nrFrame()-1; i++)
      states.push_back(mesh.statePtr(i,mesh.nrFrame()));

    //check stencil consistency
//#define DEBUG_STMGSMOOTH
#ifdef DEBUG_STMGSMOOTH
    for(sizeType i=0; i<mesh.nrFrame()-1; i++) {
      RANDOMIZEV_ALL(FluidState::VELOCITY)
      RANDOMIZEV_ALL(FluidState::RHS_VELOCITY)
      RANDOMIZEV_ALL(FluidState::GHOST_FORCE)
      RANDOMIZEV_ALL(FluidState::RHS_GHOST_FORCE)

      RANDOMIZES(FluidState::PRESSURE)
      RANDOMIZES(FluidState::RHS_PRESSURE)
      RANDOMIZES(FluidState::GHOST_FORCE_PRESSURE)
      RANDOMIZES(FluidState::RHS_GHOST_FORCE_PRESSURE)
    }
    sizeType f=4;//mesh.nrFrame()-2;
    //smoother
    sol.reset(mesh.state(0));
    opt.beginRHS();
    //debug using tridiagonal SCGS smoother
    //sol.solveSmooth(states);
    //debug using only spatial SCGS smoother
    sol.solveSmooth(mesh.statePtr(f-1,mesh.nrFrame()-1).get(),mesh.state(f),mesh.statePtr(f+1,mesh.nrFrame()-1).get());
    opt.endRHS();
    //reference
    sol.FluidSolver::reset(mesh.state(0));
    sol.buildSmooth(mesh.statePtr(f-1,mesh.nrFrame()-1).get(),mesh.state(f),mesh.statePtr(f+1,mesh.nrFrame()-1).get(),rhs,x);
    findJacobianBlock(mesh.state(1),Vec3i(0,2,0),sol.smoothLHS(),x,rhs);
#else
#define STYPE FluidSolverSpatialMG::SMOOTHST_LEAST_SQUARE
    //debug coefficients
    //debugActivePoly(opt);

    //optimize using FluidSolver::solveSmooth
    opt.beginRHS();
    mesh.getPt().put<scalar>("diagReg",1.01f);
    sol.FluidSolver::reset(mesh.state(0));
    for(sizeType it=0; it<1000; it++) {
      scalar deltaD=0;
      if(useTridiag) {
        deltaD+=sol.solveSmooth(states,STYPE);
      } else {
        for(sizeType i=0; i<mesh.nrFrame()-1; i++) {
          FluidState* last=mesh.statePtr(i-1,mesh.nrFrame()-1).get();
          FluidState* next=mesh.statePtr(i+1,mesh.nrFrame()-1).get();
          deltaD+=sol.solveSmooth(last,mesh.state(i),next,STYPE);
        }
      }
      INFOV("DeltaD at iteration %ld: %1.10f",it,deltaD)
      if(deltaD < 1E-8f)
        break;
    }

    //check convergence using FluidSolverMG::computeFASRhs
    for(sizeType i=0; i<mesh.nrFrame()-1; i++) {
      FluidState* last=mesh.statePtr(i-1,mesh.nrFrame()-1).get();
      FluidState* next=mesh.statePtr(i+1,mesh.nrFrame()-1).get();
      INFOV("Delta from computePrimalRhs at frame %ld: %f",i,sol.computePrimalRhs(last,mesh.state(i),next,(FluidState*)&(sol.levels()[1])));
      if(next)
        INFOV("Delta from computeDualRhs at frame %ld: %f",i,sol.computeDualRhs(mesh.state(i),*next,(FluidState*)&(sol.levels()[1])));
    }

    //check convergence using FluidSolverMG::smoothSTSCGS
    for(sizeType i=0; i<mesh.nrFrame()-1; i++) {
      FluidState* last=mesh.statePtr(i-1,mesh.nrFrame()-1).get();
      FluidState* next=mesh.statePtr(i+1,mesh.nrFrame()-1).get();
      INFOV("Delta from smoothSTSCGS at frame %ld: %f",i,sol.solveSmooth(last,mesh.state(i),next,STYPE));
    }

    //check convergence using FluidSolverMG::smoothSTTridiag
    INFOV("Delta from smoothSTTridiag: %f",sol.solveSmooth(states,STYPE));
    opt.endRHS();

    //check convergence using FluidSolver::solveSmooth
    for(sizeType i=0; i<mesh.nrFrame()-1; i++) {
      FluidState* last=mesh.statePtr(i-1,mesh.nrFrame()-1).get();
      FluidState* next=mesh.statePtr(i+1,mesh.nrFrame()-1).get();
      INFOV("Delta from solveSmooth at frame %ld: %f",i,sol.FluidSolver::solveSmooth(last,mesh.state(i),next,STYPE));
    }

    //check convergence using optimizeLBFGS
    opt.optimizeLBFGS();
#undef STYPE
#endif
  }
#undef DELTA
}
//check matrix drop-off functionality
void debugGradHess(sizeType N,sizeType stride)
{
#define DELTA 1E-7f
  sizeType NBlk=N/stride;
  ASSERT_MSGV(N%stride == 0,"N must be multiples of %ld",stride)
  Matd lhs=Matd::Random(N,N);
  Traits::VecD rhs=Traits::VecD::Random(N);
  Traits::VecD x=Traits::VecD::Random(N);
  vector<QuadraticTerm> terms;

  lhs=(lhs.transpose()+lhs).eval();
  for(sizeType r=0; r<N; r++)
    for(sizeType c=0; c<N; c++)
      if(abs(r/stride-c/stride) >= 3)
        lhs(r,c)=0;

  //add a set of symmetric-quadratic terms
  for(sizeType i=0; i<N; i++)
    for(sizeType j=0; j<300; j++) {
      sizeType iA=RandEngine::randI(0,N-1);
      sizeType iB=RandEngine::randI(0,N-1);
      sizeType iC=RandEngine::randI(0,N-1);
      scalarD coef=RandEngine::randR01();
      QuadraticIndex::MACVelocityFieldPtr* ptr=(QuadraticIndex::MACVelocityFieldPtr*)1;
      if(abs(iA/stride-iB/stride) < 3 && abs(iA/stride-iC/stride) < 3 && abs(iB/stride-iC/stride) < 3) {
        QuadraticIndex A(ptr,-1),B(ptr,-1),C(ptr,-1);
        A.setVariable(iA);
        B.setVariable(iB);
        C.setVariable(iC);
        terms.push_back(QuadraticTerm(iA,B,C,coef));
        terms.push_back(QuadraticTerm(iB,A,C,coef));
        terms.push_back(QuadraticTerm(iC,A,B,coef));
      }
    }

  //test RHS norm
  for(sizeType i=0; i<100; i++) {
    Matd lhs0=lhs,lhs1=lhs;
    Traits::VecD rhs0=rhs,rhs1=rhs;
    Traits::VecD dx=Traits::VecD::Random(N);
    QuadraticTerm::eval(terms,lhs0,rhs0,x);
    QuadraticTerm::eval(terms,lhs1,rhs1,x+dx*DELTA);
    INFOV("DRHSDx norm: %f, err: %f",(lhs0*dx).norm(),(lhs0*dx-(rhs1-rhs0)/DELTA).norm())
  }

  //test RHS LM norm
  for(sizeType i=0; i<100; i++) {
    Matd lhs0=lhs,lhs1=lhs;
    Traits::VecD rhs0=rhs,rhs1=rhs;
    Traits::VecD dx=Traits::VecD::Random(N);
    scalarD func0=QuadraticTerm::evalLM(terms,lhs0,rhs0,x);
    scalarD func1=QuadraticTerm::evalLM(terms,lhs1,rhs1,x+dx*DELTA);
    INFOV("DLMFuncDx norm: %f, err: %f",rhs0.dot(dx),(rhs0.dot(dx)-(func1-func0)/DELTA))
    INFOV("DLMHess norm: %f, err: %f",lhs0.norm(),(lhs0*dx-(rhs1-rhs0)/DELTA).norm())
  }

  //test tridiag solver
  Traits::MatdArr diag(NBlk,Matd::Zero(stride,stride));
  Traits::MatdArr offDiag(NBlk-1,Matd::Zero(stride,stride));
  Traits::MatdArr offDiag2(NBlk-2,Matd::Zero(stride,stride));
  Traits::VecDArr rhsBlk(NBlk,Traits::VecD::Zero(stride));
  Traits::VecDArr xBlk(NBlk,Traits::VecD::Zero(stride)),xBlkTmp,xBlkTmp2;
  for(sizeType r=0; r<N; r++) {
    sizeType rBlk=r/stride;
    rhsBlk[rBlk][r%stride]=rhs[r];
    xBlk[rBlk][r%stride]=x[r];
    for(sizeType c=0; c<N; c++) {
      sizeType cBlk=c/stride;
      if(rBlk == cBlk)
        diag[rBlk](r%stride,c%stride)=lhs(r,c);
      else if(rBlk == cBlk+1)
        offDiag[cBlk](r%stride,c%stride)=lhs(r,c);
      else if(rBlk == cBlk+2)
        offDiag2[cBlk](r%stride,c%stride)=lhs(r,c);
      else if(r > c)
        ASSERT_MSGV(lhs(r,c) == 0,"Incorrect left hand side pattern at (%ld,%ld)",r,c)
      }
  }

  //check banded matrix solve
  Traits::Banded bandMat=Traits::toBanded(diag,offDiag,&offDiag2),bandMatATA;
  for(sizeType i=0; i<(sizeType)bandMat[0].size(); i++)
    bandMat[0][i].diagonal().array()+=100.0f;
  Traits::bandedLLT(bandMatATA=bandMat,xBlkTmp=rhsBlk);
  Matd L=Traits::toDense(bandMat).llt().matrixL();
  INFOV("Banded Cholesky Err: %f, Ax-b Err: %f",
        (L-Traits::toDenseL(bandMatATA)).norm(),
        (Traits::toDense(bandMat)*Traits::toDense(xBlkTmp)-Traits::toDense(rhsBlk)).norm())
  Traits::bandedLUAsTridiag(bandMatATA=bandMat,xBlkTmp2=rhsBlk);
  INFOV("Banded LU as Tridiag Err: %f",(Traits::toDense(xBlkTmp2)-Traits::toDense(xBlkTmp)).norm());

  //check ATA
  bandMat=Traits::toBanded(diag,offDiag,&offDiag2);
  INFOV("Banded vs. Dense ATAErr: %f",(Traits::toDense(Traits::multATA(bandMatATA=bandMat))-lhs*lhs).norm());

  //check quadratic Hessian computation
  bandMat=Traits::toBanded(diag,offDiag,&offDiag2);
  QuadraticTerm::evalLM(terms,lhs,rhs,x);
  QuadraticTerm::evalLM(terms,bandMat,rhsBlk,xBlk);
  INFOV("Banded vs. Dense LHSErr: %f, RHSErr: %f",(lhs-Traits::toDense(bandMat)).norm(),(rhs-Traits::toDense(rhsBlk)).norm())
#undef DELTA
}
void debugSolveLM(sizeType N,sizeType stride)
{
  sizeType NBlk=N/stride;
  ASSERT_MSGV(N%stride == 0,"N must be multiples of %ld",stride)

  //build problem
  Traits::VecDArr xB(NBlk),rhsB(NBlk);
  Traits::MatdArr diag(NBlk),offDiag(NBlk-1),offDiag2(NBlk-2);
  for(sizeType i=0; i<NBlk; i++) {
    xB[i].setRandom(stride);
    rhsB[i].setRandom(stride);
    diag[i].setRandom(stride,stride);
    diag[i]=(diag[i].transpose()+diag[i]).eval();
    //diag[i].diagonal().array()+=100;
    if(i<NBlk-1)
      offDiag[i].setRandom(stride,stride);
    if(i<NBlk-2)
      offDiag2[i].setRandom(stride,stride);
  }
  //add a set of symmetric-quadratic terms
  vector<QuadraticTerm> terms;
  for(sizeType i=0; i<N; i++)
    for(sizeType j=0; j<300; j++) {
      sizeType iA=RandEngine::randI(0,N-1);
      sizeType iB=RandEngine::randI(0,N-1);
      sizeType iC=RandEngine::randI(0,N-1);
      scalarD coef=RandEngine::randR01();
      QuadraticIndex::MACVelocityFieldPtr* ptr=(QuadraticIndex::MACVelocityFieldPtr*)1;
      if(abs(iA/stride-iB/stride) < 3 && abs(iA/stride-iC/stride) < 3 && abs(iB/stride-iC/stride) < 3) {
        QuadraticIndex A(ptr,-1),B(ptr,-1),C(ptr,-1);
        A.setVariable(iA);
        B.setVariable(iB);
        C.setVariable(iC);
        terms.push_back(QuadraticTerm(iA,B,C,coef));
        terms.push_back(QuadraticTerm(iB,A,C,coef));
        terms.push_back(QuadraticTerm(iC,A,B,coef));
      }
    }

  //solve using normal solver
  Traits::VecD xD=Traits::toDense(xB);
  QuadraticTerm::solveLM(terms,Traits::toDense(diag,offDiag,&offDiag2),Traits::toDense(rhsB),xD,true,false);
  //solve using tridiagonal solver
  QuadraticTerm::solveLM(terms,diag,offDiag,offDiag2,rhsB,xB,true,false);
}

#undef NTENSOR
#undef NCC
#undef NC
#undef NCMG
#undef OFFFACE
#undef OFFCELL

#undef RANDOMIZEV
#undef RANDOMIZEV_ALL
#undef RANDOMIZES

PRJ_END
