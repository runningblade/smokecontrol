#ifndef FLUID_SOLVER_SPATIAL_MG_H
#define FLUID_SOLVER_SPATIAL_MG_H

#include "FluidSolver.h"
#include "CellStencil.h"

PRJ_BEGIN

struct FluidStateMG : public FluidState {
  virtual void init(Vec3i nr,Vec3 len,bool shadow=false,Vec3c periodic=Vec3c::Constant(false));
  void calcDivergence(FIELD target,FIELD source,VecD* v=NULL);
  //for spacetime FAS solve
  void initSTSCGS(scalar dt,scalar regKByRegU,FluidSolver::TIME_INTEGRATOR mode,sizeType nrFrm);
  scalar smoothSTSCGS(FluidState* last,FluidState* next,scalar dt,FluidSolver::TIME_INTEGRATOR mode);
  scalar smoothSTSCGSQuadratic(FluidState* last,FluidState* next);
  scalar smoothSTTridiag(vector<boost::shared_ptr<FluidState> > states,FluidSolver::TIME_INTEGRATOR mode);
  scalar smoothSTTridiagQuadratic(vector<boost::shared_ptr<FluidState> > states);
  scalar calcPrimalFASRhs(const FluidState* last,const FluidState* next,FluidState* target,scalar dt,scalar regKByRegU,FluidSolver::TIME_INTEGRATOR mode);
  scalar calcDualFASRhs(const FluidState* next,FluidState* target,scalar dt,FluidSolver::TIME_INTEGRATOR mode);
  //for full approximation scheme (FAS)
  scalar calcFASRhs(const FluidStateMG& finer,scalar coefTensor);
  void smoothSCGS(scalar coefTensor,bool FAS);
  //for enforce div free
  scalar calcDivRhs(const FluidStateMG& finer);
  void smoothRBGS();
  void project(VecD* v);
  //stencil extraction for debug
  Vec3i getStencilCell(sizeType off) const;
  sizeType getStencilId(const Vec3i& cid) const;
  const CellStencil& getStencil(const Vec3i& cid) const;
  CellStencil& getStencil(const Vec3i& cid);
  DECL_NEIGH_MEMBER(const,)
private:
  //for smoothing
  scalar solveSTSCGS(Matd& lhsB,VecD& rhsB,VecD& xB,Vec6c& validVelComp,const Vec6& div,const Vec3i& curr,const Vec4i& offS,bool midP,scalar coefTensor,
                     scalar dt,sizeType lhsSTId,sizeType nrPrimal,sizeType primalLast,sizeType primalNext,sizeType offPrimal,sizeType dualLast,sizeType dualNext);
  scalar solveSTSCGSQuadratic(vector<QuadraticTerm>& terms,Matd& lhsB,VecD& rhsB,VecD& xB,sizeType lhsSTId,const Vec3i& curr,const Vec4i& offS,bool solveLM);
  scalar solveSTTridiag(vector<const Matd*>& diag,MatdArr offDiag,VecDArr& rhs,VecDArr& sol,Vec6c& validVelComp,const Vec6& div,const Vec3i& curr,const Vec4i& offS,
                        bool midP,scalar coefTensor,sizeType nrPrimal,sizeType nrS);
  scalar solveSTTridiagQuadratic(vector<QuadraticTerm>& terms,MatdArr& diag,MatdArr& offDiag,MatdArr& offDiag2,VecDArr& rhs,VecDArr& sol,const Vec3i& curr,const Vec4i& offS);
  //data
  vector<CellStencil> _stencils;
  static const Vec3i _start[8];
};
class FluidSolverSpatialMG : public FluidSolver
{
public:
  FluidSolverSpatialMG(const boost::property_tree::ptree& pt);
  virtual void reset(const FluidState& s);
  virtual void solve(FluidState& from,FluidState& to);
  virtual bool enforceDivFree(FluidState& from,FluidState::FIELD f=FluidState::VELOCITY,VecD* v=NULL,FluidState::FIELD fp=FluidState::NO_FIELD);
  virtual scalarD solveSmooth(vector<boost::shared_ptr<FluidState> > states,SMOOTHST_TYPE type);
  virtual scalarD solveSmooth(FluidState* last,FluidState& curr,FluidState* next,SMOOTHST_TYPE type);
  scalarD computePrimalRhs(const FluidState* last,FluidState& curr,const FluidState* next,FluidState* target);
  scalarD computeDualRhs(FluidState& curr,const FluidState& next,FluidState* target);
  const vector<FluidStateMG>& levels() const;
private:
  bool solveInner(FluidState& from,FluidState& to,scalar dt);
  static scalar FASVCycle(vector<FluidStateMG>& levels,sizeType lvCurr,sizeType nrPre,sizeType nrPost,sizeType nrFinal,scalar coefTensor);
  static scalar VCycle(vector<FluidStateMG>& levels,sizeType lvCurr,sizeType nrPre,sizeType nrPost,sizeType nrFinal);
  //data
  vector<FluidStateMG> _level;
};
void debugFluidSolverSpatialMG(sizeType dim,Vec3c periodic);

PRJ_END

#endif
