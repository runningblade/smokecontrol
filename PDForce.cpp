#include "PDForce.h"
#include "Advector.h"
#include "FluidMacros.h"
#include "FluidTraits.h"
#include "SpaceTimeMesh.h"
#include "CommonFile/GridOp.h"
#include "CommonFile/solvers/PMinresQLP.h"
#include <boost/property_tree/ptree.hpp>

USE_PRJ_NAMESPACE

#define FAST_GAUSSIAN
void solveLaplacian(ScalarField& v,scalarD diffCoef)
{
  Vec3 coef=(v.getInvCellSize().array()*v.getInvCellSize().array()).matrix()*diffCoef;
  ASSERT_MSG(v.getAlign() == 1,"solveLaplacian only support ScalarField without alignment")

  //build system
  ParallelVector<Eigen::Triplet<scalarD,sizeType> > TRIPS;
  ITERSP_ALL(v)
  scalarD diag=1;
  sizeType row=v.getIndex(curr);
  for(sizeType d=0; d<v.getDim(); d++) {
    if(v.isSafeIndex(curr-Vec3i::Unit(d)))
      TRIPS.push_back(Eigen::Triplet<scalarD,sizeType>(row,v.getIndex(curr-Vec3i::Unit(d)),-coef[d]));
    diag+=coef[d];
    if(v.isSafeIndex(curr+Vec3i::Unit(d)))
      TRIPS.push_back(Eigen::Triplet<scalarD,sizeType>(row,v.getIndex(curr+Vec3i::Unit(d)),-coef[d]));
    diag+=coef[d];
  }
  TRIPS.push_back(Eigen::Triplet<scalarD,sizeType>(row,row,diag));
  ITERSPEND
  FixedSparseMatrix<scalarD,Kernel<scalarD> > smat;
  smat.resize(v.data().size(),v.data().size());
  smat.buildFromTriplets(TRIPS.begin(),TRIPS.end());

  //solve
  FluidSolver::VecD rhs=FluidSolver::VecD::Zero(v.data().size()),ret=rhs;
  ITERSP_ALL(v)
  rhs[v.getIndex(curr)]=v.get(curr);
  ITERSPEND
  PMINRESSolverQLP<scalarD,Kernel<scalarD> > sol;
  sol.setMatrix(smat,true);
  sol.solve(rhs,ret);
  //cout << ret.norm() << " " << rhs.norm() << endl;
  ITERSP_ALL(v)
  v.get(curr)=ret[v.getIndex(curr)];
  ITERSPEND
}
PDForce::PDForce(scalar vf,scalar vg,scalar vd):_vf(vf),_vg(vg),_vd(vd) {}
bool PDForce::setTarget(sizeType frame,const SpaceTimeMesh& mesh)
{
  _rhoStar.reset((ScalarField*)NULL);
  sizeType nextFrame=ScalarUtil<sizeType>::scalar_max;
  const KeyFrameMap& keyMap=mesh.keyFrameMap();
  for(KeyFrameMap::const_iterator beg=keyMap.begin(),end=keyMap.end(); beg!=end; beg++)
    if(beg->first >= frame && beg->first < nextFrame) {
      nextFrame=beg->first;
      _rhoStar=beg->second;
    }
  if(nextFrame != _keyFrameId) {
    vector<scalar> stencil[3];
    gaussian(_rhoStarT,*_rhoStar,NULL,mesh,stencil);
    //GridOp<scalar,scalar>::write2DScalarGridVTK("./rhoStar.vtk",*_rhoStar,false);
    //GridOp<scalar,scalar>::write2DScalarGridVTK("./rhoStarT.vtk",_rhoStarT,false);
    _keyFrameId=nextFrame;
  }
  return _rhoStar.get() != NULL;
}
void PDForce::addForce(FluidState& from,const SpaceTimeMesh& mesh)
{
  MACVelocityField& force=from.velNonConst(FluidState::GHOST_FORCE);
  force.init(Vec3::Zero());
  if(!_rhoStar)
    return;

  //initialize
  Vec3i wrapMode=from.getPeriodicS();
  sizeType dim=from._rho->getDim();
  Vec3 invCellSz=from._rho->getInvCellSize();

  //apply gaussian to rho/rhoStar
  vector<scalar> stencil[3];
  gaussian(_rhoT,*(from._rho),NULL,mesh,stencil);
  ITERSVP(_rhoStarT,force,Vec3i::Zero(),1)
  for(sizeType d=0; d<dim; d++) {
    scalar starB=max<scalar>(_rhoStarT.get(curr),1E-3f);
    scalar starA=max<scalar>(_rhoStarT.get(curr-Vec3i::Unit(d),&wrapMode),1E-3f);

    scalar B=_rhoT.getSafe(curr);
    scalar A=_rhoT.getSafe(curr-Vec3i::Unit(d),&wrapMode);

    force.getComp(d).getSafe(curr)=(starB-starA)*invCellSz[d]/((starA+starB)/2);
    force.getComp(d).getSafe(curr)*=(A+B)/2*_vf;
  }
  ITERSVPEND
  //GridOp<scalar,scalar>::write2DScalarGridVTK("./grid0.vtk",*_rhoStar,false);
  //GridOp<scalar,scalar>::write2DScalarGridVTK("./grid.vtk",rhoStarT,false);

  //enforce boundary
  for(sizeType d=0; d<dim; d++) {
    ITERBDLSET(force.getComp(d),d,from.getPeriodic())
    ITERBDRSET(force.getComp(d),d)
  }
}
void PDForce::addGather(FluidState& to,const SpaceTimeMesh& mesh)
{
  MACVelocityField gather=to.velNonConst(FluidState::VELOCITY);
  gather.init(Vec3::Zero());
  if(!_rhoStar)
    return;

  //initialize
  Vec3i wrapMode=to.getPeriodicS();
  sizeType dim=to._rho->getDim();
  Vec3 invCellSz=to._rho->getInvCellSize();

  //build gather velocity
  vector<scalar> stencil[3];
  gaussian(_rhoT,*(to._rho),NULL,mesh,stencil);
  ITERSVP(_rhoStarT,gather,Vec3i::Zero(),1)
  for(sizeType d=0; d<dim; d++) {
    scalar starB=_rhoStarT.getSafe(curr);
    scalar starA=_rhoStarT.getSafe(curr-Vec3i::Unit(d),&wrapMode);

    scalar B=to._rho->getSafe(curr)-_rhoStar->getSafe(curr);
    scalar A=to._rho->getSafe(curr-Vec3i::Unit(d),&wrapMode)-_rhoStar->getSafe(curr-Vec3i::Unit(d),&wrapMode);

    //scalar B=_rhoT.getSafe(curr)-_rhoStarT.getSafe(curr);
    //scalar A=_rhoT.getSafe(curr-Vec3i::Unit(d),&wrapMode)-_rhoStarT.getSafe(curr-Vec3i::Unit(d),&wrapMode);

    gather.getComp(d).getSafe(curr)=-(B-A)*invCellSz[d]*((starA+starB)/2);
    gather.getComp(d).getSafe(curr)*=_vg;
  }
  ITERSVPEND

  //the advector scheme
  boost::property_tree::ptree pt;
  pt.put<scalar>("dt",mesh.getPt().get<scalar>("dt",0.0f));
  ScalarField tmp=*(to._rho);
  Advector adv(pt);
  adv.reset(to);

  //apply gather
  /*pt.put<sizeType>("advector",Advector::EXPLICIT);
  scalar DX=gather.getCellSize().block(0,0,dim,1).minCoeff();
  scalar V=max<scalar>(gather.getAbsMax().maxCoeff(),1E-3f);
  scalar maxDt=DX*0.6f/V,dt=mesh.getPt().get<scalar>("dt",0.0f);
  while(dt > maxDt) {
    pt.put<scalar>("dt",maxDt);
    adv.advect(*(to._rho),tmp,gather);
    *(to._rho)=tmp;
    dt-=maxDt;
  }
  pt.put<scalar>("dt",dt);
  adv.advect(*(to._rho),tmp,gather);
  to._rho->swap(tmp);*/

  pt.put<sizeType>("advector",Advector::IMPLICIT);
  pt.put<sizeType>("currentFrameId",0);
  adv.advect(*(to._rho),tmp,gather);
  to._rho->swap(tmp);
}
void PDForce::addDiffuse(FluidState& to,const SpaceTimeMesh& mesh)
{
  MACVelocityField& vel=to.velNonConst(FluidState::VELOCITY);
  for(sizeType d=0; d<vel.getDim(); d++)
    solveLaplacian(vel.getComp(d),_vd*mesh.getPt().get<scalar>("dt"));
  //vel.mul(exp(-_vd*mesh.getPt().get<scalar>("dt")));
}
scalarD PDForce::gaussian(ScalarField& to,const ScalarField& from,const ScalarField* fromDiff,const SpaceTimeMesh& mesh,vector<scalar> stencil[3])
{
  //calculate Gaussian stencil
  Vec3i off,len;
  buildGaussianStencil(stencil,len,from,mesh.getPt());
  Vec3i wrapMode=mesh.state(0).getPeriodicS();

  //apply Gaussian Smoothing, get energy
  scalarD ret=0;
  to.makeSameGeometry(from);
#ifdef FAST_GAUSSIAN
  //init
  off.setZero();
  ScalarField gaussian1D=from;
  if(fromDiff)
    gaussian1D.sub(*fromDiff);
  //directional gaussian
  for(sizeType d=0; d<from.getDim(); d++) {
    to.init(0);
    ret=0;
    ITERSP(to,Vec3i::Zero(),1,OMP_FPRI(off) OMP_ADD(ret))
    scalar& val=to.get(offS);
    for(off[d]=-len[d]; off[d]<=len[d]; off[d]++) {
      sizeType offStencil=from.getIndexSafe(curr+off,&wrapMode);
      val+=gaussian1D.get(offStencil)*stencil[d][off[d]+len[d]];
    }
    ret+=val*val;
    ITERSPEND
    if(d < from.getDim()-1)
      gaussian1D.swap(to);
  }
#else
  ITERSP(to,Vec3i::Zero(),1,OMP_PRI(off) OMP_ADD(ret))
  scalar val=0;
  for(off[0]=-len[0]; off[0]<=len[0]; off[0]++)
    for(off[1]=-len[1]; off[1]<=len[1]; off[1]++)
      for(off[2]=-len[2]; off[2]<=len[2]; off[2]++) {
        sizeType offStencil=from.getIndexSafe(curr+off,&wrapMode);
        scalar tmp=from.get(offStencil);
        if(fromDiff)
          tmp-=fromDiff->get(offStencil);
        tmp*=stencil[0][off[0]+len[0]];
        tmp*=stencil[1][off[1]+len[1]];
        tmp*=stencil[2][off[2]+len[2]];
        val+=tmp;
      }
  to.get(offS)=val;
  ret+=val*val;
  ITERSPEND
#endif
  return ret;
}
void PDForce::gaussianT(ScalarField& to,const ScalarField& from,const SpaceTimeMesh& mesh,OP op,vector<scalar> stencil[3])
{
  Vec3i off,len;
  buildGaussianStencil(stencil,len,from,mesh.getPt());
  Vec3i wrapMode=mesh.state(0).getPeriodicS();

  to.makeSameGeometry(from);
#ifdef FAST_GAUSSIAN
  //init
  off.setZero();
  ScalarField gaussian1D=from;
  //directional gaussian
  for(sizeType d=0; d<from.getDim(); d++) {
    to.init(0);
    ITERSP(to,Vec3i::Zero(),1,OMP_FPRI(off))
    scalar G=gaussian1D.get(offS);
    for(off[d]=-len[d]; off[d]<=len[d]; off[d]++)
      op(to.getSafe(curr+off,&wrapMode),G*stencil[d][off[d]+len[d]]);
    ITERSPEND
    if(d < from.getDim()-1)
      gaussian1D.swap(to);
  }
#else
  ITERSP(from,Vec3i::Zero(),1,OMP_PRI(off))
  scalar G=from.get(offS);
  for(off[0]=-len[0]; off[0]<=len[0]; off[0]++)
    for(off[1]=-len[1]; off[1]<=len[1]; off[1]++)
      for(off[2]=-len[2]; off[2]<=len[2]; off[2]++) {
        scalar tmp=stencil[0][off[0]+len[0]]*G;
        tmp*=stencil[1][off[1]+len[1]];
        tmp*=stencil[2][off[2]+len[2]];
        op(to.getSafe(curr+off,&wrapMode),tmp);
      }
  ITERSPEND
#endif
}
void PDForce::buildGaussianStencil(vector<scalar> stencil[3],Vec3i& len,const ScalarField& rho,const boost::property_tree::ptree& pt)
{
  scalar sigma=pt.get<scalar>("gaussianSigma",0.1f);
  scalar range=pt.get<scalar>("gaussianRange",0.2f);
  for(sizeType d=0; d<3; d++)
    if(d < rho.getDim()) {
      scalar total=0;
      sizeType N=max<sizeType>((sizeType)(range*rho.getInvCellSize()[d]),3);
      stencil[d].resize(N*2+1);
      for(sizeType s=0; s<=N; s++)
        stencil[d][N-s]=stencil[d][N+s]=exp(-(scalar)s*sigma);
      for(sizeType s=0; s<=2*N; s++)
        total+=stencil[d][s];
      for(sizeType s=0; s<=2*N; s++)
        stencil[d][s]/=total;
      len[d]=N;
    } else {
      stencil[d].assign(1,1);
      len[d]=0;
    }
}
