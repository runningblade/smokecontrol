#include "CellStencil.h"
#include "FluidMacros.h"
#include "FluidState.h"
#include "poly34.h"

USE_PRJ_NAMESPACE

//velocity component reference
VelRef::VelRef(sizeType velComp,sizeType cellOff)
  :_velComp(velComp),_cellOff(cellOff) {}
bool VelRef::operator==(const VelRef& other) const
{
  return _velComp == other._velComp && _cellOff == other._cellOff;
}
bool VelRef::operator!=(const VelRef& other) const
{
  return !operator==(other);
}
//index into a certain grid cell
QuadraticIndex::QuadraticIndex()
  :_v(NULL),_p(NULL),_id(-1),_varId(-1) {}
QuadraticIndex::QuadraticIndex(MACVelocityFieldPtr* v,sizeType id)
  :_v((ConstMACVelocityFieldPtr*)v),_p(NULL),_id(id),_varId(-1) {}
QuadraticIndex::QuadraticIndex(ScalarFieldPtr* p,sizeType id)
  :_v(NULL),_p((ConstScalarFieldPtr*)p),_id(id),_varId(-1) {}
QuadraticIndex::QuadraticIndex(ConstMACVelocityFieldPtr* v,sizeType id)
  :_v(v),_p(NULL),_id(id),_varId(-1) {}
QuadraticIndex::QuadraticIndex(ConstScalarFieldPtr* p,sizeType id)
  :_v(NULL),_p(p),_id(id),_varId(-1) {}
bool QuadraticIndex::isVariable() const
{
  return (_v || _p) && _varId >= 0;
}
void QuadraticIndex::setVariable(sizeType varId)
{
  ASSERT(varId >= -1)
  _varId=varId;
}
QuadraticIndex::operator sizeType() const
{
  return (sizeType)_varId;
}
bool QuadraticIndex::operator<(const QuadraticIndex& other) const
{
  //if(_varId < other._varId)
  //  return true;
  //else if(_varId > other._varId)
  //  return false;

  if(_id < other._id)
    return true;
  else if(_id > other._id)
    return false;

  if(_v < other._v)
    return true;
  else if(_v > other._v)
    return false;

  if(_p < other._p)
    return true;
  else if(_p > other._p)
    return false;

  return false;
}
bool QuadraticIndex::operator==(const QuadraticIndex& other) const
{
  return !this->operator<(other) && !other.operator<(*this);
}
bool QuadraticIndex::operator!=(const QuadraticIndex& other) const
{
  return !operator==(other);
}
scalar* QuadraticIndex::evalPtr(const Vec4i& offS,const CellStencil& stencil) const
{
  //ASSERT(_v || _p)
  if(_v && *_v) {
    const VelRef& ref=stencil._velComps[_id];
    const scalar& ret=FETCHVELREF(**_v);
    return (scalar*)&ret;
  } else if(_p && *_p) {
    const scalar& ret=(*_p)->get(offS[0]+stencil._otherCell[_id]);
    return (scalar*)&ret;
  } else {
    return NULL;
  }
}
scalar QuadraticIndex::eval(const Vec4i& offS,const CellStencil& stencil) const
{
  scalar* ptr=evalPtr(offS,stencil);
  return ptr ? *ptr : 0;
}
//terms that are quadratic in other variables
QuadraticTerm::QuadraticTerm():_row(-1),_coef(0) {}
QuadraticTerm::QuadraticTerm(sizeType row,QuadraticIndex B,scalarD coef)
  :_row(row),_iA(QuadraticIndex()),_iB(B),_coef(coef)
{
  if(_iB < _iA)swap(_iA,_iB);
}
QuadraticTerm::QuadraticTerm(sizeType row,QuadraticIndex A,QuadraticIndex B,scalarD coef)
  :_row(row),_iA(A),_iB(B),_coef(coef)
{
  if(_iB < _iA)swap(_iA,_iB);
}
bool QuadraticTerm::isQuadratic() const
{
  return _iA.isVariable() && _iB.isVariable();
}
bool QuadraticTerm::isLinear() const
{
  return _iA.isVariable() != _iB.isVariable();
}
bool QuadraticTerm::isConstant() const
{
  return !_iA.isVariable() && !_iB.isVariable();
}
bool QuadraticTerm::operator<(const QuadraticTerm& other) const
{
  if(_row < other._row)
    return true;
  else if(_row > other._row)
    return false;

  if(_iA < other._iA)
    return true;
  else if(other._iA < _iA)
    return false;

  if(_iB < other._iB)
    return true;
  else if(other._iB < _iB)
    return false;

  return false;
}
bool QuadraticTerm::operator==(const QuadraticTerm& other) const
{
  return !this->operator<(other) && !other.operator<(*this);
}
bool QuadraticTerm::operator!=(const QuadraticTerm& other) const
{
  return !operator==(other);
}
void QuadraticTerm::remap(const Vec4i& offS,const CellStencil& stencil)
{
  if(!_iA.isVariable() && (_iA._v || _iA._p)) {
    _coef*=_iA.eval(offS,stencil);
    _iA=QuadraticIndex();
  }
  if(!_iB.isVariable() && (_iB._v || _iB._p)) {
    _coef*=_iB.eval(offS,stencil);
    _iB=QuadraticIndex();
  }
  //it is designed that way
  if(_iB < _iA)
    swap(_iA,_iB);
}
//helper
void QuadraticTerm::prune(vector<QuadraticTerm>& terms)
{
  sizeType j=0;
  sort(terms.begin(),terms.end());
  for(sizeType i=0; i<(sizeType)terms.size(); i++)
    if(j == 0)
      terms[j++]=terms[i];
    else if(terms[j-1] == terms[i])
      terms[j-1]._coef+=terms[i]._coef;
    else {
      while(j > 0 && terms[j-1]._coef == 0)
        j--;
      terms[j++]=terms[i];
    }
  terms.resize(j);
  for(sizeType i=1; i<(sizeType)terms.size(); i++) {
    ASSERT(terms[i-1] < terms[i])
  }
}
void QuadraticTerm::labelVariable(vector<QuadraticTerm>& terms,QuadraticIndex id)
{
  for(sizeType i=0; i<(sizeType)terms.size(); i++) {
    if(terms[i]._iA == id)
      terms[i]._iA.setVariable(id);
    if(terms[i]._iB == id)
      terms[i]._iB.setVariable(id);
  }
}
//dense matrix operators
void QuadraticTerm::eval(const vector<QuadraticTerm>& terms,Matd& lhsB,VecD& rhsB,const VecD& xB)
{
  rhsB+=lhsB*xB;
  for(sizeType i=0; i<(sizeType)terms.size(); i++) {
    ASSERT(terms[i].isQuadratic())
    lhsB(terms[i]._row,terms[i]._iA)+=terms[i]._coef*xB[terms[i]._iB];
    lhsB(terms[i]._row,terms[i]._iB)+=terms[i]._coef*xB[terms[i]._iA];
    rhsB[terms[i]._row]+=terms[i]._coef*xB[terms[i]._iB]*xB[terms[i]._iA];
  }
}
scalarD QuadraticTerm::evalLM(const vector<QuadraticTerm>& terms,Matd& lhsB,VecD& rhsB,const VecD& xB,bool JTJOnly)
{
  eval(terms,lhsB,rhsB,xB);
  VecD rhsB0=rhsB;

  rhsB=lhsB.transpose()*rhsB;
  lhsB=lhsB.transpose()*lhsB;
  if(!JTJOnly) {
    for(sizeType i=0; i<(sizeType)terms.size(); i++) {
      ASSERT(terms[i].isQuadratic())
      lhsB(terms[i]._iA,terms[i]._iB)+=terms[i]._coef*rhsB0[terms[i]._row];
      lhsB(terms[i]._iB,terms[i]._iA)+=terms[i]._coef*rhsB0[terms[i]._row];
    }
  }
  return rhsB0.squaredNorm()/2;
}
void QuadraticTerm::remap(const Vec4i& offS,const CellStencil& stencil,vector<QuadraticTerm>& terms,Matd& lhsB,VecD& rhsB)
{
  sizeType j=0;
  for(sizeType i=0; i<(sizeType)terms.size(); i++) {
    terms[i].remap(offS,stencil);
    if(terms[i].isConstant())
      rhsB[terms[i]._row]+=terms[i]._coef;
    else if(terms[i].isLinear()) {
      if(terms[i]._iB.isVariable())
        lhsB(terms[i]._row,terms[i]._iB)+=terms[i]._coef;
      else lhsB(terms[i]._row,terms[i]._iA)+=terms[i]._coef;
    } else {
      ASSERT(terms[i].isQuadratic())
      terms[j++]=terms[i];
    }
  }
  terms.resize(j);
}
void QuadraticTerm::assemble(const Vec4i& offS,const CellStencil& stencil,const vector<QuadraticIndex>& variables,VecD& xB)
{
  for(sizeType i=0; i<(sizeType)variables.size(); i++)
    xB[variables[i]]=variables[i].eval(offS,stencil);
}
void QuadraticTerm::assign(const Vec4i& offS,const CellStencil& stencil,const vector<QuadraticIndex>& variables,const VecD& xB)
{
  for(sizeType i=0; i<(sizeType)variables.size(); i++) {
    scalar* ref=variables[i].evalPtr(offS,stencil);
    if(ref)
      *ref=xB[variables[i]];
  }
}
//banded matrix operators
void QuadraticTerm::eval(const vector<QuadraticTerm>& terms,Banded& banded,VecDArr& rhsB,const VecDArr& xB)
{
  sizeType stride=rhsB[0].size();
  multAdd(banded,xB,rhsB,1);
  for(sizeType i=0; i<(sizeType)terms.size(); i++) {
    ASSERT(terms[i].isQuadratic())
    sizeType rBlk=terms[i]._row/stride,blkInR=terms[i]._row%stride;
    sizeType ABlk=terms[i]._iA/stride,blkInA=terms[i]._iA%stride;
    sizeType BBlk=terms[i]._iB/stride,blkInB=terms[i]._iB%stride;
    if(rBlk >= ABlk)
      banded[rBlk-ABlk][ABlk](blkInR,blkInA)+=terms[i]._coef*xB[BBlk][blkInB];
    if(rBlk >= BBlk)
      banded[rBlk-BBlk][BBlk](blkInR,blkInB)+=terms[i]._coef*xB[ABlk][blkInA];
    rhsB[rBlk][blkInR]+=terms[i]._coef*xB[BBlk][blkInB]*xB[ABlk][blkInA];
  }
}
scalarD QuadraticTerm::evalLM(const vector<QuadraticTerm>& terms,Banded& banded,VecDArr& rhsB,const VecDArr& xB,bool JTJOnly)
{
  eval(terms,banded,rhsB,xB);
  sizeType stride=banded[0][0].rows();
  VecDArr rhsB0=rhsB;

  setZero(rhsB);
  multAdd(banded,rhsB0,rhsB,1);
  multATA(banded);
  if(!JTJOnly) {
    for(sizeType i=0; i<(sizeType)terms.size(); i++) {
      ASSERT(terms[i].isQuadratic())
      sizeType rBlk=terms[i]._row/stride,blkInR=terms[i]._row%stride;
      sizeType ABlk=terms[i]._iA/stride,blkInA=terms[i]._iA%stride;
      sizeType BBlk=terms[i]._iB/stride,blkInB=terms[i]._iB%stride;
      scalarD coef=terms[i]._coef*rhsB0[rBlk][blkInR];
      if(ABlk == BBlk) {
        banded[0][ABlk](blkInA,blkInB)+=coef;
        banded[0][ABlk](blkInB,blkInA)+=coef;
      } else if(ABlk > BBlk)
        banded[ABlk-BBlk][BBlk](blkInA,blkInB)+=coef;
      else banded[BBlk-ABlk][ABlk](blkInB,blkInA)+=coef;
    }
  }
  return squaredNorm(rhsB0)/2;
}
void QuadraticTerm::remap(const Vec4i& offS,const CellStencil& stencil,vector<QuadraticTerm>& terms,MatdArr& diag,MatdArr& offDiag,MatdArr& offDiag2,VecDArr& rhsB)
{
  sizeType j=0,stride=rhsB[0].size();
  for(sizeType i=0; i<(sizeType)terms.size(); i++) {
    terms[i].remap(offS,stencil);
    sizeType blkRow=terms[i]._row/stride,blkInRow=terms[i]._row%stride;
    sizeType blkB=terms[i]._iB/stride,blkInB=terms[i]._iB%stride;
    if(terms[i].isConstant())
      rhsB[blkRow][blkInRow]+=terms[i]._coef;
    else if(terms[i].isLinear()) {
      if(blkRow == blkB)
        diag[blkB](blkInRow,blkInB)+=terms[i]._coef;
      else if(blkRow == blkB+1)
        offDiag[blkB](blkInRow,blkInB)+=terms[i]._coef;
      else if(blkRow == blkB+2)
        offDiag2[blkB](blkInRow,blkInB)+=terms[i]._coef;
      else {
        ASSERT(blkRow == blkB-1 || blkRow == blkB-2)
      }
    } else {
      ASSERT(terms[i].isQuadratic())
      terms[j++]=terms[i];
    }
  }
  terms.resize(j);
}
void QuadraticTerm::assemble(const Vec4i& offS,const CellStencil& stencil,const vector<QuadraticIndex>& variables,VecDArr& xB)
{
  sizeType stride=xB[0].size();
  for(sizeType i=0; i<(sizeType)variables.size(); i++) {
    sizeType var=variables[i];
    xB[var/stride][var%stride]=variables[i].eval(offS,stencil);
  }
}
void QuadraticTerm::assign(const Vec4i& offS,const CellStencil& stencil,const vector<QuadraticIndex>& variables,const VecDArr& xB)
{
  sizeType stride=xB[0].size();
  for(sizeType i=0; i<(sizeType)variables.size(); i++) {
    scalar* ref=variables[i].evalPtr(offS,stencil);
    if(ref) {
      sizeType var=variables[i];
      *ref=xB[var/stride][var%stride];
    }
  }
}
//solver
void QuadraticTerm::writeProb(const string& path,const vector<QuadraticTerm>& terms,const Matd& lhsB,const VecD& rhsB,const VecD& xB)
{
  boost::filesystem::ofstream os(path,ios::binary);
  sizeType n;
  writeBinaryData(n=1,os);
  writeBinaryData(n=(sizeType)terms.size(),os);
  for(sizeType i=0; i<n; i++) {
    writeBinaryData(terms[i]._row,os);
    writeBinaryData(terms[i]._coef,os);
    writeBinaryData(sizeType(terms[i]._iA),os);
    writeBinaryData(sizeType(terms[i]._iB),os);
  }
  writeBinaryData(lhsB,os);
  writeBinaryData(rhsB,os);
  writeBinaryData(xB,os);
}
void QuadraticTerm::writeProb(const string& path,const vector<QuadraticTerm>& terms,const MatdArr& diag,const MatdArr& offDiag,const MatdArr& offDiag2,const VecDArr& rhsB,const VecDArr& xB)
{
  boost::filesystem::ofstream os(path,ios::binary);
  sizeType n;
  writeBinaryData(n=2,os);
  writeBinaryData(n=(sizeType)terms.size(),os);
  for(sizeType i=0; i<n; i++) {
    writeBinaryData(terms[i]._row,os);
    writeBinaryData(terms[i]._coef,os);
    writeBinaryData(sizeType(terms[i]._iA),os);
    writeBinaryData(sizeType(terms[i]._iB),os);
  }
  writeVector(diag,os);
  writeVector(offDiag,os);
  writeVector(offDiag2,os);
  writeVector(rhsB,os);
  writeVector(xB,os);
}
bool QuadraticTerm::readProb(const string& path,vector<QuadraticTerm>& terms,Matd& lhsB,VecD& rhsB,VecD& xB)
{
  boost::filesystem::ifstream is(path,ios::binary);
  sizeType n,varId;
  readBinaryData(n,is);
  if(n != 1) {
    WARNING("Not a dense problem!")
    return false;
  }
  readBinaryData(n,is);
  terms.resize(n);
  for(sizeType i=0; i<n; i++) {
    readBinaryData(terms[i]._row,is);
    readBinaryData(terms[i]._coef,is);

    readBinaryData(varId,is);
    terms[i]._iA=QuadraticIndex((QuadraticIndex::ConstMACVelocityFieldPtr*)1,-1);
    terms[i]._iA.setVariable(varId);

    readBinaryData(varId,is);
    terms[i]._iB=QuadraticIndex((QuadraticIndex::ConstMACVelocityFieldPtr*)1,-1);
    terms[i]._iB.setVariable(varId);
  }
  readBinaryData(lhsB,is);
  readBinaryData(rhsB,is);
  readBinaryData(xB,is);
  return is.good();
}
bool QuadraticTerm::readProb(const string& path,vector<QuadraticTerm>& terms,MatdArr& diag,MatdArr& offDiag,MatdArr& offDiag2,VecDArr& rhsB,VecDArr& xB)
{
  boost::filesystem::ifstream is(path,ios::binary);
  sizeType n,varId;
  readBinaryData(n,is);
  if(n != 2) {
    WARNING("Not a banded problem!")
    return false;
  }
  readBinaryData(n,is);
  terms.resize(n);
  for(sizeType i=0; i<n; i++) {
    readBinaryData(terms[i]._row,is);
    readBinaryData(terms[i]._coef,is);

    readBinaryData(varId,is);
    terms[i]._iA=QuadraticIndex((QuadraticIndex::ConstMACVelocityFieldPtr*)1,-1);
    terms[i]._iA.setVariable(varId);

    readBinaryData(varId,is);
    terms[i]._iB=QuadraticIndex((QuadraticIndex::ConstMACVelocityFieldPtr*)1,-1);
    terms[i]._iB.setVariable(varId);
  }
  readVector(diag,is);
  readVector(offDiag,is);
  readVector(offDiag2,is);
  readVector(rhsB,is);
  readVector(xB,is);
  return is.good();
}
scalarD QuadraticTerm::solveNewton(const vector<QuadraticTerm>& terms,const Matd& lhsB,const VecD& rhsB,VecD& xB)
{
  Matd lhsB0;
  VecD rhsB0,dxB;
  evalLM(terms,lhsB0=lhsB,rhsB0=rhsB,xB);
  dxB=lhsB0.fullPivLu().solve(rhsB0);
  dxB*=lineSearch(findPolyCoef(terms,lhsB,rhsB,xB,dxB));
  xB+=dxB;
  return dxB.squaredNorm();
}
scalarD QuadraticTerm::solveLM(const vector<QuadraticTerm>& terms,const Matd& lhsB,const VecD& rhsB,VecD& xB,bool debug,bool write)
{
  Matd lhsB0;
  VecD rhsB0,xB0=xB,dxB=xB;
  scalarD fx0=0,fx1=0,lambda=0,predDec=0,rho=0,nu=2,eps=0.5f,rhsB0Norm=0;
  fx0=evalLM(terms,lhsB0=lhsB,rhsB0=rhsB,xB,true)*2;
  lambda=lhsB0.diagonal().maxCoeff();
  for(sizeType it=0;; it++) {
    //stopping criteria
    rhsB0Norm=rhsB0.squaredNorm();
    if(debug) {
      INFOV("At iteration %ld: FuncVal: %1.10f, RHS Norm: %1.10f, ",it,fx0,rhsB0Norm)
    }
    if(rhsB0Norm < 1E-10f) {
      break;
    }
    if(it > 100) {
      if(write)
        OMP_CRITICAL_ {
        INFOV("Writing problem at iteration %ld, RHS Norm: %1.10f",it,rhsB0Norm)
        writeProb("./prob.dat",terms,lhsB,rhsB,xB);
        exit(0);
      } else break;
    }
    //solve
    rhsB0*=-1;
    lhsB0.diagonal().array()+=lambda;
    dxB=lhsB0.ldlt().solve(rhsB0);
    //predicted function value decrease
    predDec=dxB.dot(rhsB0)+dxB.squaredNorm()*lambda;
    //actual function value decrease
    swap(xB,dxB);
    xB+=dxB;//save last solution
    fx1=evalLM(terms,lhsB0=lhsB,rhsB0=rhsB,xB,true)*2;
    //update LM regularization
    rho=(fx0-fx1)/predDec;
    if(rho < 0) {
      xB=dxB; //load last solution
      lambda*=nu;
      nu*=2;
      fx0=evalLM(terms,lhsB0=lhsB,rhsB0=rhsB,xB,true)*2;
    } else {
      if(rho > eps) {
        lambda*=max<scalarD>(1.0f/3.0f,1-pow(2*rho-1,3));
        nu=2;
      } else {
        lambda*=nu;
        nu*=2;
      }
      fx0=fx1;
    }
  }
  xB0-=xB;
  return xB0.squaredNorm();
}
scalarD QuadraticTerm::solveLM(const vector<QuadraticTerm>& terms,const MatdArr& diag,const MatdArr& offDiag,const MatdArr& offDiag2,const VecDArr& rhsB,VecDArr& xB,bool debug,bool write)
{
  vector<MatdArr> bandMat(3);
  VecDArr rhsB0,xB0=xB,dxB=xB;
  sizeType N=(sizeType)diag.size();
  scalarD fx0=0,fx1=0,lambda=0,predDec=0,rho=0,nu=2,eps=0.5f,rhsB0Norm=0;
  fx0=evalLM(terms,toBanded(bandMat,diag,offDiag,&offDiag2),rhsB0=rhsB,xB,true)*2;
  for(sizeType i=0; i<N; i++)
    lambda=max(lambda,bandMat[0][i].diagonal().maxCoeff());
  for(sizeType it=0;; it++) {
    //stopping criteria
    rhsB0Norm=squaredNorm(rhsB0);
    if(debug) {
      INFOV("At iteration %ld: FuncVal: %1.10f, RHS Norm: %1.10f, ",it,fx0,rhsB0Norm)
    }
    if(rhsB0Norm < 1E-10f*N) {
      break;
    }
    if(it > 100) {
      if(write)
        OMP_CRITICAL_ {
        INFOV("Writing problem at iteration %ld, RHS Norm: %1.10f",it,rhsB0Norm)
        writeProb("./prob.dat",terms,diag,offDiag,offDiag2,rhsB,xB);
        exit(0);
      } else break;
    }
    //solve
    mul(rhsB0,-1);
    for(sizeType i=0; i<N; i++)
      bandMat[0][i].diagonal().array()+=lambda;
    bandedLLT(bandMat,dxB=rhsB0);
    //predicted function value decrease
    predDec=0;
    for(sizeType i=0; i<N; i++)
      predDec+=dxB[i].dot(rhsB0[i])+dxB[i].squaredNorm()*lambda;
    //actual function value decrease
    addSave(xB,dxB);  //save last solution
    fx1=evalLM(terms,toBanded(bandMat,diag,offDiag,&offDiag2),rhsB0=rhsB,xB,true)*2;
    //update LM regularization
    rho=(fx0-fx1)/predDec;
    if(rho < 0) {
      xB=dxB; //load last solution
      lambda*=nu;
      nu*=2;
      fx0=evalLM(terms,toBanded(bandMat,diag,offDiag,&offDiag2),rhsB0=rhsB,xB,true)*2;
    } else {
      if(rho > eps) {
        lambda*=max<scalarD>(1.0f/3.0f,1-pow(2*rho-1,3));
        nu=2;
      } else {
        lambda*=nu;
        nu*=2;
      }
      fx0=fx1;
    }
  }
  sub(xB,xB0,xB0);
  return squaredNorm(xB0);
}
void QuadraticTerm::solveProbLM(const string& path,bool bandedAsDense)
{
  Matd lhsB;
  VecD rhsB,xB;
  MatdArr diag,offDiag,offDiag2;
  VecDArr rhsBArr,xBArr;
  vector<QuadraticTerm> terms;
  if(readProb(path,terms,lhsB,rhsB,xB))
    solveLM(terms,lhsB,rhsB,xB,true,false);
  else if(readProb(path,terms,diag,offDiag,offDiag2,rhsBArr,xBArr)) {
    if(bandedAsDense)
      solveLM(terms,lhsB=toDense(diag,offDiag,&offDiag2),rhsB=toDense(rhsBArr),xB=toDense(xBArr),true,false);
    else
      solveLM(terms,diag,offDiag,offDiag2,rhsBArr,xBArr,true,false);
  }
}
//line searcher
Traits::VecD QuadraticTerm::findPolyCoef(const vector<QuadraticTerm>& terms,const Matd& lhsB,const VecD& rhsB,const VecD& xB,const VecD& dxB)
{
  VecD VG0=rhsB+lhsB*xB,VG1=lhsB*dxB,VG2=VecD::Zero(rhsB.size());
  for(sizeType i=0; i<(sizeType)terms.size(); i++) {
    VG0[terms[i]._row]+=xB[terms[i]._iA]*xB[terms[i]._iB]*terms[i]._coef;
    VG1[terms[i]._row]+=(xB[terms[i]._iA]*dxB[terms[i]._iB]+xB[terms[i]._iB]*dxB[terms[i]._iA])*terms[i]._coef;
    VG2[terms[i]._row]+=dxB[terms[i]._iA]*dxB[terms[i]._iB]*terms[i]._coef;
  }

  VecD ret=VecD::Zero(5);
  ret[0]=VG2.dot(VG2);
  ret[1]=VG2.dot(VG1)*2;
  ret[2]=VG2.dot(VG0)*2+VG1.dot(VG1);
  ret[3]=VG1.dot(VG0)*2;
  ret[4]=VG0.dot(VG0);
  return ret/2;
}
Traits::VecD QuadraticTerm::findPolyCoef(const vector<QuadraticTerm>& terms,const MatdArr& diag,const MatdArr& offDiag,const MatdArr& offDiag2,const VecDArr& rhsB,const VecDArr& xB,const VecDArr& dxB)
{
  sizeType stride=(sizeType)xB[0].size();
  VecDArr VG0=rhsB,VG1,VG2(xB.size(),VecD::Zero(xB[0].size()));
  multAdd(diag,offDiag,&offDiag2,xB,VG0,1);
  multSet(diag,offDiag,&offDiag2,dxB,VG1);
  for(sizeType i=0; i<(sizeType)terms.size(); i++) {
    sizeType rBlk=terms[i]._row/stride,blkInR=terms[i]._row%stride;
    sizeType ABlk=terms[i]._iA/stride,blkInA=terms[i]._iA%stride;
    sizeType BBlk=terms[i]._iB/stride,blkInB=terms[i]._iB%stride;

    VG0[rBlk][blkInR]+=xB[ABlk][blkInA]*xB[BBlk][blkInB]*terms[i]._coef;
    VG1[rBlk][blkInR]+=(dxB[ABlk][blkInA]*xB[BBlk][blkInB]+xB[ABlk][blkInA]*dxB[BBlk][blkInB])*terms[i]._coef;
    VG2[rBlk][blkInR]+=dxB[ABlk][blkInA]*dxB[BBlk][blkInB]*terms[i]._coef;
  }

  VecD ret=VecD::Zero(5);
  ret[0]=dot(VG2,VG2);
  ret[1]=dot(VG2,VG1)*2;
  ret[2]=dot(VG2,VG0)*2+dot(VG1,VG1);
  ret[3]=dot(VG1,VG0)*2;
  ret[4]=dot(VG0,VG0);
  return ret/2;
}
scalarD QuadraticTerm::lineSearch(const VecD& coef,scalarD* minVal)
{
  scalarD diff3=coef[0]*4;
  scalarD diff2=coef[1]*3;
  scalarD diff1=coef[2]*2;
  scalarD diff0=coef[3]*1;

#define EVAL(X) ((((coef[0]*X+coef[1])*X+coef[2])*X+coef[3])*X+coef[4])
#define EPSPOLY 1E-20f
  int n;
  scalarD x[3];
  if(abs(diff3) > EPSPOLY) {
    n=SolveP3(x,diff2/diff3,diff1/diff3,diff0/diff3);
  } else if(abs(diff2) > EPSPOLY) {
    n=2;
    x[0]=(sqrt(diff1*diff1-4*diff2*diff0)-diff1)/2/diff2;
    x[1]=(sqrt(diff1*diff1-4*diff2*diff0)+diff1)/2/diff2;
  } else if(abs(diff1) > EPSPOLY) {
    n=1;
    x[0]=-diff0/diff1;
  } else n=0;

  scalarD ret=0,fx=EVAL(ret),f=0;
  for(int i=0; i<n; i++)
    if((f=EVAL(x[i])) < fx) {
      ret=x[i];
      fx=f;
    }
  if(minVal)
    *minVal=fx;
  return ret;
#undef EVAL
#undef EPSPOLY
}
//CellStencil
template <typename T>
sizeType find(const vector<T>& pool,T val,sizeType sz=-1)
{
  if(sz == -1)
    sz=(sizeType)pool.size();
  for(sizeType i=0; i<sz; i++)
    if(pool[i] == val)
      return i;
  return -1;
}
CellStencil::CellStencil():_otherCell(7),_offFace(6) {}
void CellStencil::countRows()
{
  for(sizeType i=0; i<=4; i++) {
    const vector<QuadraticTerm>& terms=i == 4 ? _termTridiagST : _termST[i];
    sizeType& rows=i == 4 ? _rowsTridiagST : _rowsST[i];

    rows=0;
    for(sizeType t=0; t<(sizeType)terms.size(); t++)
      rows=max(rows,terms[t]._row+1);
  }
}
