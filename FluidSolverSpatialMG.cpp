#include "FluidSolverSpatialMG.h"
#include "CommonFile/GridOp.h"
#include <boost/property_tree/ptree.hpp>

PRJ_BEGIN

Traits::Vec6c strideCellStencil()
{
  Traits::Vec6c stride;
  stride[0]=32;
  stride[1]=16;
  stride[2]=8;
  stride[3]=4;
  stride[4]=2;
  stride[5]=1;
  return stride;
}
VelRef buildVelComp(const FluidState& s,const Vec3i& cid,vector<VelRef>* ref,sizeType& VID)
{
  const pair<Vec3i,sizeType> vc=s.velComp(VID);
  const MACVelocityField& vel=s.vel(FluidState::VELOCITY);
  const sizeType cellOff=vel.getComp(vc.second).getIndex(vc.first-cid);
  //check existing
  for(sizeType i=0; ref && i<(sizeType)ref->size(); i++) {
    const VelRef& vref=(*ref)[i];
    if(vref._cellOff == cellOff && vref._velComp == vc.second) {
      VID=i;
      return vref;
    }
  }
  //create new
  VelRef ret=VelRef(vc.second,cellOff);
  if(ref) {
    VID=(sizeType)ref->size();
    ref->push_back(ret);
  }
  return ret;
}
void FluidStateMG::init(Vec3i nr,Vec3 len,bool shadow,Vec3c periodic)
{
  FluidState::init(nr,len,
                   PRESSURE|RHS_PRESSURE|GHOST_FORCE_PRESSURE|RHS_GHOST_FORCE_PRESSURE,
                   VELOCITY|RHS_VELOCITY|GHOST_FORCE|RHS_GHOST_FORCE,shadow,periodic);
#define OTHERCELL ((j%2 == 1) ? (Vec3i)Vec3i::Unit(j/2) : (Vec3i)-Vec3i::Unit(j/2))
#define OFFFACE ((j%2 == 1) ? (Vec3i)Vec3i::Unit(j/2) : (Vec3i)Vec3i::Zero())
  //reset stencil
  _stencils.resize(125);
  for(sizeType i=0; i<(sizeType)_stencils.size(); i++) {
    //ASSERT_MSG(getStencilId(getStencilCell(i)) == i,"Stencil id error!")
    _stencils[i]=CellStencil();
  }
  //build stencil
  Vec6 div=divStencil();
  sizeType dim=scal(PRESSURE).getDim(),nrPrimal=dim*2;
  ITERSS(scal(PRESSURE),Vec3i::Zero(),1)
  Vec6c validVelComp=isVelValid(curr);
  //check if stencil has been built
  CellStencil& stencil=getStencil(curr);
  if(stencil._lhs.size() > 0)
    continue;
  else stencil._lhs.setZero(nrPrimal+1,nrPrimal+1);
  //lhs stencil
  stencil._lhs.block(0,0,nrPrimal,nrPrimal).setIdentity();
  for(sizeType j=0; j<nrPrimal; j++)
    if(validVelComp[j])
      stencil._lhs(nrPrimal,j)=stencil._lhs(j,nrPrimal)=div[j];
  Curl adv=FluidSolver::buildAdvStencil(*this,curr);
  stencil._tensors.assign(adv.begin(),adv.end());
  //velocity component
  stencil._velComps.resize(nrPrimal,VelRef(-1,-1));
  Vec6i vid=velComp(curr);
  for(sizeType j=0; j<nrPrimal; j++)
    if(validVelComp[j])
      stencil._velComps[j]=buildVelComp(*this,curr,NULL,vid[j]);
  for(sizeType i=0; i<(sizeType)stencil._tensors.size(); i++)
    for(sizeType j=0; j<4; j++)
      buildVelComp(*this,curr,&(stencil._velComps),stencil._tensors[i].VID(j));
  //build cell/face offset
  Vec3i periodicS=getPeriodicS();
  for(sizeType j=0; j<nrPrimal; j++) {
    stencil._otherCell[j]=scal(PRESSURE).getIndex(curr+OTHERCELL,&periodicS)-scal(PRESSURE).getIndex(curr);
    stencil._offFace[j]=vel(VELOCITY).getComp(j/2).getIndex(curr+OFFFACE,&periodicS)-vel(VELOCITY).getComp(j/2).getIndex(curr);
  }
  stencil._otherCell[6]=0;
  ITERSSEND
#undef OFFFACE
#undef OTHERCELL
}
void FluidStateMG::calcDivergence(FIELD target,FIELD source,VecD* v)
{
  ScalarField& t=scalNonConst(target);
  const MACVelocityField& s=vel(source);

  //build stride for v!=NULL
  sizeType dim=t.getDim(),nrPrimal=dim*2;
  Vec3i stride=Vec3i::Zero();
  for(sizeType d=1; d<dim; d++)
    stride[d]=stride[d-1]+s.getComp(d).getNrPoint().prod();
  //compute divergence
  Vec6c validVelComp;
  Vec6 div=divStencil();
  ITERSVP_ALL(t,s,OMP_PRI(validVelComp))
  validVelComp=isVelValid(curr);
  const CellStencil& stencil=getStencil(curr);
  scalar& divVal=t.get(offS[0])=0;
  for(sizeType j=0; j<nrPrimal; j++)
    if(validVelComp[j]) {
      if(v)
        divVal+=FETCHVELV(stride,*v,j)*div[j];
      else divVal+=FETCHVEL(s,j)*div[j];
    }
  ITERSVPEND
}
//for spacetime FAS solve
void solveSubsystem(const Matd& lhs,const Traits::VecD& rhs,Traits::VecD& x,sizeType offP,sizeType szP,sizeType offD)
{
  typedef Traits::VecD VecD;
  //build lhs
  Matd lhsB=Matd::Zero(szP+1,szP+1);
  lhsB.block(0,0,szP,szP)=lhs.block(offP,offP,szP,szP);
  lhsB.block(szP,0,1,szP)=lhs.block(offD,offP,1,szP);
  lhsB.block(0,szP,szP,1)=lhs.block(offP,offD,szP,1);
  //build rhs
  VecD rhsB=VecD::Zero(szP+1);
  rhsB.block(0,0,szP,1)=rhs.block(offP,0,szP,1);
  rhsB[szP]=rhs[offD];
  //cout << lhsB << endl << endl;
  //cout << rhsB << endl << endl;
  //solve
  VecD xB=lhsB.fullPivLu().solve(rhsB);
  x.block(offP,0,szP,1)=xB.block(0,0,szP,1);
  x[offD]=xB[szP];
}
void FluidStateMG::initSTSCGS(scalar dt,scalar regKByRegU,FluidSolver::TIME_INTEGRATOR mode,sizeType nrFrame)
{
#define PRIMAL(R,C) lhsB.block((R)*nrPrimal,(C)*nrPrimal,nrPrimal,nrPrimal)
#define DUAL(R) lhsB.block(nrBlk*nrPrimal+(R),(R)*nrPrimal,1,nrPrimal)
#define DUALT(R) lhsB.block((R)*nrPrimal,nrBlk*nrPrimal+(R),nrPrimal,1)
  //check if we have initialized
  if((sizeType)_V.size() >= nrFrame)
    for(sizeType i=0; i<(sizeType)_stencils.size(); i++)
      if(_stencils[i]._lhs.size() > 0) {
        if(_stencils[i]._diagP.size() != _stencils[i]._lhs.size() || _stencils[i]._diagP(0,0) != regKByRegU)
          break;
        else return;
      }

  //reinitialize
  RESIZE_NEIGH_MEMBER(nrFrame)
  bool midP=mode == FluidSolver::MIDPOINT;
  scalarD coefTensor=midP ? 0.25f : 1.0f;
  for(sizeType i=0; i<(sizeType)_stencils.size(); i++)
    if(_stencils[i]._lhs.size() != 0) {
      sizeType rows=_stencils[i]._lhs.rows(),nrPrimal=rows-1;
      Vec6 div=divStencil();

      //initialize data-structure for spatial-temporal smoothing
      for(sizeType type=0; type<4; type++) {
        bool last=type&1;
        bool next=type&2;
        vector<scalar> offDiag;
        scalarD diag=regKByRegU;

        //build offsets
        sizeType nrBlk=1,primalLast=-1,primalNext=-1;
        sizeType offPrimal=-1,dualLast=-1,dualNext=-1;
        if(last) {
          primalLast=(nrBlk++)*nrPrimal;
          offDiag.push_back(1/dt);
        }
        if(next) {
          primalNext=(nrBlk++)*nrPrimal;
          offDiag.push_back(-1/dt);
        }
        offPrimal=nrBlk*nrPrimal;
        if(primalLast >= 0)
          dualLast=primalLast/nrPrimal+offPrimal;
        if(primalNext >= 0)
          dualNext=primalNext/nrPrimal+offPrimal;

        //build lhsST
        Matd P=_stencils[i]._lhs.block(0,0,nrPrimal,nrPrimal);
        Matd D=_stencils[i]._lhs.block(nrPrimal,0,1,nrPrimal);
        Matd& lhsB=_stencils[i]._lhsST[type];
        lhsB.setZero(rows*nrBlk,rows*nrBlk);
        PRIMAL(0,0)=P*diag;
        DUAL(0)=D;
        DUALT(0)=D.transpose();
        for(sizeType b=1; b<nrBlk; b++) {
          PRIMAL(b,b)=-P;
          PRIMAL(b,0)=PRIMAL(0,b)=P*offDiag[b-1];
          DUAL(b)=D;
          DUALT(b)=D.transpose();
        }

        //build termST
        vector<QuadraticIndex>& variables=_stencils[i]._variableST[type];
        vector<QuadraticTerm>& terms=_stencils[i]._termST[type];
        variables.resize(lhsB.rows());
        terms.clear();
        //assemble system: rhs primal
        for(sizeType j=0; j<nrPrimal; j++)
          if(_stencils[i]._velComps[j] != VelRef(-1,-1)) {
            {
              variables[j]=QuadraticIndex(&_V[0],j);
              terms.push_back(QuadraticTerm(j,variables[j],-regKByRegU));  //diagonal
              terms.push_back(QuadraticTerm(offPrimal,variables[j],-div[j])); //divergence
              terms.push_back(QuadraticTerm(j,QuadraticIndex(&_VR[0],j),1));  //RHS
              terms.push_back(QuadraticTerm(j,QuadraticIndex(&_GFP[0],j),div[j]));  //pressure gradient
              terms.push_back(QuadraticTerm(j,QuadraticIndex(&_GFP[0],6),-div[j]));
            }
            if(primalLast >= 0) {
              variables[primalLast+j]=QuadraticIndex(&_GF[1],j);
              terms.push_back(QuadraticTerm(primalLast+j,variables[primalLast+j],1)); //diagonal
              terms.push_back(QuadraticTerm(dualLast,variables[primalLast+j],-div[j])); //divergence
              terms.push_back(QuadraticTerm(primalLast+j,QuadraticIndex(&_GFR[1],j),1));  //RHS
              terms.push_back(QuadraticTerm(j,variables[primalLast+j],-1/dt));  //dvdt
              terms.push_back(QuadraticTerm(primalLast+j,variables[j],-1/dt));
              terms.push_back(QuadraticTerm(primalLast+j,QuadraticIndex(&_V[1],j),1/dt));
              terms.push_back(QuadraticTerm(primalLast+j,QuadraticIndex(&_P[1],j),div[j])); //pressure gradient
              terms.push_back(QuadraticTerm(primalLast+j,QuadraticIndex(&_P[1],6),-div[j]));
            }
            if(primalNext >= 0) {
              variables[primalNext+j]=QuadraticIndex(&_GF[0],j);
              terms.push_back(QuadraticTerm(primalNext+j,variables[primalNext+j],1)); //diagonal
              terms.push_back(QuadraticTerm(dualNext,variables[primalNext+j],-div[j])); //divergence
              terms.push_back(QuadraticTerm(primalNext+j,QuadraticIndex(&_GFR[0],j),1));  //RHS
              terms.push_back(QuadraticTerm(j,variables[primalNext+j],1/dt)); //dvdt
              terms.push_back(QuadraticTerm(primalNext+j,variables[j],1/dt));
              terms.push_back(QuadraticTerm(primalNext+j,QuadraticIndex(&_V[2],j),-1/dt));
              terms.push_back(QuadraticTerm(primalNext+j,QuadraticIndex(&_P[0],j),div[j])); //pressure gradient
              terms.push_back(QuadraticTerm(primalNext+j,QuadraticIndex(&_P[0],6),-div[j]));
            }
          }
        //assemble system: rhs dual
        {
          variables[offPrimal]=QuadraticIndex(&_GFP[0],6);
          terms.push_back(QuadraticTerm(offPrimal,QuadraticIndex(&_GFPR[0],6),1));
        }
        if(dualLast >= 0) {
          variables[dualLast]=QuadraticIndex(&_P[1],6);
          terms.push_back(QuadraticTerm(dualLast,QuadraticIndex(&_PR[1],6),1));
        }
        if(dualNext >= 0) {
          variables[dualNext]=QuadraticIndex(&_P[0],6);
          terms.push_back(QuadraticTerm(dualNext,QuadraticIndex(&_PR[0],6),1));
        }
        //assemble system: advection related lhs/rhs terms
        if(primalLast >= 0) {
          for(sizeType t=0; t<(sizeType)_stencils[i]._tensors.size(); t++)
            _stencils[i]._tensors[t].evalDerivKKTBlk(-coefTensor,nrPrimal,terms,primalLast,0,&_V[0],midP ? &_V[1] : NULL,&_GF[1]);
        }
        if(primalNext >= 0) {
          if(midP)
            for(sizeType t=0; t<(sizeType)_stencils[i]._tensors.size(); t++)
              _stencils[i]._tensors[t].evalDerivKKTBlk(-coefTensor,nrPrimal,terms,primalNext,0,&_V[0],midP ? &_V[2] : NULL,&_GF[0]);
          else
            for(sizeType t=0; t<(sizeType)_stencils[i]._tensors.size(); t++)
              _stencils[i]._tensors[t].evalDerivKKTBlk(-1,nrPrimal,terms,primalNext,0,&_V[2],NULL,NULL);
        }
        //compact terms
        for(sizeType v=0; v<(sizeType)variables.size(); v++) {
          variables[v].setVariable(v);
          QuadraticTerm::labelVariable(terms,variables[v]);
        }
        QuadraticTerm::prune(terms);
      }

      //initialize additional data-structure for spatial-temporal tridiag smoothing
      {
        //build diagP/D offDiagLast/Next
        _stencils[i]._diagP=_stencils[i]._diagD=_stencils[i]._lhs;
        _stencils[i]._diagP.block(0,0,nrPrimal,nrPrimal).diagonal().setConstant(regKByRegU);
        _stencils[i]._diagD.block(0,0,nrPrimal,nrPrimal).diagonal().setConstant(-1);
        _stencils[i]._offDiagLast=_stencils[i]._offDiagNext=Matd::Zero(rows,rows);
        _stencils[i]._offDiagLast.block(0,0,nrPrimal,nrPrimal).diagonal().setConstant(-1/dt);
        _stencils[i]._offDiagNext.block(0,0,nrPrimal,nrPrimal).diagonal().setConstant(1/dt);

        //build termTridiagST
        vector<QuadraticIndex>& variables=_stencils[i]._variableTridiagST;
        vector<QuadraticTerm>& terms=_stencils[i]._termTridiagST;
        variables.resize(rows*(nrFrame*2-1));
        terms.clear();
        //assemble system: primal linear
        for(sizeType f=0,varOff=0; f<nrFrame; f++,varOff+=rows*2) {
          for(sizeType j=0; j<nrPrimal; j++)
            if(_stencils[i]._velComps[j] != VelRef(-1,-1)) {
              //velocity
              variables[varOff+j]=QuadraticIndex(&_V[f],j);
              terms.push_back(QuadraticTerm(varOff+j,variables[varOff+j],-regKByRegU)); //diagonal
              terms.push_back(QuadraticTerm(varOff+nrPrimal,variables[varOff+j],-div[j]));  //divergence velocity
              terms.push_back(QuadraticTerm(varOff+j,QuadraticIndex(&_VR[f],j),1));  //RHS
              terms.push_back(QuadraticTerm(varOff+j,QuadraticIndex(&_GFP[f],j),div[j]));  //pressure gradient
              terms.push_back(QuadraticTerm(varOff+j,QuadraticIndex(&_GFP[f],6),-div[j]));
            }
          //assemble system: dual
          variables[varOff+nrPrimal]=QuadraticIndex(&_GFP[f],6);  //dual velocity
          terms.push_back(QuadraticTerm(varOff+nrPrimal,QuadraticIndex(&_GFPR[f],6),1));
        }
        //build advection terms
        for(sizeType f=0,varOff=0; f<nrFrame-1; f++,varOff+=rows*2) {
          //assemble system: primal linear
          for(sizeType j=0; j<nrPrimal; j++)
            if(_stencils[i]._velComps[j] != VelRef(-1,-1)) {
              //ghost force
              variables[varOff+rows+j]=QuadraticIndex(&_GF[f],j);
              terms.push_back(QuadraticTerm(varOff+rows+j,variables[varOff+rows+j],1));  //diagonal
              terms.push_back(QuadraticTerm(varOff+rows+nrPrimal,variables[varOff+rows+j],-div[j]));  //divergence ghost force
              terms.push_back(QuadraticTerm(varOff+rows+j,QuadraticIndex(&_GFR[f],j),1));  //RHS
              terms.push_back(QuadraticTerm(varOff+j,variables[varOff+rows+j],1/dt)); //dvdt curr
              terms.push_back(QuadraticTerm(varOff+rows+j,variables[varOff+j],1/dt));
              terms.push_back(QuadraticTerm(varOff+rows*2+j,variables[varOff+rows+j],-1/dt)); //dvdt next
              terms.push_back(QuadraticTerm(varOff+rows+j,variables[varOff+rows*2+j],-1/dt));
              terms.push_back(QuadraticTerm(varOff+rows+j,QuadraticIndex(&_P[f],j),div[j])); //pressure gradient
              terms.push_back(QuadraticTerm(varOff+rows+j,QuadraticIndex(&_P[f],6),-div[j]));
            }
          //assemble system: primal quadratic
          for(sizeType t=0; t<(sizeType)_stencils[i]._tensors.size(); t++)
            _stencils[i]._tensors[t].evalDerivKKTBlk(-coefTensor,nrPrimal,terms,varOff+rows,varOff+rows*2,&_V[f+1],midP ? &_V[f] : NULL,&_GF[f]);
          //assemble system: dual
          variables[varOff+rows+nrPrimal]=QuadraticIndex(&_P[f],6);  //dual pressure
          terms.push_back(QuadraticTerm(varOff+rows+nrPrimal,QuadraticIndex(&_PR[f],6),1));
        }
        //compact terms
        for(sizeType v=0; v<(sizeType)variables.size(); v++) {
          variables[v].setVariable(v);
          QuadraticTerm::labelVariable(terms,variables[v]);
        }
        QuadraticTerm::prune(terms);
      }
      _stencils[i].countRows();
    }
#undef PRIMAL
#undef DUAL
#undef DUALT
}
scalar FluidStateMG::smoothSTSCGS(FluidState* last,FluidState* next,scalar dt,FluidSolver::TIME_INTEGRATOR mode)
{
  FILL_NEIGH_MEMBER(0,this)
  FILL_NEIGH_MEMBER(1,last)
  FILL_NEIGH_MEMBER(2,next)

  sizeType dim=_P[0]->getDim(),nrPrimal=dim*2;
  sizeType nrBlk=1,primalLast=-1,primalNext=-1,dualLast=-1,dualNext=-1;
  bool midP=mode == FluidSolver::MIDPOINT;
  if(last) {
    primalLast=nrBlk*nrPrimal;
    nrBlk++;
  }
  if(next) {
    primalNext=nrBlk*nrPrimal;
    nrBlk++;
  }
  sizeType offPrimal=nrBlk*nrPrimal;
  sizeType lhsSTId=(last ? 1 : 0)+(next ? 2 : 0);
  if(primalLast >= 0)
    dualLast=primalLast/nrPrimal+offPrimal;
  if(primalNext >= 0)
    dualNext=primalNext/nrPrimal+offPrimal;
  scalar coefTensor=midP ? 0.25f : 1.0f,delta=0;

  Matd lhsB;
  VecD rhsB,xB;
  Vec6c validVelComp;
  Vec6 div=divStencil();
  rhsB=xB=VecD::Zero(nrBlk*(nrPrimal+1));

  for(sizeType pass=0; pass<pow(2,dim); pass++) {
    ITERSVP(*(_P[0]),*(_V[0]),_start[pass],2,OMP_FPRI(lhsB,rhsB,xB,validVelComp) OMP_ADD(delta))
    delta+=solveSTSCGS(lhsB,rhsB,xB,validVelComp,div,curr,offS,midP,coefTensor,
                       dt,lhsSTId,nrPrimal,primalLast,primalNext,offPrimal,dualLast,dualNext);
    ITERSVPEND
  }
  //output norm of changed values, this is deprecated
  return sqrt(delta);
}
scalar FluidStateMG::smoothSTSCGSQuadratic(FluidState* last,FluidState* next)
{
  FILL_NEIGH_MEMBER(0,this)
  FILL_NEIGH_MEMBER(1,last)
  FILL_NEIGH_MEMBER(2,next)

  sizeType dim=_P[0]->getDim(),nrPrimal=dim*2,nrBlk=1;
  if(last)
    nrBlk++;
  if(next)
    nrBlk++;
  sizeType lhsSTId=(last ? 1 : 0)+(next ? 2 : 0);
  scalar delta=0;

  Matd lhsB;
  VecD rhsB,xB;
  rhsB=xB=VecD::Zero(nrBlk*(nrPrimal+1));
  lhsB=Matd::Zero(rhsB.size(),rhsB.size());
  //reserve enough entries
  vector<QuadraticTerm> terms;
  for(sizeType i=0; i<(sizeType)_stencils.size(); i++)
    for(sizeType j=0; j<4; j++)
      terms.reserve(_stencils[i]._termST[j].size());

  for(sizeType pass=0; pass<pow(2,dim); pass++) {
    ITERSVP(*(_P[0]),*(_V[0]),_start[pass],2,OMP_FPRI(lhsB,rhsB,xB,terms) OMP_ADD(delta))
    delta+=solveSTSCGSQuadratic(terms,lhsB,rhsB,xB,lhsSTId,curr,offS,false);
    ITERSVPEND
  }
  return (scalar)sqrt(delta);
}
scalar FluidStateMG::smoothSTTridiag(vector<boost::shared_ptr<FluidState> > states,FluidSolver::TIME_INTEGRATOR mode)
{
  sizeType nrS=(sizeType)states.size();
  Vec6c validVelComp;
  Vec6 div=divStencil();
  for(sizeType i=0; i<nrS; i++) {
    FILL_NEIGH_MEMBER(i,states[i].get())
  }
  sizeType dim=_P[0]->getDim(),nrPrimal=dim*2;
  vector<const Matd*> diag(nrS*2-1);
  MatdArr offDiag(nrS*2-2,Matd::Zero(nrPrimal+1,nrPrimal+1));
  VecDArr rhs(nrS*2-1,VecD::Zero(nrPrimal+1));
  VecDArr sol(nrS*2-1,VecD::Zero(nrPrimal+1));
  bool midP=mode == FluidSolver::MIDPOINT;
  scalar coefTensor=midP ? 0.25f : 1.0f,delta=0;

  for(sizeType pass=0; pass<pow(2,dim); pass++) {
    ITERSVP(*(_P[0]),*(_V[0]),_start[pass],2,OMP_FPRI(diag,offDiag,rhs,sol,validVelComp) OMP_ADD(delta))
    delta+=solveSTTridiag(diag,offDiag,rhs,sol,validVelComp,div,curr,offS,midP,coefTensor,nrPrimal,nrS);
    ITERSVPEND
  }
  //output #frame-independent norm of changed values, this is deprecated
  return sqrt(delta/(scalar)nrS);
}
scalar FluidStateMG::smoothSTTridiagQuadratic(vector<boost::shared_ptr<FluidState> > states)
{
  sizeType nrS=(sizeType)states.size();
  for(sizeType i=0; i<nrS; i++) {
    FILL_NEIGH_MEMBER(i,states[i].get())
  }
  sizeType dim=_P[0]->getDim(),nrPrimal=dim*2;
  scalar delta=0;

  MatdArr diag(nrS*2-1,Matd::Zero(nrPrimal+1,nrPrimal+1));
  MatdArr offDiag(nrS*2-2,Matd::Zero(nrPrimal+1,nrPrimal+1));
  MatdArr offDiag2(nrS*2-3,Matd::Zero(nrPrimal+1,nrPrimal+1));
  VecDArr rhs(nrS*2-1,VecD::Zero(nrPrimal+1));
  VecDArr sol(nrS*2-1,VecD::Zero(nrPrimal+1));
  //reserve enough entries
  vector<QuadraticTerm> terms;
  for(sizeType i=0; i<(sizeType)_stencils.size(); i++)
    terms.reserve(_stencils[i]._termTridiagST.size());

  for(sizeType pass=0; pass<pow(2,dim); pass++) {
    ITERSVP(*(_P[0]),*(_V[0]),_start[pass],2,OMP_FPRI(diag,offDiag,offDiag2,rhs,sol,terms) OMP_ADD(delta))
    delta+=solveSTTridiagQuadratic(terms,diag,offDiag,offDiag2,rhs,sol,curr,offS);
    ITERSVPEND
  }
  //output #frame-independent norm of changed values, this is deprecated
  return sqrt(delta/(scalar)nrS);
}
scalar FluidStateMG::calcPrimalFASRhs(const FluidState* last,const FluidState* next,FluidState* target,scalar dt,scalar regKByRegU,FluidSolver::TIME_INTEGRATOR mode)
{
  FILL_NEIGH_MEMBER(0,this)
  FILL_NEIGH_MEMBER(1,last)
  FILL_NEIGH_MEMBER(2,next)
  MACVelocityField* TVR=target ? target->velPtr(RHS_VELOCITY).get() : NULL;
  ScalarField* TGFPR=target ? target->scalPtr(RHS_GHOST_FORCE_PRESSURE).get() : NULL;
  Vec3i periodicST=target ? target->getPeriodicS() : Vec3i::Zero();
  bool midP=mode == FluidSolver::MIDPOINT;
  scalar coefTensor=midP ? 0.25f : 1.0f;
  scalar residualNorm=0,residual=0;

  VecD rhsB;
  Vec6c validVelComp;
  Vec6 div=divStencil();
  sizeType dim=_P[0]->getDim(),nrPrimal=dim*2;
  rhsB=VecD::Zero(nrPrimal+1);

  //add f(x) or R*(RHS-f(x))
  scalar coef=1/(scalar)pow(2,dim);
  ITERSVP_ALL(*(_P[0]),*(_V[0]),OMP_FPRI(rhsB,validVelComp,residual))
  validVelComp=isVelValid(curr);
  const CellStencil& stencil=getStencil(curr);
  //compute rhs primal
  rhsB.setZero();
  if(last)
    for(sizeType i=0; i<(sizeType)stencil._tensors.size(); i++)
      stencil._tensors[i].evalDerivTBlk(offS,stencil._velComps,*(_V[0]),*(_GF[1]),coefTensor,rhsB,midP ? _V[1] : NULL);
  if(next && midP)
    for(sizeType i=0; i<(sizeType)stencil._tensors.size(); i++)
      stencil._tensors[i].evalDerivTBlk(offS,stencil._velComps,*(_V[0]),*(_GF[0]),coefTensor,rhsB,midP ? _V[2] : NULL);
  for(sizeType j=0; j<nrPrimal; j++)
    if(validVelComp[j]) {
      rhsB[nrPrimal]+=div[j]*FETCHVELP(_V[0],j);
      if(j%2 == 0) {
        rhsB[j]+=FETCHVELP(_V[0],j)*regKByRegU+div[j]*(FETCHSCALPC(_GFP[0])-FETCHSCALP(_GFP[0],j));
        if(last)
          rhsB[j]+=FETCHVELP(_GF[1],j)/dt;
        if(next)
          rhsB[j]-=FETCHVELP(_GF[0],j)/dt;
        if(target) {
          residual=FETCHVELP_COND(_VR[0],j)-rhsB[j];
          restrict(*TVR,curr,j/2,residual*coef,&add,periodicST);
          add(residualNorm,residual*residual);
        } else FETCHVELP((MACVelocityField*)_VR[0],j)+=rhsB[j];
      }
    }
  if(target) {
    residual=(_GFPR[0] ? FETCHSCALPC(_GFPR[0]) : 0)-rhsB[nrPrimal];
    restrict(*TGFPR,curr,residual*coef,&add);
    add(residualNorm,residual*residual);
  } else FETCHSCALPC((ScalarField*)_GFPR[0])+=rhsB[nrPrimal];
  ITERSVPEND
  return residualNorm;
}
scalar FluidStateMG::calcDualFASRhs(const FluidState* next,FluidState* target,scalar dt,FluidSolver::TIME_INTEGRATOR mode)
{
  FILL_NEIGH_MEMBER(0,this)
  FILL_NEIGH_MEMBER(2,next)
  //target
  MACVelocityField* TGFR=target ? target->velPtr(RHS_GHOST_FORCE).get() : NULL;
  ScalarField* TPR=target ? target->scalPtr(RHS_PRESSURE).get() : NULL;
  Vec3i periodicST=target ? target->getPeriodicS() : Vec3i::Zero();
  bool midP=mode == FluidSolver::MIDPOINT;
  scalar coefTensor=midP ? 0.25f : 1.0f;
  scalar residualNorm=0,residual=0;

  VecD rhsB;
  Vec6c validVelComp;
  Vec6 div=divStencil();
  sizeType dim=_P[0]->getDim(),nrPrimal=dim*2;
  rhsB.resize(nrPrimal+1);

  //add f(x) or R*(RHS-f(x))
  scalar coef=1/(scalar)pow(2,_P[0]->getDim());
  ITERSVP_ALL(*(_P[0]),*(_V[0]),OMP_FPRI(rhsB,validVelComp,residual))
  validVelComp=isVelValid(curr);
  const CellStencil& stencil=getStencil(curr);
  rhsB.setZero();
  for(sizeType i=0; i<(sizeType)stencil._tensors.size(); i++)
    stencil._tensors[i].evalBlk(offS,stencil._velComps,*(_V[2]),coefTensor,rhsB,midP ? _V[0] : NULL);
  for(sizeType j=0; j<nrPrimal; j++)
    if(validVelComp[j]) {
      rhsB[nrPrimal]+=div[j]*FETCHVELP(_GF[0],j);
      if(j%2 == 0) {
        rhsB[j]+=(FETCHVELP(_V[2],j)-FETCHVELP(_V[0],j))/dt-FETCHVELP(_GF[0],j)+div[j]*(FETCHSCALPC(_P[0])-FETCHSCALP(_P[0],j));
        if(target) {
          residual=FETCHVELP_COND(_GFR[0],j)-rhsB[j];
          restrict(*TGFR,curr,j/2,residual*coef,&add,periodicST);
          add(residualNorm,residual*residual);
        } else FETCHVELP((MACVelocityField*)_GFR[0],j)+=rhsB[j];
      }
    }
  if(target) {
    residual=FETCHSCALPC_COND(_PR[0])-rhsB[nrPrimal];
    restrict(*TPR,curr,residual*coef,&add);
    add(residualNorm,residual*residual);
  } else FETCHSCALPC((ScalarField*)_PR[0])+=rhsB[nrPrimal];
  ITERSVPEND
  return residualNorm;
}
//for full approximation scheme (FAS)
scalar FluidStateMG::calcFASRhs(const FluidStateMG& finer,scalar coefTensor)
{
  //coarser
  const MACVelocityField& V=vel(VELOCITY);
  const ScalarField& P=scal(PRESSURE);
  MACVelocityField& VR=velNonConst(RHS_VELOCITY);
  ScalarField& PR=scalNonConst(RHS_PRESSURE);
  Vec3i periodicSC=getPeriodicS();
  //finer
  const MACVelocityField& FV=finer.vel(VELOCITY);
  const ScalarField& FP=finer.scal(PRESSURE);
  const MACVelocityField& FVR=finer.vel(RHS_VELOCITY);
  const ScalarField& FPR=finer.scal(RHS_PRESSURE);

  VecD rhsB;
  Vec6c validVelComp;
  Vec6 div=divStencil();
  sizeType dim=P.getDim(),nrPrimal=dim*2;
  rhsB.resize(nrPrimal+1);

  //set to f(R*x)
  ITERSVP_ALL(P,V,OMP_FPRI(rhsB,validVelComp))
  validVelComp=isVelValid(curr);
  const CellStencil& stencil=getStencil(curr);
  rhsB.setZero();
  for(sizeType i=0; i<(sizeType)stencil._tensors.size(); i++)
    stencil._tensors[i].evalBlk(offS,stencil._velComps,V,coefTensor,rhsB);
  for(sizeType j=0; j<nrPrimal; j++)
    if(validVelComp[j]) {
      rhsB[nrPrimal]+=div[j]*FETCHVEL(V,j);
      if(j%2 == 0)
        FETCHVEL(VR,j)=FETCHVEL(V,j)+div[j]*(FETCHSCALC(P)-FETCHSCAL(P,j))+rhsB[j];
    }
  PR.get(curr)=rhsB[nrPrimal];
  ITERSVPEND

  //add R*(rhs-f(x))
  div=finer.divStencil();
  scalar coef=1/(scalar)pow(2,P.getDim()),errNorm=0,err=0;
  ITERSVP_ALL(FP,FV,OMP_FPRI(rhsB,validVelComp,err))
  validVelComp=finer.isVelValid(curr);
  const CellStencil& stencil=finer.getStencil(curr);
  rhsB.setZero();
  for(sizeType i=0; i<(sizeType)stencil._tensors.size(); i++)
    stencil._tensors[i].evalBlk(offS,stencil._velComps,FV,coefTensor,rhsB);
  for(sizeType j=0; j<nrPrimal; j++) {
    if(validVelComp[j]) {
      rhsB[nrPrimal]+=div[j]*FETCHVEL(FV,j);
      if(j%2 == 0) {
        err=FETCHVEL(FVR,j)-FETCHVEL(FV,j)-div[j]*(FETCHSCALC(FP)-FETCHSCAL(FP,j))-rhsB[j];
        restrict(VR,curr,j/2,err*coef,&add,periodicSC);
        add(errNorm,err*err);
      }
    }
  }
  restrict(PR,curr,(FPR.get(curr)-rhsB[nrPrimal])*coef,&add);
  ITERSVPEND

  //enforce boundary
  for(sizeType d=0; d<dim; d++) {
    ITERBDLSET(VR.getComp(d),d,getPeriodic())
    ITERBDRSET(VR.getComp(d),d)
  }
  return sqrt(errNorm);
}
void FluidStateMG::smoothSCGS(scalar coefTensor,bool FAS)
{
  const MACVelocityField& VR=vel(RHS_VELOCITY);
  const ScalarField& PR=scal(RHS_PRESSURE);
  MACVelocityField& V=velNonConst(VELOCITY);
  ScalarField& P=scalNonConst(PRESSURE);

  Matd lhsB;
  Vec6c validVelComp;
  Vec6 div=divStencil();
  VecD rhsB,xB,rhsB0,rhsB1,rhsB2;
  sizeType dim=P.getDim(),nrPrimal=dim*2;
  rhsB=VecD::Zero(nrPrimal+1);
  rhsB0=rhsB1=rhsB2=VecD::Zero(nrPrimal);
  for(sizeType pass=0; pass<pow(2,dim); pass++)
    ITERSVP(P,V,_start[pass],2,OMP_FPRI(lhsB,rhsB,xB,validVelComp , rhsB0,rhsB1,rhsB2))
    validVelComp=isVelValid(curr);
  const CellStencil& stencil=getStencil(curr);
  //assemble rhs
  rhsB.setZero();
  for(sizeType j=0; j<nrPrimal; j++)
    if(validVelComp[j])
      rhsB[j]=FETCHVEL(VR,j)+div[j]*FETCHSCAL(P,j);
  rhsB0=rhsB.block(0,0,nrPrimal,1);
  rhsB[nrPrimal]=PR.get(curr);
  //assemble lhs
  lhsB=stencil._lhs;
  for(sizeType i=0; i<(sizeType)stencil._tensors.size(); i++)
    stencil._tensors[i].evalSmoothBlk(offS,stencil._velComps,V,coefTensor,rhsB,&lhsB,FAS);
  //solve and assign
  xB=lhsB.fullPivLu().solve(rhsB);
  for(sizeType j=0; j<nrPrimal; j++)
    if(validVelComp[j])
      FETCHVEL(V,j)=(scalar)xB[j];
  P.get(curr)=(scalar)xB[nrPrimal];
  ITERSVPEND
}
//for enforce div free
scalar FluidStateMG::calcDivRhs(const FluidStateMG& finer)
{
  //coarser
  ScalarField& PR=scalNonConst(RHS_PRESSURE);
  //finer
  const ScalarField& FP=finer.scal(PRESSURE);
  const ScalarField& FPR=finer.scal(RHS_PRESSURE);

  Vec6c validVelComp;
  Vec6 lap=finer.lapStencil();
  sizeType dim=FP.getDim(),nrPrimal=dim*2;
  scalar coef=1/(scalar)pow(2,PR.getDim()),errNorm=0,err=0,diag=0;
  ITERSP_ALL(FPR,OMP_FPRI(validVelComp,err,diag))
  diag=0;
  err=FPR.get(offS);
  validVelComp=finer.isVelValid(curr);
  const CellStencil& stencil=finer.getStencil(curr);
  for(sizeType j=0; j<nrPrimal; j++)
    if(validVelComp[j]) {
      err+=lap[j]*FETCHSCALS(FP,j);
      diag-=lap[j];
    }
  err+=diag*FP.get(offS);
  restrict(PR,curr,err*coef,&Traits::add);
  add(errNorm,err*err);
  ITERSVPEND
  return sqrt(errNorm);
}
void FluidStateMG::smoothRBGS()
{
  const ScalarField& PR=scal(RHS_PRESSURE);
  ScalarField& P=scalNonConst(PRESSURE);

  scalar diag,val;
  Vec6c validVelComp;
  Vec6 lap=lapStencil();
  sizeType dim=P.getDim(),nrPrimal=dim*2;
  for(sizeType pass=0; pass<2; pass++)
    ITERSP_ALL(P,OMP_PRI(validVelComp,diag,val))
    if(curr.sum()%2 != pass)
      continue;
  diag=0;
  val=PR.get(offS);
  validVelComp=isVelValid(curr);
  const CellStencil& stencil=getStencil(curr);
  for(sizeType j=0; j<nrPrimal; j++)
    if(validVelComp[j]) {
      val+=lap[j]*FETCHSCALS(P,j);
      diag+=lap[j];
    }
  P.get(offS)=val/diag;
  ITERSPEND
}
void FluidStateMG::project(VecD* v)
{
  scalar delta;
  Vec6c validVelComp;
  const ScalarField& pre=scal(FluidState::PRESSURE);
  MACVelocityField& vel=velNonConst(FluidState::VELOCITY);
  Vec3 invCellSz=pre.getInvCellSize();
  for(sizeType d=0,offV=0; d<vel.getDim(); offV+=vel.getComp(d).getNrPoint().prod(),d++) {
    ITERSP_PERIODIC(pre,getPeriodic(),OMP_PRI(delta,validVelComp))
    validVelComp=isVelValid(curr);
    const CellStencil& stencil=getStencil(curr);
    delta=(pre.get(offS)-FETCHSCALS(pre,d*2))*invCellSz[d];
    if(v)
      (*v)[offV+vel.getComp(d).getIndex(curr)]-=delta;
    else vel.getComp(d).get(curr)-=delta;
    ITERSPEND
    //enforce boundary condition
    if(v) {
      ITERBDL(vel.getComp(d),d)
      if(!getPeriodic()[d])(*v)[offV+vel.getComp(d).getIndex(curr)]=0;
      ITERBDLEND
      ITERBDR(vel.getComp(d),d)
      (*v)[offV+vel.getComp(d).getIndex(curr)]=0;
      ITERBDLEND
    } else {
      ITERBDLSET(vel.getComp(d),d,getPeriodic())
      ITERBDRSET(vel.getComp(d),d)
    }
  }
}
//stencil extraction for debug
Vec3i FluidStateMG::getStencilCell(sizeType off) const
{
  Vec3i ret=Vec3i::Zero();
  const Vec3i nrCell=scal(PRESSURE).getNrCell();
  sizeType type=0;
  for(sizeType d=0; d<3; d++) {
    type=off%5;
    if(type == 0)
      ret[d]=0;
    else if(type == 1)
      ret[d]=1;
    else if(type == 4)
      ret[d]=nrCell[d]-1;
    else if(type == 3)
      ret[d]=nrCell[d]-2;
    else {
      ASSERT(type == 2)
      ret[d]=2;
    }
    off/=5;
  }
  return ret;
}
sizeType FluidStateMG::getStencilId(const Vec3i& cid) const
{
  const Vec3i nrCell=scal(PRESSURE).getNrCell();
  sizeType id=0,type=0;
  for(sizeType d=0,coef=1; d<3; d++,coef*=5) {
    if(cid[d] == 0)
      type=0;
    else if(cid[d] == 1)
      type=1;
    else if(cid[d] == nrCell[d]-1)
      type=4;
    else if(cid[d] == nrCell[d]-2)
      type=3;
    else type=2;
    id+=type*coef;
  }
  return id;
}
const CellStencil& FluidStateMG::getStencil(const Vec3i& cid) const
{
  return _stencils[getStencilId(cid)];
}
CellStencil& FluidStateMG::getStencil(const Vec3i& cid)
{
  return _stencils[getStencilId(cid)];
}
//for smoothing
scalar FluidStateMG::solveSTSCGS(Matd& lhsB,VecD& rhsB,VecD& xB,Vec6c& validVelComp,const Vec6& div,const Vec3i& curr,const Vec4i& offS,bool midP,scalar coefTensor,
                                 scalar dt,sizeType lhsSTId,sizeType nrPrimal,sizeType primalLast,sizeType primalNext,sizeType offPrimal,sizeType dualLast,sizeType dualNext)
{
  scalar delta=0,err=0;
  validVelComp=isVelValid(curr);
  const CellStencil& stencil=getStencil(curr);
  //assemble system: rhs primal
  rhsB.setZero();
  for(sizeType j=0; j<nrPrimal; j++)
    if(validVelComp[j]) {
      {
        xB[j]=FETCHVELP(_V[0],j);
        rhsB[j]+=FETCHVELP_COND(_VR[0],j)+div[j]*FETCHSCALP(_GFP[0],j);
      }
      if(primalLast >= 0) {
        xB[primalLast+j]=FETCHVELP(_GF[1],j);
        rhsB[primalLast+j]=FETCHVELP_COND(_GFR[1],j)+FETCHVELP(_V[1],j)/dt+div[j]*FETCHSCALP(_P[1],j);
      }
      if(primalNext >= 0) {
        xB[primalNext+j]=FETCHVELP(_GF[0],j);
        rhsB[primalNext+j]=FETCHVELP_COND(_GFR[0],j)-FETCHVELP(_V[2],j)/dt+div[j]*FETCHSCALP(_P[0],j);
      }
    }
  //assemble system: rhs dual
  {
    xB[offPrimal]=FETCHSCALPC(_GFP[0]);
    if(_GFPR[0])
      rhsB[offPrimal]+=FETCHSCALPC(_GFPR[0]);
  }
  if(dualLast >= 0) {
    xB[dualLast]=FETCHSCALPC(_P[1]);
    if(_PR[1])
      rhsB[dualLast]+=FETCHSCALPC(_PR[1]);
  }
  if(dualNext >= 0) {
    xB[dualNext]=FETCHSCALPC(_P[0]);
    if(_PR[0])
      rhsB[dualNext]+=FETCHSCALPC(_PR[0]);
  }
  //assemble system: advection related lhs/rhs terms
  lhsB=stencil._lhsST[lhsSTId];
  if(primalLast >= 0) {
    Eigen::Block<VecD> rhsBV=rhsB.block(0,0,nrPrimal,1);
    Eigen::Block<VecD> rhsBGF=rhsB.block(primalLast,0,nrPrimal,1);
    Eigen::Block<Matd> lhsBGF=lhsB.block(primalLast,0,nrPrimal,nrPrimal);
    for(sizeType i=0; i<(sizeType)stencil._tensors.size(); i++)
      stencil._tensors[i].evalDerivKKTBlk(offS,stencil._velComps,*(_V[0]),*(_GF[1]),coefTensor,rhsBGF,rhsBV,lhsBGF,true,midP ? _V[1] : NULL);
    lhsB.block(0,primalLast,nrPrimal,nrPrimal)=lhsBGF.transpose();
  }
  if(primalNext >= 0) {
    if(midP) {
      Eigen::Block<VecD> rhsBV=rhsB.block(0,0,nrPrimal,1);
      Eigen::Block<VecD> rhsBGF=rhsB.block(primalNext,0,nrPrimal,1);
      Eigen::Block<Matd> lhsBGF=lhsB.block(primalNext,0,nrPrimal,nrPrimal);
      for(sizeType i=0; i<(sizeType)stencil._tensors.size(); i++)
        stencil._tensors[i].evalDerivKKTBlk(offS,stencil._velComps,*(_V[0]),*(_GF[0]),coefTensor,rhsBGF,rhsBV,lhsBGF,true,midP ? _V[2] : NULL);
      lhsB.block(0,primalNext,nrPrimal,nrPrimal)=lhsBGF.transpose();
    } else {
      Eigen::Block<VecD> rhsBGF=rhsB.block(primalNext,0,nrPrimal,1);
      for(sizeType i=0; i<(sizeType)stencil._tensors.size(); i++)
        stencil._tensors[i].evalBlk(offS,stencil._velComps,*(_V[2]),-1,rhsBGF,NULL);
    }
  }
  //solve system: Levenberg-Marquardt step
  rhsB-=lhsB*xB;
  xB=lhsB.fullPivLu().solve(rhsB);
  //assign: primal
  for(sizeType j=0; j<nrPrimal; j++)
    if(validVelComp[j]) {
      {
        scalar& val=FETCHVELP(_V[0],j);
        val+=err=(scalar)xB[j];
        add(delta,err*err);
      }
      if(primalLast >= 0) {
        scalar& val=FETCHVELP(_GF[1],j);
        val+=err=(scalar)xB[primalLast+j];
        add(delta,err*err);
      }
      if(primalNext >= 0) {
        scalar& val=FETCHVELP(_GF[0],j);
        val+=err=(scalar)xB[primalNext+j];
        add(delta,err*err);
      }
    }
  //assign: dual
  {
    scalar& val=FETCHSCALPC(_GFP[0]);
    val+=err=(scalar)xB[offPrimal];
    add(delta,err*err);
  }
  if(dualLast >= 0) {
    scalar& val=FETCHSCALPC(_P[1]);
    val+=err=(scalar)xB[dualLast];
    add(delta,err*err);
  }
  if(dualNext >= 0) {
    scalar& val=FETCHSCALPC(_P[0]);
    val+=err=(scalar)xB[dualNext];
    add(delta,err*err);
  }
  return delta;
}
scalar FluidStateMG::solveSTSCGSQuadratic(vector<QuadraticTerm>& terms,Matd& lhsB,VecD& rhsB,VecD& xB,sizeType lhsSTId,const Vec3i& curr,const Vec4i& offS,bool solveLM)
{
  scalar delta=0;
  const CellStencil& stencil=getStencil(curr);
  terms=stencil._termST[lhsSTId];
  lhsB.setZero(stencil._rowsST[lhsSTId],stencil._rowsST[lhsSTId]);
  rhsB.setZero(stencil._rowsST[lhsSTId]);
  QuadraticTerm::assemble(offS,stencil,stencil._variableST[lhsSTId],xB);
  QuadraticTerm::remap(offS,stencil,terms,lhsB,rhsB);
  //Least-Square solve
  if(solveLM)
    add(delta,(scalar)QuadraticTerm::solveLM(terms,lhsB,rhsB,xB,false,false));
  else add(delta,(scalar)QuadraticTerm::solveNewton(terms,lhsB,rhsB,xB));
  QuadraticTerm::assign(offS,stencil,stencil._variableST[lhsSTId],xB);
  return delta;
}
scalar FluidStateMG::solveSTTridiag(vector<const Matd*>& diag,MatdArr offDiag,VecDArr& rhs,VecDArr& sol,Vec6c& validVelComp,const Vec6& div,const Vec3i& curr,const Vec4i& offS,
                                    bool midP,scalar coefTensor,sizeType nrPrimal,sizeType nrS)
{
  scalar delta=0,err=0;
  validVelComp=isVelValid(curr);
  const CellStencil& stencil=getStencil(curr);
  //assemble system: initialize
  for(sizeType s=0,solId=0; s<nrS; s++,solId+=2) {
    {
      sol[solId][nrPrimal]=FETCHSCALPC(_GFP[s]);
      rhs[solId].setZero();
      if(_GFPR[s])
        rhs[solId][nrPrimal]=FETCHSCALPC(_GFPR[s]);
      diag[solId]=&(stencil._diagP);
    }
    if(s<nrS-1) {
      sol[solId+1][nrPrimal]=FETCHSCALPC(_P[s]);
      rhs[solId+1].setZero();
      if(_PR[s])
        rhs[solId+1][nrPrimal]=FETCHSCALPC(_PR[s]);
      diag[solId+1]=&(stencil._diagD);
      offDiag[solId]=stencil._offDiagLast;
      offDiag[solId+1]=stencil._offDiagNext;
    }
  }
  //assemble system: rhs
  for(sizeType j=0; j<nrPrimal; j++)
    if(validVelComp[j])
      for(sizeType s=0,solId=0; s<nrS; s++,solId+=2) {
        {
          sol[solId][j]=FETCHVELP(_V[s],j);
          rhs[solId][j]=FETCHVELP_COND(_VR[s],j)+div[j]*FETCHSCALP(_GFP[s],j);
        }
        if(s<nrS-1) {
          sol[solId+1][j]=FETCHVELP(_GF[s],j);
          rhs[solId+1][j]=FETCHVELP_COND(_GFR[s],j)+div[j]*FETCHSCALP(_P[s],j);
        }
      }
  //assemble system: advection related lhs/rhs terms
  for(sizeType s=0,solId=0; s<nrS-1; s++,solId+=2) {
    if(midP)
      for(sizeType i=0; i<(sizeType)stencil._tensors.size(); i++)
        stencil._tensors[i].evalDerivKKTBlk(offS,stencil._velComps,*(_V[s]),*(_GF[s]),coefTensor,rhs[solId+1],rhs[solId],offDiag[solId],false,midP ? _V[s+1] : NULL);
    for(sizeType i=0; i<(sizeType)stencil._tensors.size(); i++)
      stencil._tensors[i].evalDerivKKTBlk(offS,stencil._velComps,*(_V[s+1]),*(_GF[s]),coefTensor,rhs[solId+1],rhs[solId+2],offDiag[solId+1],true,midP ? _V[s] : NULL);
    offDiag[solId+1]=offDiag[solId+1].transpose().eval();
  }
  //solve system: Newton-step
  Traits::multAdd(diag,offDiag,NULL,sol,rhs,-1);
  Traits::triDiag(diag,offDiag,rhs,sol);
  //assign: primal
  for(sizeType j=0; j<nrPrimal; j++)
    if(validVelComp[j])
      for(sizeType s=0,solId=0; s<nrS; s++,solId+=2) {
        {
          scalar& val=FETCHVELP(_V[s],j);
          val+=err=(scalar)sol[solId][j];
          add(delta,err*err);
        }
        if(s<nrS-1) {
          scalar& val=FETCHVELP(_GF[s],j);
          val+=err=(scalar)sol[solId+1][j];
          add(delta,err*err);
        }
      }
  //assign: dual
  for(sizeType s=0,solId=0; s<nrS; s++,solId+=2) {
    {
      scalar& val=FETCHSCALPC(_GFP[s]);
      val+=err=(scalar)sol[solId][nrPrimal];
      add(delta,err*err);
    }
    if(s<nrS-1) {
      scalar& val=FETCHSCALPC(_P[s]);
      val+=err=(scalar)sol[solId+1][nrPrimal];
      add(delta,err*err);
    }
  }
  return delta;
}
scalar FluidStateMG::solveSTTridiagQuadratic(vector<QuadraticTerm>& terms,MatdArr& diag,MatdArr& offDiag,MatdArr& offDiag2,VecDArr& rhs,VecDArr& sol,const Vec3i& curr,const Vec4i& offS)
{
  scalar delta=0;
  const CellStencil& stencil=getStencil(curr);
  terms=stencil._termTridiagST;

  //VecD x=VecD::Zero((nrS*2-1)*(nrPrimal+1));
  //Matd lhs=Matd::Zero(x.size(),x.size());
  //VecD rhs=VecD::Zero(x.size());
  //QuadraticTerm::assemble(offS,stencil,stencil._variableTridiagST,x);
  //QuadraticTerm::remap(offS,stencil,terms,lhs,rhs);
  //add(delta,QuadraticTerm::solveLM(terms,lhs,rhs,x));
  //QuadraticTerm::assign(offS,stencil,stencil._variableTridiagST,x);

  Traits::setZero(diag);
  Traits::setZero(offDiag);
  Traits::setZero(offDiag2);
  Traits::setZero(rhs);
  QuadraticTerm::assemble(offS,stencil,stencil._variableTridiagST,sol);
  QuadraticTerm::remap(offS,stencil,terms,diag,offDiag,offDiag2,rhs);
  //Tridiag Least-Square solve
  add(delta,(scalar)QuadraticTerm::solveLM(terms,diag,offDiag,offDiag2,rhs,sol,false,false));
  QuadraticTerm::assign(offS,stencil,stencil._variableTridiagST,sol);
  return delta;
}
const Vec3i FluidStateMG::_start[8] = {
  Vec3i(0,0,0),
  Vec3i(1,0,0),
  Vec3i(0,1,0),
  Vec3i(1,1,0),
  Vec3i(0,0,1),
  Vec3i(1,0,1),
  Vec3i(0,1,1),
  Vec3i(1,1,1),
};

FluidSolverSpatialMG::FluidSolverSpatialMG(const boost::property_tree::ptree& pt):FluidSolver(pt) {}
void FluidSolverSpatialMG::reset(const FluidState& s)
{
  //we only support POT grid size
  ASSERT_MSG(s.scal(FluidState::PRESSURE).getNrPoint() == makePOT(s.scal(FluidState::PRESSURE).getNrPoint(),2),
             "We only support grid with number of cells exactly power of 2!");
  //build MG levels
  _level.clear();
  FluidStateMG level;
  level.init(s.scal(FluidState::PRESSURE).getNrPoint(),s.scal(FluidState::PRESSURE).getBB().getExtent(),false,s.getPeriodic());
  sizeType dim=level.scal(FluidState::PRESSURE).getDim();
  while(true) {
    _level.push_back(level);
    Vec3i nrPoint=level.scal(FluidState::PRESSURE).getNrPoint();
    if(nrPoint.block(0,0,dim,1).minCoeff() <= 2)
      break;
    else level.init(nrPoint/2,s.scal(FluidState::PRESSURE).getBB().getExtent(),false,s.getPeriodic());
  }
  _adv.reset(s);
}
void FluidSolverSpatialMG::solve(FluidState& from,FluidState& to)
{
  scalar dt=_pt.get<scalar>("dt",0.0f);
  sizeType substep=_pt.get<sizeType>("substep",1);
  FluidState A=from,B=to;
  B.velPtr(FluidState::GHOST_FORCE)=A.velPtr(FluidState::GHOST_FORCE);
  INFOV("Using %ld substeps",substep)
  for(sizeType i=0; i<substep;) {
    INFOV("At substep %ld",i)
    if(!solveInner(A,B,dt/(scalar)substep)) {
      //start again with more substeps
      substep++;
      A=from;
      B=to;
      i=0;
    } else {
      swap(A,B);
      i++;
    }
  }
  to.velNonConst(FluidState::VELOCITY)=A.vel(FluidState::VELOCITY);
  to.scalNonConst(FluidState::PRESSURE)=A.scal(FluidState::PRESSURE);
}
bool FluidSolverSpatialMG::enforceDivFree(FluidState& from,FluidState::FIELD f,VecD* v,FluidState::FIELD fp)
{
  _level[0].save();
  if(!v) {
    ASSERT_MSG(from.velPtr(f),"We cannot find vector field for projection!")
    ASSERT_MSG(from.scalPtr(FluidState::PRESSURE),"We cannot find pressure field for projection!")
    _level[0].velPtr(FluidState::VELOCITY)=from.velPtr(f);
  }
  if(fp >= 0) {
    ASSERT_MSGV(from.scalPtr(fp),"We cannot find field %d for projection!",fp)
    _level[0].scalPtr(FluidState::PRESSURE)=from.scalPtr(fp);
  }
  _level[0].calcDivergence(FluidState::RHS_PRESSURE,FluidState::VELOCITY,v);
  _level[0].scalNonConst(FluidState::PRESSURE).init(0);
  //solve by VCycle
  sizeType it=0,maxIt=_pt.get<sizeType>("maxIterDiv",20);
  scalar eps=_pt.get<scalar>("epsilon",1E-5f);
  scalar totalDiv=_level[0].scal(FluidState::RHS_PRESSURE).sum();
  scalar maxDiv=_level[0].scal(FluidState::RHS_PRESSURE).getAbsMax();
  for(; it<maxIt; it++) {
    scalar err=VCycle(_level,0,2,2,10);
    if(it > 15) {
      INFOV("Div total: %f, Div max: %f, Error: %f",totalDiv,maxDiv,err)
    }
    if(err<eps || err<eps*eps*maxDiv)
      break;
  }
  //project out
  _level[0].project(v);
  _level[0].load();
  return it<maxIt;
}
scalarD FluidSolverSpatialMG::solveSmooth(vector<boost::shared_ptr<FluidState> > states,SMOOTHST_TYPE type)
{
  scalarD ret=0;
  FluidStateMG* lv=NULL;
  for(sizeType i=0; i < (sizeType)_level.size(); i++)
    if(_level[i].nrVelComp() == states[0]->nrVelComp()) {
      lv=&(_level[i]);
      break;
    }
  ASSERT_MSG(lv,"Provided smoothing grid size inconsistent with any level!")
  lv->save();
  //create necessary field
  TIME_INTEGRATOR mode=(TIME_INTEGRATOR)_pt.get<sizeType>("timeIntegrator",BACKWARD);
  scalar dt=_pt.get<scalar>("dt",0.0f);
  scalar regK=_pt.get<scalar>("regK",1000.0f);
  scalar regU=_pt.get<scalar>("regU",100.0f);
  lv->initSTSCGS(dt,regK/regU,mode,(sizeType)states.size());
  switch(type) {
  case SMOOTHST_STANDARD:
    ret=lv->smoothSTTridiag(states,mode);
    break;
  case SMOOTHST_LEAST_SQUARE:
    ret=lv->smoothSTTridiagQuadratic(states);
    break;
  }
  //Traits::debugWrite();
  lv->load();
  return ret;
}
scalarD FluidSolverSpatialMG::solveSmooth(FluidState* last,FluidState& curr,FluidState* next,SMOOTHST_TYPE type)
{
  scalarD ret=0;
  FluidStateMG* lv=NULL;
  for(sizeType i=0; i < (sizeType)_level.size(); i++)
    if(_level[i].nrVelComp() == curr.nrVelComp()) {
      lv=&(_level[i]);
      break;
    }
  ASSERT_MSG(lv,"Provided smoothing grid size inconsistent with any level!")
  lv->save();
  //create necessary field
  ASSERT(curr.scalPtr(FluidState::GHOST_FORCE_PRESSURE))
  if(last) {
    ASSERT(last->velPtr(FluidState::GHOST_FORCE))
  }
  if(next) {
    ASSERT(curr.velPtr(FluidState::GHOST_FORCE))
  }
  //PRESSURE|RHS_PRESSURE|GHOST_FORCE_PRESSURE|RHS_GHOST_FORCE_PRESSURE
  lv->scalPtr(FluidState::PRESSURE)=curr.scalPtr(FluidState::PRESSURE);
  lv->scalPtr(FluidState::RHS_PRESSURE)=curr.scalPtr(FluidState::RHS_PRESSURE);
  lv->scalPtr(FluidState::GHOST_FORCE_PRESSURE)=curr.scalPtr(FluidState::GHOST_FORCE_PRESSURE);
  lv->scalPtr(FluidState::RHS_GHOST_FORCE_PRESSURE)=curr.scalPtr(FluidState::RHS_GHOST_FORCE_PRESSURE);
  //VELOCITY|RHS_VELOCITY|GHOST_FORCE|RHS_GHOST_FORCE
  lv->velPtr(FluidState::VELOCITY)=curr.velPtr(FluidState::VELOCITY);
  lv->velPtr(FluidState::RHS_VELOCITY)=curr.velPtr(FluidState::RHS_VELOCITY);
  lv->velPtr(FluidState::GHOST_FORCE)=curr.velPtr(FluidState::GHOST_FORCE);
  lv->velPtr(FluidState::RHS_GHOST_FORCE)=curr.velPtr(FluidState::RHS_GHOST_FORCE);
  //smooth
  TIME_INTEGRATOR mode=(TIME_INTEGRATOR)_pt.get<sizeType>("timeIntegrator",BACKWARD);
  scalar dt=_pt.get<scalar>("dt",0.0f);
  scalar regK=_pt.get<scalar>("regK",1000.0f);
  scalar regU=_pt.get<scalar>("regU",100.0f);
  lv->initSTSCGS(dt,regK/regU,mode,3);
  switch(type) {
  case SMOOTHST_STANDARD:
    ret=lv->smoothSTSCGS(last,next,dt,mode);
    break;
  case SMOOTHST_LEAST_SQUARE:
    ret=lv->smoothSTSCGSQuadratic(last,next);
    break;
  }
  lv->load();
  return ret;
}
scalarD FluidSolverSpatialMG::computePrimalRhs(const FluidState* last,FluidState& curr,const FluidState* next,FluidState* target)
{
  FluidStateMG* lv=NULL;
  for(sizeType i=0; i < (sizeType)_level.size(); i++)
    if(_level[i].nrVelComp() == curr.nrVelComp()) {
      lv=&(_level[i]);
      break;
    }
  ASSERT_MSG(lv,"Provided smoothing grid size inconsistent with any level!")
  lv->save();
  //PRESSURE|RHS_PRESSURE
  lv->scalPtr(FluidState::GHOST_FORCE_PRESSURE)=curr.scalPtr(FluidState::GHOST_FORCE_PRESSURE);
  lv->scalPtr(FluidState::RHS_GHOST_FORCE_PRESSURE)=curr.scalPtr(FluidState::RHS_GHOST_FORCE_PRESSURE);
  //VELOCITY|GHOST_FORCE|RHS_GHOST_FORCE
  lv->velPtr(FluidState::VELOCITY)=curr.velPtr(FluidState::VELOCITY);
  lv->velPtr(FluidState::GHOST_FORCE)=curr.velPtr(FluidState::GHOST_FORCE);
  lv->velPtr(FluidState::RHS_VELOCITY)=curr.velPtr(FluidState::RHS_VELOCITY);
  //compute dual rhs with selective reduction
  TIME_INTEGRATOR mode=(TIME_INTEGRATOR)_pt.get<sizeType>("timeIntegrator",BACKWARD);
  scalar dt=_pt.get<scalar>("dt",0.0f);
  scalar regK=_pt.get<scalar>("regK",1000.0f);
  scalar regU=_pt.get<scalar>("regU",100.0f);
  lv->initSTSCGS(dt,regK/regU,mode,3);
  scalarD ret=lv->calcPrimalFASRhs(last,next,target,dt,regK/regU,mode);
  lv->load();
  return ret;
}
scalarD FluidSolverSpatialMG::computeDualRhs(FluidState& curr,const FluidState& next,FluidState* target)
{
  FluidStateMG* lv=NULL;
  for(sizeType i=0; i < (sizeType)_level.size(); i++)
    if(_level[i].nrVelComp() == curr.nrVelComp()) {
      lv=&(_level[i]);
      break;
    }
  ASSERT_MSG(lv,"Provided smoothing grid size inconsistent with any level!")
  lv->save();
  //PRESSURE|RHS_PRESSURE
  lv->scalPtr(FluidState::PRESSURE)=curr.scalPtr(FluidState::PRESSURE);
  lv->scalPtr(FluidState::RHS_PRESSURE)=curr.scalPtr(FluidState::RHS_PRESSURE);
  //VELOCITY|GHOST_FORCE|RHS_GHOST_FORCE
  lv->velPtr(FluidState::VELOCITY)=curr.velPtr(FluidState::VELOCITY);
  lv->velPtr(FluidState::GHOST_FORCE)=curr.velPtr(FluidState::GHOST_FORCE);
  lv->velPtr(FluidState::RHS_GHOST_FORCE)=curr.velPtr(FluidState::RHS_GHOST_FORCE);
  //compute dual rhs with selective reduction
  TIME_INTEGRATOR mode=(TIME_INTEGRATOR)_pt.get<sizeType>("timeIntegrator",BACKWARD);
  scalar dt=_pt.get<scalar>("dt",0.0f);
  scalar regK=_pt.get<scalar>("regK",1000.0f);
  scalar regU=_pt.get<scalar>("regU",100.0f);
  lv->initSTSCGS(dt,regK/regU,mode,3);
  scalarD ret=lv->calcDualFASRhs(&next,target,dt,mode);
  lv->load();
  return ret;
}
const vector<FluidStateMG>& FluidSolverSpatialMG::levels() const
{
  return _level;
}
bool FluidSolverSpatialMG::solveInner(FluidState& from,FluidState& to,scalar dt)
{
  _level[0].save();
  TIME_INTEGRATOR mode=(TIME_INTEGRATOR)_pt.get<sizeType>("timeIntegrator",BACKWARD);
  scalar coefRhs=mode == MIDPOINT ? 2 : 1;
  scalar coefTensor=mode == MIDPOINT ? 0.25f*dt : dt;
  //set LHS
  _level[0].velPtr(FluidState::VELOCITY)=to.velPtr(FluidState::VELOCITY);
  _level[0].scalPtr(FluidState::PRESSURE)=from.scalPtr(FluidState::PRESSURE);
  _level[0].velNonConst(FluidState::VELOCITY)=from.vel(FluidState::VELOCITY);
  _level[0].setZero(FluidState::PRESSURE);
  //set RHS dual
  _level[0].velNonConst(FluidState::RHS_VELOCITY)=from.vel(FluidState::VELOCITY);
  _level[0].velNonConst(FluidState::RHS_VELOCITY).mul(coefRhs);
  if(from.velPtr(FluidState::GHOST_FORCE))
    _level[0].velNonConst(FluidState::RHS_VELOCITY).addScaled(from.vel(FluidState::GHOST_FORCE),dt);
  if(coefRhs == 2)
    _level[0].calcDivergence(FluidState::RHS_PRESSURE,FluidState::VELOCITY);
  //solve
  sizeType it=0;
  scalar lastErr=ScalarUtil<scalar>::scalar_max,err;
  for(; it<_pt.get<sizeType>("maxIterSolve",10000); it++) {
    //inner solver loop
    err=FASVCycle(_level,0,2,2,10,coefTensor);
    //check error
    if(_pt.get<bool>("checkError",false)) {
      sizeType nrC=_level[0].nrCell();
      sizeType nrV=_level[0].nrVelComp();
      VecD rhs=VecD::Zero(nrV+nrC);
      _level[0].toVec(FluidState::RHS_VELOCITY,rhs);
      _level[0].toVecScal(FluidState::RHS_PRESSURE,rhs,&set,nrV);
      VecD lhs=VecD::Zero(nrV+nrC);
      _level[0].toVec(FluidState::VELOCITY,lhs);
      _level[0].toVecScal(FluidState::PRESSURE,lhs,&set,nrV);
      checkConvergence(_level[0],lhs,rhs,coefTensor,!_pt.get<bool>("quiet",false));
    }
    if(!_pt.get<bool>("quiet",false)) {
      INFOV("Error: %f",err)
    }
    if(err<_pt.get<scalar>("epsilon",1E-5f))
      break;
    else if(err > lastErr || isNan(err) || isInf(err))
      return false;
    else lastErr=err;
  }
  //update velocity
  if(mode == MIDPOINT)
    _level[0].velNonConst(FluidState::VELOCITY).sub(from.vel(FluidState::VELOCITY));
  if(!_pt.get<bool>("quiet",false)) {
    INFOV("\tIter: %ld, VNorm: %f",it,sqrt(_level[0].vel(FluidState::VELOCITY).squaredNorm()))
  }
  _level[0].load();
  return true;
}
scalar FluidSolverSpatialMG::FASVCycle(vector<FluidStateMG>& levels,sizeType lvCurr,sizeType nrPre,sizeType nrPost,sizeType nrFinal,scalar coefTensor)
{
  scalar ret=0;
  if(lvCurr == (sizeType)levels.size()-1) {
    for(sizeType i=0; i<nrFinal; i++)
      levels[lvCurr].smoothSCGS(coefTensor,true);
  } else {
    for(sizeType i=0; i<nrPre; i++)
      levels[lvCurr].smoothSCGS(coefTensor,true);
    //restrict solution: R*x
    levels[lvCurr+1].setZero(FluidState::VELOCITY|FluidState::PRESSURE);
    levels[lvCurr+1].restrictV(levels[lvCurr],FluidState::VELOCITY,FluidState::VELOCITY,&add);
    levels[lvCurr+1].restrictS(levels[lvCurr],FluidState::PRESSURE,FluidState::PRESSURE,&add);
    //restrict residual: f(R*x)+R*(rhs-f(x))
    ret=levels[lvCurr+1].calcFASRhs(levels[lvCurr],coefTensor);
    //solve coarse
    levels[lvCurr+1].interpolateV(levels[lvCurr],FluidState::VELOCITY,FluidState::VELOCITY,&sub);
    levels[lvCurr+1].interpolateS(levels[lvCurr],FluidState::PRESSURE,FluidState::PRESSURE,&sub);
    FASVCycle(levels,lvCurr+1,nrPre,nrPost,nrFinal,coefTensor);
    levels[lvCurr+1].interpolateV(levels[lvCurr],FluidState::VELOCITY,FluidState::VELOCITY,&add);
    levels[lvCurr+1].interpolateS(levels[lvCurr],FluidState::PRESSURE,FluidState::PRESSURE,&add);
    for(sizeType i=0; i<nrPost; i++)
      levels[lvCurr].smoothSCGS(coefTensor,true);
  }
  return ret;
}
scalar FluidSolverSpatialMG::VCycle(vector<FluidStateMG>& levels,sizeType lvCurr,sizeType nrPre,sizeType nrPost,sizeType nrFinal)
{
  scalar ret=0;
  if(lvCurr == (sizeType)levels.size()-1) {
    for(sizeType i=0; i<nrFinal; i++)
      levels[lvCurr].smoothRBGS();
  } else {
    for(sizeType i=0; i<nrPre; i++)
      levels[lvCurr].smoothRBGS();
    //restrict solution: R*x
    levels[lvCurr+1].setZero(FluidState::PRESSURE|FluidState::RHS_PRESSURE);
    //restrict residual: R*(rhs-f(x))
    ret=levels[lvCurr+1].calcDivRhs(levels[lvCurr]);
    //solve coarse
    VCycle(levels,lvCurr+1,nrPre,nrPost,nrFinal);
    levels[lvCurr+1].interpolateS(levels[lvCurr],FluidState::PRESSURE,FluidState::PRESSURE,&add);
    for(sizeType i=0; i<nrPost; i++)
      levels[lvCurr].smoothRBGS();
  }
  return ret;
}

PRJ_END
