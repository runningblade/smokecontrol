#ifndef ITERATOR_H
#define ITERATOR_H

//#include "CommonFile/MathBasic.h"

//for iterating all points parallelly
#define ITERSP(S,I,STEP,...){  \
Vec3i nrP=(S).getNrPoint(); \
Vec3i curr=Vec3i::Zero(),II=I;  \
Vec3i strideS=(S).getStride(),strideSS=strideS*STEP; \
sizeType offS0=0,offS=0; \
PRAGMA(STRINGIFY_OMP(omp parallel for firstprivate(curr,offS0,offS) __VA_ARGS__)) \
for(sizeType x=II[0];x<nrP[0];x+=STEP) \
for(curr[0]=x,curr[1]=II[1],offS0=strideS[0]*x+strideS[1]*curr[1]; \
    curr[1]<nrP[1]; \
    curr[1]+=STEP,offS0+=strideSS[1]) \
for(curr[2]=II[2],offS=offS0+strideS[2]*curr[2];  \
    curr[2]<nrP[2]; \
    curr[2]+=STEP,offS+=strideSS[2]) {
#define ITERSPEND }}
#define ITERSP_ALL(S,...) ITERSP(S,Vec3i::Zero(),1,__VA_ARGS__)
#define ITERSP_PERIODIC(S,PERIODIC,...) ITERSP(S,Vec3i::Unit(d)*(1-(PERIODIC)[d]),1,__VA_ARGS__)

#define ITERSVP(S,V,I,STEP,...){  \
Vec3i nrP=(S).getNrPoint(); \
Vec3i curr=Vec3i::Zero(),II=I;  \
Vec3i strideS=(S).getStride(),strideSS=strideS*STEP; \
Vec3i strideVX=(V).getComp(0).getStride(),strideVXS=strideVX*STEP;  \
Vec3i strideVY=(V).getComp(1).getStride(),strideVYS=strideVY*STEP;  \
Vec3i strideVZ=(V).getComp(2).getStride(),strideVZS=strideVZ*STEP;  \
Vec4i offS0=Vec4i::Zero(),offS=Vec4i::Zero(); \
PRAGMA(STRINGIFY_OMP(omp parallel for firstprivate(curr,offS0,offS) __VA_ARGS__)) \
for(sizeType x=II[0];x<nrP[0];x+=STEP) \
for(curr[0]=x,curr[1]=II[1], \
    offS0[0]=strideS[0]*x+strideS[1]*curr[1], \
    offS0[1]=strideVX[0]*x+strideVX[1]*curr[1],  \
    offS0[2]=strideVY[0]*x+strideVY[1]*curr[1],  \
    offS0[3]=strideVZ[0]*x+strideVZ[1]*curr[1]; \
    curr[1]<nrP[1]; \
    curr[1]+=STEP,  \
    offS0[0]+=strideSS[1],  \
    offS0[1]+=strideVXS[1], \
    offS0[2]+=strideVYS[1], \
    offS0[3]+=strideVZS[1]) \
for(curr[2]=II[2], \
    offS[0]=offS0[0]+strideS[2]*curr[2],  \
    offS[1]=offS0[1]+strideVX[2]*curr[2],  \
    offS[2]=offS0[2]+strideVY[2]*curr[2],  \
    offS[3]=offS0[3]+strideVZ[2]*curr[2];  \
    curr[2]<nrP[2]; \
    curr[2]+=STEP,  \
    offS[0]+=strideSS[2], \
    offS[1]+=strideVXS[2],  \
    offS[2]+=strideVYS[2],  \
    offS[3]+=strideVZS[2]) {
#define ITERSVPEND }}
#define ITERSVP_ALL(S,V,...) ITERSVP(S,V,Vec3i::Zero(),1,__VA_ARGS__)

//for iterating all points serially
#define ITERSS(S,I,STEP) {omp_set_num_threads(1);ITERSP(S,I,STEP)
#define ITERSS_ALL(S) {omp_set_num_threads(1);ITERSP_ALL(S)
#define ITERSS_PERIODIC(S,PERIODIC) {omp_set_num_threads(1);ITERSP_PERIODIC(S,PERIODIC)
#define ITERSSEND ITERSPEND omp_set_num_threads(omp_get_num_procs());}

#define ITERSVS(S,V,I,STEP) {omp_set_num_threads(1);ITERSVP(S,V,I,STEP)
#define ITERSVSEND ITERSVPEND omp_set_num_threads(omp_get_num_procs());}

//for iterating boundary points
#define ITERBDL(G,D,...){ \
Vec3i nrP=(G).getNrPoint(); \
Vec3i curr=Vec3i::Zero(); \
Vec3i strideG=(G).getStride(); \
sizeType d0=(D+1)%3,d1=(D+2)%3,offS=0; \
if(nrP[d0] < nrP[d1])swap(d0,d1);  \
PRAGMA(STRINGIFY_OMP(omp parallel for firstprivate(curr,offS) __VA_ARGS__)) \
for(sizeType x=0;x<nrP[d0];x++) \
for(curr[d0]=x,curr[d1]=0,offS=strideG[d0]*x;curr[d1]<nrP[d1];curr[d1]++,offS+=strideG[d1]){
#define ITERBDLEND }}
//only in zero boundary condition, we set this value to zero
#define ITERBDLSET(G,D,PERIODIC) ITERBDL(G,D) if(!(PERIODIC)[d])(G).get(offS)=0; ITERBDLEND

#define ITERBDR(G,D,...){ \
Vec3i nrP=(G).getNrPoint(); \
Vec3i curr=Vec3i::Unit(D)*(nrP[D]-1); \
Vec3i strideG=(G).getStride(); \
sizeType d0=(D+1)%3,d1=(D+2)%3,off0=strideG[D]*(nrP[D]-1),offS=0; \
if(nrP[d0] < nrP[d1])swap(d0,d1);  \
PRAGMA(STRINGIFY_OMP(omp parallel for firstprivate(curr,offS) __VA_ARGS__)) \
for(sizeType x=0;x<nrP[d0];x++) \
for(curr[d0]=x,curr[d1]=0,offS=off0+strideG[d0]*x; curr[d1]<nrP[d1]; curr[d1]++,offS+=strideG[d1]) {
#define ITERBDREND }}
//in either case (zero boundary or periodic boundary), we can safely set this value to zero
#define ITERBDRSET(G,D) ITERBDR(G,D) (G).get(offS)=0; ITERBDREND

//for extract velocity or scalar field component
#define FETCHVEL(V,FACE) (V).getComp((FACE)/2).get(offS[(FACE)/2+1]+stencil._offFace[FACE])
#define FETCHVELV(STRIDE,V,FACE) (V)[STRIDE[(FACE)/2]+offS[(FACE)/2+1]+stencil._offFace[FACE]]
#define FETCHVELP(V,FACE) FETCHVEL(*(V),FACE)
#define FETCHVELP_COND(V,FACE) (V ? FETCHVELP(V,FACE) : 0)

#define FETCHSCAL(S,OTHERCELL) (S).get(offS[0]+stencil._otherCell[OTHERCELL])
#define FETCHSCALS(S,OTHERCELL) (S).get(offS+stencil._otherCell[OTHERCELL])
#define FETCHSCALC(S) FETCHSCAL(S,6)
#define FETCHSCALP(S,OTHERCELL) FETCHSCAL(*(S),OTHERCELL)
#define FETCHSCALPC(S) FETCHSCALP(S,6)
#define FETCHSCALPC_COND(S) (S ? FETCHSCALPC(S) : 0)

//for curlElem build
#define COEFA(I,OP)  \
OP(_coefV[I],_coefV[0], advB*_coefW[0],I,0) \
OP(_coefV[I],_coefV[1],-advB*_coefW[0],I,1) \
OP(_coefV[I],_coefV[2], w+advB*_coefW[1],I,2) \
OP(_coefV[I],_coefV[3], w-advB*_coefW[1],I,3)
#define COEFB(I,OP)  \
OP(_coefV[I],_coefV[0],-w-advA*_coefW[0],I,0) \
OP(_coefV[I],_coefV[1],-w+advA*_coefW[0],I,1) \
OP(_coefV[I],_coefV[2],-advA*_coefW[1],I,2) \
OP(_coefV[I],_coefV[3], advA*_coefW[1],I,3)
#define FETCHVELREF(V) (V).getComp(ref._velComp).get(offS[ref._velComp+1]+ref._cellOff);
//for sparse matrix implementation
#define ADDDERIVTRIP(R,C,VAL,VR,VC) trips->push_back(Eigen::Triplet<scalarD,sizeType>(R,C,VAL));
#define ADDDERIVT(R,C,VAL,VR,VC) vout[C]+=rhs[R]*(VAL);
//for multigrid implementation
#define ADDDERIVTBLK(R,C,VAL,VR,VC) if(C < nrPrimal) rhsB[C]+=gFor[VR]*(VAL);
#define ADDSMOOTHBLK(R,C,VAL,VR,VC) if(C < nrPrimal) (*lhsB)(R,C)+=(VAL); else rhsB[R]-=(VAL)*vel[VC];
#define ADDKKTBLK(R,C,VAL,VR,VC) val=VAL;  \
if(R < nrPrimal) {  \
  if(C < nrPrimal) {  \
    lhsBGF(R,C)+=val;  \
    rhsBGF[R]+=val*vel0[VC]; \
  } \
} else if(C < nrPrimal) \
  rhsBV[C]-=val*gFor[VR];

//for STFAS
#define DECL_NEIGH_MEMBER(PREFIXR,PREFIX)  \
vector<PREFIXR MACVelocityField*> _VR; \
vector<PREFIXR MACVelocityField*> _GFR;  \
vector<PREFIXR ScalarField*> _PR;  \
vector<PREFIXR ScalarField*> _GFPR;  \
vector<PREFIX MACVelocityField*> _V;  \
vector<PREFIX MACVelocityField*> _GF; \
vector<PREFIX ScalarField*> _P; \
vector<PREFIX ScalarField*> _GFP;

#define RESIZE_NEIGH_MEMBERO(NR,OBJ) \
OBJ._VR.resize(NR); \
OBJ._GFR.resize(NR); \
OBJ._PR.resize(NR); \
OBJ._GFPR.resize(NR); \
OBJ._V.resize(NR); \
OBJ._GF.resize(NR); \
OBJ._P.resize(NR); \
OBJ._GFP.resize(NR);
#define RESIZE_NEIGH_MEMBER(NR) RESIZE_NEIGH_MEMBERO(NR,(*this))

#define FILL_NEIGH_MEMBERO(T,OBJ,PTR) \
if(PTR){OBJ._VR[T]=PTR->velPtr(FluidState::RHS_VELOCITY).get();  \
OBJ._GFR[T]=PTR->velPtr(FluidState::RHS_GHOST_FORCE).get();  \
OBJ._PR[T]=PTR->scalPtr(FluidState::RHS_PRESSURE).get(); \
OBJ._GFPR[T]=PTR->scalPtr(FluidState::RHS_GHOST_FORCE_PRESSURE).get(); \
OBJ._V[T]=PTR->velPtr(FluidState::VELOCITY).get(); \
OBJ._GF[T]=PTR->velPtr(FluidState::GHOST_FORCE).get(); \
OBJ._P[T]=PTR->scalPtr(FluidState::PRESSURE).get();  \
OBJ._GFP[T]=PTR->scalPtr(FluidState::GHOST_FORCE_PRESSURE).get(); \
} else {  \
OBJ._VR[T]=NULL;  \
OBJ._GFR[T]=NULL; \
OBJ._PR[T]=NULL;  \
OBJ._GFPR[T]=NULL;  \
OBJ._V[T]=NULL; \
OBJ._GF[T]=NULL;  \
OBJ._P[T]=NULL; \
OBJ._GFP[T]=NULL;}
#define FILL_NEIGH_MEMBER(T,PTR) FILL_NEIGH_MEMBERO(T,(*this),PTR)

#endif
