#include "FluidTraits.h"
#include <iomanip>

USE_PRJ_NAMESPACE

//interpolation op
void Traits::restrict(MACVelocityField& v,const Vec3i& curr,sizeType d,scalar val,OP f,const Vec3i& periodicSC)
{
  Vec3i nrCell=v.getNrCell();
  for(sizeType d=0; d<v.getDim(); d++)
    ASSERT(periodicSC[d] == 0 || periodicSC[d] == nrCell[d])
    if(curr[d]%2 == 1) {
      f(v.getComp(d).get(curr/2),val/2);
      f(v.getComp(d).get(curr/2+Vec3i::Unit(d),&periodicSC),val/2);
    } else f(v.getComp(d).get(curr/2),val);
}
void Traits::restrict(ScalarField& s,const Vec3i& curr,scalar val,OP f)
{
  f(s.get(curr/2),val);
}
//tridiagonal solve op
template <typename MAT_ARR>
void Traits::triDiagTpl(const MAT_ARR& diag,const MatdArr& off,const VecDArr& rhs,VecDArr& sol,OPVD op)
{
  sizeType n=(sizeType)diag.size();
  ASSERT(off.size() == diag.size()-1)
  //Matd tmp;
  Eigen::FullPivLU<Matd> tmp;
  MatdArr H(n-1);
  VecDArr g(n);
  //forward pass
  tmp=extract(diag,0).fullPivLu();
  H[0]=-tmp.solve(off[0].transpose());
  g[0]=tmp.solve(rhs[0]);
  for(sizeType i=1; i<n; i++) {
    tmp=(extract(diag,i)+off[i-1]*H[i-1]).fullPivLu();
    if(i<n-1)
      H[i]=-tmp.solve(off[i].transpose());
    g[i]=tmp.solve(rhs[i]-off[i-1]*g[i-1]);
  }
  //backward pass
  VecD lastSol=g[n-1];
  op(sol[n-1],lastSol);
  for(sizeType i=n-2; i>=0; i--) {
    lastSol=g[i]+H[i]*lastSol;
    op(sol[i],lastSol);
  }
}
void Traits::triDiag(const MatdArr& diag,const MatdArr& off,const VecDArr& rhs,VecDArr& sol,OPVD op)
{
  triDiagTpl(diag,off,rhs,sol,op);
}
void Traits::triDiag(const vector<const Matd*>& diag,const MatdArr& off,const VecDArr& rhs,VecDArr& sol,OPVD op)
{
  triDiagTpl(diag,off,rhs,sol,op);
}
template <typename MAT_ARR>
void Traits::triDiagNaiveTpl(const MAT_ARR& diag,const MatdArr& off,const VecDArr& rhs,VecDArr& sol)
{
  sizeType n=(sizeType)diag.size();
  sizeType szBlk=(sizeType)extract(diag,0).rows();
  ASSERT(off.size() == diag.size()-1)
  VecD rhsV=VecD::Zero(n*szBlk),solV=VecD::Zero(n*szBlk);
  Matd lhs=Matd::Zero(n*szBlk,n*szBlk);
  for(sizeType i=0; i<n; i++) {
    lhs.block(i*szBlk,i*szBlk,szBlk,szBlk)=extract(diag,i);
    rhsV.block(i*szBlk,0,szBlk,1)=rhs[i];
  }
  for(sizeType i=0; i<n-1; i++) {
    lhs.block((i+1)*szBlk,i*szBlk,szBlk,szBlk)=off[i];
    lhs.block(i*szBlk,(i+1)*szBlk,szBlk,szBlk)=off[i].transpose();
  }
  solV=lhs.fullPivLu().solve(rhsV);
  for(sizeType i=0; i<n; i++)
    sol[i]=solV.block(i*szBlk,0,szBlk,1);
}
void Traits::triDiagNaive(const MatdArr& diag,const MatdArr& off,const VecDArr& rhs,VecDArr& sol)
{
  triDiagNaiveTpl(diag,off,rhs,sol);
}
void Traits::triDiagNaive(const vector<const Matd*>& diag,const MatdArr& off,const VecDArr& rhs,VecDArr& sol)
{
  triDiagNaiveTpl(diag,off,rhs,sol);
}
void Traits::triDiagKKT(scalarD diagP,scalarD diagD,const VecD& div,const MatdArr& off,const VecDArr& rhs,VecDArr& sol,bool naive)
{
  sizeType n=(sizeType)off.size()+1;
  sizeType szBlk=(sizeType)off[0].rows();
  vector<const Matd*> diag(n);
  Matd diagKKTP=Matd::Zero(szBlk+1,szBlk+1);
  diagKKTP.block(0,0,szBlk,szBlk).diagonal().setConstant(diagP);
  diagKKTP.block(szBlk,0,1,szBlk)=div.transpose();
  diagKKTP.block(0,szBlk,szBlk,1)=div;
  Matd diagKKTD=Matd::Zero(szBlk+1,szBlk+1);
  diagKKTD.block(0,0,szBlk,szBlk).diagonal().setConstant(diagD);
  diagKKTD.block(szBlk,0,1,szBlk)=div.transpose();
  diagKKTD.block(0,szBlk,szBlk,1)=div;
  for(sizeType i=0; i<n; i++)
    diag[i]=(i%2 == 0) ? &diagKKTP : &diagKKTD;
  MatdArr offAug(n-1);
  for(sizeType i=0; i<n-1; i++) {
    offAug[i]=Matd::Zero(szBlk+1,szBlk+1);
    offAug[i].block(0,0,szBlk,szBlk)=off[i];
  }
  if(naive)
    triDiagNaive(diag,offAug,rhs,sol);
  else triDiag(diag,offAug,rhs,sol);
}
void Traits::triDiagKKTReorder(scalarD diagP,scalarD diagD,const VecD& div,const MatdArr& off,const VecDArr& rhs,VecDArr& sol)
{
  sizeType n=(sizeType)off.size()+1;
  sizeType szBlk=(sizeType)off[0].rows();
  sizeType offPrimal=szBlk*n;
  VecD rhsV=VecD::Zero(n*(szBlk+1)),solV;
  Matd lhs=Matd::Zero(n*(szBlk+1),n*(szBlk+1));
  for(sizeType i=0; i<n; i++) {
    lhs.block(i*szBlk,i*szBlk,szBlk,szBlk).diagonal().setConstant((i%2 == 0) ? diagP : diagD);
    if(i<n-1) {
      lhs.block((i+1)*szBlk,i*szBlk,szBlk,szBlk)=off[i];
      lhs.block(i*szBlk,(i+1)*szBlk,szBlk,szBlk)=off[i].transpose();
    }
    lhs.block(offPrimal+i,i*szBlk,1,szBlk)=div.transpose();
    lhs.block(i*szBlk,offPrimal+i,szBlk,1)=div;
    //right hand side
    rhsV.block(i*szBlk,0,szBlk,1)=rhs[i].block(0,0,szBlk,1);
    rhsV[offPrimal+i]=rhs[i][szBlk];
  }
  solV=lhs.fullPivLu().solve(rhsV);
  for(sizeType i=0; i<n; i++) {
    sol[i].setZero(szBlk+1);
    sol[i].block(0,0,szBlk,1)=solV.block(i*szBlk,0,szBlk,1);
    sol[i][szBlk]=solV[offPrimal+i];
  }
}
//tridiagonal matrix helper
template <typename MAT_ARR>
void Traits::multAddTpl(const MAT_ARR& diag,const MatdArr& off,const MatdArr* off2,const VecDArr& lhs,VecDArr& rhs,scalarD coef)
{
  for(sizeType i=0; i<(sizeType)diag.size(); i++)
    rhs[i]+=extract(diag,i)*lhs[i]*coef;
  for(sizeType i=0; i<(sizeType)off.size(); i++) {
    rhs[i]+=off[i].transpose()*lhs[i+1]*coef;
    rhs[i+1]+=off[i]*lhs[i]*coef;
  }
  if(off2) {
    for(sizeType i=0; i<(sizeType)off2->size(); i++) {
      rhs[i]+=(*off2)[i].transpose()*lhs[i+2]*coef;
      rhs[i+2]+=(*off2)[i]*lhs[i]*coef;
    }
  }
}
void Traits::multAdd(const MatdArr& diag,const MatdArr& off,const MatdArr* off2,const VecDArr& lhs,VecDArr& rhs,scalarD coef)
{
  multAddTpl(diag,off,off2,lhs,rhs,coef);
}
void Traits::multAdd(const vector<const Matd*>& diag,const MatdArr& off,const MatdArr* off2,const VecDArr& lhs,VecDArr& rhs,scalarD coef)
{
  multAddTpl(diag,off,off2,lhs,rhs,coef);
}
void Traits::multSet(const MatdArr& diag,const MatdArr& off,const MatdArr* off2,const VecDArr& lhs,VecDArr& rhs)
{
  rhs.assign(lhs.size(),VecD::Zero(lhs[0].size()));
  multAddTpl(diag,off,off2,lhs,rhs,1);
}
void Traits::multSet(const vector<const Matd*>& diag,const MatdArr& off,const MatdArr* off2,const VecDArr& lhs,VecDArr& rhs)
{
  rhs.assign(lhs.size(),VecD::Zero(lhs[0].size()));
  multAddTpl(diag,off,off2,lhs,rhs,1);
}
//banded matrix helper
void Traits::bandedLLT(Banded& lhs,VecDArr& rhs)
{
  //block cholesky factorization
  MatdArr invs(lhs[0].size());
  sizeType NDiag=(sizeType)lhs.size();
  sizeType NBlk=(sizeType)lhs[0].size();
  for(sizeType c=0; c<NBlk; c++) {
    //factorize LHS(c,c)=LL^{T}
    lhs[0][c]=lhs[0][c].llt().matrixL();
    //mult LHS(r,c)=LHS(r,c)*L^{-T}
    invs[c]=lhs[0][c].inverse();
    for(sizeType r=c+1; r<min(c+NDiag,NBlk); r++)
      lhs[r-c][c]*=invs[c].transpose();
    //update the rest of the matrix
    for(sizeType C=c+1; C<min(c+NDiag,NBlk); C++)
      for(sizeType R=C; R<min(c+NDiag,NBlk); R++)
        //lhs(R,C)-=lhs(R,c)*lhs(C,c)^T
        lhs[R-C][C]-=lhs[R-c][c]*lhs[C-c][c].transpose();
  }
  //solve L
  for(sizeType c=0; c<NBlk; c++) {
    rhs[c]=invs[c]*rhs[c];
    for(sizeType r=c+1; r<min(c+NDiag,NBlk); r++)
      rhs[r]-=lhs[r-c][c]*rhs[c];
  }
  //solve LT
  for(sizeType c=NBlk-1; c>=0; c--) {
    rhs[c]=invs[c].transpose()*rhs[c];
    for(sizeType r=max<sizeType>(0,c-NDiag+1); r<c; r++)
      rhs[r]-=lhs[c-r][r].transpose()*rhs[c];
  }
}
void Traits::bandedLUAsTridiag(const Banded& lhs,VecDArr& rhs)
{
  sizeType szSuperBlk=(sizeType)lhs.size()-1;
  sizeType NBlk=(sizeType)lhs[0].size();
  //ASSERT_MSGV(NBlk%szSuperBlk == 0,"lhs[0].size() must be multiples of %ld",szSuperBlk)

  //build lhs
  sizeType stride=lhs[0][0].rows();
  sizeType NSuperBlk=(NBlk+szSuperBlk-1)/szSuperBlk;
  sizeType superStride=stride*szSuperBlk;
  MatdArr diag(NSuperBlk,Matd::Zero(superStride,superStride));
  MatdArr offDiag(NSuperBlk-1,Matd::Zero(superStride,superStride));
  for(sizeType D=0; D<(sizeType)lhs.size(); D++)
    for(sizeType r=D,c=0; c<NBlk-D; r++,c++) {
      sizeType rSuperBlk=r/szSuperBlk,blkInR=r%szSuperBlk;
      sizeType cSuperBlk=c/szSuperBlk,blkInC=c%szSuperBlk;
      if(rSuperBlk == cSuperBlk) {
        diag[cSuperBlk].block(blkInR*stride,blkInC*stride,stride,stride)=lhs[D][c];
        diag[cSuperBlk].block(blkInC*stride,blkInR*stride,stride,stride)=lhs[D][c].transpose();
      } else {
        ASSERT(rSuperBlk == cSuperBlk+1)
        offDiag[cSuperBlk].block(blkInR*stride,blkInC*stride,stride,stride)=lhs[D][c];
      }
    }
  //build rhs
  VecDArr rhsSuper(NSuperBlk,VecD::Zero(superStride));
  for(sizeType i=0; i<NBlk; i++)
    rhsSuper[i/szSuperBlk].block((i%szSuperBlk)*stride,0,stride,1)=rhs[i];
  //solve and assign
  triDiag(diag,offDiag,rhsSuper,rhsSuper);
  for(sizeType i=0; i<NBlk; i++)
    rhs[i]=rhsSuper[i/szSuperBlk].block((i%szSuperBlk)*stride,0,stride,1);
}
void Traits::multAdd(const Banded& bandMat,const VecDArr& lhs,VecDArr& rhs,scalarD coef)
{
  sizeType NBlk=(sizeType)bandMat[0].size();
  for(sizeType diag=0; diag<(sizeType)bandMat.size(); diag++)
    for(sizeType blk=0; blk<NBlk-diag; blk++) {
      rhs[blk+diag]+=bandMat[diag][blk]*lhs[blk]*coef;
      if(diag > 0)
        rhs[blk]+=bandMat[diag][blk].transpose()*lhs[blk+diag]*coef;
    }
}
Traits::Banded& Traits::multATA(Banded& bandMat)
{
  sizeType NDiag=(sizeType)bandMat.size();
  sizeType NBlk=(sizeType)bandMat[0].size();
  sizeType stride=bandMat[0][0].rows();
  Banded tmp=bandMat;

  bandMat.clear();
  for(sizeType i=0,sz=NBlk; i<NDiag*2-1; i++,sz--)
    if(sz > 0) {
      bandMat.push_back(MatdArr(sz,Matd::Zero(stride,stride)));
      for(sizeType r=i,c=0; r<NBlk; r++,c++) {
        Matd& blk=bandMat.back()[c];
        //[r-NDiag+1,r+NDiag-1] [c-NDiag+1,c+NDiag-1]
        for(sizeType k=max<sizeType>(0,r-NDiag+1),to=min<sizeType>(NBlk-1,c+NDiag-1); k<=to; k++)
          //blk+=BLK(r,k)*BLK(k,c);
          if(k > r)
            blk+=tmp[k-r][r].transpose()*tmp[k-c][c];
          else if(k > c)
            blk+=tmp[r-k][k]*tmp[k-c][c];
          else blk+=tmp[r-k][k]*tmp[c-k][k].transpose();
      }
    }
  return bandMat;
}
//tridiagonal helper
template <typename MAT_ARR>
void Traits::setZeroTpl(MAT_ARR& diag)
{
  for(sizeType i=0; i<(sizeType)diag.size(); i++)
    extract(diag,i).setZero();
}
void Traits::setZero(MatdArr& diag)
{
  setZeroTpl(diag);
}
void Traits::setZero(VecDArr& diag)
{
  setZeroTpl(diag);
}
template <typename MAT_ARR>
void Traits::copyTpl(const MAT_ARR& from,MAT_ARR& to)
{
  ASSERT(from.size() == to.size())
  for(sizeType i=0; i<(sizeType)from.size(); i++)
    to[i]=from[i];
}
void Traits::copy(const MatdArr& from,MatdArr& to)
{
  copyTpl(from,to);
}
void Traits::copy(const VecDArr& from,VecDArr& to)
{
  copyTpl(from,to);
}
Traits::MatdArr& Traits::mul(MatdArr& a,scalarD coef)
{
  for(sizeType i=0; i<(sizeType)a.size(); i++)
    a[i]*=coef;
  return a;
}
Traits::VecDArr& Traits::mul(VecDArr& a,scalarD coef)
{
  for(sizeType i=0; i<(sizeType)a.size(); i++)
    a[i]*=coef;
  return a;
}
scalarD Traits::dot(const VecDArr& a,const VecDArr& b)
{
  scalarD ret=0;
  for(sizeType i=0; i<(sizeType)a.size(); i++)
    ret+=a[i].dot(b[i]);
  return ret;
}
Traits::VecDArr& Traits::sub(const VecDArr& a,const VecDArr& b,VecDArr& out)
{
  ASSERT(a.size() == b.size() && b.size() == out.size())
  for(sizeType i=0; i<(sizeType)out.size(); i++)
    out[i]=a[i]-b[i];
  return out;
}
Traits::VecDArr& Traits::add(const VecDArr& a,const VecDArr& b,VecDArr& out)
{
  ASSERT(a.size() == b.size() && b.size() == out.size())
  for(sizeType i=0; i<(sizeType)out.size(); i++)
    out[i]=a[i]+b[i];
  return out;
}
void Traits::addSave(VecDArr& a,VecDArr& b)
{
  ASSERT(a.size() == b.size())
  for(sizeType i=0; i<(sizeType)a.size(); i++) {
    swap(a[i],b[i]);
    a[i]+=b[i];
  }
}
scalarD Traits::squaredNorm(const VecDArr& delta)
{
  scalarD ret=0;
  for(sizeType i=0; i<(sizeType)delta.size(); i++)
    ret+=delta[i].squaredNorm();
  return ret;
}
//debugger
void Traits::saveA(const MatdArr& diag,const MatdArr& offDiag,const VecDArr& rhs)
{
  DA=diag;
  ODA=offDiag;
  RHSA=rhs;
}
void Traits::saveB(const MatdArr& diag,const MatdArr& offDiag,const VecDArr& rhs)
{
  DB=diag;
  ODB=offDiag;
  RHSB=rhs;
}
void Traits::saveA(const vector<const Matd*>& diag,const MatdArr& offDiag,const VecDArr& rhs)
{
  MatdArr diagCpy(diag.size());
  for(sizeType i=0; i<(sizeType)diag.size(); i++)
    diagCpy[i]=*diag[i];
  saveA(diagCpy,offDiag,rhs);
}
void Traits::saveB(const vector<const Matd*>& diag,const MatdArr& offDiag,const VecDArr& rhs)
{
  MatdArr diagCpy(diag.size());
  for(sizeType i=0; i<(sizeType)diag.size(); i++)
    diagCpy[i]=*diag[i];
  saveB(diagCpy,offDiag,rhs);
}
void Traits::debugWrite()
{
  INFO("-----------------------------------------------------Tridiagonal System Begin")
  INFO("-----------------------------------------------------Diagonal")
  //D
  for(sizeType i=0; i<(sizeType)DA.size(); i++) {
    cout << setw(12) << "Diagonal block-" << i << ":" << endl;
    for(sizeType r=0; r<DA[i].rows(); r++) {
      for(sizeType c=0; c<DA[i].cols(); c++)
        cout << setw(12) << DA[i](r,c) << " ";
      cout << setw(12) << "| ";
      if(i<(sizeType)DB.size() && DB[i].size() == DA[i].size())
        for(sizeType c=0; c<DB[i].cols(); c++)
          cout << setw(12) << DB[i](r,c) << " ";
      cout << endl;
    }
    cout << endl;
  }
  INFO("-----------------------------------------------------Off-Diagonal")
  //OD
  for(sizeType i=0; i<(sizeType)ODA.size(); i++) {
    cout << "Off-diagonal block-" << i << ":" << endl;
    for(sizeType r=0; r<ODA[i].rows(); r++) {
      for(sizeType c=0; c<ODA[i].cols(); c++)
        cout << setw(12) << ODA[i](r,c) << " ";
      cout << setw(12) << "| ";
      if(i<(sizeType)ODB.size() && ODB[i].size() == ODA[i].size())
        for(sizeType c=0; c<ODB[i].cols(); c++)
          cout << setw(12) << ODB[i](r,c) << " ";
      cout << setw(12) << endl;
    }
    cout << endl;
  }
  INFO("-----------------------------------------------------RHS")
  //RHS
  for(sizeType i=0; i<(sizeType)RHSA.size(); i++) {
    cout << "Rhs block-" << i << ":" << endl;
    for(sizeType c=0; c<(sizeType)RHSA[i].size(); c++)
      cout << setw(12) << RHSA[i][c] << " ";
    cout << endl;
    if(i<(sizeType)RHSB.size() && RHSB[i].size() == RHSA[i].size())
      for(sizeType c=0; c<(sizeType)RHSB[i].size(); c++)
        cout << setw(12) << RHSB[i][c] << " ";
    cout << endl;
  }
  INFO("-----------------------------------------------------Tridiagonal System End")
}
Traits::MatdArr Traits::DA,Traits::DB;
Traits::MatdArr Traits::ODA,Traits::ODB;
Traits::VecDArr Traits::RHSA,Traits::RHSB;
//converter
Traits::Banded& Traits::toBanded(Banded& banded,const MatdArr& diag,const MatdArr& offDiag,const MatdArr* offDiag2)
{
  banded.resize(offDiag2 ? 3 : 2);
  banded[0]=diag;
  banded[1]=offDiag;
  if(offDiag2)
    banded[2]=*offDiag2;
  return banded;
}
Traits::Banded Traits::toBanded(const MatdArr& diag,const MatdArr& offDiag,const MatdArr* offDiag2)
{
  Banded banded(offDiag2 ? 3 : 2);
  banded[0]=diag;
  banded[1]=offDiag;
  if(offDiag2)
    banded[2]=*offDiag2;
  return banded;
}
Matd Traits::toDense(const MatdArr& diag,const MatdArr& offDiag,const MatdArr* offDiag2)
{
  sizeType stride=(sizeType)diag[0].rows();
  Matd mD=Matd::Zero(diag.size()*stride,diag.size()*stride);
  for(sizeType i=0; i<(sizeType)diag.size(); i++)
    mD.block(i*stride,i*stride,stride,stride)=diag[i];
  for(sizeType i=0; i<(sizeType)offDiag.size(); i++) {
    mD.block((i+1)*stride,i*stride,stride,stride)=offDiag[i];
    mD.block(i*stride,(i+1)*stride,stride,stride)=offDiag[i].transpose();
  }
  if(offDiag2)
    for(sizeType i=0; i<(sizeType)offDiag2->size(); i++) {
      mD.block((i+2)*stride,i*stride,stride,stride)=(*offDiag2)[i];
      mD.block(i*stride,(i+2)*stride,stride,stride)=(*offDiag2)[i].transpose();
    }
  return mD;
}
Matd Traits::toDenseL(const Banded& banded)
{
  sizeType NBlk=(sizeType)banded[0].size();
  sizeType stride=(sizeType)banded[0][0].rows();

  Matd ret=Matd::Zero(NBlk*stride,NBlk*stride);
  for(sizeType diag=0; diag<(sizeType)banded.size(); diag++)
    for(sizeType blk=0; blk<NBlk-diag; blk++)
      ret.block((diag+blk)*stride,blk*stride,stride,stride)=banded[diag][blk];
  return ret;
}
Matd Traits::toDense(const Banded& banded)
{
  sizeType NBlk=(sizeType)banded[0].size();
  sizeType stride=(sizeType)banded[0][0].rows();

  Matd ret=Matd::Zero(NBlk*stride,NBlk*stride);
  for(sizeType diag=0; diag<(sizeType)banded.size(); diag++)
    for(sizeType blk=0; blk<NBlk-diag; blk++) {
      ret.block((diag+blk)*stride,blk*stride,stride,stride)=banded[diag][blk];
      ret.block(blk*stride,(diag+blk)*stride,stride,stride)=banded[diag][blk].transpose();
    }
  return ret;
}
Traits::VecD Traits::toDense(const VecDArr& x)
{
  sizeType stride=(sizeType)x[0].size();
  VecD xD=VecD::Zero(x.size()*stride);
  for(sizeType i=0; i<(sizeType)x.size(); i++)
    xD.block(i*stride,0,stride,1)=x[i];
  return xD;
}
//extractors
const Matd& Traits::extract(const MatdArr& arr,sizeType i)
{
  return arr[i];
}
Matd& Traits::extract(MatdArr& arr,sizeType i)
{
  return arr[i];
}
const Traits::VecD& Traits::extract(const VecDArr& arr,sizeType i)
{
  return arr[i];
}
Traits::VecD& Traits::extract(VecDArr& arr,sizeType i)
{
  return arr[i];
}
const Matd& Traits::extract(const vector<const Matd*>& arr,sizeType i)
{
  return *arr[i];
}
