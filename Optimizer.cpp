#include "Optimizer.h"
#include "FluidSolverSpatialMG.h"
#include "PDForce.h"
#include "CommonFile/solvers/Minimizer.h"
#include <boost/property_tree/xml_parser.hpp>
#include <boost/filesystem/operations.hpp>
#include <boost/lexical_cast.hpp>

USE_PRJ_NAMESPACE

//Optimizer
FluidOptObjective::FluidOptObjective(SpaceTimeMesh& mesh,const std::set<sizeType>* fixed)
  :_pt(mesh.getPt()),_fixed(fixed),_mesh(mesh)
{
  if(_pt.get<bool>("clearField",true))
    _mesh.state(_mesh.nrFrame()-1).clearField();
}
int FluidOptObjective::inputs() const
{
  sizeType nrV=_mesh.state(0).nrVelComp();
  return (int)nrV*((int)_mesh.nrFrame()-1);
}
int FluidOptObjective::inputsVG() const
{
  sizeType nrV=_mesh.state(0).nrVelComp();
  return (int)nrV*(((int)_mesh.nrFrame()-1)+(int)_mesh.nrFrame()-2);
}
void FluidOptObjective::assembleV(Vec& x) const
{
  x.setZero(inputs());
  sizeType nrV=_mesh.state(0).nrVelComp();
  for(sizeType i=0; i<_mesh.nrFrame()-1; i++)
    _mesh.state(i).toVec(FluidState::VELOCITY,x,&set,i*nrV);
}
void FluidOptObjective::assignV(const Vec& x)
{
  ASSERT((int)x.size() == inputs() || (int)x.size() == inputsVG())
  sizeType nrV=_mesh.state(0).nrVelComp();
  for(sizeType i=0; i<_mesh.nrFrame()-1; i++)
    _mesh.state(i).fromVec(FluidState::VELOCITY,x,&set,i*nrV);
}
void FluidOptObjective::assembleVG(Vec& x) const
{
  x.setZero(inputsVG());
  sizeType nrV=_mesh.state(0).nrVelComp();
  for(sizeType i=0; i<_mesh.nrFrame()-1; i++)
    _mesh.state(i).toVec(FluidState::VELOCITY,x,&set,i*nrV);
  for(sizeType i=0; i<_mesh.nrFrame()-2; i++)
    _mesh.state(i).toVec(FluidState::GHOST_FORCE,x,&set,(i+_mesh.nrFrame()-1)*nrV);
}
void FluidOptObjective::assignVG(const Vec& x)
{
  ASSERT((int)x.size() == inputsVG())
  sizeType nrV=_mesh.state(0).nrVelComp();
  for(sizeType i=0; i<_mesh.nrFrame()-1; i++)
    _mesh.state(i).fromVec(FluidState::VELOCITY,x,&set,i*nrV);
  for(sizeType i=0; i<_mesh.nrFrame()-2; i++)
    _mesh.state(i).fromVec(FluidState::GHOST_FORCE,x,&set,(i+_mesh.nrFrame()-1)*nrV);
}
bool FluidOptObjective::isFixed(sizeType i) const
{
  return _fixed && _fixed->find(i) != _fixed->end();
}
void FluidOptObjective::filter(Vec& DFDX) const
{
  sizeType nrV=_mesh.state(0).nrVelComp();
  if(_fixed)
    for(std::set<sizeType>::const_iterator beg=_fixed->begin(),end=_fixed->end(); beg!=end; beg++)
      DFDX.block(*beg*nrV,0,nrV,1).setZero();
}
string FluidOptObjective::outputFileName(const string& method) const
{
  ostringstream oss;
  Vec3i nrCell=_mesh.state(0).scal(FluidState::PRESSURE).getNrCell();
  oss << method << "<" << nrCell[0] << "," << nrCell[1] << "," << nrCell[2] << "," << _mesh.nrFrame() << ">.txt";
  return oss.str();
}
//PassiveOpt
PassiveOpt::PassiveOpt(SpaceTimeMesh& mesh,const std::set<sizeType>* fixed)
  :FluidOptObjective(mesh,fixed),_stepSz(1.0f) {}
void PassiveOpt::DEDVel(scalarD& FX,Vec* DFDX) const
{
  if(DFDX)
    DFDX->setZero(inputs());
  _mesh.updateAdvect();
  ScalarField DEDRho=_mesh.state(0).scal(FluidState::PRESSURE);
  DEDRho.init(0);

  FX=0;
  sizeType nrV=_mesh.state(0).nrVelComp();
  scalarD regK=_pt.get<scalar>("regK",1000.0f);
  Vec DFDXI=Vec::Zero(nrV);
  for(sizeType i=_mesh.nrFrame()-1; i>=0; i--) {
    FX+=updateGradient(i,&DFDXI,&DEDRho,regK,false);
    FX+=backPropGradient(i,&DEDRho);
    if(DFDX && i<_mesh.nrFrame()-1)
      DFDX->block(i*nrV,0,nrV,1)=DFDXI;
  }
  if(DFDX)
    filter(*DFDX);
}
void PassiveOpt::DEDRho(scalarD& FX,ScalarField* DEDRho) const
{
  _mesh.updateAdvect();
  if(DEDRho) {
    *DEDRho=_mesh.state(0).scal(FluidState::PRESSURE);
    DEDRho->init(0);
  }

  FX=0;
  scalarD regK=_pt.get<scalar>("regK",1000.0f);
  for(sizeType i=_mesh.nrFrame()-1; i>=0; i--) {
    FX+=updateGradient(i,NULL,DEDRho,regK,false);
    FX+=backPropGradient(i,DEDRho);
  }
}
int PassiveOpt::operator()(const Vec& x,scalarD& FX,Vec& DFDX,const scalarD& step,bool wantGradient)
{
  //parameter
  bool divFreePassive=_pt.get<bool>("divFreePassive",true);
  //compute gradient
  assignV(x);
  DEDVel(FX,&DFDX);
  assignV(DFDX);
  if(divFreePassive)
    for(sizeType i=0; i<_mesh.nrFrame()-1; i++)
      _mesh.solver<FluidSolver>().enforceDivFree(_mesh.state(i));
  assembleV(DFDX);
  _mesh.getPt().put<scalarD>("maxChange",DFDX.cwiseAbs().maxCoeff());
  return 0;
}
scalarD PassiveOpt::optimizeGD()
{
  //parameter
  bool divFreePassive=_pt.get<bool>("divFreePassive",true);
  scalarD alpha=_pt.get<scalar>("stepSizeGD",1E-2f);
  sizeType nrPassive=_pt.get<sizeType>("maxIterPassive",1000);
  //optimize
  _mesh.checkValidity();
  Vec DFDX=Vec::Zero(_mesh.state(0).nrVelComp());
  scalarD FX,maxDelta=0;
  scalarD dt=_pt.get<scalar>("dt",0.0f);
  scalarD regK=_pt.get<scalar>("regK",1000.0f);
  scalarD CFL=_pt.get<scalar>("CFL",numeric_limits<scalar>::infinity());
  sizeType frameTo=_pt.get<scalar>("frameTo",_mesh.nrFrame()-1);
  sizeType frameFrom=_pt.get<scalar>("frameFrom",0);
  for(sizeType it=0; it<nrPassive; it++) {
    //forward pass
    _mesh.updateAdvect();
    ScalarField DEDRho=_mesh.state(0).scal(FluidState::PRESSURE);
    DEDRho.init(0);
    //backward pass
    FX=0,maxDelta=0;
    for(sizeType i=frameTo; i>=frameFrom; i--) {
      FX+=updateGradient(i,&DFDX,&DEDRho,regK,false);
      if(i < _mesh.nrFrame()-1 && !isFixed(i)) {
        _mesh.state(i).fromVec(FluidState::VELOCITY,DFDX,&sub,-1,alpha);
        maxDelta=max<scalarD>(maxDelta,DFDX.cwiseAbs().maxCoeff());
        _mesh.state(i).limitCFL(FluidState::VELOCITY,dt,CFL);
        if(divFreePassive)
          _mesh.solver<FluidSolver>().enforceDivFree(_mesh.state(i));
      }
      FX+=backPropGradient(i,&DEDRho);
    }
    //check convergence
    INFOV("Performed %ld GD iterations, gradient norm=%lf, FX=%lf, maxDelta=%lf!",it,DFDX.norm(),FX,maxDelta)
  }
  return maxDelta;
}
scalarD PassiveOpt::optimizeKKT()
{
#define STEP_DEC_COEF 0.2f
#define STEP_INC_COEF 2.1f
  //parameter
  bool divFreePassive=_pt.get<bool>("divFreePassive",true);
  sizeType nrPassive=_pt.get<sizeType>("maxIterPassive",100);
  //optimize
  _mesh.checkValidity();
  scalarD dt=_pt.get<scalar>("dt",0.0f);
  scalarD regK=_pt.get<scalar>("regK",1000.0f);
  scalarD CFL=_pt.get<scalar>("CFL",numeric_limits<scalar>::infinity());
  sizeType frameTo=_pt.get<scalar>("frameTo",_mesh.nrFrame()-1);
  sizeType frameFrom=_pt.get<scalar>("frameFrom",0);
  scalarD reductionRateCurr=0,reductionRateTol=0,reductionTol=0;
  scalarD FX=0,maxDelta=0,maxDeltaCurr=0,lastFX=0,lowerBound=1E-7f;
  Vec DFDX=Vec::Zero(_mesh.state(0).nrVelComp()),DFDXTmp=DFDX,saved;
  //sizeType gradientMode=_pt.get<sizeType>("densityGradient",GAUSSIAN);
  //if(gradientMode == HIERARCHICAL || gradientMode == GAUSSIAN_HIERARCHICAL) {
  reductionRateTol=_pt.get<scalar>("minReductionRate",10.0f);
  reductionTol=_pt.get<scalar>("minReduction",1.0f);
  //} else {
  //  reductionRateTol=_pt.get<scalar>("minReductionRate",0.01f);
  //  reductionTol=_pt.get<scalar>("minReduction",0.01f);
  //}
  //compute initial function value
  _mesh.updateAdvect();
  for(sizeType i=frameTo; i>=frameFrom; i--) {
    lastFX+=updateGradient(i,NULL,NULL,regK,true);
    lastFX+=backPropGradient(i,NULL);
  }
  assembleV(saved);
  //update
  for(sizeType it=0; it<nrPassive; it++) {
    //forward pass
    _mesh.updateAdvect();
    ScalarField DEDRho=_mesh.state(0).scal(FluidState::PRESSURE);
    DEDRho.init(0);
    //backward pass
    maxDeltaCurr=0;
    for(sizeType i=frameTo; i>=frameFrom; i--) {
      updateGradient(i,&DFDX,&DEDRho,regK,true);
      if(i < _mesh.nrFrame()-1 && !isFixed(i)) {
        //apply constraints: divergent-free/CFL constraints
        _mesh.state(i).toVec(FluidState::VELOCITY,DFDXTmp);
        maxDeltaCurr=max<scalarD>(maxDeltaCurr,(DFDXTmp-DFDX).norm());
        _mesh.state(i).fromVec(FluidState::VELOCITY,DFDX);
        _mesh.state(i).limitCFL(FluidState::VELOCITY,dt,CFL);
        if(divFreePassive && !_mesh.solver<FluidSolver>().enforceDivFree(_mesh.state(i))) {
          //then stepsize is too large
          FX=ScalarUtil<scalar>::scalar_max;
          break;
        }
        //a simple line search
        _mesh.state(i).toVec(FluidState::VELOCITY,DFDX);
        DFDX=DFDXTmp*(1-_stepSz)+DFDX*_stepSz;
        _mesh.state(i).fromVec(FluidState::VELOCITY,DFDX);
      } else if(isFixed(i)) {
        INFOV("Fixed velocity norm at frame %ld: %f",i,sqrt(_mesh.state(i).vel(FluidState::VELOCITY).squaredNorm()))
      }
      backPropGradient(i,&DEDRho);
    }
    //compute function value
    FX=0;
    _mesh.updateAdvect();
    for(sizeType i=frameTo; i>=frameFrom; i--) {
      FX+=updateGradient(i,NULL,NULL,regK,true);
      FX+=backPropGradient(i,NULL);
      if(FX > lastFX || isNan(FX) || isInf(FX))
        break;  //early break
    }
    //check convergence
    INFOV("Performed %ld KKT iterations, stepsize=%lf, FX=%lf!",it,_stepSz,FX)
    if(FX > lastFX || isNan(FX) || isInf(FX)) {
      //recover
      assignV(saved);
      //reduce step size
      _stepSz=max<scalar>(_stepSz*STEP_DEC_COEF,lowerBound);
    } else {
      //exit test
      if(it > 0) {
        reductionRateCurr=(lastFX-FX)/_stepSz;
        INFOV("Function reduction: %lf, reduction rate: %lf",reductionRateCurr*_stepSz,reductionRateCurr)
        if(reductionRateCurr < reductionRateTol || reductionRateCurr*_stepSz < reductionTol)
          break;
      }
      //accept
      assembleV(saved);
      lastFX=FX;
      //tentatively increase step size
      _stepSz=min<scalar>(_stepSz*STEP_INC_COEF,1.0f);
      maxDelta=max(maxDelta,maxDeltaCurr);
    }
    if(_stepSz == lowerBound)
      break;
  }
  INFOV("Current passive step size: %f",_stepSz)
  return maxDelta;
#undef STEP_DEC_COEF
#undef STEP_INC_COEF
}
scalarD PassiveOpt::optimizeLBFGS()
{
  _mesh.checkValidity();
  Vec x;
  scalarD fx;
  LBFGSMinimizer<scalarD> sol;
  ConvergencyCounter<scalarD,Kernel<scalarD> > cb(outputFileName("./passiveHistoryLBFGS"));
  //in the default version we use LBFGS
  assembleV(x);
  sol.maxIterations()=_pt.get<sizeType>("maxIterPassive",5);
  sol.epsilon()=_pt.get<scalar>("epsilonLBFGS",1E-5f);
  sol.minimize(x,fx,*this,cb);
  assignV(x);
  return _pt.get<scalarD>("maxChange");
}
scalarD PassiveOpt::updateGradient(sizeType i,Vec* DFDX,const ScalarField* DEDRho,scalarD regK,bool KKT) const
{
  scalarD ret=0;
  const ScalarField& rho=*(_mesh.state(i)._rho);
  if(i < _mesh.nrFrame()-1) {
    //rho matching
    if(DFDX)
      DFDX->setZero();
    const MACVelocityField& vel=_mesh.state(i).vel(FluidState::VELOCITY);
    if(DFDX && DEDRho) {
      _mesh.getPt().put<sizeType>("currentFrameId",i);
      _mesh.solver().advector().advectV(*DFDX,*DEDRho,rho,vel);
    }
    //regularization
    const MACVelocityField* velRef=_mesh.state(i).velPtr(FluidState::VELOCITY_REFERENCE).get();
    for(sizeType d=0,offV=0; d<rho.getDim(); offV+=vel.getComp(d).getNrPoint().prod(),d++)
      ITERSP_PERIODIC(rho,_mesh.state(i).getPeriodic(),OMP_ADD(ret))
      scalar deltaV=0;
    deltaV=vel.getComp(d).get(curr);
    if(velRef)
      deltaV-=velRef->getComp(d).get(curr);
    ret+=deltaV*deltaV;

    if(DFDX) {
      scalarD& grad=(*DFDX)[offV+vel.getComp(d).getIndex(curr)];
      grad+=deltaV*regK;
      if(KKT)
        grad=-grad/regK+vel.getComp(d).get(curr);
    }
    ITERSPEND
    ret*=0.5f*regK;
  }
  return ret;
}
scalarD PassiveOpt::backPropGradient(sizeType i,ScalarField* DEDRho) const
{
  //back propagate
  if(DEDRho && i < _mesh.nrFrame()-1) {
    _mesh.getPt().put<sizeType>("currentFrameId",i);
    _mesh.solver().advector().advectT(*DEDRho,*(_mesh.state(i)._rho),_mesh.state(i).vel(FluidState::VELOCITY));
  }
  //update DEDRho: keyFrame matching term
  if(_mesh.keyFrame(i))
    return addGradient(DEDRho,*(_mesh.state(i)._rho),*(_mesh.keyFrame(i)),_mesh.solver().advector().cacheA());
  else return 0;
}
scalarD PassiveOpt::addGradient(ScalarField* DEDRho,const ScalarField& rho,const ScalarField& rhoStar,ScalarField& restrictPool,OP op) const
{
  switch(_pt.get<sizeType>("densityGradient",GAUSSIAN_HIERARCHICAL)) {
  case HIERARCHICAL:
    return addGradientHierarchical(DEDRho,rho,rhoStar,restrictPool,op);
  case GAUSSIAN:
    return addGradientGaussian(DEDRho,rho,rhoStar,restrictPool,op);
  case GAUSSIAN_HIERARCHICAL:
    return addGradientGaussianHierarchical(DEDRho,rho,rhoStar,restrictPool,op);
  default:
    return 0;
  }
}
scalarD PassiveOpt::addGradientHierarchical(ScalarField* DEDRho,const ScalarField& rho,const ScalarField& rhoStar,ScalarField& restrictPool,OP op) const
{
  //count levels
  sizeType nrLevel=1,dim=rho.getNrPoint().block(0,0,rho.getDim(),1).minCoeff();
  while(true) {
    if(dim <= 2)
      break;
    nrLevel++;
    dim/=2;
  }

  //accumulate energy gradient
  scalarD ret=0,coef=1;
  restrictPool.makeSameGeometry(rho);
  for(sizeType l=0,off=1; l<nrLevel; l++,off*=2) {
    //restrict
    restrictPool.init(0);
    ITERSP_ALL(restrictPool)
    scalar& val=restrictPool.get(curr/off);
    OMP_ATOMIC_
    val+=rho.get(offS)-rhoStar.get(offS);
    ITERSPEND
    //add gradient
    ITERSP_ALL(restrictPool,OMP_ADD(ret))
    if(DEDRho)
      op(DEDRho->get(offS),restrictPool.get(curr/off)*coef);
    //add energy
    scalarD dRho=restrictPool.get(offS);
    scalarD dE=dRho*dRho*coef;
    ret+=dE;
    ITERSPEND
    coef/=(scalar)pow(2,rho.getDim());
  }
  return ret/2;
}
scalarD PassiveOpt::addGradientGaussian(ScalarField* DEDRho,const ScalarField& rho,const ScalarField& rhoStar,ScalarField& restrictPool,OP op) const
{
  //apply Gaussian Smoothing, get energy
  scalarD ret=0;
  vector<scalar> stencil[3];
  restrictPool.makeSameGeometry(rho);
  ret=PDForce::gaussian(restrictPool,rho,&rhoStar,_mesh,stencil);
  //add gradient
  if(DEDRho)
    PDForce::gaussianT(*DEDRho,restrictPool,_mesh,op,stencil);
  return ret/2;
}
scalarD PassiveOpt::addGradientGaussianHierarchical(ScalarField* DEDRho,const ScalarField& rho,const ScalarField& rhoStar,ScalarField& restrictPool,OP op) const
{
  //save
  scalar sigma=_mesh.getPt().get<scalar>("gaussianSigma",0.1f),sigma0=sigma;
  scalar range=_mesh.getPt().get<scalar>("gaussianRange",0.2f),range0=range;
  //apply multiple gaussian
  scalarD ret=0;
  vector<scalar> stencil[3];
  ScalarField tmp=restrictPool;
  if(DEDRho) {
    DEDRho->init(0);
  }
  do {
    //apply Gaussian Smoothing, get energy
    restrictPool.makeSameGeometry(rho);
    ret+=PDForce::gaussian(restrictPool,rho,&rhoStar,_mesh,stencil);
    //add gradient
    if(DEDRho) {
      PDForce::gaussianT(tmp,restrictPool,_mesh,op,stencil);
      DEDRho->add(tmp);
    }
    //shrink gaussian range
    _mesh.getPt().put<scalar>("gaussianSigma",sigma=sigma*2);
    _mesh.getPt().put<scalar>("gaussianRange",range=range/2);
    //INFOV("Stencil Size: %ld",stencil[0].size())
  } while(stencil[0].size() > 7);
  //load
  _mesh.getPt().put<scalar>("gaussianSigma",sigma0);
  _mesh.getPt().put<scalar>("gaussianRange",range0);
  return ret/2;
}
//ActiveOpt
class NoDeletor
{
public:
  void operator()(SpaceTimeMesh* p) {}
};
ActiveOpt::ActiveOpt(SpaceTimeMesh& mesh)
  :FluidOptObjective(mesh,NULL),_stepSz(1.0f) {}
void ActiveOpt::DEDVel(scalarD& FX,Vec* DFDX)
{
  scalarD regK=_pt.get<scalar>("regK",1000.0f);
  scalarD regU=_pt.get<scalar>("regU",100.0f);

  FX=0;
  if(DFDX)
    DFDX->setZero(inputs());
  sizeType nrV=_mesh.state(0).nrVelComp();
  FluidSolver& sol=_mesh.solver<FluidSolver>();
  for(sizeType i=0; i<_mesh.nrFrame()-2; i++) {
    FluidState& s=_mesh.state(i);
    if(!s.velPtr(FluidState::GHOST_FORCE))
      s.addField(0,FluidState::GHOST_FORCE);
    sol.solveForce(s,_mesh.state(i+1),DFDX,i*nrV,(i+1)*nrV);
    FX+=s.vel(FluidState::GHOST_FORCE).squaredNorm()*regU;
  }
  if(DFDX)
    (*DFDX)*=regU;
  for(sizeType i=0; i<_mesh.nrFrame()-1; i++) {
    FluidState& s=_mesh.state(i);
    if(s.velPtr(FluidState::VELOCITY_REFERENCE)) {
      if(DFDX) {
        s.toVec(FluidState::VELOCITY,*DFDX,&add,i*nrV,regK);
        s.toVec(FluidState::VELOCITY_REFERENCE,*DFDX,&sub,i*nrV,regK);
      }
      FX+=s.vel(FluidState::VELOCITY).squaredDistTo(s.vel(FluidState::VELOCITY_REFERENCE))*regK;
    } else {
      if(DFDX)
        s.toVec(FluidState::VELOCITY,*DFDX,&add,i*nrV,regK);
      FX+=s.vel(FluidState::VELOCITY).squaredNorm()*regK;
    }
  }
  FX*=0.5f;
  if(DFDX)
    filter(*DFDX);
}
int ActiveOpt::operator()(const Vec& x,scalarD& FX,Vec& DFDX,const scalarD& step,bool wantGradient)
{
  if(!wantGradient) {
    assignV(x);
    DEDVel(FX,NULL);
  } else {
    assignV(x);
    DEDVel(FX,&DFDX);
    assignV(DFDX);
    FluidSolver& sol=_mesh.solver<FluidSolver>();
    for(sizeType i=0; i<_mesh.nrFrame()-1; i++) {
      FluidState::FIELD f;
      if(!_mesh.state(i).scalPtr(FluidState::GHOST_FORCE_PRESSURE))
        f=FluidState::NO_FIELD;
      else f=FluidState::GHOST_FORCE_PRESSURE;

      if(_pt.get<bool>("useExactProjector",false))
        sol.FluidSolver::enforceDivFree(_mesh.state(i),FluidState::VELOCITY,NULL,f);
      else sol.enforceDivFree(_mesh.state(i),FluidState::VELOCITY,NULL,f);
    }
    assembleV(DFDX);
  }
  return 0;
}
const vector<boost::shared_ptr<SpaceTimeMesh> >& ActiveOpt::levels() const
{
  return _levelST;
}
void ActiveOpt::optimizeGD()
{
  Vec DFDX=Vec::Zero(inputs()),X;
  scalarD FX,lastFX=ScalarUtil<scalar>::scalar_max;
  sizeType nrActive=_pt.get<sizeType>("maxIterActive",1000);
  scalarD reduction,reductionTol=_pt.get<scalar>("minReduction",0.1f);
  //update
  for(sizeType it=0; it<nrActive; it++) {
    //compute gradient
    assembleV(X);
    operator()(X,FX,DFDX,1,true);
    //check convergence
    INFOV("Performed %ld KKT iterations, FX=%lf!",it,FX)
    //exit test
    if(it > 0) {
      INFOV("Function reduction: %lf",reduction=(lastFX-FX))
      if(reduction < reductionTol)
        break;
    }
    //apply line search
    _stepSz=QuadraticTerm::lineSearch(findPolyCoef(X,DFDX,&FX));
    //_stepSz=QuadraticTerm::lineSearch(findPolyCoef(X,DFDX));
    X+=DFDX*_stepSz;
    assignV(X);
    lastFX=FX;
  }
}
void ActiveOpt::optimizeKKT()
{
#define STEP_DEC_COEF 0.2f
#define STEP_INC_COEF 1.1f
  Vec DFDX=Vec::Zero(inputs()),saved,DFDXSaved;
  scalarD regK=_pt.get<scalar>("regK",1000.0f);
  scalarD regU=_pt.get<scalar>("regU",100.0f);
  sizeType nrActive=_pt.get<sizeType>("maxIterActive",1000);
  sizeType nrV=_mesh.state(0).nrVelComp();
  scalar FX,lastFX=ScalarUtil<scalar>::scalar_max,lowerBound=1E-7f;
  scalarD reductionRateCurr=0,reductionRateTol=0,reductionTol=0;
  reductionRateTol=_pt.get<scalar>("minReductionRate",1.0f);
  reductionTol=_pt.get<scalar>("minReduction",0.1f);
  //update
  for(sizeType it=0; it<nrActive; it++) {
    //compute gradient
    FX=0;
    FluidSolver& sol=_mesh.solver<FluidSolver>();
    for(sizeType i=0; i<_mesh.nrFrame()-1; i++) {
      FluidState& s=_mesh.state(i);
      if(s.velPtr(FluidState::VELOCITY_REFERENCE))
        FX+=s.vel(FluidState::VELOCITY).squaredDistTo(s.vel(FluidState::VELOCITY_REFERENCE))*regK;
      else FX+=s.vel(FluidState::VELOCITY).squaredNorm()*regK;
    }
    DFDX.setZero();
    for(sizeType i=0; i<_mesh.nrFrame()-2; i++) {
      FluidState& s=_mesh.state(i);
      if(!s.velPtr(FluidState::GHOST_FORCE))
        s.addField(0,FluidState::GHOST_FORCE);
      //project out ghost force divergence
      sol.solveForce(s,_mesh.state(i+1),&DFDX,i*nrV,(i+1)*nrV);
      FX+=s.vel(FluidState::GHOST_FORCE).squaredNorm()*regU;
    }
    FX*=0.5f;
    //check convergence
    INFOV("Performed %ld KKT iterations, stepsize=%lf, FX=%lf!",it,_stepSz,FX)
    if(it == 0) {
      lastFX=FX;
      assembleVG(saved);
      DFDXSaved=DFDX;
    } else if(FX > lastFX || isNan(FX) || isInf(FX)) {
      //recover
      assignVG(saved);
      DFDX=DFDXSaved;
      //reduce step size
      _stepSz=max<scalar>(_stepSz*STEP_DEC_COEF,lowerBound);
    } else {
      //exit test
      if(it > 0) {
        reductionRateCurr=(lastFX-FX)/_stepSz;
        INFOV("Function reduction: %lf, reduction rate: %lf",reductionRateCurr*_stepSz,reductionRateCurr)
        if(reductionRateCurr < reductionRateTol || reductionRateCurr*_stepSz < reductionTol)
          break;
      }
      //accept
      assembleVG(saved);
      DFDXSaved=DFDX;
      lastFX=FX;
      //tentatively increase step size
      _stepSz=min<scalar>(_stepSz*STEP_INC_COEF,1.0f);
    }
    if(_stepSz == lowerBound)
      break;
    //apply fixed iteration
    for(sizeType i=0; i<_mesh.nrFrame()-1; i++) {
      FluidState& s=_mesh.state(i);
      s.velNonConst(FluidState::VELOCITY).mul(1-_stepSz);
      if(s.velPtr(FluidState::VELOCITY_REFERENCE)) {
        s.velNonConst(FluidState::VELOCITY).addScaled(s.vel(FluidState::VELOCITY_REFERENCE),_stepSz);
        s.fromVec(FluidState::VELOCITY,DFDX,&add,i*nrV,-regU/regK*_stepSz);
      } else
        s.fromVec(FluidState::VELOCITY,DFDX,&add,i*nrV,-regU/regK*_stepSz);
      //project out velocity divergence
      if(!s.velPtr(FluidState::GHOST_FORCE_PRESSURE))
        s.addField(FluidState::GHOST_FORCE_PRESSURE,0);
      sol.enforceDivFree(s,FluidState::VELOCITY,NULL,FluidState::GHOST_FORCE_PRESSURE);
    }
  }
  INFOV("Current active step size: %f",_stepSz)
#undef STEP_DEC_COEF
#undef STEP_INC_COEF
}
void ActiveOpt::optimizeLBFGS()
{
  Vec x;
  scalarD fx;
  LBFGSMinimizer<scalarD> sol;
  ConvergencyCounter<scalarD,Kernel<scalarD> > cb(outputFileName("./activeHistoryLBFGS"));
  //in the default version we use LBFGS
  assembleV(x);
  sol.maxIterations()=_pt.get<sizeType>("maxIterActive",1000);
  sol.epsilon()=_pt.get<scalar>("epsilonLBFGS",1E-4f);
  sol.minimize(x,fx,*this,cb);
  assignV(x);
  DEDVel(fx,NULL);
}
void ActiveOpt::optimizeSTMG()
{
#define REGK_DEC_COEF 0.5f
#define REGK_INC_COEF 2.1f
#define REGK_STMG_THRES 0.8f
  //save VELOCITY_REFERENCE
  vector<MACVelocityField> velRef(_mesh.nrFrame());
  for(sizeType i=0; i<_mesh.nrFrame(); i++)
    if(_mesh.state(i).velPtr(FluidState::VELOCITY_REFERENCE))
      velRef[i]=_mesh.state(i).vel(FluidState::VELOCITY_REFERENCE);
  //settings
  scalar eps=0;
  scalar regK=_pt.get<scalar>("regK",1000.0f);
  scalar regU=_pt.get<scalar>("regU",100.0f);
  sizeType nrActive=_pt.get<sizeType>("maxIterActive",1000);
  _stepSz=max<scalar>(regU/regK,1);
  //solve using STMG VCycle
  initializeSTMG();
  beginRHS();
  ConvergencyCounter<scalarD,Kernel<scalarD> > cb(outputFileName("./activeHistorySTMG"));
  for(sizeType it=0; it<nrActive; it++) {
    //set VELOCITY_REFERENCE
    _mesh.getPt().put<scalar>("regK",regK*_stepSz);
    for(sizeType i=0; i<_mesh.nrFrame(); i++)
      if(_mesh.state(i).velPtr(FluidState::VELOCITY_REFERENCE)) {
        MACVelocityField& vRefI=_mesh.state(i).velNonConst(FluidState::VELOCITY_REFERENCE);
        vRefI.init(Vec3::Zero());
        vRefI.addScaled(_mesh.state(i).vel(FluidState::VELOCITY),regK*(_stepSz-1)/regU);
        vRefI.addScaled(velRef[i],regK/regU);
      }
    //STMG loop
    scalarD lastKKTResidual=computeFASRhsST(*(_levelST[0]),*(_levelST[1]),true),KKTResidual=0;
    cb(Vec(),lastKKTResidual,it);
    INFOV("KKT Residual initial: %f, RegKCoef: %f",lastKKTResidual,_stepSz)
    if(it == 0)
      eps=max<scalar>(_pt.get<scalar>("epsilonSTMG",1E-5f),KKTResidual*0.01f);
    if(lastKKTResidual < eps)
      break;
    bool lowConvergence=false;
    saveSolutionToFile();
    for(sizeType it=0; it<nrActive; it++) {
      //apply multigrid and calculate function value
      STMGVCycle(0,2,2,10);
      KKTResidual=computeFASRhsST(*(_levelST[0]),*(_levelST[1]),true);
      //check result
      cb(Vec(),KKTResidual,it);
      INFOV("KKT residual at iteration %ld: %f",it,KKTResidual)
      if(KKTResidual < eps)
        break;
      if(it > 0 && KKTResidual > lastKKTResidual*REGK_STMG_THRES) {
        lowConvergence=true;
        loadSolutionFromFile();
        break;
      }
      lastKKTResidual=KKTResidual;
    }
    if(lowConvergence)
      _stepSz=max<scalar>(_stepSz-1,1/REGK_INC_COEF)*REGK_INC_COEF+1;
    else _stepSz=max<scalar>((_stepSz-1)*REGK_DEC_COEF+1,1);
  }
  endRHS();
  _mesh.getPt().put<scalar>("regK",regK);
#undef REGK_DEC_COEF
#undef REGK_INC_COEF
#undef REGK_STMG_THRES
}
void ActiveOpt::initializeSTMG()
{
  //initialize solverMG
  FluidSolverSpatialMG& sol=_mesh.solver<FluidSolverSpatialMG>();
  const vector<FluidStateMG>& mg=sol.levels();
  //initialize STMG hierarchy
  if(_levelST.size() != mg.size()) {
    sizeType N=_mesh.nrFrame()-1;
    _levelST.clear();
    _levelST.push_back(boost::shared_ptr<SpaceTimeMesh>(&_mesh,NoDeletor()));
    while(true) {
      const ScalarField& pre=_levelST.back()->state(0).scal(FluidState::PRESSURE);
      const Vec3i nrC=pre.getNrCell()/2;
      if(nrC.block(0,0,pre.getDim(),1).minCoeff() <= 2)
        break;
      INFOV("Creating spacetime level: nrFrame=%ld, resolution=(%ld,%ld,%ld)!",N,nrC[0],nrC[1],nrC[2])
      _levelST.push_back(boost::shared_ptr<SpaceTimeMesh>(new SpaceTimeMesh(_mesh.getPt(),false)));
      _levelST.back()->getPt().put<sizeType>("solverType",SpaceTimeMesh::NO_SOLVER);
      _levelST.back()->init(nrC,pre.getBB().getExtent(),N);
    }
    //create fields
    for(sizeType i=0; i<(sizeType)_levelST.size(); i++) {
      SpaceTimeMesh& mesh=*(_levelST[i]);
      sizeType nrFrame=(i==0) ? mesh.nrFrame()-1 : mesh.nrFrame();
      for(sizeType f=0; f<nrFrame; f++) {
        {
          //create primal-dual ghost force
          if(!mesh.state(f).scalPtr(FluidState::GHOST_FORCE_PRESSURE))
            mesh.state(f).addField(FluidState::GHOST_FORCE_PRESSURE,0);
          if(f<nrFrame-1 && !mesh.state(f).velPtr(FluidState::GHOST_FORCE))
            mesh.state(f).addField(0,FluidState::GHOST_FORCE);
        }
        if(i > 0) {
          //create primal-dual rhs
          mesh.state(f).addField(FluidState::RHS_GHOST_FORCE_PRESSURE,FluidState::RHS_VELOCITY);
          if(f<nrFrame-1)
            mesh.state(f).addField(FluidState::RHS_PRESSURE,FluidState::RHS_GHOST_FORCE);
        }
      }
    }
  }
}
void ActiveOpt::beginRHS()
{
  //save memory by reusing VELOCITY_REFERENCE as RHS_VELOCITY
  scalar regK=_pt.get<scalar>("regK",1000.0f);
  scalar regU=_pt.get<scalar>("regU",100.0f);
  for(sizeType f=0; f<_mesh.nrFrame()-1; f++)
    if(_mesh.state(f).velPtr(FluidState::VELOCITY_REFERENCE)) {
      _mesh.state(f).velPtr(FluidState::RHS_VELOCITY)=_mesh.state(f).velPtr(FluidState::VELOCITY_REFERENCE);
      _mesh.state(f).velNonConst(FluidState::RHS_VELOCITY).mul(regK/regU);
    }
}
void ActiveOpt::endRHS()
{
  //save memory by reusing VELOCITY_REFERENCE as RHS_VELOCITY
  scalar regK=_pt.get<scalar>("regK",1000.0f);
  scalar regU=_pt.get<scalar>("regU",100.0f);
  for(sizeType f=0; f<_mesh.nrFrame()-1; f++)
    if(_mesh.state(f).velPtr(FluidState::VELOCITY_REFERENCE)) {
      _mesh.state(f).velNonConst(FluidState::RHS_VELOCITY).mul(regU/regK);
      _mesh.state(f).velPtr(FluidState::RHS_VELOCITY).reset((MACVelocityField*)NULL);
    }
}
//spacetime multigrid VCycle
scalarD ActiveOpt::smoothST(SpaceTimeMesh& mesh,sizeType nrF)
{
#define TRIDIAG true
#define STYPE FluidSolverSpatialMG::SMOOTHST_STANDARD
  FluidSolverSpatialMG& sol=_mesh.solver<FluidSolverSpatialMG>();
  if(TRIDIAG) {
    vector<boost::shared_ptr<FluidState> > states;
    for(sizeType i=0; i<nrF; i++)
      states.push_back(mesh.statePtr(i,nrF));
    return sol.solveSmooth(states,STYPE);
  } else {
    scalarD maxDelta=0,delta;
    for(sizeType pass=0; pass<2; pass++)
      for(sizeType i=0; i<nrF; i++)
        if(i%2 == pass) {
          delta=sol.solveSmooth(mesh.statePtr(i-1,nrF).get(),mesh.state(i),mesh.statePtr(i+1,nrF).get(),STYPE);
          maxDelta=max<scalarD>(maxDelta,delta);
        }
    return maxDelta;
  }
#undef STYPE
#undef TRIDIAG
}
void ActiveOpt::restrictST(const SpaceTimeMesh& currLv,SpaceTimeMesh& nextLv,OP op)
{
  sizeType nrCurr=(&currLv == &_mesh) ? currLv.nrFrame()-1 : currLv.nrFrame();
  ASSERT(nrCurr == nextLv.nrFrame())
  for(sizeType ic=0; ic<nrCurr; ic++) {
    nextLv.state(ic).restrictV(currLv.state(ic),FluidState::VELOCITY,FluidState::VELOCITY,op);
    nextLv.state(ic).restrictS(currLv.state(ic),FluidState::GHOST_FORCE_PRESSURE,FluidState::GHOST_FORCE_PRESSURE,op);
    if(ic<nrCurr-1) {
      nextLv.state(ic).restrictV(currLv.state(ic),FluidState::GHOST_FORCE,FluidState::GHOST_FORCE,op);
      nextLv.state(ic).restrictS(currLv.state(ic),FluidState::PRESSURE,FluidState::PRESSURE,op);
    }
  }
}
void ActiveOpt::interpolateST(SpaceTimeMesh& currLv,const SpaceTimeMesh& nextLv,OP op)
{
  sizeType nrCurr=(&currLv == &_mesh) ? currLv.nrFrame()-1 : currLv.nrFrame();
  ASSERT(nrCurr == nextLv.nrFrame())
  for(sizeType ic=0; ic<nrCurr; ic++) {
    nextLv.state(ic).interpolateV(currLv.state(ic),FluidState::VELOCITY,FluidState::VELOCITY,op);
    nextLv.state(ic).interpolateS(currLv.state(ic),FluidState::GHOST_FORCE_PRESSURE,FluidState::GHOST_FORCE_PRESSURE,op);
    if(ic<nrCurr-1) {
      nextLv.state(ic).interpolateV(currLv.state(ic),FluidState::GHOST_FORCE,FluidState::GHOST_FORCE,op);
      nextLv.state(ic).interpolateS(currLv.state(ic),FluidState::PRESSURE,FluidState::PRESSURE,op);
    }
  }
}
scalarD ActiveOpt::computeFASRhsST(SpaceTimeMesh& currLv,SpaceTimeMesh& nextLv,bool forResidual)
{
  scalarD retPrimal=0,retDual=0;
  sizeType nrCurr=(&currLv == &_mesh) ? currLv.nrFrame()-1 : currLv.nrFrame();
  ASSERT(nrCurr == nextLv.nrFrame())
  FluidSolverSpatialMG& sol=_mesh.solver<FluidSolverSpatialMG>();
  for(sizeType ic=0; ic<nrCurr; ic++) {
    retPrimal+=sol.computePrimalRhs(currLv.statePtr(ic-1,nrCurr).get(),currLv.state(ic),currLv.statePtr(ic+1,nrCurr).get(),nextLv.statePtr(ic,nrCurr).get());
    if(!forResidual)
      sol.computePrimalRhs(nextLv.statePtr(ic-1,nrCurr).get(),nextLv.state(ic),nextLv.statePtr(ic+1,nrCurr).get(),NULL);
    if(ic<nrCurr-1) {
      retDual+=sol.computeDualRhs(currLv.state(ic),currLv.state(ic+1),nextLv.statePtr(ic,nrCurr).get());
      if(!forResidual)
        sol.computeDualRhs(nextLv.state(ic),nextLv.state(ic+1),NULL);
    }
  }
  //INFOV("Primal: %f, Dual: %f",retPrimal,retDual)
  //return sqrt(retPrimal);
  return sqrt(retPrimal+retDual);
}
Traits::VecD ActiveOpt::findPolyCoef(const Vec& x0,const Vec& dir,const scalarD* FX0)
{
  static Matd stencil;
  static scalarD alpha=1.0f;
  if(stencil.size() != 25) {
    stencil.setZero(5,5);
    stencil <<
            pow(0*alpha,4)  ,pow(0*alpha,3)  ,pow(0*alpha,2)  ,pow(0*alpha,1)  ,1,
                pow(1*alpha,4)  ,pow(1*alpha,3)  ,pow(1*alpha,2)  ,pow(1*alpha,1)  ,1,
                pow(2*alpha,4)  ,pow(2*alpha,3)  ,pow(2*alpha,2)  ,pow(2*alpha,1)  ,1,
                pow(3*alpha,4)  ,pow(3*alpha,3)  ,pow(3*alpha,2)  ,pow(3*alpha,1)  ,1,
                pow(4*alpha,4)  ,pow(4*alpha,3)  ,pow(4*alpha,2)  ,pow(4*alpha,1)  ,1;
    //cout << "Poly stencil: " << stencil << endl;
  }

  Vec tmp;
  VecD ret=VecD::Zero(5);
  if(FX0)
    ret[0]=*FX0;
  else operator()(x0+dir*0*alpha,ret[0],tmp,1,false);
  operator()(x0+dir*1*alpha,ret[1],tmp,1,false);
  operator()(x0+dir*2*alpha,ret[2],tmp,1,false);
  operator()(x0+dir*3*alpha,ret[3],tmp,1,false);
  operator()(x0+dir*4*alpha,ret[4],tmp,1,false);
  return stencil.inverse()*ret;
}
Traits::VecD ActiveOpt::findPolyCoef(Vec VG0,const Vec& dir)
{
  Vec tmp;
  scalarD P1,N1;
  if(VG0.size() != inputsVG()) {
    tmp=VG0;
    VG0.setZero(inputsVG());
    VG0.block(0,0,tmp.size(),1)=tmp;
  }
  Vec VG1=VG0,VG2=VG0;
  VecD ret=VecD::Zero(5);

  //fill VG0/1/2
  sizeType nrV=_mesh.state(0).nrVelComp();
  VG1.block(0,0,dir.size(),1)-=dir;
  VG2.block(0,0,dir.size(),1)+=dir;

  //operator()(VG0,ret[1],tmp,1,false); //we assume this has been called
  for(sizeType i=0,off=0; i<_mesh.nrFrame()-1; i++,off+=nrV)
    _mesh.state(i).toVec(FluidState::VELOCITY_REFERENCE,const_cast<Vec&>(VG0),&sub,off);
  for(sizeType i=0,off=dir.size(); i<_mesh.nrFrame()-2; i++,off+=nrV)
    _mesh.state(i).toVec(FluidState::GHOST_FORCE,const_cast<Vec&>(VG0),&set,off);

  operator()(VG1,N1,tmp,1,false);
  for(sizeType i=0,off=0; i<_mesh.nrFrame()-1; i++,off+=nrV)
    _mesh.state(i).toVec(FluidState::VELOCITY_REFERENCE,const_cast<Vec&>(VG1),&sub,off);
  for(sizeType i=0,off=dir.size(); i<_mesh.nrFrame()-2; i++,off+=nrV)
    _mesh.state(i).toVec(FluidState::GHOST_FORCE,const_cast<Vec&>(VG1),&set,off);

  operator()(VG2,P1,tmp,1,false);
  for(sizeType i=0,off=0; i<_mesh.nrFrame()-1; i++,off+=nrV)
    _mesh.state(i).toVec(FluidState::VELOCITY_REFERENCE,const_cast<Vec&>(VG2),&sub,off);
  VG2.block(dir.size(),0,VG2.size()-dir.size(),1).setZero();
  for(sizeType i=0,off=dir.size(); i<_mesh.nrFrame()-2; i++,off+=nrV)
    _mesh.state(i).toVec(FluidState::GHOST_FORCE,const_cast<Vec&>(VG2),&set,off);

  //prepare function evaluation
  scalarD regK=_pt.get<scalar>("regK",1000.0f);
  scalarD regU=_pt.get<scalar>("regU",100.0f);
  VG0.block(0,0,dir.size(),1)*=sqrt(regK);
  VG1.block(0,0,dir.size(),1)*=sqrt(regK);
  VG2.block(0,0,dir.size(),1)*=sqrt(regK);
  VG0.block(dir.size(),0,VG0.size()-dir.size(),1)*=sqrt(regU);
  VG1.block(dir.size(),0,VG1.size()-dir.size(),1)*=sqrt(regU);
  VG2.block(dir.size(),0,VG2.size()-dir.size(),1)*=sqrt(regU);

  //fill coefs
  VG1=(VG2-VG1)/2;
  VG2-=(VG1+VG0);
  ret[0]=VG2.squaredNorm();
  ret[1]=VG2.dot(VG1)*2;
  ret[2]=VG2.dot(VG0)*2+VG1.squaredNorm();
  ret[3]=VG1.dot(VG0)*2;
  ret[4]=VG0.squaredNorm();
  return ret/2;
}
void ActiveOpt::saveSolutionToFile()
{
  IOData dat;
  boost::filesystem::ofstream os("./solution.dat",ios::binary);
  for(sizeType i=0; i<_mesh.nrFrame(); i++) {
    if(_mesh.state(i).velPtr(FluidState::VELOCITY))
      _mesh.state(i).vel(FluidState::VELOCITY).write(os,&dat);
    if(_mesh.state(i).scalPtr(FluidState::PRESSURE))
      _mesh.state(i).scal(FluidState::PRESSURE).write(os,&dat);
    if(_mesh.state(i).velPtr(FluidState::GHOST_FORCE))
      _mesh.state(i).vel(FluidState::GHOST_FORCE).write(os,&dat);
    if(_mesh.state(i).scalPtr(FluidState::GHOST_FORCE_PRESSURE))
      _mesh.state(i).scal(FluidState::GHOST_FORCE_PRESSURE).write(os,&dat);
  }
}
void ActiveOpt::loadSolutionFromFile()
{
  IOData dat;
  ASSERT(boost::filesystem::exists("./solution.dat"))
  boost::filesystem::ifstream is("./solution.dat",ios::binary);
  for(sizeType i=0; i<_mesh.nrFrame(); i++) {
    if(_mesh.state(i).velPtr(FluidState::VELOCITY))
      _mesh.state(i).velNonConst(FluidState::VELOCITY).read(is,&dat);
    if(_mesh.state(i).scalPtr(FluidState::PRESSURE))
      _mesh.state(i).scalNonConst(FluidState::PRESSURE).read(is,&dat);
    if(_mesh.state(i).velPtr(FluidState::GHOST_FORCE))
      _mesh.state(i).velNonConst(FluidState::GHOST_FORCE).read(is,&dat);
    if(_mesh.state(i).scalPtr(FluidState::GHOST_FORCE_PRESSURE))
      _mesh.state(i).scalNonConst(FluidState::GHOST_FORCE_PRESSURE).read(is,&dat);
  }
}
scalarD ActiveOpt::STMGVCycle(sizeType lvCurr,sizeType nrPre,sizeType nrPost,sizeType nrFinal)
{
  scalarD ret=0;
  SpaceTimeMesh& currLv=*(_levelST[lvCurr]);
  sizeType nrF=(lvCurr == 0) ? currLv.nrFrame()-1 : currLv.nrFrame();
  //return smoothST(currLv,nrF);
  if(lvCurr == (sizeType)_levelST.size()-1) {
    for(sizeType i=0; i<nrFinal; i++)
      smoothST(currLv,nrF);
  } else {
    SpaceTimeMesh& nextLv=*(_levelST[lvCurr+1]);
    for(sizeType i=0; i<nrPre; i++)
      smoothST(currLv,nrF);
    //restrict solution: R*x
    for(sizeType i=0; i<nextLv.nrFrame(); i++)
      nextLv.state(i).setZero(FluidState::VELOCITY|FluidState::GHOST_FORCE| //lhs stuff
                              FluidState::PRESSURE|FluidState::GHOST_FORCE_PRESSURE|
                              FluidState::RHS_VELOCITY|FluidState::RHS_GHOST_FORCE| //rhs stuff
                              FluidState::RHS_PRESSURE|FluidState::RHS_GHOST_FORCE_PRESSURE);
    restrictST(currLv,nextLv,&add);
    //restrict residual: f(R*x)+R*(rhs-f(x))
    ret=computeFASRhsST(currLv,nextLv,false);
    //solve coarse
    interpolateST(currLv,nextLv,&sub);
    STMGVCycle(lvCurr+1,nrPre,nrPost,nrFinal);
    interpolateST(currLv,nextLv,&add);
    for(sizeType i=0; i<nrPost; i++)
      smoothST(currLv,nrF);
  }
  return ret;
}
//FluidOpt
FluidOpt::FluidOpt(SpaceTimeMesh& mesh)
  :_pt(mesh.getPt()),_mesh(mesh),_PO(mesh,&_fixed),_AO(mesh)
{
  for(sizeType i=0; i<mesh.nrFrame(); i++)
    if(_pt.get<bool>("fixFrame"+boost::lexical_cast<string>(i),false)) {
      INFOV("Fixing frame %ld",i)
      _fixed.insert(i);
    }
}
void FluidOpt::fixFrame(sizeType frm,bool fix)
{
  if(fix)
    _fixed.insert(frm);
  else _fixed.erase(frm);
}
void FluidOpt::updateRefVelocity(bool updateLambda,scalar maxLambdaFrac)
{
  scalar maxLambdaVal,maxVelDiff;
  scalar regK=_pt.get<scalar>("regK",1000.0f);
  scalar coef=_pt.get<scalar>("lambdaUpdateCoef",1.0f);
  MACVelocityField tmp=_mesh.state(0).vel(FluidState::VELOCITY);
  for(sizeType i=0; i<_mesh.nrFrame()-1; i++) {
    //create reference velocity
    if(!_mesh.state(i).velPtr(FluidState::VELOCITY_REFERENCE)) {
      //INFOV("Need to create VELOCITY_REFERENCE/VELOCITY_LAMBDA for frame %ld!",i)
      _mesh.state(i).addField(0,FluidState::VELOCITY_REFERENCE|FluidState::VELOCITY_LAMBDA);
    }
    //swap
    if(_fixed.find(i) == _fixed.end()) {
      MACVelocityField& lambda=_mesh.state(i).velNonConst(FluidState::VELOCITY_LAMBDA);
      if(updateLambda) {
        tmp.init(Vec3::Zero());
        tmp.add(_mesh.state(i).vel(FluidState::VELOCITY_REFERENCE));
        tmp.sub(_mesh.state(i).vel(FluidState::VELOCITY));
        lambda.addScaled(tmp,coef*regK);
        {
          //renormalize
          maxLambdaVal=lambda.getAbsMax().maxCoeff();
          maxVelDiff=tmp.getAbsMax().maxCoeff();
          _mesh.getPt().put<scalar>("maxLambda"+boost::lexical_cast<string>(i),maxLambdaVal);
          _mesh.getPt().put<scalar>("maxVelDiff"+boost::lexical_cast<string>(i),maxVelDiff);
          INFOV("Lambda inf-norm at frame %ld: %f, max lambda: %f, velDiff: %f!",i,maxLambdaVal,regK*maxLambdaFrac,maxVelDiff)
          if(maxLambdaVal > regK*maxLambdaFrac)
            lambda.mul(regK*maxLambdaFrac/maxLambdaVal);
        }
      }
      lambda.mul(-1);
      _mesh.state(i).velNonConst(FluidState::VELOCITY_REFERENCE).swap(_mesh.state(i).velNonConst(FluidState::VELOCITY));
    } else _mesh.state(i).velNonConst(FluidState::VELOCITY_REFERENCE).swap(_mesh.state(i).velNonConst(FluidState::VELOCITY));
  }
}
scalarD FluidOpt::optimizePassive()
{
  addLambda(1);
  scalarD ret=0;
  sizeType modeP=(MODE)_pt.get<sizeType>("modePassive",KKT);
  if(modeP == LBFGS)
    ret=_PO.optimizeLBFGS();
  else if(modeP == GD)
    ret=_PO.optimizeGD();
  else if(modeP == KKT)
    ret=_PO.optimizeKKT();
  else {
    ASSERT_MSG(false,"Unknown passive mode!")
  }
  addLambda(-1);
  INFOV("Delta ADMM=%f",ret)
  return ret;
}
void FluidOpt::optimizeActive()
{
  if(_pt.get<bool>("bootstrap",false)) {
    INFO("Skipping active optimization in bootstrap mode!")
    for(sizeType i=0; i<_mesh.nrFrame()-1; i++)
      if(_mesh.state(i).velPtr(FluidState::VELOCITY_REFERENCE))
        _mesh.state(i).velNonConst(FluidState::VELOCITY)=_mesh.state(i).vel(FluidState::VELOCITY_REFERENCE);
  } else {
    addLambda(1);
    sizeType modeA=(MODE)_pt.get<sizeType>("modeActive",STMG);
    if(modeA == LBFGS)
      _AO.optimizeLBFGS();
    else if(modeA == GD)
      _AO.optimizeGD();
    else if(modeA == KKT)
      _AO.optimizeKKT();
    else if(modeA == STMG)
      _AO.optimizeSTMG();
    else {
      ASSERT_MSG(false,"Unknown active mode!")
    }
    addLambda(-1);
  }
}
void FluidOpt::optimize()
{
  string outputPath=_pt.get<string>("outputPath");
  boost::filesystem::create_directory(outputPath);
  //boost::property_tree::xml_writer_settings<char> settings('\t',1);
  //boost::property_tree::write_xml("./ptreeBegin.xml",_pt,std::locale(),settings);
  boost::property_tree::write_xml(outputPath+"/ptreeBegin.xml",_pt,std::locale());
  sizeType maxIter=_pt.get<sizeType>("maxIterOutter",100000);
  sizeType outputIter=_pt.get<sizeType>("outputIter",10);
  scalarD maxLambdaFrac=0.3f;

  //bootstrap mode
  vector<sizeType> keyFrames;
  bool bootstrap=_pt.get<bool>("bootstrap",false);
  if(bootstrap) {
    keyFrames.push_back(0);
    const SpaceTimeMesh::KeyFrameMap& keys=_mesh.keyFrameMap();
    for(SpaceTimeMesh::KeyFrameMap::const_iterator
        beg=keys.begin(),end=keys.end(); beg!=end; beg++)
      keyFrames.push_back(beg->first);
    sort(keyFrames.begin(),keyFrames.end());
    outputIter=1;
  }

  //ADMM outer loop
  sizeType fromIter=_pt.get<sizeType>("fromIter",0);
  INFOV("Starting from iteration %ld",fromIter)
  for(sizeType i=fromIter; i<maxIter; i++) {
    if(bootstrap) {
      if(keyFrames.size() < 2)
        break;
      _mesh.getPt().put<sizeType>("frameTo",keyFrames[1]);
      _mesh.getPt().put<sizeType>("frameFrom",keyFrames[0]);
      scalar regK=_pt.get<scalar>("regKBootstrap",1.0f);
      _mesh.getPt().put<scalar>("regK",regK);
      INFOV("bootstrap optimizing frames %ld-%ld, with regK: %f",keyFrames[0],keyFrames[1],regK)
      keyFrames.erase(keyFrames.begin());
    }

    //passive
    TBEGT
    optimizePassive();
    TENDT("passiveTime",_mesh.getPt())
    INFOV("Optimize Passive %ld of %ld (used %fs)!",i,maxIter,_mesh.getPt().get<scalar>("passiveTime"))
    updateRefVelocity(false,maxLambdaFrac);

    //{
    //  IOData dat;
    //  boost::filesystem::ofstream os("./mesh.dat",ios::binary);
    //  _mesh.write(os,&dat);
    //}
    //exit(0);

    //active
    TBEGT
    optimizeActive();
    TENDT("activeTime",_mesh.getPt())
    INFOV("Optimize Active %ld of %ld (used %fs)!",i,maxIter,_mesh.getPt().get<scalar>("activeTime"))
    updateRefVelocity(true,maxLambdaFrac);

    //output
    if((i%outputIter) == 0) {
      IOData dat;
      INFO("Output Data")
      //output vtk data
      string outputPathIter=outputPath+"/optimizedIter"+boost::lexical_cast<string>(i);
      _mesh.updateAdvect(Advector::IMPLICIT,true);
      _mesh.writeVTK(outputPathIter);
      //output temporary data for continuing
      boost::filesystem::ofstream os(outputPathIter+"/mesh.dat",ios::binary);
      _mesh.write(os,&dat);
      //output xml represented convergence history
      //boost::property_tree::write_xml(outputPathIter+"/ptreeEnd.xml",_pt,std::locale(),settings);
      boost::property_tree::write_xml(outputPathIter+"/ptreeEnd.xml",_pt,std::locale());
      if(checkStop() && !bootstrap)
        break;
    }
  }
  _mesh.updateAdvect();
}
void FluidOpt::addLambda(scalar coef)
{
  const scalar regK=_pt.get<scalar>("regK",1000.0f);
  for(sizeType i=0; i<_mesh.nrFrame()-1; i++) {
    //create reference velocity
    if(!_mesh.state(i).velPtr(FluidState::VELOCITY_REFERENCE)) {
      //INFOV("Need to create VELOCITY_REFERENCE/VELOCITY_LAMBDA for frame %ld!",i)
      _mesh.state(i).addField(0,FluidState::VELOCITY_REFERENCE|FluidState::VELOCITY_LAMBDA);
    }
    //swap
    if(_fixed.find(i) == _fixed.end()) {
      const MACVelocityField& lambda=_mesh.state(i).vel(FluidState::VELOCITY_LAMBDA);
      MACVelocityField& v=_mesh.state(i).velNonConst(FluidState::VELOCITY_REFERENCE);
      v.addScaled(lambda,coef/regK);
    }
  }
}
void FluidOpt::clearLambda()
{
  for(sizeType i=0; i<_mesh.nrFrame(); i++)
    if(_mesh.state(i).velPtr(FluidState::VELOCITY_LAMBDA))
      _mesh.state(i).velNonConst(FluidState::VELOCITY_LAMBDA).init(Vec3::Zero());
}
bool FluidOpt::checkStop()
{
  //get max diff
  scalar maxDiff=ScalarUtil<scalar>::scalar_inf;
  if((sizeType)_lastRho.size() == _mesh.nrFrame()) {
    maxDiff=0;
    for(sizeType f=0; f<_mesh.nrFrame(); f++) {
      _lastRho[f].sub(*(_mesh.state(f)._rho));
      maxDiff=max<scalar>(maxDiff,_lastRho[f].getAbsMax());
    }
  }
  //copy
  _lastRho.resize(_mesh.nrFrame());
  for(sizeType f=0; f<_mesh.nrFrame(); f++)
    _lastRho[f]=*(_mesh.state(f)._rho);
  //determine
  scalar thres=_pt.get<scalar>("stopRhoCoef",0.01f)*_mesh.state(0)._rho->getAbsMax();
  INFOV("Stopping Criteron Check, maxDiff: %f, thres: %f",maxDiff,thres)
  return maxDiff < thres;
}
