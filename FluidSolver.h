#ifndef FLUID_SOLVER_H
#define FLUID_SOLVER_H

#include "FluidState.h"
#include "Advector.h"

PRJ_BEGIN

class SmoothLHS;
template <typename T,typename KERNEL_TYPE>
struct KrylovMatrix;
struct SolverStruct;
class FluidSolver : public Traits
{
public:
  enum SMOOTHST_TYPE
  {
    SMOOTHST_STANDARD,
    SMOOTHST_LEAST_SQUARE,
  };
  enum TIME_INTEGRATOR {
    MIDPOINT,
    BACKWARD,
  };
  FluidSolver(const boost::property_tree::ptree& pt);
  virtual ~FluidSolver() {}
  virtual void reset(const FluidState& s);
  //integrate velocity forward
  virtual void solve(FluidState& from,FluidState& to);
  //compute ghost-force and its gradient against velocity
  virtual void solveForce(FluidState& from,const FluidState& to,VecD* deriv,sizeType iFrom,sizeType iTo);
  //project out divergence component
  virtual bool enforceDivFree(FluidState& from,FluidState::FIELD f=FluidState::VELOCITY,VecD* v=NULL,FluidState::FIELD fp=FluidState::NO_FIELD);
  //smoothing operator for spacetime optimization
  virtual scalarD solveSmooth(FluidState* last,FluidState& curr,FluidState* next,SMOOTHST_TYPE type);
  void buildSmooth(const FluidState* last,FluidState& curr,const FluidState* next,VecD& rhs,VecD& x);
  const KrylovMatrix<scalarD,Kernel<scalarD> >& smoothLHS() const;
  const Advector& advector() const;
  static Curl buildAdvStencil(const FluidState& s);
  static Curl buildAdvStencil(const FluidState& s,const Vec3i& curr);
  static VecD solve(const FluidState& s,TRIPS& trips,const VecD& rhs);
  static VecD solve(const FluidState& s,SolverStruct& sol,const VecD& rhs);
  static void buildDivergenceFree(const FluidState& s,TRIPS& trips,bool KKT=true);
  //these are really slow, call if you want debug
  static scalarD checkConvergence(const FluidState& from,const VecD& vP,VecD rhs,scalar coefTensor,bool print);
protected:
  const boost::property_tree::ptree& _pt;
  Advector _adv;
private:
  boost::shared_ptr<SmoothLHS> _smoothLHS;
  boost::shared_ptr<SolverStruct> _solDivFree;
  Curl _AVCoef;
  SMat _div;
};
void debugFluidSolver(sizeType dim,Vec3c periodic);

PRJ_END

#endif
