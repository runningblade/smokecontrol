#include "FluidSolver.h"
#include "CommonFile/solvers/PMinresQLP.h"
#include <boost/property_tree/ptree.hpp>
//#include <Eigen/UmfPackSupport>

USE_PRJ_NAMESPACE

#define BUILD_CURLX {  \
coefV[0]=s.velComp(curr)[4];  \
coefV[1]=s.velComp(curr-Vec3i::Unit(1))[4]; \
coefV[2]=s.velComp(curr)[2];  \
coefV[3]=s.velComp(curr-Vec3i::Unit(2))[2]; \
if(coefV[0] >= 0 && coefV[1] >= 0 && coefV[2] >= 0 && coefV[3] >= 0)  \
curl.push_back(CurlElem(coefV,Vec2d(invCellSz[1],-invCellSz[2])));}
#define BUILD_CURLX_CELL(CELL)curr=CELL;BUILD_CURLX
#define BUILD_CURLY {\
coefV[0]=s.velComp(curr)[0];  \
coefV[1]=s.velComp(curr-Vec3i::Unit(2))[0]; \
coefV[2]=s.velComp(curr)[4];  \
coefV[3]=s.velComp(curr-Vec3i::Unit(0))[4]; \
if(coefV[0] >= 0 && coefV[1] >= 0 && coefV[2] >= 0 && coefV[3] >= 0)  \
curl.push_back(CurlElem(coefV,Vec2d(invCellSz[2],-invCellSz[0])));}
#define BUILD_CURLY_CELL(CELL)curr=CELL;BUILD_CURLY
#define BUILD_CURLZ { \
coefV[0]=s.velComp(curr)[2];  \
coefV[1]=s.velComp(curr-Vec3i::Unit(0))[2]; \
coefV[2]=s.velComp(curr)[0];  \
coefV[3]=s.velComp(curr-Vec3i::Unit(1))[0]; \
if(coefV[0] >= 0 && coefV[1] >= 0 && coefV[2] >= 0 && coefV[3] >= 0)  \
curl.push_back(CurlElem(coefV,Vec2d(invCellSz[0],-invCellSz[1])));}
#define BUILD_CURLZ_CELL(CELL)curr=CELL;BUILD_CURLZ
PRJ_BEGIN
class SmoothLHS : public Traits, public KrylovMatrix<scalarD>
{
public:
  //using Traits::Vec;
  SmoothLHS(const SMat& div):_div(div) {}
  void multiply(const Vec& b,Vec& out) const {
#define PRIMAL(ID,VEC) VEC.block((ID)*_div.cols(),0,_div.cols(),1)
#define DUAL(ID,VEC) VEC.block(_primalBlk*_div.cols()+(ID)*_div.rows(),0,_div.rows(),1)
    for(sizeType i=0; i<_primalBlk; i++) {
      PRIMAL(i,out)=PRIMAL(i,b)*_diag[i];
      if(i > 0) {
        PRIMAL(0,out)+=_DAdvDv[i-1].transpose()*PRIMAL(i,b);
        PRIMAL(i,out)+=_DAdvDv[i-1]*PRIMAL(0,b);
      }
      DUAL(i,out)=_div*PRIMAL(i,b);
      PRIMAL(i,out)+=_div.transpose()*DUAL(i,b);
    }
#undef PRIMAL
#undef DUAL
  }
  sizeType n() const {
    return (_div.rows()+_div.cols())*_primalBlk;
  }
  const SMat& _div;
  SMat _DAdvDv[2];
  scalarD _diag[3];
  sizeType _primalBlk;
};
struct SolverStruct : public Traits {
  Eigen::SparseLU<SMat> _lu;
  vector<sizeType> _remap;
  sizeType _nrRemap;
  TRIPS _trips;
};
PRJ_END

//FluidSolver
FluidSolver::FluidSolver(const boost::property_tree::ptree& pt):_pt(pt),_adv(pt) {}
void FluidSolver::reset(const FluidState& s)
{
  //build divergence operator
  _solDivFree.reset(new SolverStruct);
  buildDivergenceFree(s,_solDivFree->_trips,false);
  _div.resize(s.nrCell(),s.nrVelComp());
  _div.setFromTriplets(_solDivFree->_trips.begin(),_solDivFree->_trips.end());
  //build laplacian operator
  buildDivergenceFree(s,_solDivFree->_trips);
  //build advector
  _AVCoef=buildAdvStencil(s);
  //smoothing operator
  _smoothLHS.reset(new SmoothLHS(_div));
  _adv.reset(s);
}
void FluidSolver::solve(FluidState& from,FluidState& to)
{
  //lazy initialize
  if(_AVCoef.getVector().empty())
    FluidSolver::reset(from);
  TIME_INTEGRATOR mode=(TIME_INTEGRATOR)_pt.get<sizeType>("timeIntegrator",BACKWARD);
  scalar dt=_pt.get<scalar>("dt",0.0f);
  scalar coefRhs=mode == MIDPOINT ? 2 : 1;
  scalar coefTensor=mode == MIDPOINT ? 0.25f*dt : dt;
  //Crank-Nicolson scheme:
  //v+Adv[(v0+v)/2]*dt+D^T*P=v0+[ext_force]*dt  s.t. D*v=0
  //We add both side by v0, and let V=(v+v0), we get:
  //V+Adv[V]*(dt/4)+D^T*P=2*v0+[ext_force]*dt  s.t. D*V=D*v0
  //by Taylor expansion, we have:
  //(i.e. V+Adv[V']*(dt/4)+DAdvDt*(V-V')*(dt/4)+D^T*P=2*v0+[ext_force]*dt)
  //so that we have rhs: 2*v0+[ext_force]*dt-Adv[V']*(dt/4)+DAdvDt*V'*(dt/4) and lhs: I+DAdvDt*(dt/4)
  //
  //Backward-Euler scheme:
  //v+Adv[v]*dt+D^T*P=v0+[ext_force]*dt  s.t. D*v=0
  //by Taylor expansion, we have:
  //(i.e. v+Adv[v']*dt+DAdvDt*(v-v')*dt+D^T*P=v0+[ext_force]*dt)
  //so that we have rhs: v0+[ext_force]*dt-Adv[v']*dt+DAdvDt*v'*dt and lhs: I+DAdvDt*dt
  TRIPS trips;
  sizeType nrC=from.nrCell();
  sizeType nrV=from.nrVelComp();
  //right hand side
  VecD rhs=VecD::Zero(nrC+nrV+1),rhsTmp;
  from.toVec(FluidState::VELOCITY,rhsTmp);
  rhs.block(0,0,nrV,1)=rhsTmp*coefRhs;
  if(coefRhs == 2)
    rhs.block(nrV,0,nrC,1)=_div*rhsTmp;
  VecD solution=rhs;
  if(from.velPtr(FluidState::GHOST_FORCE))
    from.toVec(FluidState::GHOST_FORCE,rhs,&add,0,dt);
  //main iteration
  sizeType it=0;
  for(; it<_pt.get<sizeType>("maxIterSolve",10000); it++) {
    //build system
    rhsTmp=rhs;
    trips=_solDivFree->_trips;
    for(Curl::const_iterator beg=_AVCoef.begin(),end=_AVCoef.end(); beg!=end; beg++)
      beg->evalDeriv(solution,coefTensor,&trips,&rhsTmp);
    //solve system
    VecD tmp=solution.block(0,0,nrV,1);
    solution=solve(from,trips,rhsTmp);
    //check relative error
    if(_pt.get<bool>("checkError",false))
      checkConvergence(from,solution,rhs,coefTensor,!_pt.get<bool>("quiet",false));
    scalarD delta=(solution.block(0,0,nrV,1)-tmp).norm();
    if(!_pt.get<bool>("quiet",false))
      INFOV("\tDelta: %f",delta)
      if(delta<_pt.get<scalar>("epsilon",1E-5f))
        break;
  }
  if(mode == MIDPOINT)
    from.toVec(FluidState::VELOCITY,solution,&sub);
  from.fromVecScal(FluidState::PRESSURE,solution,&set,nrV);
  to.fromVec(FluidState::VELOCITY,solution);
  if(!_pt.get<bool>("quiet",false))
    INFOV("\tIter: %ld, VNorm: %f",it,solution.block(0,0,nrV,1).norm())
  }
void FluidSolver::solveForce(FluidState& from,const FluidState& to,VecD* deriv,sizeType iFrom,sizeType iTo)
{
  //lazy initialize
  if(_AVCoef.getVector().empty())
    FluidSolver::reset(from);
  TIME_INTEGRATOR mode=(TIME_INTEGRATOR)_pt.get<sizeType>("timeIntegrator",BACKWARD);
  scalar dt=_pt.get<scalar>("dt",0.0f);
  scalar coefTensor=mode == MIDPOINT ? 0.25f : 1;

  VecD tmp;
  to.toVec(FluidState::VELOCITY,tmp);
  VecD force=tmp;
  from.toVec(FluidState::VELOCITY,force,&sub);
  if(mode == MIDPOINT)
    tmp=2*tmp-force;

  //compute ghost force
  force/=dt;
  for(Curl::const_iterator beg=_AVCoef.begin(),end=_AVCoef.end(); beg!=end; beg++)
    beg->eval(tmp,force,coefTensor);
  if(_pt.get<bool>("useExactProjector",false))
    FluidSolver::enforceDivFree(from,FluidState::VELOCITY,&force,FluidState::PRESSURE);
  else enforceDivFree(from,FluidState::VELOCITY,&force,FluidState::PRESSURE);
  from.fromVec(FluidState::GHOST_FORCE,force);

  if(deriv) {
    //compute derivative ghost force against vTo
    Eigen::Block<VecD> blk=deriv->block(iTo,0,iTo-iFrom,1);
    blk+=force/dt;
    for(Curl::const_iterator beg=_AVCoef.begin(),end=_AVCoef.end(); beg!=end; beg++)
      beg->evalDerivT(tmp,force,coefTensor,blk);
  }
  if(deriv) {
    //compute derivative ghost force against vFrom
    Eigen::Block<VecD> blk=deriv->block(iFrom,0,iTo-iFrom,1);
    blk-=force/dt;
    if(mode == MIDPOINT) {
      for(Curl::const_iterator beg=_AVCoef.begin(),end=_AVCoef.end(); beg!=end; beg++)
        beg->evalDerivT(tmp,force,coefTensor,blk);
    }
  }
}
bool FluidSolver::enforceDivFree(FluidState& from,FluidState::FIELD f,VecD* v,FluidState::FIELD fp)
{
  //lazy initialize
  if(_AVCoef.getVector().empty())
    FluidSolver::reset(from);
  sizeType nrC=from.nrCell();
  sizeType nrV=from.nrVelComp();
  VecD rhs=VecD::Zero(nrV+nrC+1);

  if(v) {
    rhs.block(0,0,nrV,1)=*v;
    rhs=solve(from,*_solDivFree,rhs);
    *v=rhs.block(0,0,nrV,1);
  } else {
    ASSERT_MSG(from.velPtr(f),"We cannot find vector field for projection!")
    from.toVec(f,rhs);
    rhs=solve(from,*_solDivFree,rhs);
    from.fromVec(f,rhs);
  }
  //set pressure
  if(fp >= 0) {
    ASSERT_MSGV(from.scalPtr(fp),"We cannot find field %d for projection!",fp)
    from.fromVecScal(fp,rhs,&set,nrV,-1);
  }
  return true;
}
scalarD FluidSolver::solveSmooth(FluidState* last,FluidState& curr,FluidState* next,SMOOTHST_TYPE type)
{
  //average pressure fields
  sizeType nrC=_div.rows(),nrV=_div.cols();
  {
    ScalarField& s=curr.scalNonConst(FluidState::GHOST_FORCE_PRESSURE);
    s.sub(s.sum()/nrC);
  }
  if(last) {
    ScalarField& s=last->scalNonConst(FluidState::PRESSURE);
    s.sub(s.sum()/nrC);
  }
  if(next) {
    ScalarField& s=curr.scalNonConst(FluidState::PRESSURE);
    s.sub(s.sum()/nrC);
  }

  //build system
  VecD rhs,x,x0;
  buildSmooth(last,curr,next,rhs,x);

  //solve system and update velocity
  x0=x;
  PMINRESSolverQLP<scalarD> sol;
  sol.setKrylovMatrix(_smoothLHS);
  sol.setSolverParameters(1E-8f,10000);
  ASSERT(sol.solve(rhs,x) == PMINRESSolverQLP<scalarD>::SUCCESSFUL);
  //INFOV("PMinres solver took %ld iterations, ||delta0-delta||=%f!",sol.getIterationsCount(),(delta-delta0).norm())

  //assign
  sizeType offPrimal=0;
  sizeType offDual=nrV*_smoothLHS->_primalBlk;
  {
    curr.fromVec(FluidState::VELOCITY,x,&set,offPrimal);
    curr.fromVecScal(FluidState::GHOST_FORCE_PRESSURE,x,&set,offDual);
    offPrimal+=nrV;
    offDual+=nrC;
  }
  if(last) {
    last->fromVec(FluidState::GHOST_FORCE,x,&set,offPrimal);
    last->fromVecScal(FluidState::PRESSURE,x,&set,offDual);
    offPrimal+=nrV;
    offDual+=nrC;
  }
  if(next) {
    curr.fromVec(FluidState::GHOST_FORCE,x,&set,offPrimal);
    curr.fromVecScal(FluidState::PRESSURE,x,&set,offDual);
    offPrimal+=nrV;
    offDual+=nrC;
  }
  ASSERT(offDual == x.size())
  return (x0-x).norm();
}
void FluidSolver::buildSmooth(const FluidState* last,FluidState& curr,const FluidState* next,VecD& rhs,VecD& x)
{
  TIME_INTEGRATOR mode=(TIME_INTEGRATOR)_pt.get<sizeType>("timeIntegrator",BACKWARD);
  scalar dt=_pt.get<scalar>("dt",0.0f);
  scalar coefTensor=mode == MIDPOINT ? 0.25f : 1;
  scalarD regK=_pt.get<scalar>("regK",1000.0f);
  scalarD regU=_pt.get<scalar>("regU",1000.0f);
  sizeType nrC=_div.rows(),nrV=_div.cols();
  //decide problem size
  TRIPS trips;
  //build LHS diag
  _smoothLHS->_diag[0]=regK/regU;
  _smoothLHS->_primalBlk=1;
  if(last)
    _smoothLHS->_diag[_smoothLHS->_primalBlk++]=-1;
  if(next)
    _smoothLHS->_diag[_smoothLHS->_primalBlk++]=-1;
  //initialize RHS block0
  rhs=VecD::Zero(_smoothLHS->n()),x=rhs;
  if(curr.velPtr(FluidState::VELOCITY_REFERENCE))
    curr.toVec(FluidState::VELOCITY_REFERENCE,rhs,&add,0,regK/regU);
  //build LHS,RHS primal blocks
  sizeType offPrimal=nrV;
  SMat* offBlk=_smoothLHS->_DAdvDv;
  for(sizeType neigh=1; neigh>=-1; neigh-=2) {
    if(neigh == -1 && !next)
      continue;
    if(neigh == 1 && !last)
      continue;
    const FluidState& A=(neigh == 1) ? *last : curr;
    const FluidState& B=(neigh == 1) ? curr : *next;
    B.toVec(FluidState::VELOCITY,x);
    A.toVec(FluidState::VELOCITY,x,&set,offPrimal);
    //build offdiag part
    trips.clear();
    for(sizeType d=0; d<curr.nrVelComp(); d++)
      trips.push_back(Eigen::Triplet<scalarD,sizeType>(d,d,neigh/dt));
    if(mode == MIDPOINT)
      x.block(0,0,nrV,1)+=x.block(offPrimal,0,nrV,1);
    bool specialStencil=(neigh == 1 || mode == MIDPOINT);
    for(Curl::const_iterator beg=_AVCoef.begin(),end=_AVCoef.end(); beg!=end; beg++)
      beg->evalDeriv(x,specialStencil ? coefTensor : -coefTensor,specialStencil ? &trips : NULL,&rhs,offPrimal);
    offBlk->resize(nrV,nrV);
    offBlk->setFromTriplets(trips.begin(),trips.end());
    //build rhs
    if(&A == &curr)
      B.toVec(FluidState::VELOCITY,x,&set,offPrimal);
    else A.toVec(FluidState::VELOCITY,x,&set,offPrimal);
    if(A.velPtr(FluidState::RHS_GHOST_FORCE))
      A.toVec(FluidState::RHS_GHOST_FORCE,rhs,&add,offPrimal,1);
    if(mode == MIDPOINT)
      rhs.block(offPrimal,0,nrV,1)+=x.block(offPrimal,0,nrV,1)*(2*neigh/dt)-(*offBlk)*x.block(offPrimal,0,nrV,1);
    else rhs.block(offPrimal,0,nrV,1)+=x.block(offPrimal,0,nrV,1)*(neigh/dt);
    offPrimal+=nrV;
    offBlk++;
  }
  ASSERT(offPrimal == _smoothLHS->_primalBlk*nrV)
  //initialize LHS
  offPrimal=0;
  sizeType offDual=nrV*_smoothLHS->_primalBlk;
  {
    if(!curr.scalPtr(FluidState::GHOST_FORCE_PRESSURE))
      curr.addField(FluidState::GHOST_FORCE_PRESSURE,0);
    curr.toVec(FluidState::VELOCITY,x,&set,offPrimal);
    curr.toVecScal(FluidState::GHOST_FORCE_PRESSURE,x,&set,offDual);
    if(curr.scalPtr(FluidState::RHS_GHOST_FORCE_PRESSURE))
      curr.toVecScal(FluidState::RHS_GHOST_FORCE_PRESSURE,rhs,&add,offDual,1);
    offPrimal+=nrV;
    offDual+=nrC;
  }
  if(last) {
    last->toVec(FluidState::GHOST_FORCE,x,&set,offPrimal);
    last->toVecScal(FluidState::PRESSURE,x,&set,offDual);
    if(last->scalPtr(FluidState::RHS_PRESSURE))
      last->toVecScal(FluidState::RHS_PRESSURE,rhs,&add,offDual,1);
    offPrimal+=nrV;
    offDual+=nrC;
  }
  if(next) {
    curr.toVec(FluidState::GHOST_FORCE,x,&set,offPrimal);
    curr.toVecScal(FluidState::PRESSURE,x,&set,offDual);
    if(curr.scalPtr(FluidState::RHS_PRESSURE))
      curr.toVecScal(FluidState::RHS_PRESSURE,rhs,&add,offDual,1);
    offPrimal+=nrV;
    offDual+=nrC;
  }
  ASSERT(offDual == x.size())
}
const KrylovMatrix<scalarD,Kernel<scalarD> >& FluidSolver::smoothLHS() const
{
  return *_smoothLHS;
}
const Advector& FluidSolver::advector() const
{
  return _adv;
}
Traits::Curl FluidSolver::buildAdvStencil(const FluidState& s)
{
  Curl curl;
  Vec4i coefV;
  const ScalarField& pre=s.scal(FluidState::PRESSURE);
  const Vec3d invCellSz=pre.getInvCellSize().cast<scalarD>()*0.25f;
  ITERSP_ALL(pre,OMP_PRI(coefV))
  BUILD_CURLX
  BUILD_CURLY
  BUILD_CURLZ
  ITERSPEND
  return curl;
}
Traits::Curl FluidSolver::buildAdvStencil(const FluidState& s,const Vec3i& cell)
{
  Vec3i curr;
  Curl curl;
  Vec4i coefV;
  const ScalarField& pre=s.scal(FluidState::PRESSURE);
  const Vec3d invCellSz=pre.getInvCellSize().cast<scalarD>()*0.25f;
  BUILD_CURLX_CELL(cell)
  BUILD_CURLX_CELL(cell+Vec3i(0,1,0))
  BUILD_CURLX_CELL(cell+Vec3i(0,0,1))
  BUILD_CURLX_CELL(cell+Vec3i(0,1,1))
  BUILD_CURLY_CELL(cell)
  BUILD_CURLY_CELL(cell+Vec3i(1,0,0))
  BUILD_CURLY_CELL(cell+Vec3i(0,0,1))
  BUILD_CURLY_CELL(cell+Vec3i(1,0,1))
  BUILD_CURLZ_CELL(cell)
  BUILD_CURLZ_CELL(cell+Vec3i(1,0,0))
  BUILD_CURLZ_CELL(cell+Vec3i(0,1,0))
  BUILD_CURLZ_CELL(cell+Vec3i(1,1,0))
  return curl;
}
FluidSolver::VecD FluidSolver::solve(const FluidState& s,TRIPS& trips,const VecD& rhs)
{
  SolverStruct sol;
  sol._trips=trips;
  //check if rhs.size() < maxover(i,max(trips[i].row(),trips[i].col()))
  //this can happen if we have additional sum of pressure constraints
  sizeType maxId=0;
  for(TRIPS::iterator beg=sol._trips.begin(),end=sol._trips.end(); beg!=end; beg++)
    maxId=max(max(maxId,beg->row()),beg->col());
  maxId++;
  ASSERT(maxId == rhs.size() || maxId == rhs.size()+1)
  //if so extend rhs
  VecD rhsExtend=VecD::Zero(maxId);
  rhsExtend.block(0,0,rhs.size(),1)=rhs;
  return solve(s,sol,rhsExtend).block(0,0,rhs.size(),1);
}
FluidSolver::VecD FluidSolver::solve(const FluidState& s,SolverStruct& sol,const VecD& rhs)
{
  //we add this method in case some solvers one use is not rank-revealing, e.g. Umfpack
  //build solver
  sizeType nrV=s.nrVelComp();
  if(sol._remap.empty()) {
    sol._nrRemap=0;
    sol._remap.resize(rhs.size());
    for(sizeType i=0; i<rhs.size(); i++) {
      if(i < nrV && s.isBoundary(i))
        sol._remap[i]=-1;
      else sol._remap[i]=sol._nrRemap++;
    }

    TRIPS remappedTrips;
    for(TRIPS::iterator beg=sol._trips.begin(),end=sol._trips.end(); beg!=end; beg++) {
      ASSERT(beg->row() < (sizeType)sol._remap.size() && sol._remap[beg->row()] >= 0)
      ASSERT(beg->col() < (sizeType)sol._remap.size() && sol._remap[beg->col()] >= 0)
      remappedTrips.push_back(Eigen::Triplet<scalarD,sizeType>(sol._remap[beg->row()],sol._remap[beg->col()],beg->value()));
    }

    SMat m;
    m.resize((int)sol._nrRemap,(int)sol._nrRemap);
    m.setFromTriplets(remappedTrips.begin(),remappedTrips.end());
    sol._lu.compute(m);
    ASSERT(sol._lu.info() == Eigen::Success)
  }

  VecD rhsRemap=VecD::Zero(sol._nrRemap);
  for(sizeType i=0; i<rhs.size(); i++)
    if(sol._remap[i] != -1)
      rhsRemap[sol._remap[i]]=rhs[i];

  //solve
  VecD xRemap=sol._lu.solve(rhsRemap);
  ASSERT(sol._lu.info() == Eigen::Success)

  //map back
  VecD x=VecD::Zero(rhs.size());
  for(sizeType i=0; i<rhs.size(); i++)
    if(sol._remap[i] != -1)
      x[i]=xRemap[sol._remap[i]];
  return x;
}
scalarD FluidSolver::checkConvergence(const FluidState& from,const VecD& lhs,VecD rhs,scalar coefTensor,bool print)
{
  //build tensor
  Curl AVCoef=buildAdvStencil(from);
  //build divergence
  sizeType nrC=from.nrCell();
  sizeType nrV=from.nrVelComp();
  SMat div;
  TRIPS trips;
  buildDivergenceFree(from,trips,false);
  div.resize((int)nrC,(int)nrV);
  div.setFromTriplets(trips.begin(),trips.end());
  //compute residual
  VecD V=lhs.block(0,0,nrV,1);
  VecD P=lhs.block(nrV,0,nrC,1);
  rhs.block(0,0,nrV,1)-=V+div.transpose()*P;
  for(Curl::const_iterator beg=AVCoef.begin(),end=AVCoef.end(); beg!=end; beg++)
    beg->eval(V,rhs,-coefTensor);
  rhs.block(nrV,0,nrC,1)-=div*V;
  //print
  scalarD ret=rhs.cwiseAbs().maxCoeff();
  if(print)
    INFOV("\tResidual=%f!",ret)
    return ret;
}
void FluidSolver::buildDivergenceFree(const FluidState& s,TRIPS& trips,bool KKT)
{
  trips.clear();
  Vec6 div=s.divStencil();
  sizeType nrV=KKT ? s.nrVelComp() : 0;
  sizeType nrC=s.scal(FluidState::PRESSURE).getNrPoint().prod();
  sizeType dim=s.scal(FluidState::PRESSURE).getDim();
  ITERSS_ALL(s.scal(FluidState::PRESSURE))
  Vec6i coef=s.velComp(curr);
  for(sizeType j=0; j<dim*2; j++)
    if(coef[j] >= 0) {
      //ASSERT(!s.isBoundary(coef[j]))
      trips.push_back(Eigen::Triplet<scalarD,sizeType>(offS+nrV,coef[j],div[j]));
      if(KKT)
        trips.push_back(Eigen::Triplet<scalarD,sizeType>(coef[j],offS+nrV,div[j]));
    }
  //add the sum of pressure constraint
  if(KKT) {
    trips.push_back(Eigen::Triplet<scalarD,sizeType>(nrV+nrC,offS+nrV,1));
    trips.push_back(Eigen::Triplet<scalarD,sizeType>(offS+nrV,nrV+nrC,1));
  }
  ITERSSEND
  if(KKT) {
    //add diagonal elements
    for(sizeType i=0; i<nrV; i++)
      if(!s.isBoundary(i))
        trips.push_back(Eigen::Triplet<scalarD,sizeType>(i,i,1));
  }
}

#undef BUILD_CURLX
#undef BUILD_CURLY
#undef BUILD_CURLZ
#undef BUILD_CURLX_COND
#undef BUILD_CURLY_COND
#undef BUILD_CURLZ_COND
