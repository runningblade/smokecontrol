#ifndef CELL_STENCIL_H
#define CELL_STENCIL_H

#include "FluidTraits.h"

PRJ_BEGIN

//velocity component reference
struct VelRef : public Traits {
  VelRef(sizeType velComp,sizeType cellOff);
  bool operator==(const VelRef& other) const;
  bool operator!=(const VelRef& other) const;
  sizeType _velComp,_cellOff;
};
//index into a certain grid cell
struct CellStencil;
struct QuadraticIndex {
  typedef MACVelocityField* MACVelocityFieldPtr;
  typedef const MACVelocityField* ConstMACVelocityFieldPtr;
  typedef ScalarField* ScalarFieldPtr;
  typedef const ScalarField* ConstScalarFieldPtr;
  friend struct CellStencil;
  friend struct QuadraticTerm;
  QuadraticIndex();
  QuadraticIndex(MACVelocityFieldPtr* v,sizeType id);
  QuadraticIndex(ScalarFieldPtr* p,sizeType id);
  QuadraticIndex(ConstMACVelocityFieldPtr* v,sizeType id);
  QuadraticIndex(ConstScalarFieldPtr* p,sizeType id);
  bool isVariable() const;
  void setVariable(sizeType varId);
  operator sizeType() const;
  bool operator<(const QuadraticIndex& other) const;
  bool operator==(const QuadraticIndex& other) const;
  bool operator!=(const QuadraticIndex& other) const;
  scalar* evalPtr(const Vec4i& offS,const CellStencil& stencil) const;
  scalar eval(const Vec4i& offS,const CellStencil& stencil) const;
private:
  ConstMACVelocityFieldPtr* _v;
  ConstScalarFieldPtr* _p;
  sizeType _id,_varId;
  //you can set this index as variable by assigning _varId >= 0
};
//terms that are quadratic in other variables
struct QuadraticTerm : public Traits {
  friend struct CellStencil;
  QuadraticTerm();
  QuadraticTerm(sizeType row,QuadraticIndex B,scalarD coef);
  QuadraticTerm(sizeType row,QuadraticIndex A,QuadraticIndex B,scalarD coef);
  bool isQuadratic() const;
  bool isLinear() const;
  bool isConstant() const;
  bool operator<(const QuadraticTerm& other) const;
  bool operator==(const QuadraticTerm& other) const;
  bool operator!=(const QuadraticTerm& other) const;
  void remap(const Vec4i& offS,const CellStencil& stencil);
  //helper
  static void prune(vector<QuadraticTerm>& terms);
  static void labelVariable(vector<QuadraticTerm>& terms,QuadraticIndex id);
  //dense matrix operators
  static void eval(const vector<QuadraticTerm>& terms,Matd& lhsB,VecD& rhsB,const VecD& xB);
  static scalarD evalLM(const vector<QuadraticTerm>& terms,Matd& lhsB,VecD& rhsB,const VecD& xB,bool JTJOnly=false);
  static void remap(const Vec4i& offS,const CellStencil& stencil,vector<QuadraticTerm>& terms,Matd& lhsB,VecD& rhsB);
  static void assemble(const Vec4i& offS,const CellStencil& stencil,const vector<QuadraticIndex>& variables,VecD& xB);
  static void assign(const Vec4i& offS,const CellStencil& stencil,const vector<QuadraticIndex>& variables,const VecD& xB);
  //banded matrix operators
  static void eval(const vector<QuadraticTerm>& terms,Banded& banded,VecDArr& rhsB,const VecDArr& xB);
  static scalarD evalLM(const vector<QuadraticTerm>& terms,Banded& banded,VecDArr& rhsB,const VecDArr& xB,bool JTJOnly=false);
  static void remap(const Vec4i& offS,const CellStencil& stencil,vector<QuadraticTerm>& terms,MatdArr& diag,MatdArr& offDiag,MatdArr& offDiag2,VecDArr& rhs);
  static void assemble(const Vec4i& offS,const CellStencil& stencil,const vector<QuadraticIndex>& variables,VecDArr& xB);
  static void assign(const Vec4i& offS,const CellStencil& stencil,const vector<QuadraticIndex>& variables,const VecDArr& xB);
  //solver
  static void writeProb(const string& path,const vector<QuadraticTerm>& terms,const Matd& lhsB,const VecD& rhsB,const VecD& xB);
  static void writeProb(const string& path,const vector<QuadraticTerm>& terms,const MatdArr& diag,const MatdArr& offDiag,const MatdArr& offDiag2,const VecDArr& rhsB,const VecDArr& xB);
  static bool readProb(const string& path,vector<QuadraticTerm>& terms,Matd& lhsB,VecD& rhsB,VecD& xB);
  static bool readProb(const string& path,vector<QuadraticTerm>& terms,MatdArr& diag,MatdArr& offDiag,MatdArr& offDiag2,VecDArr& rhsB,VecDArr& xB);
  static scalarD solveNewton(const vector<QuadraticTerm>& terms,const Matd& lhsB,const VecD& rhsB,VecD& xB);
  static scalarD solveLM(const vector<QuadraticTerm>& terms,const Matd& lhsB,const VecD& rhsB,VecD& xB,bool debug,bool write);
  static scalarD solveLM(const vector<QuadraticTerm>& terms,const MatdArr& diag,const MatdArr& offDiag,const MatdArr& offDiag2,const VecDArr& rhsB,VecDArr& xB,bool debug,bool write);
  static void solveProbLM(const string& path,bool bandedAsDense);
  //line searcher
  static VecD findPolyCoef(const vector<QuadraticTerm>& terms,const Matd& lhsB,const VecD& rhsB,const VecD& xB,const VecD& dxB);
  static VecD findPolyCoef(const vector<QuadraticTerm>& terms,const MatdArr& diag,const MatdArr& offDiag,const MatdArr& offDiag2,const VecDArr& rhsB,const VecDArr& xB,const VecDArr& dxB);
  static scalarD lineSearch(const VecD& coef,scalarD* minVal=NULL);
private:
  sizeType _row;
  QuadraticIndex _iA,_iB;
  scalarD _coef;
};
//information needed for multigrid applied to a grid at certain location
struct CellStencil : public Traits {
  CellStencil();
  void countRows();
  vector<VelRef> _velComps; //12 edge*4 face at most
  vector<CurlElem> _tensors;  //12 edge at most
  vector<sizeType> _otherCell;  //strides to neighboring cells, _otherCell[6]=0
  vector<sizeType> _offFace; //strides to neighboring faces
  //for spatial temporal smoothing
  Matd _lhs,_lhsST[4];
  sizeType _rowsST[4];
  //for spatial temporal smoothing QuadraticTerms
  vector<QuadraticTerm> _termST[4];
  vector<QuadraticIndex> _variableST[4];
  //for spatial temporal tridiag smoothing
  Matd _diagP,_diagD;
  Matd _offDiagLast,_offDiagNext;
  //for spatial temporal tridiag smoothing QuadraticTerms
  vector<QuadraticTerm> _termTridiagST;
  vector<QuadraticIndex> _variableTridiagST;
  sizeType _rowsTridiagST;
};
void debugGradHess(sizeType N,sizeType stride);
void debugSolveLM(sizeType N,sizeType stride);

PRJ_END

#endif
