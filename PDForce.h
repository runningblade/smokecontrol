#ifndef PD_FORCE_H
#define PD_FORCE_H

#include "FluidState.h"
#include <boost/property_tree/ptree_fwd.hpp>

PRJ_BEGIN

class SpaceTimeMesh;
class PDForce : public Traits
{
public:
  PDForce(scalar vf,scalar vg,scalar vd);
  bool setTarget(sizeType frame,const SpaceTimeMesh& mesh);
  void addForce(FluidState& from,const SpaceTimeMesh& mesh);
  void addGather(FluidState& to,const SpaceTimeMesh& mesh);
  void addDiffuse(FluidState& to,const SpaceTimeMesh& mesh);
  static scalarD gaussian(ScalarField& to,const ScalarField& from,const ScalarField* fromDiff,const SpaceTimeMesh& mesh,vector<scalar> stencil[3]);
  static void gaussianT(ScalarField& to,const ScalarField& from,const SpaceTimeMesh& mesh,OP op,vector<scalar> stencil[3]);
  static void buildGaussianStencil(vector<scalar> stencil[3],Vec3i& len,const ScalarField& rho,const boost::property_tree::ptree& pt);
private:
  scalar _vf,_vg,_vd;
  boost::shared_ptr<ScalarField> _rhoStar;
  ScalarField _rhoT,_rhoStarT;
  sizeType _keyFrameId;
};

PRJ_END

#endif
