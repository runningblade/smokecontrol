#ifndef FLUID_STATE_H
#define FLUID_STATE_H

#include "FluidMacros.h"
#include "CellStencil.h"

PRJ_BEGIN

struct CurlElem : public Traits {
  CurlElem() {}
  CurlElem(const Vec4i& coefV,const Vec2d& coefW):_coefV(coefV),_coefW(coefW) {}
  const sizeType& VID(sizeType i) const {
    return _coefV[i];
  }
  sizeType& VID(sizeType i) {
    return _coefV[i];
  }
  //for sparse matrix implementation
  template <typename VEC,typename VEC2>
  void eval(const VEC& v,VEC2& vout,scalarD coef=1) const {
    scalarD w=(v[_coefV[0]]-v[_coefV[1]])*_coefW[0]+(v[_coefV[2]]-v[_coefV[3]])*_coefW[1];
    scalarD advA=w*(v[_coefV[0]]+v[_coefV[1]])*coef;
    scalarD advB=w*(v[_coefV[2]]+v[_coefV[3]])*coef;
    vout[_coefV[0]]+=advB;
    vout[_coefV[1]]+=advB;
    vout[_coefV[2]]-=advA;
    vout[_coefV[3]]-=advA;
  }
  template <typename VEC,typename VEC2=VEC>
  void evalDeriv(const VEC& v,scalarD coef,TRIPS* trips,VEC2* vout,sizeType offRow=0) const {
    scalarD w=(v[_coefV[0]]-v[_coefV[1]])*_coefW[0]+(v[_coefV[2]]-v[_coefV[3]])*_coefW[1];
    scalarD advA=(v[_coefV[0]]+v[_coefV[1]])*coef;
    scalarD advB=(v[_coefV[2]]+v[_coefV[3]])*coef;
    if(vout) {
      (*vout)[_coefV[0]+offRow]+=w*advB;
      (*vout)[_coefV[1]+offRow]+=w*advB;
      (*vout)[_coefV[2]+offRow]-=w*advA;
      (*vout)[_coefV[3]+offRow]-=w*advA;
    }
    if(trips) {
      w*=coef;
      COEFA(0,ADDDERIVTRIP)
      COEFA(1,ADDDERIVTRIP)
      COEFB(2,ADDDERIVTRIP)
      COEFB(3,ADDDERIVTRIP)
    }
  }
  template <typename VEC,typename VEC2>
  void evalDerivT(const VEC& v,const VEC& rhs,scalarD coef,VEC2& vout) const {
    scalarD w=(v[_coefV[0]]-v[_coefV[1]])*_coefW[0]+(v[_coefV[2]]-v[_coefV[3]])*_coefW[1];
    scalarD advA=(v[_coefV[0]]+v[_coefV[1]])*coef;
    scalarD advB=(v[_coefV[2]]+v[_coefV[3]])*coef;
    w*=coef;
    COEFA(0,ADDDERIVT)
    COEFA(1,ADDDERIVT)
    COEFB(2,ADDDERIVT)
    COEFB(3,ADDDERIVT)
  }
  //for multigrid implementation
  template <typename VEC,typename MAT=Matd>
  void evalBlk(const Vec4i& offS,const vector<VelRef>& VRefs,const MACVelocityField& v,
               scalarD coef,VEC& rhsB,const MACVelocityField* vOther=NULL) const {
    evalSmoothBlk(offS,VRefs,v,coef,rhsB,(Matd*)NULL,true,vOther);
  }
  template <typename VEC,typename MAT=Matd>
  void evalSmoothBlk(const Vec4i& offS,const vector<VelRef>& VRefs,const MACVelocityField& v,
                     scalarD coef,VEC& rhsB,MAT* lhsB,bool addEval,const MACVelocityField* vOther=NULL) const {
    Vec4d vel=Vec4d::Zero();
    evalVel(offS,VRefs,v,vel);
    if(vOther)
      evalVel(offS,VRefs,*vOther,vel);
    scalarD w=(vel[0]-vel[1])*_coefW[0]+(vel[2]-vel[3])*_coefW[1];
    scalarD advA=(vel[0]+vel[1])*coef;
    scalarD advB=(vel[2]+vel[3])*coef;
    sizeType nrPrimal=v.getDim()*2;
    if(addEval) {
      if(_coefV[0] < nrPrimal)
        rhsB[_coefV[0]]+=w*advB;
      if(_coefV[1] < nrPrimal)
        rhsB[_coefV[1]]+=w*advB;
      if(_coefV[2] < nrPrimal)
        rhsB[_coefV[2]]-=w*advA;
      if(_coefV[3] < nrPrimal)
        rhsB[_coefV[3]]-=w*advA;
    }
    if(lhsB) {
      w*=coef;
      if(_coefV[0] < nrPrimal) {
        COEFA(0,ADDSMOOTHBLK)
      }
      if(_coefV[1] < nrPrimal) {
        COEFA(1,ADDSMOOTHBLK)
      }
      if(_coefV[2] < nrPrimal) {
        COEFB(2,ADDSMOOTHBLK)
      }
      if(_coefV[3] < nrPrimal) {
        COEFB(3,ADDSMOOTHBLK)
      }
    }
  }
  template <typename VEC>
  void evalDerivTBlk(const Vec4i& offS,const vector<VelRef>& VRefs,const MACVelocityField& v,const MACVelocityField& gf,
                     scalarD coef,VEC& rhsB,const MACVelocityField* vOther=NULL) const {
    Vec4d vel=Vec4d::Zero(),gFor=Vec4d::Zero();
    evalVel(offS,VRefs,v,vel);
    evalVel(offS,VRefs,gf,gFor);
    if(vOther)
      evalVel(offS,VRefs,*vOther,vel);
    scalarD w=(vel[0]-vel[1])*_coefW[0]+(vel[2]-vel[3])*_coefW[1];
    scalarD advA=(vel[0]+vel[1])*coef;
    scalarD advB=(vel[2]+vel[3])*coef;
    sizeType nrPrimal=v.getDim()*2;
    w*=coef;
    COEFA(0,ADDDERIVTBLK)
    COEFA(1,ADDDERIVTBLK)
    COEFB(2,ADDDERIVTBLK)
    COEFB(3,ADDDERIVTBLK)
  }
  template <typename VEC,typename MAT=Matd>
  void evalDerivKKTBlk(const Vec4i& offS,const vector<VelRef>& VRefs,const MACVelocityField& v,const MACVelocityField& gf,
                       scalarD coef,VEC& rhsBGF,VEC& rhsBV,MAT& lhsBGF,bool addEval,const MACVelocityField* vOther) const {
    //do the three things below:
    //add to primal block lhsB(offRow,0): DAdvDv
    //add to primal block lhsB(0,offRow): DAdvDv^T
    //add to primal block rhsB(0): DAdvDv^T*coef
    Vec4d vel=Vec4d::Zero(),vel0,gFor=Vec4d::Zero();
    evalVel(offS,VRefs,v,vel);
    vel0=vel;
    evalVel(offS,VRefs,gf,gFor);
    if(vOther)
      evalVel(offS,VRefs,*vOther,vel);
    scalarD val;
    scalarD w=(vel[0]-vel[1])*_coefW[0]+(vel[2]-vel[3])*_coefW[1];
    scalarD advA=(vel[0]+vel[1])*coef;
    scalarD advB=(vel[2]+vel[3])*coef;
    sizeType nrPrimal=v.getDim()*2;
    if(addEval) {
      if(_coefV[0] < nrPrimal)
        rhsBGF[_coefV[0]]-=w*advB;
      if(_coefV[1] < nrPrimal)
        rhsBGF[_coefV[1]]-=w*advB;
      if(_coefV[2] < nrPrimal)
        rhsBGF[_coefV[2]]+=w*advA;
      if(_coefV[3] < nrPrimal)
        rhsBGF[_coefV[3]]+=w*advA;
    }
    w*=coef;
    COEFA(0,ADDKKTBLK)
    COEFA(1,ADDKKTBLK)
    COEFB(2,ADDKKTBLK)
    COEFB(3,ADDKKTBLK)
  }
  //build quaratic terms
  void evalDerivKKTBlk(scalarD coef,sizeType nrPrimal,vector<QuadraticTerm>& terms,sizeType offRowGF,sizeType offRowV,
                       MACVelocityField** vCurr,MACVelocityField** vOther,MACVelocityField** gf);
private:
  void evalVel(const Vec4i& offS,const vector<VelRef>& VRefs,const MACVelocityField& v,Vec4d& vel) const;
  Vec4i _coefV;
  Vec2d _coefW;
};
struct FluidState : public Traits, public Serializable {
  enum FIELD {
    //scalar fields
    PRESSURE=1,
    RHS_PRESSURE=2,
    GHOST_FORCE_PRESSURE=4,
    RHS_GHOST_FORCE_PRESSURE=8,
    //velocity fields
    VELOCITY=16,
    RHS_VELOCITY=32,
    GHOST_FORCE=64,
    RHS_GHOST_FORCE=128,
    VELOCITY_REFERENCE=256,
    VELOCITY_LAMBDA=512,
    //MASK
    NO_FIELD=-1,
  };
  FluidState();
  FluidState(const FluidState& other);
  virtual ~FluidState();
  virtual FluidState& operator=(const FluidState& other);
  virtual boost::shared_ptr<Serializable> copy() const;
  virtual bool write(ostream& os,IOData* dat) const;
  virtual bool read(istream& is,IOData* dat);
  //setter
  virtual void init(Vec3i nr,Vec3 len,sizeType SF,sizeType VF,bool shadow=false,Vec3c periodic=Vec3c::Constant(false));
  virtual void addField(sizeType SF,sizeType VF,bool shadow=false);
  virtual void removeField(sizeType SF,sizeType VF);
  virtual void clearField();
  void writeVTK(const string& path,bool writeVel,bool writeRho) const;
  sizeType nrCell() const;
  sizeType nrVelComp() const;
  Vec6c isVelValid(Vec3i curr) const;
  Vec6i velComp(Vec3i curr) const;
  pair<Vec3i,sizeType> velComp(sizeType vid) const;
  bool isBoundary(sizeType vid) const;
  const Vec3c& getPeriodic() const;
  Vec3i getPeriodicS() const;
  Vec6 divStencil() const;
  Vec6 lapStencil() const;
  //velocity
  const MACVelocityField& vel(FIELD v) const;
  MACVelocityField& velNonConst(FIELD v);
  boost::shared_ptr<MACVelocityField>& velPtr(FIELD v);
  const boost::shared_ptr<MACVelocityField>& velPtr(FIELD v) const;
  //pre
  const ScalarField& scal(FIELD s) const;
  ScalarField& scalNonConst(FIELD s);
  boost::shared_ptr<ScalarField>& scalPtr(FIELD s);
  const boost::shared_ptr<ScalarField>& scalPtr(FIELD s) const;
  //helper
  void setZero(sizeType f=PRESSURE|VELOCITY);
  void limitCFL(FIELD f,scalar dt,scalar CFL);
  void toVec(FIELD f,VecD& ret,OPD op=&set,sizeType off=-1,scalarD coef=1) const;
  void fromVec(FIELD f,const VecD& vec,OP op=&set,sizeType off=-1,scalarD coef=1);
  void toVecScal(FIELD f,VecD& ret,OPD op=&set,sizeType off=-1,scalarD coef=1) const;
  void fromVecScal(FIELD f,const VecD& vec,OP op=&set,sizeType off=-1,scalarD coef=1);
  void save();
  void load();
  static sizeType search(const Vec6i& vid,sizeType id);
  //interpolate/restrict for multigrid
  void interpolateV(FluidState& finer,FIELD vCurr,FIELD vFiner,OP f,scalar coef=1) const;
  void interpolateS(FluidState& finer,FIELD sCurr,FIELD sFiner,OP f,scalar coef=1) const;
  void restrictV(const FluidState& finer,FIELD vCurr,FIELD vFiner,OP f,scalar coef=1);
  void restrictS(const FluidState& finer,FIELD sCurr,FIELD sFiner,OP f,scalar coef=1);
  //you can also add a density field
  boost::shared_ptr<ScalarField> _rho;
private:
  bool clampBoundary(Vec3i& curr) const;
  BitVector<boost::shared_ptr<ScalarField> > _pre,_preSave;
  BitVector<boost::shared_ptr<MACVelocityField> > _v,_vSave;
  Vec3c _periodic;
};
void debugInterp();
void debugBoundaryCondition();
void debugTridiagSolver();

PRJ_END

#endif
