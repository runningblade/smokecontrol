#ifdef OPENCV_SUPPORT
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#endif
#include "Articulated/ArticulatedBody.h"

#include "SpaceTimeMesh.h"
#include "FluidSolverSpatialMG.h"
#include "PDForce.h"
#include "CommonFile/GridOp.h"
#include "CommonFile/ObjMesh.h"
#include "CommonFile/MakeMesh.h"
#include "CommonFile/ImplicitFunc.h"
#include "CommonFile/geom/StaticGeomCell.h"

#include <boost/filesystem/operations.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/regex.hpp>

USE_PRJ_NAMESPACE

//Convenient function for constructing problem
SphereFunc::SphereFunc(const Vec3& ctr,scalar rad,scalar srad):_ctr(ctr),_rad(rad),_srad(srad)
{
  ASSERT(_srad > 0)
}
scalar SphereFunc::operator()(const Vec3& pos) const
{
  scalar dist=(pos-_ctr).norm()-_rad;
  if(dist <= 0)
    return 1;
  else if(dist < _srad)
    return interp1D<scalar>(1,0,dist/_srad);
  return 0;
}
CubeFunc::CubeFunc(const Vec3& ctr,const Vec3& ext,scalar srad):_bb(ctr-ext,ctr+ext),_srad(srad)
{
  ASSERT(_srad > 0)
}
CubeFunc::CubeFunc(const BBox<scalar>& bb,scalar srad):_bb(bb),_srad(srad)
{
  ASSERT(_srad > 0)
}
scalar CubeFunc::operator()(const Vec3& pos) const
{
  scalar dist=_bb.distTo(pos,_bb._minC[2] == _bb._maxC[2] ? 2 : 3);
  if(dist <= 0)
    return 1;
  else if(dist < _srad)
    return interp1D<scalar>(1,0,dist/_srad);
  return 0;
}
ConstantVelFunc::ConstantVelFunc(const ImplicitFunc<scalar>& func,const Vec3& vel)
  :_func(func),_vel(vel) {}
Vec3 ConstantVelFunc::operator()(const Vec3& pos) const
{
  if(_func(pos) > 0)
    return _vel;
  else return Vec3::Zero();
}
class TaylorVortexVel : public VelFunc<scalar>
{
public:
  TaylorVortexVel(const Vec3& ctr,scalar a,scalar sep,scalar coef)
    :_ctr0(ctr+Vec3::Unit(0)*sep),_ctr1(ctr-Vec3::Unit(0)*sep),_a(a),_coef(coef) {}
  Vec3 operator()(const Vec3& pos) const {
    scalar r,t;
    Vec3 vel0=(pos-_ctr0);
    vel0.z()=0.0f;
    r=vel0.squaredNorm()/(_a*_a);
    t=exp((1.0f-r)/2.0f)*_coef;
    vel0=Vec3(vel0.y(),-vel0.x(),0.0f)*t;
    if(vel0.norm() < 1E-10f)
      vel0.setZero();

    Vec3 vel1=(pos-_ctr1);
    vel1.z()=0.0f;
    r=vel1.squaredNorm()/(_a*_a);
    t=exp((1.0f-r)/2.0f)*_coef;
    vel1=Vec3(vel1.y(),-vel1.x(),0.0f)*t;
    if(vel1.norm() < 1E-10f)
      vel1.setZero();
    return vel0+vel1;
  }
  Vec3 _ctr0,_ctr1;
  scalar _a,_coef;
};
class TaylorVortexColor : public ImplicitFunc<scalar>
{
public:
  TaylorVortexColor(const Vec3& ctr,scalar a,scalar sep,scalar coef)
    :_ctr0(ctr+Vec3::Unit(0)*sep),_ctr1(ctr-Vec3::Unit(0)*sep),_a(a),_coef(coef) {}
  scalar operator()(const Vec3& pos) const {
    scalar r,t0,t1;
    Vec3 vel0=(pos-_ctr0);
    vel0.z()=0.0f;
    r=vel0.squaredNorm()/(_a*_a);
    t0=exp((1.0f-r)/2.0f)*_coef;

    Vec3 vel1=(pos-_ctr1);
    vel1.z()=0.0f;
    r=vel1.squaredNorm()/(_a*_a);
    t1=exp((1.0f-r)/2.0f)*_coef;
    return t0+t1;
  }
  Vec3 _ctr0,_ctr1;
  scalar _a,_coef;
};
//SpaceTimeMesh
SpaceTimeMesh::SpaceTimeMesh()
  :Serializable(typeid(SpaceTimeMesh).name()) {}
SpaceTimeMesh::SpaceTimeMesh(const SpaceTimeMesh& mesh)
  :Serializable(typeid(SpaceTimeMesh).name())
{
  operator=(mesh);
}
SpaceTimeMesh::SpaceTimeMesh(const boost::property_tree::ptree& pt,bool initialize)
  :Serializable(typeid(SpaceTimeMesh).name()),_pt(pt)
{
  //breakpoint continue
  string path,filename;
  if(!(path=_pt.get<string>("breakpoint","")).empty()) {
    IOData dat;
    filename=boost::filesystem::path(path).filename().string();
    //INFOV("Continue optimization from %s!",path.c_str())
    boost::match_results<string::const_iterator> what;
    boost::regex expRegex("(optimizedIter)([0-9]+)");
    if(boost::regex_match(filename,what,expRegex)) {
      _pt.put<sizeType>("fromIter",atoi(string(what[2].first,what[0].second).c_str())+1);
      path+="/mesh.dat";
      ASSERT_MSGV(boost::filesystem::exists(path),"Cannot find mesh file @ %s",path.c_str())
      boost::filesystem::ifstream is(path,ios::binary);
      read(is,&dat);
    }
  } else if(!initialize) {
    return;
  } else {
    //PD controller instead of OC controller
    scalar PDForceCoef=_pt.get<scalar>("PDVf",0.0f);
    scalar PDGatherCoef=_pt.get<scalar>("PDVg",0.0f);
    scalar PDDiffuseCoef=_pt.get<scalar>("PDVd",0.0f);
    if(PDForceCoef > 0) {
      _PDForce.reset(new PDForce(PDForceCoef,PDGatherCoef,PDDiffuseCoef));
      //we don't need to save frame if user want PD controller
      _pt.put("initialize.nrFrame",1);
    }

    //initialize
    boost::optional<boost::property_tree::ptree> child;
    if(child=_pt.get_child_optional("initialize")) {
      Vec3i nr(child.get().get<sizeType>("nrX",0),
               child.get().get<sizeType>("nrY",0),
               child.get().get<sizeType>("nrZ",0));
      Vec3 len(child.get().get<scalar>("lenX",0.0f),
               child.get().get<scalar>("lenY",0.0f),
               child.get().get<scalar>("lenZ",0.0f));
      sizeType N=child.get().get<sizeType>("nrFrame",0);
      ASSERT_MSG(nr[0] > 0 && nr[1] > 0 && len[0] > 0 && len[1] > 0 && N > 0,"Initial parameter error!")
      init(nr,len,N);

      //keyFrame setup
      TRANS T;
      for(boost::property_tree::ptree::const_iterator beg=_pt.begin(),end=_pt.end(); beg!=end; beg++)
        if(beginsWith(beg->first,"keyFrameSphere"))
          readKeyFrameSphere(beg->second);
        else if(beginsWith(beg->first,"keyFrameCube"))
          readKeyFrameCube(beg->second);
        else if(beginsWith(beg->first,"keyFrameImage"))
          readKeyFrameImage(beg->second);
        else if(beginsWith(beg->first,"keyFrameMesh"))
          readKeyFrameMesh(beg->second,T);
        else if(beginsWith(beg->first,"keyFrameArticulated"))
          readKeyFrameArticulated(beg->second,T);

      //check validity
      checkValidity();
    }

    //debug output
    path=_pt.get<string>("outputPath","./");
    if(_pt.get<bool>("writeFrame",true)) {
      if(nrFrame() > 0)
        writeKeyFrame(path,-1,*(state(0)._rho));
      for(KeyFrameMap::const_iterator beg=_keyFrame.begin(),end=_keyFrame.end(); beg!=end; beg++) {
        INFO("Writing keyframe!")
        writeKeyFrame(path,beg->first,*(beg->second));
      }
    }
  }
}
SpaceTimeMesh::SpaceTimeMesh(const SpaceTimeMesh& mesh,sizeType start,sizeType end)
  :Serializable(typeid(SpaceTimeMesh).name()),_pt(mesh._pt)
{
  for(sizeType i=start; i<end; i++)
    _state.push_back(mesh._state[i]);

  ASSERT_MSG(mesh._keyFrame.find(end-1) != mesh._keyFrame.end(),"We cannot find keyFrame to define subsegment!")
  _keyFrame[end-start-1]=mesh._keyFrame.find(end-1)->second;

  initSolver();
}
SpaceTimeMesh::~SpaceTimeMesh() {}
SpaceTimeMesh& SpaceTimeMesh::operator=(const SpaceTimeMesh& other)
{
  _pt=other._pt;
  _state.resize(other.nrFrame());
  for(sizeType i=0; i<(sizeType)other.nrFrame(); i++)
    _state[i].reset(new FluidState(other.state(i)));
  initSolver();
  return *this;
}
boost::shared_ptr<Serializable> SpaceTimeMesh::copy() const
{
  return boost::shared_ptr<Serializable>(new SpaceTimeMesh());
}
bool SpaceTimeMesh::write(ostream& os,IOData* dat) const
{
  dat->registerType<FluidState>();
  //state sequence
  writeVector(_state,os,dat);
  //advectionOrder",10000s
  sizeType nrKey=(sizeType)_keyFrame.size();
  writeBinaryData(nrKey,os);
  for(KeyFrameMap::const_iterator beg=_keyFrame.begin(),end=_keyFrame.end(); beg!=end; beg++) {
    writeBinaryData(beg->first,os);
    writeBinaryData(beg->second,os,dat);
  }
  return os.good();
}
bool SpaceTimeMesh::read(istream& is,IOData* dat)
{
  dat->registerType<FluidState>();
  //state sequence
  readVector(_state,is,dat);
  //keyFrames
  sizeType nrKey;
  readBinaryData(nrKey,is);
  _keyFrame.clear();
  for(sizeType i=0; i<nrKey; i++) {
    sizeType key;
    readBinaryData(key,is);
    readBinaryData(_keyFrame[key],is,dat);
  }
  if(nrFrame() > 0)
    initSolver();
  return is.good();
}
//getter
const boost::property_tree::ptree& SpaceTimeMesh::getPt() const
{
  return _pt;
}
boost::property_tree::ptree& SpaceTimeMesh::getPt()
{
  return _pt;
}
const ScalarField* SpaceTimeMesh::keyFrame(sizeType id) const
{
  KeyFrameMap::const_iterator it=_keyFrame.find(id);
  if(it == _keyFrame.end())
    return NULL;
  else return it->second.get();
}
const SpaceTimeMesh::KeyFrameMap& SpaceTimeMesh::keyFrameMap() const
{
  return _keyFrame;
}
boost::shared_ptr<FluidState> SpaceTimeMesh::statePtr(sizeType id,sizeType maxId)
{
  if(id < 0 || id >= maxId)
    return boost::shared_ptr<FluidState>();
  return _state[id];
}
const FluidState& SpaceTimeMesh::state(sizeType id) const
{
  return *(_state[id]);
}
FluidState& SpaceTimeMesh::state(sizeType id)
{
  return *(_state[id]);
}
sizeType SpaceTimeMesh::nrFrame() const
{
  return (sizeType)_state.size();
}
//setter
void SpaceTimeMesh::initSolver()
{
  sizeType mode=_pt.get<sizeType>("solverType",SPATIAL_SOLVE);
  if(mode == SPATIAL_SOLVE)
    _sol.reset(new FluidSolver(_pt));
  else if(mode == SPATIAL_MG_SOLVE)
    _sol.reset(new FluidSolverSpatialMG(_pt));
  //else ASSERT_MSG(false,"Unknown solving mode!")
  if(_sol)
    _sol->reset(*(_state[0]));
}
void SpaceTimeMesh::init(const Vec3i& nr,const Vec3& len,sizeType N)
{
  FluidState s;
  _state.clear();
  Vec3c periodic(_pt.get<bool>("periodicX",false),_pt.get<bool>("periodicY",false),_pt.get<bool>("periodicZ",false));
  s.init(nr,len,FluidState::PRESSURE,FluidState::VELOCITY,false,periodic);
  s._rho.reset(new ScalarField(s.scal(FluidState::PRESSURE)));
  for(sizeType i=0; i<N; i++)
    _state.push_back(boost::shared_ptr<FluidState>(new FluidState(s)));
  initSolver();
}
void SpaceTimeMesh::writeVTK(const string& path,sizeType start,sizeType end,bool vel,bool rho) const
{
  if(boost::filesystem::exists(path))
    boost::filesystem::remove_all(path);
  boost::filesystem::create_directory(path);
  if(start == -1)
    start=0;
  if(end == -1)
    end=nrFrame();
  for(sizeType i=start; i<end; i++)
    _state[i]->writeVTK(path+"/frm"+boost::lexical_cast<string>(i),vel,rho);
}
void SpaceTimeMesh::update()
{
  sizeType N=nrFrame();
  for(sizeType i=0; i<N-2; i++)
    _sol->solve(*(_state[i]),*(_state[i+1]));
}
void SpaceTimeMesh::updateAdvect(sizeType fromFrame)
{
  sizeType N=nrFrame();
  for(sizeType i=fromFrame == -1 ? 0 : fromFrame; i<N-1; i++) {
    _pt.put<sizeType>("currentFrameId",i);
    _sol->advector().advect(*(_state[i]->_rho),*(_state[i+1]->_rho),_state[i]->vel(FluidState::VELOCITY));
  }
}
void SpaceTimeMesh::updateAdvect(Advector::ADVECTOR adv,bool secondOrder)
{
  sizeType advRef=_pt.get<sizeType>("advector",Advector::SEMI_LAGRANGIAN);
  bool secondOrderRef=_pt.get<bool>("secondOrderAdvector",false);

  _pt.put<sizeType>("advector",adv);
  _pt.put<bool>("secondOrderAdvector",secondOrder);
  updateAdvect();
  _pt.put<sizeType>("advector",advRef);
  _pt.put<bool>("secondOrderAdvector",secondOrderRef);
}
void SpaceTimeMesh::setFrameTaylorVortex(scalar a,scalar sep,scalar coef)
{
  BBox<scalar> bb=_state[0]->scal(FluidState::PRESSURE).getBB();
  Vec3 ctr=(bb._minC+bb._maxC)/2;
  GridOp<scalar,scalar>::copyVelFromFunc(_state[0]->velNonConst(FluidState::VELOCITY),TaylorVortexVel(ctr,a,sep,coef));
  GridOp<scalar,scalar>::copyFromImplictFunc(*(_state[0]->_rho),TaylorVortexColor(ctr,a,sep,coef));
  _sol->enforceDivFree(*(_state[0]));
}
bool SpaceTimeMesh::setFrameAutoFitArea(sizeType fid,const string& path,Vec4i loc,const scalar* extMin,const scalar* extMax)
{
  //coarse tunning
  ASSERT(fid >= 0)
  scalar initSum=getTarget(-1)->sum();
  setFrame(fid,path,loc,-0.5f,extMin,extMax);
  ScalarField* target=getTarget(fid);
  //GridOp<scalar,scalar>::write3DScalarGridVTK("./grid.vtk",*target);
  for(sizeType it=0; initSum < target->sum(); it++) {
    if(it%2 == 0) {
      loc[0]++;
      loc[1]++;
    } else {
      loc[2]++;
      loc[3]++;
    }
    setFrame(fid,path,loc,-0.5f,extMin,extMax);
    if(target->getNrPoint()[0]-loc[2]-loc[0] <= 0 ||
       target->getNrPoint()[1]-loc[3]-loc[1] <= 0) {
      _keyFrame.erase(fid);
      return false;
    }
  }
  setFrame(fid,path,loc,+0.5f,extMin,extMax);
  for(sizeType it=0; initSum > target->sum(); it++) {
    if(it%2 == 0) {
      loc[0]--;
      loc[1]--;
    } else {
      loc[2]--;
      loc[3]--;
    }
    setFrame(fid,path,loc,+0.5f,extMin,extMax);
  }
  //fine tuning
  scalar a=-0.5f,b=0.5f,mid;
  setFrame(fid,path,loc,a,extMin,extMax);
  ASSERT_MSG(target->sum() < initSum,"Keyframe auto-fit error!")
  setFrame(fid,path,loc,b,extMin,extMax);
  ASSERT_MSG(target->sum() > initSum,"Keyframe auto-fit error!")
  while(abs(a-b) > 1E-4f) {
    mid=(a+b)/2;
    setFrame(fid,path,loc,mid,extMin,extMax);
    if(target->sum() < initSum)
      a=mid;
    else b=mid;
  }
  setFrame(fid,path,loc,mid,extMin,extMax);
  INFOV("Found target sum: %f, init sum: %f",target->sum(),initSum)
  return true;
}
bool SpaceTimeMesh::setFrameAutoFitArea(sizeType fid,const string& path,Vec3 marginMin,Vec3 marginMax)
{
  ASSERT(fid >= 0)
  scalar initSum=getTarget(-1)->sum();
  setFrame(fid,path,marginMin,marginMax,NULL,false);
  ScalarField* target=getTarget(fid);
  while(initSum < target->sum()) {
    marginMin+=Vec3::Ones();
    marginMax+=Vec3::Ones();
    setFrame(fid,path,marginMin,marginMax,NULL,false);
    if(target->getNrPoint()[0]-marginMax[0]-marginMin[0] <= 0 ||
       target->getNrPoint()[1]-marginMax[1]-marginMin[1] <= 0 ||
       target->getNrPoint()[2]-marginMax[2]-marginMin[2] <= 0) {
      _keyFrame.erase(fid);
      return false;
    }
  }
  while(initSum > target->sum()) {
    marginMin-=Vec3::Ones();
    marginMax-=Vec3::Ones();
    setFrame(fid,path,marginMin,marginMax,NULL,false);
  }
  scalar a=-1,b=1,mid;
  setFrame(fid,path,marginMin+Vec3::Constant(a),marginMax+Vec3::Constant(a),NULL,false);
  ASSERT_MSG(target->sum() > initSum,"Keyframe auto-fit error!")
  setFrame(fid,path,marginMin+Vec3::Constant(b),marginMax+Vec3::Constant(b),NULL,false);
  ASSERT_MSG(target->sum() < initSum,"Keyframe auto-fit error!")
  while(abs(a-b) > 1E-4f) {
    mid=(a+b)/2;
    setFrame(fid,path,marginMin+Vec3::Constant(mid),marginMax+Vec3::Constant(mid),NULL,false);
    if(target->sum() > initSum)
      a=mid;
    else b=mid;
  }
  setFrame(fid,path,marginMin+Vec3::Constant(mid),marginMax+Vec3::Constant(mid),NULL,false);
  INFOV("Found target sum: %f, init sum: %f",target->sum(),initSum)
  return true;
}
scalar distToDensity(scalar dist,scalar cellSz)
{
  dist=-dist+cellSz;
  return max<scalar>(min<scalar>(dist,cellSz),0);
}
SpaceTimeMesh::TRANS SpaceTimeMesh::setFrame(sizeType fid,const string& path,const Vec3& marginMin,const Vec3& marginMax,const TRANS* T,bool lazy)
{
  ScalarField* target=getTarget(fid);
  ASSERT_MSG(target->getDim() == 3,"You can only load obj mesh in 3D scenario!")
  //read mesh
  TRANS TRet=TRANS::Zero();
  ObjMesh mesh;
  {
    boost::filesystem::ifstream is(path);
    mesh.read(is,false,false,-1,-1,false);

    BBox<scalar> bb,bbMesh=mesh.getBB();
    {
      Vec3i marginMinF=floorV(marginMin);
      bb._minC=target->getPt(marginMinF);
      bb._minC.array()+=target->getCellSize().array()*(marginMin-marginMinF.cast<scalar>()).array();
    }
    {
      Vec3i marginMaxF=floorV(marginMax);
      bb._maxC=target->getPt(target->getNrPoint()-marginMaxF);
      bb._maxC.array()-=target->getCellSize().array()*(marginMax-marginMaxF.cast<scalar>()).array();
    }

    Vec3 scale=(bb.getExtent().array()/bbMesh.getExtent().array()).matrix();
    if(T) {
      mesh.getPos()=T->block<3,1>(0,0);
      mesh.getScale()=(*T)[6];
      TRet=*T;
      mesh.applyTrans(T->block<3,1>(3,0));
    } else {
      mesh.getPos()=TRet.block<3,1>(0,0)=(bb._minC+bb._maxC)/2-(bbMesh._minC+bbMesh._maxC)/2;
      mesh.getScale()=TRet[6]=scale.minCoeff();
      TRet.block<3,1>(3,0)=(bbMesh._minC+bbMesh._maxC)/2;
      mesh.applyTrans(TRet.block<3,1>(3,0));
    }
  }
  //we convert that mesh to density field but do that in a lazy manner
  //first check if one conversion already exists
  boost::filesystem::path ppath(path);
  ppath.replace_extension(".scal");
  if(lazy && boost::filesystem::exists(ppath)) {
    ScalarField scal;
    boost::filesystem::ifstream is(ppath,ios::binary);
    scal.read(is);

    ASSERT_MSG(target->getNrPoint() == scal.getNrPoint(),"target and scal datasize mismatch!")
    ITERSP(*target,Vec3i::Zero(),1)
    target->get(curr)=scal.get(curr);
    ITERSPEND
  } else {
    ObjMeshGeomCell cell(Mat4::Identity(),mesh,mesh.getBB().getExtent().norm(),false);
    ImplicitFuncMeshRef func(cell);
    ImplicitFuncReinit funcCached(target->getCellSize()[0],func,true);
    //assign
    target->init(0);
    ITERSS(*target,floorV(marginMin).cwiseMax(Vec3i::Zero()),1)
    target->get(offS)=distToDensity(funcCached(target->getPt(curr)),target->getCellSize()[0]);
    ITERSSEND
    scalar minV,maxV;
    target->minMax(minV,maxV);
    if(maxV < EPS)
      target->init(0);
    else target->mul(1/maxV);

    if(lazy) {
      boost::filesystem::ofstream os(ppath,ios::binary);
      target->write(os);
    }
  }
  //INFOV("Total density=%f",target->sum())
  return TRet;
}
void SpaceTimeMesh::setFrame(sizeType fid,const string& path,const Vec4i& loc,scalar frac,const scalar* extMin,const scalar* extMax)
{
#ifdef OPENCV_SUPPORT
  ScalarField* target=getTarget(fid);
  //ASSERT_MSG(target->getDim() == 2,"You can only load image in 2D scenario!")
  //read image
  cv::Mat imgRaw,img;
  imgRaw=cv::imread(path,cv::IMREAD_UNCHANGED);
  if(imgRaw.size[0] != 1024 || imgRaw.size[1] != 1024) {
    //rescale to avoid opencv bug
    cv::resize(imgRaw,img,cv::Size(1024,1024),0,0,cv::INTER_AREA);
    imgRaw=img;
  }
  int sz0=target->getNrPoint()[0]-loc[2]-loc[0];
  int sz1=target->getNrPoint()[1]-loc[3]-loc[1];
  frac=min<scalar>(max<scalar>(frac,-0.5f+EPS),0.5f-EPS);
  scalarD fx=((scalarD)sz0+frac)/imgRaw.size[0];
  scalarD fy=((scalarD)sz1+frac)/imgRaw.size[1];
  cv::resize(imgRaw,img,cv::Size(),fx,fy,cv::INTER_AREA);
  ASSERT(img.size[0] == sz0 && img.size[1] == sz1)
  //assign
  target->init(0);
  int c=img.channels();
  ITERSS(*target,Vec3i(loc[0],loc[1],0).cwiseMax(Vec3i::Zero()),1)
  int x=img.rows-1-(curr[1]-loc[1]);
  int y=(curr[0]-loc[0]);
  if(extMin && extMax) {
    if(target->getPt(curr)[2] < *extMin)
      continue;
    if(target->getPt(curr)[2] > *extMax)
      continue;
  }
  if(x < 0 || y < 0 || x >= img.rows || y >= img.cols)
    continue;
  else if(c == 4)
    target->get(offS)=img.at<cv::Vec4b>(x,y)[3];
  else if(c == 3)
    target->get(offS)=img.at<cv::Vec3b>(x,y)[0];
  else if(c == 1)
    target->get(offS)=img.at<unsigned char>(x,y);
  else ASSERT("Unknown image format!")
    ITERSSEND
    scalar minV,maxV;
  target->minMax(minV,maxV);
  if(maxV < EPS)
    target->init(0);
  else target->mul(1/maxV);
#else
  ASSERT_MSG(false,"Opencv not supported!")
#endif
}
void SpaceTimeMesh::setFrame(sizeType fid,const ImplicitFunc<scalar>& f,bool add)
{
  ScalarField* target=getTarget(fid);
  if(add) {
    ScalarField tmp=*target;
    GridOp<scalar,scalar>::copyFromImplictFunc(tmp,f);
    target->add(tmp);
  } else {
    GridOp<scalar,scalar>::copyFromImplictFunc(*target,f);
  }
}
void SpaceTimeMesh::setInitVel(const VelFunc<scalar>& f)
{
  MACVelocityField* v=_state[0]->velPtr(FluidState::VELOCITY).get();
  GridOp<scalar,scalar>::copyVelFromFunc(*v,f);
}
void SpaceTimeMesh::checkValidity() const
{
  bool hasFinal=false;
  for(KeyFrameMap::const_iterator beg=_keyFrame.begin(),end=_keyFrame.end(); beg!=end; beg++) {
    ASSERT_MSG(beg->first >= 0 && (beg->first < nrFrame() || _PDForce) && beg->second,"KeyFrame out of bound!")
    if(beg->first == nrFrame()-1)
      hasFinal=true;
  }
  ASSERT_MSG(hasFinal || _PDForce,"You must have a keyFrame at the end of the sequence!")
}
void SpaceTimeMesh::continueAnimation(const string& path,sizeType N)
{
  if(N <= 0) {
    for(KeyFrameMap::const_iterator beg=_keyFrame.begin(),end=_keyFrame.end(); beg!=end; beg++)
      N=max(N,beg->first);
    ASSERT_MSG(N > 0,"No to continue animation!")
  }

  boost::filesystem::create_directory(path);
  FluidState from=state(0),to=from;
  from._rho.reset(new ScalarField(*(state(0)._rho)));
  to._rho.reset(new ScalarField(*(state(0)._rho)));
  from.writeVTK(path+"/frm0",true,true);
  for(sizeType i=0; i<N; i++) {
    INFOV("At frame %ld",i+1)
    //advect
    _pt.put<sizeType>("currentFrameId",i+1);
    if(!_PDForce)
      _pt.put<bool>("secondOrderAdvector",true);
    _sol->advector().advect(*(from._rho),*(to._rho),from.vel(FluidState::VELOCITY));
    to.writeVTK(path+"/frm"+boost::lexical_cast<string>(i+1),true,true);
    //simulate
    if(_PDForce) {
      _PDForce->setTarget(i+1,*this);
      from.addField(0,FluidState::GHOST_FORCE);
      _PDForce->addForce(from,*this);
      _sol->solve(from,to);
      _PDForce->addGather(to,*this);
      _PDForce->addDiffuse(to,*this);
    } else if(i+1 < nrFrame() && state(i+1).velPtr(FluidState::VELOCITY)) {
      to.velNonConst(FluidState::VELOCITY)=state(i+1).vel(FluidState::VELOCITY);
      to.velPtr(FluidState::GHOST_FORCE).reset((MACVelocityField*)NULL);
    } else _sol->solve(from,to);
    swap(from,to);
  }
}
//private
ScalarField* SpaceTimeMesh::getTarget(sizeType fid)
{
  if(fid < 0)
    return _state[0]->_rho.get();
  else {
    ASSERT(fid < nrFrame() || _PDForce)
    if(_keyFrame.find(fid) == _keyFrame.end())
      _keyFrame[fid].reset(new ScalarField(*(_state[0]->_rho)));
    return _keyFrame[fid].get();
  }
}
void SpaceTimeMesh::writeKeyFrame(const string& path,sizeType fid,const ScalarField& f) const
{
  ostringstream oss;
  if(fid < 0)
    oss << path << "/initFrame.vtk";
  else oss << path << "/keyFrame" << fid << ".vtk";
  INFOV("Sum of density field %s=%f",oss.str().c_str(),f.sum())
  if(f.getDim() == 2)
    GridOp<scalar,scalar>::write2DScalarGridVTK(oss.str(),f,false);
  else GridOp<scalar,scalar>::write3DScalarGridVTK(oss.str(),f);
}
void SpaceTimeMesh::readKeyFrameSphere(const boost::property_tree::ptree& pt)
{
  sizeType fid=pt.get<sizeType>("frameId",-1);
  if(fid == 0)
    fid=-1;
  SphereFunc sphere(Vec3(pt.get<scalar>("ctrX",0.0f),
                         pt.get<scalar>("ctrY",0.0f),
                         pt.get<scalar>("ctrZ",0.0f)),
                    pt.get<scalar>("rad",0.0f),
                    pt.get<scalar>("smoothRad",0.0f));
  setFrame(fid,sphere,pt.get<bool>("add",false));
  if(fid == -1 && pt.get<bool>("setVelocity",false))
    setInitVel(ConstantVelFunc(sphere,Vec3(pt.get<scalar>("velX",0.0f),
                                           pt.get<scalar>("velY",0.0f),
                                           pt.get<scalar>("velZ",0.0f))));
}
void SpaceTimeMesh::readKeyFrameCube(const boost::property_tree::ptree& pt)
{

  sizeType fid=pt.get<sizeType>("frameId",-1);
  if(fid == 0)
    fid=-1;
  CubeFunc cube(Vec3(pt.get<scalar>("ctrX",0.0f),
                     pt.get<scalar>("ctrY",0.0f),
                     pt.get<scalar>("ctrZ",0.0f)),
                Vec3(pt.get<scalar>("extX",0.0f),
                     pt.get<scalar>("extY",0.0f),
                     pt.get<scalar>("extZ",0.0f)),
                pt.get<scalar>("smoothRad",0.0f));
  setFrame(fid,cube,pt.get<bool>("add",false));
  if(fid == -1 && pt.get<bool>("setVelocity",false))
    setInitVel(ConstantVelFunc(cube,Vec3(pt.get<scalar>("velX",0.0f),
                                         pt.get<scalar>("velY",0.0f),
                                         pt.get<scalar>("velZ",0.0f))));
}
void SpaceTimeMesh::readKeyFrameImage(const boost::property_tree::ptree& pt)
{
  INFO("Reading images!")
  sizeType fid=pt.get<sizeType>("frameId",-1);
  if(fid == 0)
    fid=-1;
  boost::optional<scalar> extMin=pt.get_optional<scalar>("extMin");
  boost::optional<scalar> extMax=pt.get_optional<scalar>("extMax");
  Vec4i loc(pt.get<sizeType>("marginXL",0),
            pt.get<sizeType>("marginYL",0),
            pt.get<sizeType>("marginXR",0),
            pt.get<sizeType>("marginYR",0));
  if(pt.get<bool>("autoFit",false))
    setFrameAutoFitArea(fid,pt.get<string>("imagePath"),loc,extMin.get_ptr(),extMax.get_ptr());
  else setFrame(fid,pt.get<string>("imagePath"),loc,0,extMin.get_ptr(),extMax.get_ptr());
}
void SpaceTimeMesh::readKeyFrameMesh(const boost::property_tree::ptree& pt,TRANS& T)
{
  sizeType fid=pt.get<sizeType>("frameId",-1);
  if(fid == 0)
    fid=-1;
  Vec3 locL(pt.get<scalar>("marginXL",0),
            pt.get<scalar>("marginYL",0),
            pt.get<scalar>("marginZL",0));
  Vec3 locR(pt.get<scalar>("marginXR",0),
            pt.get<scalar>("marginYR",0),
            pt.get<scalar>("marginZR",0));
  if(pt.get<bool>("autoFit",false))
    setFrameAutoFitArea(fid,pt.get<string>("meshPath"),locL,locR);
  else T=setFrame(fid,pt.get<string>("meshPath"),locL,locR,
                    pt.get<bool>("useTrans",false) ? &T : NULL,
                    pt.get<bool>("lazy",true));
}
void SpaceTimeMesh::readKeyFrameArticulated(const boost::property_tree::ptree& pt,TRANS& T)
{
  boost::property_tree::ptree child=pt;
  sizeType begId=pt.get<sizeType>("mocapFrameBeg",-1);
  sizeType endId=pt.get<sizeType>("mocapFrameEnd",-1);
  sizeType interval=pt.get<sizeType>("mocapInterval",-1);
  sizeType intervalFrm=pt.get<sizeType>("interval",-1);

  ObjMesh sphere;
  ArticulatedBody body;
  string path=pt.get<string>("outputPath","./mocapData");
  if(!boost::filesystem::exists(path)) {
    MakeMesh::makeSphere3D(sphere,pt.get<scalar>("jointRad",0.4f),pt.get<sizeType>("jointResolution",32));
    string pathASF=pt.get<string>("asfFile");
    INFOV("Reading %s",path.c_str())
    boost::filesystem::ifstream isASF(pathASF);
    body.readASF(isASF,sphere);

    string pathAMC=pt.get<string>("amcFile");
    INFOV("Reading %s",path.c_str())
    boost::filesystem::ifstream isAMC(pathAMC);
    body.readAMC(isAMC);

    INFO("Outputing frames")
    body.writeMocapVTK(path,begId,interval,pt.get<string>("fixJoint",""));
  }

  for(sizeType i=begId,j=0; i<=endId; i+=interval,j+=intervalFrm) {
    child.put<string>("meshPath",path+"/frm"+boost::lexical_cast<string>(i)+".obj");
    child.put<sizeType>("frameId",j == 0 ? -1 : j);
    child.put<bool>("useTrans",j > 0);
    INFOV("Setting frame: %ld from articulated frame %ld",j,i)
    readKeyFrameMesh(child,T);
  }
}
