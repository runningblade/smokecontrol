#include "Advector.h"
#include "FluidMacros.h"
#include <boost/lexical_cast.hpp>
#include <boost/property_tree/ptree.hpp>

USE_PRJ_NAMESPACE

//Advector
Advector::Advector(const boost::property_tree::ptree& pt):_pt(pt),_A(new ScalarField),_B(new ScalarField) {}
void Advector::reset(const FluidState& s)
{
  _periodic=s.getPeriodic();
  _periodicS=s.getPeriodicS();
}
void Advector::advect(const ScalarField& from,ScalarField& to,const MACVelocityField& v) const
{
  ADVECTOR adv=(ADVECTOR)_pt.get<sizeType>("advector",SEMI_LAGRANGIAN);
  if(adv == SEMI_LAGRANGIAN)
    advectSemiLagrangian(from,to,v);
  else if(adv == SEMI_LAGRANGIAN_CUBIC)
    advectSemiLagrangianCubic(from,to,v);
  else if(adv == EXPLICIT)
    advectExplicit(from,to,v);
  else if(adv == IMPLICIT)
    advectImplicit(from,to,v);
  else ASSERT_MSG(false,"Unknown Advector Type!")
  }
void Advector::advectT(ScalarField& lambda,const ScalarField& rho,const MACVelocityField& v) const
{
  ADVECTOR adv=(ADVECTOR)_pt.get<sizeType>("advector",SEMI_LAGRANGIAN);
  if(adv == SEMI_LAGRANGIAN)
    advectTSemiLagrangian(lambda,rho,v);
  else if(adv == SEMI_LAGRANGIAN_CUBIC)
    advectTSemiLagrangianCubic(lambda,rho,v);
  else if(adv == EXPLICIT)
    advectTExplicit(lambda,rho,v);
  else if(adv == IMPLICIT)
    advectTImplicit(lambda,rho,v);
  else ASSERT_MSG(false,"Unknown Advector Type!")
  }
void Advector::advectV(VecD& DFDX,const ScalarField& lambda,const ScalarField& rho,const MACVelocityField& v) const
{
  ADVECTOR adv=(ADVECTOR)_pt.get<sizeType>("advector",SEMI_LAGRANGIAN);
  if(adv == SEMI_LAGRANGIAN)
    advectVSemiLagrangian(DFDX,lambda,rho,v);
  else if(adv == SEMI_LAGRANGIAN_CUBIC)
    advectVSemiLagrangianCubic(DFDX,lambda,rho,v);
  else if(adv == EXPLICIT)
    advectVExplicit(DFDX,lambda,rho,v);
  else if(adv == IMPLICIT)
    advectVImplicit(DFDX,lambda,rho,v);
  else ASSERT_MSG(false,"Unknown Advector Type!")
  }
ScalarField& Advector::cacheA() const
{
  return *_A;
}
ScalarField& Advector::cacheB() const
{
  return *_B;
}
//our first version use semi-lagrangian, not a good choice
void Advector::advectSemiLagrangian(const ScalarField& from,ScalarField& to,const MACVelocityField& v) const
{
  Vec3 pos;
  scalar dt=_pt.get<scalar>("dt",0.0f);
  ITERSP_ALL(to,OMP_PRI(pos))
  pos=to.getPt(curr);
  to.get(offS)=from.sampleSafe(pos-v.sampleSafe(pos,&_periodic)*dt,&_periodicS);
  ITERSPEND
}
void Advector::advectTSemiLagrangian(ScalarField& lambda,const ScalarField& rho,const MACVelocityField& v) const
{
  Vec3 pos;
  sizeType pts[8]= {0,0,0,0,0,0,0,0};
  scalar coefs[8]= {0,0,0,0,0,0,0,0};
  scalar dt=_pt.get<scalar>("dt",0.0f);
  sizeType nrS=pow(2,lambda.getDim());

  ScalarField& A=cacheA();
  A=lambda;
  A.init(0);
  ITERSP_ALL(lambda,OMP_FPRI(pos,pts,coefs))
  pos=lambda.getPt(curr);
  rho.getSampleStencilSafe(pos-v.sampleSafe(pos,&_periodic)*dt,coefs,NULL,pts,&_periodicS);
  for(sizeType i=0; i<nrS; i++)
    add(A.get(pts[i]),lambda.get(offS)*coefs[i]);
  ITERSPEND
  lambda=A;
}
void Advector::advectVSemiLagrangian(VecD& DFDX,const ScalarField& lambda,const ScalarField& rho,const MACVelocityField& v) const
{
  Vec3 pos,grad;
  sizeType pts[8]= {0,0,0,0,0,0,0,0};
  scalar coefs[8]= {0,0,0,0,0,0,0,0};
  scalar dt=_pt.get<scalar>("dt",0.0f);
  sizeType dim=lambda.getDim();
  sizeType nrS=pow(2,lambda.getDim());
  Vec3i off=Vec3i::Zero();
  for(sizeType d=1; d<dim; d++)
    off[d]=off[d-1]+v.getComp(d-1).getNrPoint().prod();

  ITERSP_ALL(lambda,OMP_FPRI(pos,grad,pts,coefs))
  pos=lambda.getPt(curr);
  grad=rho.sampleSafeGrad(pos-v.sampleSafe(pos,&_periodic)*dt,&_periodicS);
  for(sizeType d=0; d<dim; d++) {
    v.getComp(d).getSampleStencilSafe(pos,coefs,NULL,pts,&_periodicS);
    for(sizeType i=0; i<nrS; i++)
      sub<scalarD>(DFDX[off[d]+pts[i]],grad[d]*coefs[i]*lambda[offS]*dt);
  }
  ITERSPEND
}
//our second version use MacCormack, more accurate
void Advector::advectSemiLagrangianCubic(const ScalarField& from,ScalarField& to,const MACVelocityField& v) const
{
  Vec3 pos;
  scalar dt=_pt.get<scalar>("dt",0.0f);
  ITERSP_ALL(to,OMP_PRI(pos))
  pos=to.getPt(curr);
  to.get(offS)=from.sampleSafeCubic(pos-v.sampleSafe(pos,&_periodic)*dt,&_periodicS);
  ITERSPEND
}
void Advector::advectTSemiLagrangianCubic(ScalarField& lambda,const ScalarField& rho,const MACVelocityField& v) const
{
  Vec3 pos;
  sizeType pts[4][4][4];
  scalar coefs[4][4][4];
  scalar dt=_pt.get<scalar>("dt",0.0f);
  sizeType nrS=pow(4,lambda.getDim());

  ScalarField& A=cacheA();
  A=lambda;
  A.init(0);
  ITERSP_ALL(lambda,OMP_FPRI(pos,pts,coefs))
  pos=lambda.getPt(curr);
  rho.getSampleStencilSafeCubic(pos-v.sampleSafe(pos,&_periodic)*dt,coefs,pts,&_periodicS);
  for(sizeType i=0; i<nrS; i++)
    add(A.get(pts[0][0][i]),lambda.get(offS)*coefs[0][0][i]);
  ITERSPEND
  lambda=A;
}
void Advector::advectVSemiLagrangianCubic(VecD& DFDX,const ScalarField& lambda,const ScalarField& rho,const MACVelocityField& v) const
{
  Vec3 pos,grad;
  sizeType pts[8]= {0,0,0,0,0,0,0,0};
  scalar coefs[8]= {0,0,0,0,0,0,0,0};
  scalar dt=_pt.get<scalar>("dt",0.0f);
  sizeType dim=lambda.getDim();
  sizeType nrS=pow(2,lambda.getDim());
  Vec3i off=Vec3i::Zero();
  for(sizeType d=1; d<dim; d++)
    off[d]=off[d-1]+v.getComp(d-1).getNrPoint().prod();

  ITERSP_ALL(lambda,OMP_FPRI(pos,grad,pts,coefs))
  pos=lambda.getPt(curr);
  grad=rho.sampleSafeGradCubic(pos-v.sampleSafe(pos,&_periodic)*dt,&_periodicS);
  for(sizeType d=0; d<dim; d++) {
    v.getComp(d).getSampleStencilSafe(pos,coefs,NULL,pts,&_periodicS);
    for(sizeType i=0; i<nrS; i++)
      sub<scalarD>(DFDX[off[d]+pts[i]],grad[d]*coefs[i]*lambda[offS]*dt);
  }
  ITERSPEND
}
//our next version, explicit first order advection operator
void Advector::advectExplicit(const ScalarField& from,ScalarField& to,const MACVelocityField& v) const
{
  to=from;
  scalar coef;
  scalar dt=_pt.get<scalar>("dt",0.0f);
  Vec3 STENCIL=getStencil(),invCellSzDt=from.getInvCellSize()*dt;

  for(sizeType d=0; d<from.getDim(); d++) {
    ITERSP_PERIODIC(from,_periodic,OMP_PRI(coef))
    if((coef=v.getComp(d).get(curr)*invCellSzDt[d]) > 0)
      coef*=from.getSafe(curr-Vec3i::Unit(d)*2,&_periodicS)*STENCIL[0]+
            from.get(curr-Vec3i::Unit(d),&_periodicS)*STENCIL[1]+
            from.get(curr)*STENCIL[2];
    else
      coef*=from.getSafe(curr+Vec3i::Unit(d),&_periodicS)*STENCIL[0]+
            from.get(curr)*STENCIL[1]+
            from.get(curr-Vec3i::Unit(d),&_periodicS)*STENCIL[2];
    add(to.get(curr),coef);
    sub(to.get(curr-Vec3i::Unit(d),&_periodicS),coef);
    ITERSPEND
  }
}
void Advector::advectTExplicit(ScalarField& lambda,const ScalarField& rho,const MACVelocityField& v) const
{
  //back propagate DEDRho
  scalar coef;
  scalar dt=_pt.get<scalar>("dt",0.0f);
  Vec3 STENCIL=getStencil(),invCellSzDt=lambda.getInvCellSize()*dt;

  ScalarField& A=cacheA();
  A=lambda;
  A.init(0);
  for(sizeType d=0; d<lambda.getDim(); d++)
    ITERSP_PERIODIC(lambda,_periodic,OMP_PRI(coef))
    scalar deltaDEDLambda=lambda.get(curr)-lambda.get(curr-Vec3i::Unit(d),&_periodicS);
  if((coef=v.getComp(d).get(curr)*invCellSzDt[d]) > 0) {
    add(A.getSafe(curr-Vec3i::Unit(d)*2,&_periodicS),coef*deltaDEDLambda*STENCIL[0]);
    add(A.get(curr-Vec3i::Unit(d),&_periodicS),coef*deltaDEDLambda*STENCIL[1]);
    add(A.get(curr),coef*deltaDEDLambda*STENCIL[2]);
  } else {
    add(A.getSafe(curr+Vec3i::Unit(d),&_periodicS),coef*deltaDEDLambda*STENCIL[0]);
    add(A.get(curr),coef*deltaDEDLambda*STENCIL[1]);
    add(A.get(curr-Vec3i::Unit(d),&_periodicS),coef*deltaDEDLambda*STENCIL[2]);
  }
  ITERSPEND
  lambda.add(A);
}
void Advector::advectVExplicit(VecD& DFDX,const ScalarField& lambda,const ScalarField& rho,const MACVelocityField& v) const
{
  scalar dt=_pt.get<scalar>("dt",0.0f);
  Vec3 STENCIL=getStencil(),invCellSzDt=rho.getInvCellSize()*dt;

  for(sizeType d=0,offV=0; d<rho.getDim(); offV+=v.getComp(d).getNrPoint().prod(),d++)
    ITERSP_PERIODIC(rho,_periodic)
    scalarD& grad=DFDX[offV+v.getComp(d).getIndex(curr)];
  scalar deltaDEDRho=lambda.get(curr)-lambda.get(curr-Vec3i::Unit(d),&_periodicS);
  if(v.getComp(d).get(curr) > 0) {
    add<scalarD>(grad,rho.getSafe(curr-Vec3i::Unit(d)*2,&_periodicS)*invCellSzDt[d]*deltaDEDRho*STENCIL[0]);
    add<scalarD>(grad,rho.get(curr-Vec3i::Unit(d),&_periodicS)*invCellSzDt[d]*deltaDEDRho*STENCIL[1]);
    add<scalarD>(grad,rho.get(curr)*invCellSzDt[d]*deltaDEDRho*STENCIL[2]);
  } else {
    add<scalarD>(grad,rho.getSafe(curr+Vec3i::Unit(d),&_periodicS)*invCellSzDt[d]*deltaDEDRho*STENCIL[0]);
    add<scalarD>(grad,rho.get(curr)*invCellSzDt[d]*deltaDEDRho*STENCIL[1]);
    add<scalarD>(grad,rho.get(curr-Vec3i::Unit(d),&_periodicS)*invCellSzDt[d]*deltaDEDRho*STENCIL[2]);
  }
  ITERSPEND
}
//our final version, high order truncated implicit method
void sumFact(vector<ScalarField>& F,sizeType off)
{
  for(sizeType i=off-1,fact=(sizeType)F.size(); i>=0; i--,fact--) {
    F[off].mul(1/(scalar)fact);
    F[off].add(F[i]);
  }
}
void Advector::advectImplicit(const ScalarField& from,ScalarField& to,const MACVelocityField& v) const
{
  scalar coef,dt=_pt.get<scalar>("dt",0.0f);
  sizeType order=max<sizeType>(1,_pt.get<sizeType>("advectionOrder",10000));
  Vec3 STENCIL=getStencil(),invCellSzDt=from.getInvCellSize()*dt,invCellSzDtFact;

  //check if user want variable order
  sizeType currFrameId=-1;
  boost::property_tree::ptree& pt=const_cast<boost::property_tree::ptree&>(_pt);
  scalar variableThres=pt.get<scalar>("variableAdvectionThres",1E-5f);
  if(variableThres > 0) {
    currFrameId=_pt.get<sizeType>("currentFrameId",-1);
    ASSERT_MSG(currFrameId >= 0,"currentFrameId not set, when trying to use variable order advector!")
    //clear to avoid confusion
    pt.erase("currentFrameId");
    ASSERT(pt.get<sizeType>("currentFrameId",-1) == -1)
  }

  ScalarField& A=cacheA();
  ScalarField& B=cacheB();
  A=B=to=from;
  sizeType it=0;
  for(; it<order; it++) {
    B.init(0);
    invCellSzDtFact=invCellSzDt/(scalar)(it+1);
    for(sizeType d=0; d<from.getDim(); d++)
      ITERSP_PERIODIC(from,_periodic,OMP_PRI(coef))
      if((coef=v.getComp(d).get(curr)*invCellSzDtFact[d]) > 0)
        coef*=A.getSafe(curr-Vec3i::Unit(d)*2,&_periodicS)*STENCIL[0]+
              A.get(curr-Vec3i::Unit(d),&_periodicS)*STENCIL[1]+
              A.get(curr,&_periodicS)*STENCIL[2];
      else
        coef*=A.getSafe(curr+Vec3i::Unit(d),&_periodicS)*STENCIL[0]+
              A.get(curr,&_periodicS)*STENCIL[1]+
              A.get(curr-Vec3i::Unit(d),&_periodicS)*STENCIL[2];
    add(B.get(curr),coef);
    sub(B.get(curr-Vec3i::Unit(d),&_periodicS),coef);
    ITERSPEND
    to.add(B);
    //check variable order
    if(variableThres > 0 && B.getAbsMax() < variableThres) {
      it++;
      break;
    }
    B.swap(A);
  }

  //set variable order
  if(variableThres > 0)
    pt.put<sizeType>("advectionOrderAtFrame"+boost::lexical_cast<string>(currFrameId),it);
}
void Advector::advectTImplicit(ScalarField& lambda,const ScalarField& rho,const MACVelocityField& v) const
{
  //back propagate DEDRho
  scalar coef,dt=_pt.get<scalar>("dt",0.0f);
  sizeType order=max<sizeType>(1,_pt.get<sizeType>("advectionOrder",10000));
  Vec3 STENCIL=getStencil(),invCellSzDt=lambda.getInvCellSize()*dt,invCellSzDtFact;
  checkVariableOrder(order);

  ScalarField& A=cacheA();
  ScalarField& B=cacheB();
  A=B=lambda;
  for(sizeType it=0; it<order; it++) {
    B.init(0);
    invCellSzDtFact=invCellSzDt/(scalar)(it+1);
    for(sizeType d=0; d<lambda.getDim(); d++)
      ITERSP_PERIODIC(lambda,_periodic,OMP_PRI(coef))
      scalar deltaDEDLambda=(A.get(curr)-A.get(curr-Vec3i::Unit(d),&_periodicS));
    if((coef=v.getComp(d).get(curr)*invCellSzDtFact[d]) > 0) {
      add(B.getSafe(curr-Vec3i::Unit(d)*2,&_periodicS),coef*deltaDEDLambda*STENCIL[0]);
      add(B.get(curr-Vec3i::Unit(d),&_periodicS),coef*deltaDEDLambda*STENCIL[1]);
      add(B.get(curr,&_periodicS),coef*deltaDEDLambda*STENCIL[2]);
    } else {
      add(B.getSafe(curr+Vec3i::Unit(d),&_periodicS),coef*deltaDEDLambda*STENCIL[0]);
      add(B.get(curr),coef*deltaDEDLambda*STENCIL[1]);
      add(B.get(curr-Vec3i::Unit(d),&_periodicS),coef*deltaDEDLambda*STENCIL[2]);
    }
    ITERSPEND
    lambda.add(B);
    B.swap(A);
  }
}
void Advector::advectVImplicit(VecD& DFDX,const ScalarField& lambda,const ScalarField& rho,const MACVelocityField& v) const
{
  scalar coef,dt=_pt.get<scalar>("dt",0.0f);
  sizeType order=max<sizeType>(1,_pt.get<sizeType>("advectionOrder",10000));
  Vec3 STENCIL=getStencil(),invCellSzDt=rho.getInvCellSize()*dt,invCellSzDtFact;
  checkVariableOrder(order);

  //forward pass
  ScalarField& A=cacheA();
  ScalarField& B=cacheB();
  vector<ScalarField>& LCache=const_cast<vector<ScalarField>&>(_LCache);
  LCache.resize(order);
  LCache[0]=A=B=lambda;
  for(sizeType it=1; it<order; it++) {
    B.init(0);
    for(sizeType d=0; d<rho.getDim(); d++)
      ITERSP_PERIODIC(rho,_periodic,OMP_PRI(coef))
      scalar deltaDEDRho=(A.get(curr)-A.get(curr-Vec3i::Unit(d),&_periodicS));
    if((coef=v.getComp(d).get(curr)*invCellSzDt[d]) > 0) {
      add(B.getSafe(curr-Vec3i::Unit(d)*2,&_periodicS),coef*deltaDEDRho*STENCIL[0]);
      add(B.get(curr-Vec3i::Unit(d),&_periodicS),coef*deltaDEDRho*STENCIL[1]);
      add(B.get(curr),coef*deltaDEDRho*STENCIL[2]);
    } else {
      add(B.getSafe(curr+Vec3i::Unit(d),&_periodicS),coef*deltaDEDRho*STENCIL[0]);
      add(B.get(curr),coef*deltaDEDRho*STENCIL[1]);
      add(B.get(curr-Vec3i::Unit(d),&_periodicS),coef*deltaDEDRho*STENCIL[2]);
    }
    ITERSPEND
    LCache[it]=B;
    B.swap(A);
  }

  //backward pass
  A=B=rho;
  sumFact(LCache,order-1);
  advectVExplicit(DFDX,LCache[order-1],B,v);
  for(sizeType it=1; it<order; it++) {
    B.init(0);
    invCellSzDtFact=invCellSzDt/(scalar)(it+1);
    for(sizeType d=0; d<rho.getDim(); d++)
      ITERSP_PERIODIC(rho,_periodic,OMP_PRI(coef))
      if((coef=v.getComp(d).get(curr)*invCellSzDtFact[d]) > 0)
        coef*=A.getSafe(curr-Vec3i::Unit(d)*2,&_periodicS)*STENCIL[0]+
              A.get(curr-Vec3i::Unit(d),&_periodicS)*STENCIL[1]+
              A.get(curr,&_periodicS)*STENCIL[2];
      else
        coef*=A.getSafe(curr+Vec3i::Unit(d),&_periodicS)*STENCIL[0]+
              A.get(curr,&_periodicS)*STENCIL[1]+
              A.get(curr-Vec3i::Unit(d),&_periodicS)*STENCIL[2];
    add(B.get(curr),coef);
    sub(B.get(curr-Vec3i::Unit(d),&_periodicS),coef);
    ITERSPEND
    sumFact(LCache,order-1-it);
    advectVExplicit(DFDX,LCache[order-1-it],B,v);
    B.swap(A);
  }
}
//data
Vec3 Advector::getStencil() const
{
  if(!_pt.get<bool>("secondOrderAdvector",false))
    return Vec3(0.0f,1.0f,0.0f);
  else return Vec3(-1.0f,6.0f,3.0f)/8.0f;
}
void Advector::checkVariableOrder(sizeType& order) const
{
  //check if user want variable order
  boost::property_tree::ptree& pt=const_cast<boost::property_tree::ptree&>(_pt);
  scalar variableThres=pt.get<scalar>("variableAdvectionThres",1E-5f);
  if(variableThres > 0) {
    sizeType currFrameId=pt.get<sizeType>("currentFrameId",-1);
    ASSERT_MSG(currFrameId >= 0,"currentFrameId not set, when trying to use variable order advector!")
    order=pt.get<sizeType>("advectionOrderAtFrame"+boost::lexical_cast<string>(currFrameId),-1);
    ASSERT_MSG(order >= 0,"order not set, when trying to use variable order advector!")
    //clear to avoid confusion
    pt.erase("currentFrameId");
    ASSERT(pt.get<sizeType>("currentFrameId",-1) == -1)
  }
}
