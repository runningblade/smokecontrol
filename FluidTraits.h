#ifndef FLUID_TRAITS_H
#define FLUID_TRAITS_H

#include "CommonFile/GridBasic.h"
#include "CommonFile/solvers/MatVec.h"

PRJ_BEGIN

struct CurlElem;
template <typename T>
class ParallelVector
{
public:
  typedef typename vector<T>::const_iterator const_iterator;
  typedef typename vector<T>::iterator iterator;
  ParallelVector() {
    clear();
  }
  void clear() {
    _blocks.assign(omp_get_num_procs(),vector<T>());
  }
  void push_back(const T& newVal) {
    _blocks[omp_get_thread_num()].push_back(newVal);
  }
  template <typename IT>
  void insert(IT beg,IT end) {
    vector<T>& v=_blocks[omp_get_thread_num()];
    v.insert(v.end(),beg,end);
  }
  const_iterator begin() const {
    const_cast<ParallelVector<T>&>(*this).join();
    return _blocks[0].begin();
  }
  const_iterator end() const {
    const_cast<ParallelVector<T>&>(*this).join();
    return _blocks[0].end();
  }
  iterator begin() {
    join();
    return _blocks[0].begin();
  }
  iterator end() {
    join();
    return _blocks[0].end();
  }
  const vector<T>& getVector() const {
    const_cast<ParallelVector<T>*>(this)->join();
    return _blocks[0];
  }
  vector<T>& getVector() {
    join();
    return _blocks[0];
  }
private:
  void join() {
    for(sizeType i=1; i<(sizeType)_blocks.size(); i++) {
      _blocks[0].insert(_blocks[0].end(),_blocks[i].begin(),_blocks[i].end());
      _blocks[i].clear();
    }
  }
  vector<vector<T> > _blocks;
};
template <typename T>
class BitVector
{
public:
  void resize(sizeType nr) {
    ASSERT(countBits<int>(nr-1) == 1)
    _inner.resize(log2<sizeType>(nr-1)+1);
  }
  void resizeForce(sizeType nr) const {
    const_cast<BitVector<T>&>(*this).resize(nr);
  }
  sizeType size() const {
    return pow(2,_inner.size()-1)+1;
  }
  const vector<T>& getVector() const {
    return _inner;
  }
  const T& operator[](sizeType i) const {
    checkPos(i);
    return _inner.at(log2<sizeType>(i));
  }
  const T& at(sizeType i) const {
    checkPos(i);
    return _inner.at(log2<sizeType>(i));
  }
  vector<T>& getVector() {
    return _inner;
  }
  T& operator[](sizeType i) {
    checkPos(i);
    return _inner.at(log2<sizeType>(i));
  }
  T& at(sizeType i) {
    checkPos(i);
    return _inner.at(log2<sizeType>(i));
  }
  void clear() {
    _inner.clear();
  }
private:
  static void checkPos(sizeType i) {
    ASSERT_MSG(countBits<int>(i) == 1,"NPOT position not supported by BitVector!")
  }
  vector<T> _inner;
};
struct Traits {
  typedef ScalarUtil<scalarD>::ScalarVecx VecD;
  typedef Eigen::SparseMatrix<scalarD> SMat;
  typedef Eigen::Matrix<sizeType,6,1> Vec6i;
  typedef Eigen::Matrix<char,6,1> Vec6c;
  typedef Eigen::Matrix<scalarD,6,1> Vec6;
  typedef ParallelVector<CurlElem> Curl;
  typedef ParallelVector<Eigen::Triplet<scalarD,sizeType> > TRIPS;
  typedef map<sizeType,boost::shared_ptr<ScalarField> > KeyFrameMap;
  typedef vector<Matd,Eigen::aligned_allocator<Matd> > MatdArr;
  typedef vector<VecD,Eigen::aligned_allocator<VecD> > VecDArr;
  typedef vector<MatdArr> Banded;
  //data copy op
  typedef void (*OP)(scalar& f,scalar b);
  typedef void (*OPD)(scalarD& f,scalarD b);
  typedef void (*OPVD)(VecD& f,VecD b);
  template <typename T>
  static void add(T& f,T b) {
    OMP_ATOMIC_
    f+=b;
  }
  template <typename T>
  static void adds(T& f,T b) {
    f+=b;
  }
  template <typename T>
  static void sub(T& f,T b) {
    OMP_ATOMIC_
    f-=b;
  }
  template <typename T>
  static void subs(T& f,T b) {
    f-=b;
  }
  template <typename T>
  static void set(T& f,T b) {
    f=b;
  }
  template <typename T>
  static void sets(T& f,T b) {
    f=b;
  }
  //interpolation op
  static void restrict(MACVelocityField& v,const Vec3i& curr,sizeType d,scalar val,OP f,const Vec3i& periodicSC);
  static void restrict(ScalarField& s,const Vec3i& curr,scalar val,OP f);
  //tridiagonal solve op
  template <typename MAT_ARR>
  static void triDiagTpl(const MAT_ARR& diag,const MatdArr& off,const VecDArr& rhs,VecDArr& sol,OPVD op=&set<VecD>);
  static void triDiag(const MatdArr& diag,const MatdArr& off,const VecDArr& rhs,VecDArr& sol,OPVD op=&set<VecD>);
  static void triDiag(const vector<const Matd*>& diag,const MatdArr& off,const VecDArr& rhs,VecDArr& sol,OPVD op=&set<VecD>);
  template <typename MAT_ARR>
  static void triDiagNaiveTpl(const MAT_ARR& diag,const MatdArr& off,const VecDArr& rhs,VecDArr& sol);
  static void triDiagNaive(const MatdArr& diag,const MatdArr& off,const VecDArr& rhs,VecDArr& sol);
  static void triDiagNaive(const vector<const Matd*>& diag,const MatdArr& off,const VecDArr& rhs,VecDArr& sol);
  static void triDiagKKT(scalarD diagP,scalarD diagD,const VecD& div,const MatdArr& off,const VecDArr& rhs,VecDArr& sol,bool naive=false);
  static void triDiagKKTReorder(scalarD diagP,scalarD diagD,const VecD& div,const MatdArr& off,const VecDArr& rhs,VecDArr& sol);
  //tridiagonal matrix helper
  template <typename MAT_ARR>
  static void multAddTpl(const MAT_ARR& diag,const MatdArr& off,const MatdArr* off2,const VecDArr& lhs,VecDArr& rhs,scalarD coef);
  static void multAdd(const MatdArr& diag,const MatdArr& off,const MatdArr* off2,const VecDArr& lhs,VecDArr& rhs,scalarD coef);
  static void multAdd(const vector<const Matd*>& diag,const MatdArr& off,const MatdArr* off2,const VecDArr& lhs,VecDArr& rhs,scalarD coef);
  static void multSet(const MatdArr& diag,const MatdArr& off,const MatdArr* off2,const VecDArr& lhs,VecDArr& rhs);
  static void multSet(const vector<const Matd*>& diag,const MatdArr& off,const MatdArr* off2,const VecDArr& lhs,VecDArr& rhs);
  //banded matrix helper
  static void bandedLLT(Banded& lhs,VecDArr& rhs);
  static void bandedLUAsTridiag(const Banded& lhs,VecDArr& rhs);
  static void multAdd(const Banded& bandMat,const VecDArr& lhs,VecDArr& rhs,scalarD coef);
  static Banded& multATA(Banded& bandMat);
  //tridiagonal helper
  template <typename MAT_ARR>
  static void setZeroTpl(MAT_ARR& diag);
  static void setZero(MatdArr& diag);
  static void setZero(VecDArr& diag);
  template <typename MAT_ARR>
  static void copyTpl(const MAT_ARR& from,MAT_ARR& to);
  static void copy(const MatdArr& from,MatdArr& to);
  static void copy(const VecDArr& from,VecDArr& to);
  static MatdArr& mul(MatdArr& a,scalarD coef);
  static VecDArr& mul(VecDArr& a,scalarD coef);
  static scalarD dot(const VecDArr& a,const VecDArr& b);
  static VecDArr& sub(const VecDArr& a,const VecDArr& b,VecDArr& out);
  static VecDArr& add(const VecDArr& a,const VecDArr& b,VecDArr& out);
  static void addSave(VecDArr& a,VecDArr& b);
  static scalarD squaredNorm(const VecDArr& delta);
  //debugger
  static void saveA(const MatdArr& diag,const MatdArr& offDiag,const VecDArr& rhs);
  static void saveB(const MatdArr& diag,const MatdArr& offDiag,const VecDArr& rhs);
  static void saveA(const vector<const Matd*>& diag,const MatdArr& offDiag,const VecDArr& rhs);
  static void saveB(const vector<const Matd*>& diag,const MatdArr& offDiag,const VecDArr& rhs);
  static void debugWrite();
  static MatdArr DA,DB,ODA,ODB;
  static VecDArr RHSA,RHSB;
  //converter
  static Banded& toBanded(Banded& banded,const MatdArr& diag,const MatdArr& offDiag,const MatdArr* offDiag2);
  static Banded toBanded(const MatdArr& diag,const MatdArr& offDiag,const MatdArr* offDiag2);
  static Matd toDense(const MatdArr& diag,const MatdArr& offDiag,const MatdArr* offDiag2);
  static Matd toDenseL(const Banded& banded);
  static Matd toDense(const Banded& banded);
  static VecD toDense(const VecDArr& x);
private:
  static const Matd& extract(const MatdArr& diag,sizeType i);
  static Matd& extract(MatdArr& diag,sizeType i);
  static const VecD& extract(const VecDArr& diag,sizeType i);
  static VecD& extract(VecDArr& diag,sizeType i);
  static const Matd& extract(const vector<const Matd*>& diag,sizeType i);
};

PRJ_END

#endif
