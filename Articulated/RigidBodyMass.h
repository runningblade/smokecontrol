#ifndef RIGID_BODY_MASS_H
#define RIGID_BODY_MASS_H

#include "Utils.h"
#include "../CommonFile/ObjMesh.h"

PRJ_BEGIN

struct RigidBodyMass {
  RigidBodyMass(const ObjMesh& mesh);
  Vec3 getCtr() const {
    return _ctr;
  }
  Eigen::Matrix<scalar,6,6> getMass() const {
    return _mat;
  }
  Eigen::Matrix<scalar,6,6> getMassCOM() const {
    return _matCOM;
  }
  static scalar approximateKineticEnergy(const ObjMesh& mesh,const Mat4& DTDt,scalar delta);
  static void debugRigidBodyMass();
private:
  Eigen::Matrix<scalar,6,6> _mat,_matCOM;
  Vec3 _ctr;
};

PRJ_END

#endif
