#include "ArticulatedBody.h"
#include "RigidBodyMass.h"
#include "../CommonFile/ObjMesh.h"
#include <boost/filesystem/operations.hpp>
#include <boost/lexical_cast.hpp>

USE_PRJ_NAMESPACE

bool check(std::string& buf,const char* name)
{
  std::string bufTmp=buf;
  std::transform(bufTmp.begin(),bufTmp.end(),bufTmp.begin(),::tolower);

  if(bufTmp == name) {
    buf.clear();
    return true;
  } else return false;
}
bool check(std::string& buf,sizeType& num)
{
  try {
    num=boost::lexical_cast<sizeType>(buf);
    return true;
  } catch(...) {
    return false;
  }
}

DOF::DOF():Serializable(typeid(DOF).name()),_lmt(-numeric_limits<scalar>::infinity(),numeric_limits<scalar>::infinity()) {}
DOF::DOF(DOF_TYPE type,sizeType off):Serializable(typeid(DOF).name()),_type(type),_off(off),_lmt(-numeric_limits<scalar>::infinity(),numeric_limits<scalar>::infinity()) {}
bool DOF::read(istream& is, IOData* dat)
{
  readBinaryData(_type,is);
  readBinaryData(_off,is);
  readBinaryData(_lmt,is);
  return is.good();
}
bool DOF::write(ostream& os, IOData* dat) const
{
  writeBinaryData(_type,os);
  writeBinaryData(_off,os);
  writeBinaryData(_lmt,os);
  return os.good();
}
boost::shared_ptr<Serializable> DOF::copy() const
{
  return boost::shared_ptr<Serializable>(new DOF);
}
void DOF::setUnit(scalar length, scalar angle)
{
  if(_type < RX)
    _lmt*=length;
  else _lmt*=angle;
}

Body::ForceContact::ForceContact() {}
Body::ForceContact::ForceContact(const Vec3& r,const Vec3& f)
  :_r(r),_f(f),_isForce(true) {}
Body::ForceContact::ForceContact(const Vec3& r,const Vec3& x0,const Vec3& n)
  :_r(r),_x0(x0),_n(n),_isForce(false) {}

Body::Body() :Serializable(typeid(Body).name())
{
  _M.setZero();
  _dir.setZero();
  _COM.setZero();
  _toParent.setIdentity();
}
bool Body::read(istream& is, IOData* dat)
{
  readBinaryData(_M,is);
  readBinaryData(_dir, is);
  readBinaryData(_COM, is);
  readBinaryData(_toParent, is);
  readVector(_dof, is, dat);
  readBinaryData(_geom, is, dat);

  readBinaryData(_id, is);
  readBinaryData(_name, is);
  readBinaryData(_child, is, dat);
  readBinaryData(_next, is, dat);
  readBinaryData(_parent, is, dat);
  return is.good();
}
bool Body::write(ostream& os, IOData* dat) const
{
  writeBinaryData(_M,os);
  writeBinaryData(_dir, os);
  writeBinaryData(_COM, os);
  writeBinaryData(_toParent, os);
  writeVector(_dof, os, dat);
  writeBinaryData(_geom, os, dat);

  writeBinaryData(_id, os);
  writeBinaryData(_name, os);
  writeBinaryData(_child, os, dat);
  writeBinaryData(_next, os, dat);
  writeBinaryData(_parent, os, dat);
  return os.good();
}
boost::shared_ptr<Serializable> Body::copy() const
{
  return boost::shared_ptr<Serializable>(new Body);
}
bool Body::readASF(std::string& buf,std::istream& is,scalar length,scalar angle,sizeType& dof)
{
  scalar len;
  Vec3 axis;
  char bracket;

  //main loop
  bool begun = false;
  while (is.good() && !is.eof())
    if (!begun && check(buf,"begin"))
      begun = true;
    else if (check(buf,"end")) {
      _dir=(rotation(axis).transpose()*_dir*len).eval();
      _toParent.setIdentity();
      _toParent.block<3,3>(0,0)=rotation(axis);
      _toParent.block<3,1>(0,3)=_dir;
      return begun;
    } else if (check(buf,"id"))
      is >> _id;
    else if (check(buf,"name"))
      is >> _name;
    else if (check(buf,"direction")) {
      is >> _dir[0] >> _dir[1] >> _dir[2];
      _dir /= std::max<scalar>(_dir.norm(), EPS);
    } else if (check(buf,"length")) {
      is >> len;
      len *= length;
    } else if (check(buf,"axis")) {
      is >> axis[0] >> axis[1] >> axis[2] >> buf;
      axis *= angle;
      ASSERT_MSG(check(buf,"xyz"),"We can only handle axis order=XYZ!")
    } else if (check(buf,"dof")) {
      is >> buf;
      while (is.good() && !is.eof())
        if (check(buf,"tx"))
          _dof.push_back(DOF(TX,dof++));
        else if (check(buf,"ty"))
          _dof.push_back(DOF(TY,dof++));
        else if (check(buf,"tz"))
          _dof.push_back(DOF(TZ,dof++));
        else if (check(buf,"rx"))
          _dof.push_back(DOF(RX,dof++));
        else if (check(buf,"ry"))
          _dof.push_back(DOF(RY,dof++));
        else if (check(buf,"rz"))
          _dof.push_back(DOF(RZ,dof++));
        else if (buf == "limits")
          break;
        else is >> buf;
    } else if (check(buf,"limits")) {
      for (sizeType i=0; i<(sizeType)_dof.size(); i++) {
        is >> bracket >> _dof[i]._lmt[0] >> _dof[i]._lmt[1] >> bracket;
        _dof[i].setUnit(length,angle);
      }
    } else if(beginsWith(buf,":"))
      return false;
    else is >> buf;
  return false;
}
Mat4 Body::buildMat(const Mat& dat,vector<DTEntry>& dR,Mat4& dRdt,bool ddR) const
{
  //build matrix
  Mat3 drot[3],ddrot[3][3];
  Vec3 rot=Vec3::Zero(),dRotDt=Vec3::Zero();
  Vec3 trans=Vec3::Zero(),dTransDt=Vec3::Zero();
  Vec3i rotI=Vec3i::Constant(-1),transI=Vec3i::Constant(-1);
  for(sizeType d=0; d<(char)_dof.size(); d++) {
    const DOF& dof=_dof[d];
    if(dof._type >= RX) {
      rotI[dof._type-RX]=dof._off;
      rot[dof._type-RX]=dat(dof._off,0);
      dRotDt[dof._type-RX]=dat(dof._off,1);
    } else {
      transI[dof._type]=dof._off;
      trans[dof._type]=dat(dof._off,0);
      dTransDt[dof._type]=dat(dof._off,1);
    }
  }
  Mat4 T=Mat4::Identity();
  T.block<3,3>(0,0)=rotationDeriv(rot,drot,ddrot);
  T.block<3,1>(0,3)=trans;

  //build dR
  dR.clear();
  for(sizeType d=0; d<3; d++)
    if(transI[d] >= 0) {
      DTEntry e;
      e._col=transI[d];
      e._Dq.block<3,1>(0,3)=Vec3::Unit(d);
      dR.push_back(e);
    }
  for(sizeType d=0; d<3; d++)
    if(rotI[d] >= 0) {
      DTEntry e;
      e._col=rotI[d];
      e._Dq.block<3,3>(0,0)=drot[d];
      e._DqDt.block<3,3>(0,0)=ddrot[d][0]*dRotDt[0]+ddrot[d][1]*dRotDt[1]+ddrot[d][2]*dRotDt[2];
      //compute full second order derivative on request
      for(sizeType dd=0; ddR && dd<3; dd++)
        if(rotI[dd] >= 0) {
          Mat4 DDRDDq=Mat4::Zero();
          DDRDDq.block<3,3>(0,0)=ddrot[d][dd];
          e._DDTDDq.push_back(make_pair(rotI[dd],DDRDDq));
        }
      dR.push_back(e);
    }

  //build dRdT
  dRdt.setZero();
  dRdt.block<3,3>(0,0)=drot[0]*dRotDt[0]+drot[1]*dRotDt[1]+drot[2]*dRotDt[2];
  dRdt.block<3,1>(0,3)=dTransDt;
  return T;
}
Mat4 Body::buildMat(const Vec& dat) const
{
  Vec3 rot=Vec3::Zero();
  Vec3 trans=Vec3::Zero();
  for(sizeType d=0; d<(char)_dof.size(); d++) {
    const DOF& dof=_dof[d];
    if(dof._type >= RX)
      rot[dof._type-RX]=dat[dof._off];
    else trans[dof._type]=dat[dof._off];
  }
  Mat4 T=Mat4::Identity();
  T.block<3,3>(0,0)=rotation(rot);
  T.block<3,1>(0,3)=trans;
  return T;
}
const Mat6& Body::M() const
{
  return _M;
}
const Vec3& Body::dir() const
{
  return _dir;
}
const Vec3& Body::COM() const
{
  return _COM;
}
const Mat4& Body::toParent() const
{
  return _toParent;
}
const vector<DOF>& Body::dof() const
{
  return _dof;
}
sizeType Body::id() const
{
  return _id;
}
const std::string& Body::name() const
{
  return _name;
}
bool Body::hasParent() const
{
  return (bool)_parent;
}
const Body& Body::parent() const
{
  return *_parent;
}
bool Body::hasMesh() const
{
  return (bool)_geom;
}
const StaticGeomCell& Body::mesh() const
{
  return *_geom;
}
void Body::debugRotationDeriv(sizeType dof) const
{
  Mat dat=Mat::Random(dof,2);

  Vec tmp;
  Mat tmpM;
  Mat4 dRdt;
  vector<DTEntry> dR,dRTmp;
  Mat4 R=buildMat(dat,dR,dRdt);

#define DELTA 1E-8f
  tmp=dat.col(0)+dat.col(1)*DELTA;
  INFOV("DRDt Val: %f, Err: %f!",dRdt.norm(),(dRdt-(buildMat(tmp)-R)/DELTA).norm())

  for(sizeType i=0; i<(sizeType)dR.size(); i++) {
    const DTEntry& E=dR[i];
    tmp=dat.col(0);
    tmp[E._col]+=DELTA;
    INFOV("DRDq Val: %f, Err: %f!",E._Dq.norm(),(E._Dq-(buildMat(tmp)-R)/DELTA).norm())

    tmpM=dat;
    tmpM.col(0)+=tmpM.col(1)*DELTA;
    buildMat(tmpM,dRTmp,dRdt);
    INFOV("DDRDqDt Val: %f, Err: %f!",E._DqDt.norm(),(E._DqDt-(dRTmp[i]._Dq-dR[i]._Dq)/DELTA).norm())
  }
}
void Body::addChild(boost::shared_ptr<Body> parent,boost::shared_ptr<Body> child)
{
  //set children
  boost::shared_ptr<Body>* curr = &(parent->_child);
  while (*curr) {
    if(*curr == child)
      return;
    curr = &((*curr)->_next);
  }
  *curr = child;
  //set parent
  child->_parent=parent;
}
void Body::buildMap(boost::unordered_map<std::string, boost::shared_ptr<Body> >& BodyMap,boost::shared_ptr<Body> body)
{
  ASSERT(BodyMap.find(body->name()) == BodyMap.end())
  BodyMap[body->name()]=body;
  for(boost::shared_ptr<Body> curr=body->_child; curr; curr=curr->_next)
    buildMap(BodyMap,curr);
}

PRJ_BEGIN
class BodyOpVTK : public BodyOpT<const Body>
{
public:
  BodyOpVTK(sizeType nrBody,const std::string& path,const std::string& refName)
    :BodyOpT(nrBody),_writer("articulatedFrm",path,true),_refName(refName),_T0(Mat4::Identity()) {}
  virtual void operator()(const Body& b,void* data) {
    BodyOpT::operator()(b,data);
    if(b.name() == _refName)
      _T0=_T[b.id()];
    scalar len=b.dir().norm();
    if(len == 0)
      return;

    const Mat4& T=_T[b.id()];
    if(!b.hasMesh()) {
      Vec3 pts[2]= {T.block<3,1>(0,3),transformHomo<scalar>(T,b.dir())};
      _writer.setRelativeIndex();
      _writer.appendPoints(pts,pts+2);
      _writer.appendCells(VTKWriter<scalar>::IteratorIndex<Vec3i>(0,1,0),VTKWriter<scalar>::IteratorIndex<Vec3i>(1,1,0),VTKWriter<scalar>::LINE,true);
    } else {
      ObjMesh tmp;
      b.mesh().getMesh(tmp);
      Mat4 TT=_T0.inverse()*T;
      tmp.getPos()=TT.block<3,1>(0,3);
      tmp.getT()=TT.block<3,3>(0,0);
      tmp.applyTrans(Vec3::Zero());
      tmp.smooth();
      tmp.writeVTK(_writer,false,false);
      _meshAssembly.addMesh(tmp,b.name());
    }
  }
  VTKWriter<scalar> _writer;
  ObjMesh _meshAssembly;
  const string& _refName;
  Mat4 _T0;
};
class BodyOpBuildM : public BodyOpT<Body>
{
  class BodyTransVTK
  {
  public:
    typedef Vec3 value_type;
    typedef vector<Vec3,Eigen::aligned_allocator<Vec3> >::const_iterator CIter;
    BodyTransVTK(CIter it,const Vec3& dir=Vec3::Zero(),const Mat4& T=Mat4::Identity()):_it(it) {
      _len=dir.norm();
      Mat4 R=Mat4::Identity();
      R.block<3,3>(0,0)=Quat::FromTwoVectors(Vec3::Unit(1)*_len,dir).toRotationMatrix();
      _TT=T*R;
    }
    bool operator!=(const BodyTransVTK& other) const {
      return _it != other._it;
    }
    virtual Vec3 operator*() const {
      Vec3 pt=*_it;
      if(pt[1] > 0)pt[1]+=_len;
      return transformHomo<scalar>(_TT,pt);
    }
    void operator++() {
      _it++;
    }
    CIter _it;
    scalar _len;
    Mat4 _TT;
  };
public:
  BodyOpBuildM(sizeType nrBody,const ObjMesh& joint)
    :BodyOpT(nrBody),_joint(joint) {}
  virtual void operator()(Body& b,void* data) {
    BodyOpT::operator()(b,data);
    scalar len=b.dir().norm();
    if(len == 0 || _joint.getV().empty())
      return;

    ObjMesh mesh=_joint;
    mesh.getV().clear();
    for(BodyTransVTK beg(_joint.getV().begin(),b.dir(),Mat4::Identity()),end(_joint.getV().end()); beg!=end; ++beg)
      mesh.getV().push_back(*beg);
    mesh.smooth();

    StaticGeom geom(mesh.getDim());
    geom.addGeomMesh(Mat4::Identity(),mesh);

    RigidBodyMass mass(mesh);
    b._M=mass.getMass();
    b._COM=mass.getCtr();
    b._geom=geom.getGPtr(geom.nrG()-1);
  }
  const ObjMesh& _joint;
};
PRJ_END
ArticulatedBody::ArticulatedBody():Serializable("")
{
  _bodyMap["root"]=boost::shared_ptr<Body>(new Body);
}
bool ArticulatedBody::read(istream& is)
{
  IOData dat;
  StaticGeom::registerType(dat);
  dat.registerType<DOF>();
  dat.registerType<Body>();

  boost::shared_ptr<Body> root;
  readBinaryData(root,is,&dat);
  readBinaryData(_frm,is);

  _bodyMap.clear();
  Body::buildMap(_bodyMap,root);
  return is.good();
}
bool ArticulatedBody::write(ostream& os) const
{
  IOData dat;
  StaticGeom::registerType(dat);
  dat.registerType<DOF>();
  dat.registerType<Body>();

  writeBinaryData(_bodyMap.find("root")->second,os,&dat);
  writeBinaryData(_frm,os);
  return os.good();
}
bool ArticulatedBody::readASF(std::istream& is,const ObjMesh& joint)
{
  std::string buf;
  sizeType dof=0;
  scalar mass=1, length=1, angle=1;
  boost::shared_ptr<Body> root;

  //main loop
  is >> buf;
  while (is.good() && !is.eof())
    if (check(buf,":units")) {
      is >> buf;
      while (is.good() && !is.eof())
        if (check(buf,"mass"))
          is >> mass;
        else if (check(buf,"length"))
          is >> length;
        else if (check(buf,"angle")) {
          is >> buf;
          angle = check(buf,"deg") ? M_PI / 180.0f : 1.0f;
        } else if(beginsWith(buf,":"))
          break;
        else is >> buf;
    } else if (check(buf,":root")) {
      ostringstream oss;
      oss << "begin" << std::endl;
      oss << "id 0" << std::endl;
      oss << "name root" << std::endl;
      oss << "length 0.0" << std::endl;
      oss << "direction 0.0 0.0 0.0" << std::endl;
      oss << "axis 0.0 0.0 0.0 xyz" << std::endl;
      oss << "dof " << std::endl;

      is >> buf;
      while (is.good() && !is.eof())
        if(check(buf,"order")) {
          is >> buf;
          oss << buf << " ";
          is >> buf;
          oss << buf << " ";
          is >> buf;
          oss << buf << " ";
          is >> buf;
          oss << buf << " ";
          is >> buf;
          oss << buf << " ";
          is >> buf;
          oss << buf << std::endl;
          break;
        } else is >> buf;

      //oss << "limits (0 0) (0 0) (0 0) (0 0) (0 0) (0 0)" << std::endl;
      oss << "end" << std::endl;
      root.reset(new Body);
      istringstream iss(oss.str());
      root->readASF(buf,iss,length,angle,dof);
      _bodyMap["root"] = root;
    } else if (check(buf,":bonedata"))
      while (is.good() && !is.eof()) {
        boost::shared_ptr<Body> b(new Body);
        if (b->readASF(buf,is,length,angle,dof))
          _bodyMap[b->_name] = b;
        else break;
      }
    else if (check(buf,":hierarchy")) {
      is >> buf;
      bool begun = false;
      while (is.good() && !is.eof())
        if (check(buf,"begin"))
          begun = true;
        else if (check(buf,"end")) {
          //valid return state
          buildToParent(*root);
          BodyOpBuildM op(nrBody(),joint);
          transform(*root,op,NULL);
          return begun && is.good();
        } else if(_bodyMap.find(buf) != _bodyMap.end()) {
          std::string child;
          getline(is, child);
          std::istringstream iss(child);
          while (!iss.eof()) {
            iss >> child;
            if (_bodyMap.find(child) == _bodyMap.end()) {
              WARNINGV("Cannot find Body %s!", child.c_str())
              return false;
            }
            Body::addChild(_bodyMap.find(buf)->second,_bodyMap.find(child)->second);
          }
          buf.clear();
        } else is >> buf;
    } else is >> buf;
  return false;
}
bool ArticulatedBody::readAMC(std::istream& is)
{
  scalar angle=1;
  sizeType nrFrm,idFrm,nrBody;
  std::string buf,name;

  //count nrFrm
  while(getline(is,buf).good() && !is.eof()) {
    istringstream(buf) >> name;
    if(check(name,":degrees"))
      angle=M_PI / 180.0f;
    else check(name,nrFrm);
  }
  INFOV("Found %ld frames!",nrFrm)

  //read frm
  Mat frm(nrDof(),nrFrm);
  is.clear(ios::eofbit);
  is.seekg(0,ios::beg);
  getline(is,buf);
  while(is.good() && !is.eof()) {
    istringstream(buf) >> name;
    if(!check(name,idFrm)) {
      getline(is,buf);
      continue;
    }
    for(nrBody=0; getline(is,buf).good() && !is.eof() && nrBody < nrValidBody(); nrBody++) {
      istringstream iss(buf);
      iss >> name;
      if(_bodyMap.find(name) == _bodyMap.end()) {
        WARNINGV("Cannot find Body %s!", name.c_str())
        return false;
      }
      Body& b=*(_bodyMap.find(name)->second);
      for(sizeType d=0; d<(sizeType)b._dof.size(); d++) {
        const DOF& dof=b._dof[d];
        iss >> frm(dof._off,idFrm-1);
        if(dof._type > TZ)	//it is an angle
          frm(dof._off,idFrm-1)*=angle;
      }
    }
    if(nrBody != nrValidBody()) {
      WARNINGV("nrBody %ld != nrValidBody %ld!",nrBody,nrValidBody())
      return false;
    }
  }
  if(idFrm == nrFrm) {
    Mat tmp=_frm;
    _frm.resize(frm.rows(),tmp.cols()+frm.cols());
    _frm.block(0,0,tmp.rows(),tmp.cols())=tmp;
    _frm.block(0,tmp.cols(),frm.rows(),frm.cols())=frm;
    INFOV("Read %ld frames in all!",_frm.cols())
    return true;
  } else return false;
}
void ArticulatedBody::randomShuffle()
{
  vector<sizeType> id(nrBody());
  for(sizeType i=0; i<nrBody(); i++)id[i]=i;
  std::random_shuffle(id.begin(),id.end());

  sizeType i=0;
  for(BodyMap::const_iterator beg=_bodyMap.begin(),end=_bodyMap.end(); beg!=end; beg++,i++)
    beg->second->_id=id[i];
}
Body& ArticulatedBody::root()
{
  return *(_bodyMap.find("root")->second);
}
const Body& ArticulatedBody::root() const
{
  return *(_bodyMap.find("root")->second);
}
const Mat& ArticulatedBody::frm() const
{
  return _frm;
}
sizeType ArticulatedBody::nrFrm() const
{
  return _frm.cols();
}
sizeType ArticulatedBody::nrDof() const
{
  sizeType nrDof=0;
  for(BodyMap::const_iterator beg=_bodyMap.begin(),end=_bodyMap.end(); beg!=end; beg++)
    nrDof+=(sizeType)beg->second->_dof.size();
  return nrDof;
}
sizeType ArticulatedBody::nrBody() const
{
  return (sizeType)_bodyMap.size();
}
sizeType ArticulatedBody::nrValidBody() const
{
  sizeType nrValidBody=0;
  for(BodyMap::const_iterator beg=_bodyMap.begin(),end=_bodyMap.end(); beg!=end; beg++)
    nrValidBody+=beg->second->_dof.empty() ? 0 : 1;
  return nrValidBody;
}
const Body& ArticulatedBody::findBody(sizeType id) const
{
  for(BodyMap::const_iterator beg=_bodyMap.begin(),end=_bodyMap.end(); beg!=end; beg++)
    if(beg->second->_id == id)return *(beg->second);
  return *(_bodyMap.find("root")->second);
}
const Body& ArticulatedBody::findBody(const std::string& name) const
{
  return *(_bodyMap.find(name)->second);
}
void ArticulatedBody::writeMocapVTK(const std::string& path,sizeType begId,sizeType step,const std::string& refName) const
{
  boost::filesystem::create_directory(path);
  sizeType nrF=nrFrm();
  for(sizeType i=begId; i<nrF; i+=step) {
    ostringstream oss;
    oss << path << "/frm" << i << ".vtk";
    Vec v(_frm.col(i));
    writeFrameVTK(oss.str(),&v,refName);
  }
}
void ArticulatedBody::writeFrameVTK(const std::string& path,const Vec* dof,const std::string& refName) const
{
  BodyOpVTK op(nrBody(),path,refName);
  transform(root(),op,(void*)dof);

  boost::filesystem::path pathObj=path;
  pathObj.replace_extension(".obj");
  boost::filesystem::ofstream os(pathObj);
  op._meshAssembly.write(os);
}
void ArticulatedBody::transform(Body& b,BodyOp<Body>& op,void* data)
{
  //draw current Body
  op(b,data);
  //draw children
  boost::shared_ptr<Body> curr=b._child;
  while(curr) {
    transform(*curr,op,data);
    curr=curr->_next;
  }
}
void ArticulatedBody::transform(const Body& b,BodyOp<const Body>& op,void* data) const
{
  //draw current Body
  op(b,data);
  //draw children
  boost::shared_ptr<Body> curr=b._child;
  while(curr) {
    transform(*curr,op,data);
    curr=curr->_next;
  }
}
void ArticulatedBody::debugRotationDeriv() const
{
  for(BodyMap::const_iterator beg=_bodyMap.begin(),end=_bodyMap.end(); beg!=end; beg++)
    beg->second->debugRotationDeriv(nrDof());
}
void ArticulatedBody::buildToParent(Body& b)
{
  //build children first
  boost::shared_ptr<Body> curr=b._child;
  while(curr) {
    buildToParent(*curr);
    curr=curr->_next;
  }

  //build toParent
  Mat4 childF=b._toParent;
  Mat4 parentF=b._parent ? b._parent->_toParent : Mat4::Identity();
  childF.block<3,1>(0,3)=parentF.block<3,3>(0,0)*parentF.block<3,1>(0,3);
  parentF.block<3,1>(0,3).setZero();
  b._toParent=parentF.transpose()*childF;
}
