#ifndef ARTICULATED_BODY_H
#define ARTICULATED_BODY_H

#include "Utils.h"
#include "../CommonFile/geom/StaticGeom.h"
#include <boost/unordered_map.hpp>

PRJ_BEGIN

enum DOF_TYPE {TX,TY,TZ,RX,RY,RZ};
class DOF : public Serializable
{
  friend class Body;
public:
  DOF();
  DOF(DOF_TYPE type,sizeType off);
  virtual bool read(istream& is, IOData* dat);
  virtual bool write(ostream& os, IOData* dat) const;
  virtual boost::shared_ptr<Serializable> copy() const;
  void setUnit(scalar length, scalar angle);
  char _type;
  sizeType _off;
  Vec2 _lmt;
};
class Body : public Serializable
{
  friend class ArticulatedBody;
  friend class BodyOpBuildM;
public:
  class ForceContact
  {
  public:
    ForceContact();
    ForceContact(const Vec3& r,const Vec3& f);
    ForceContact(const Vec3& r,const Vec3& x0,const Vec3& n);
    Vec3 _r;	//in reference space
    Vec3 _f;	//in global space
    Vec3 _x0,_n;	//contact point/normal with depth
    bool _isForce;
    const Body* _body;
  };
  Body();
  virtual bool read(istream& is, IOData* dat);
  virtual bool write(ostream& os, IOData* dat) const;
  virtual boost::shared_ptr<Serializable> copy() const;
  bool readASF(std::string& buf,std::istream& is, scalar length, scalar angle,sizeType& dof);
  Mat4 buildMat(const Mat& dat,vector<DTEntry>& dR,Mat4& dRdt,bool ddR=false) const;
  Mat4 buildMat(const Vec& dat) const;
  //getter
  const Mat6& M() const;
  const Vec3& dir() const;
  const Vec3& COM() const;
  const Mat4& toParent() const;
  const vector<DOF>& dof() const;
  sizeType id() const;
  const std::string& name() const;
  bool hasParent() const;
  const Body& parent() const;
  bool hasMesh() const;
  const StaticGeomCell& mesh() const;
  void debugRotationDeriv(sizeType nrDof) const;
private:
  static void addChild(boost::shared_ptr<Body> parent,boost::shared_ptr<Body> child);
  static void buildMap(boost::unordered_map<std::string, boost::shared_ptr<Body> >& BodyMap,boost::shared_ptr<Body> body);
  //param
  Mat6 _M;
  Vec3 _dir,_COM;
  Mat4 _toParent;
  vector<DOF> _dof;
  //misc
  sizeType _id;
  std::string _name;
  boost::shared_ptr<Body> _child,_next,_parent;
  boost::shared_ptr<StaticGeomCell> _geom;
};
template <typename Body>
class BodyOp
{
public:
  BodyOp(sizeType nrBody) {}
  virtual void operator()(Body& b,void* data)=0;
  virtual ~BodyOp() {}
};
template <typename Body>
class BodyOpT : public BodyOp<Body>
{
public:
  BodyOpT(sizeType nrBody):BodyOp<Body>(nrBody),_T(nrBody) {}
  virtual void operator()(Body& b,void* data) {
    Mat4& T=_T[b.id()];
    T.setIdentity();
    if(b.hasParent())
      T=_T[b.parent().id()];

    T*=b.toParent();
    if(data)T*=b.buildMat(*(Vec*)data);
  }
  virtual ~BodyOpT() {}
  vector<Mat4> _T;
};
class ArticulatedBody : public Serializable
{
  typedef boost::unordered_map<std::string, boost::shared_ptr<Body> > BodyMap;
public:
  ArticulatedBody();
  virtual bool read(istream& is);
  virtual bool write(ostream& os) const;
  bool readASF(std::istream& is,const ObjMesh& joint);
  bool readAMC(std::istream& is);
  void randomShuffle();
  Body& root();
  const Body& root() const;
  const Mat& frm() const;
  sizeType nrFrm() const;
  sizeType nrDof() const;
  sizeType nrBody() const;
  sizeType nrValidBody() const;
  const Body& findBody(sizeType id) const;
  const Body& findBody(const std::string& name) const;
  void writeMocapVTK(const std::string& path,sizeType begId=0,sizeType step=1,const std::string& refName="") const;
  void writeFrameVTK(const std::string& path,const Vec* dof=NULL,const std::string& refName="") const;
  void transform(Body& b,BodyOp<Body>& op,void* dof);
  void transform(const Body& b,BodyOp<const Body>& op,void* dof) const;
  void debugRotationDeriv() const;
private:
  void buildToParent(Body& b);
  BodyMap  _bodyMap;
  Mat _frm;
};

PRJ_END

#endif
