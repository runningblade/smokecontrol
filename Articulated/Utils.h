#ifndef ROTATION_H
#define ROTATION_H

#include "../CommonFile/MathBasic.h"

PRJ_BEGIN

typedef Eigen::Matrix<scalar,-1,-1> Mat;
typedef Eigen::Matrix<scalar,-1,1> Vec;
typedef Eigen::Matrix<scalar,6,6> Mat6;
typedef Eigen::Matrix<scalar,6,1> Vec6;

//rotation
Mat3 rotationDeriv(Vec3 xyz,Mat3 dR[3],Mat3 ddR[3][3]);
Mat3 rotation(Vec3 xyz);
Mat3 cross(Vec3 r);
Vec3 invCross(Mat3 r);
void debugRotationDeriv();

//sparse matrix
class DTEntry
{
public:
  DTEntry();
  DTEntry(const Mat4& Dq,const Mat4& DqDt,sizeType col);
  virtual ~DTEntry() {}
  bool operator<(const DTEntry& other) const;
  bool operator==(const DTEntry& other) const;
  static void merge(const vector<DTEntry>& I,const vector<DTEntry>& J,vector<DTEntry>& out);
  static void merge(vector<pair<sizeType,Mat4> >& J,vector<pair<sizeType,Mat4> >& out);
  Mat4 _Dq,_DqDt;
  sizeType _col;
  Vec6 _J,_DJDt;
  //compute on request
  Mat6 _DMgDq;
  vector<pair<sizeType,Mat4> > _DDTDDq,_crossTerm;
};
class DTTransfer
{
public:
  void computeJ(const Mat6& M);
  void computeDMgDq(const Mat6& M);
  vector<DTEntry> _entry;
  Mat6 _Mg,_cwMg;
  Mat4 _T,_DTDt;
  Vec6 _vel;
};

PRJ_END

#endif
