#include "Utils.h"

PRJ_BEGIN

//rotation
Mat3 rotationX(scalar angle,bool zero)
{
  Mat3 ret;
  if(zero)ret.setZero();
  else ret.setIdentity();
  ret(1,1)=ret(2,2)=cos(angle);
  ret(2,1)=sin(angle);
  ret(1,2)=-ret(2,1);
  return ret;
}
Mat3 rotationY(scalar angle,bool zero)
{
  Mat3 ret;
  if(zero)ret.setZero();
  else ret.setIdentity();
  ret(0,0)=ret(2,2)=cos(angle);
  ret(0,2)=sin(angle);
  ret(2,0)=-ret(0,2);
  return ret;
}
Mat3 rotationZ(scalar angle,bool zero)
{
  Mat3 ret;
  if(zero)ret.setZero();
  else ret.setIdentity();
  ret(0,0)=ret(1,1)=cos(angle);
  ret(1,0)=sin(angle);
  ret(0,1)=-ret(1,0);
  return ret;
}
Mat3 rotationDeriv(Vec3 xyz,Mat3 dR[3],Mat3 ddR[3][3])
{
  Mat3 RZ=rotationZ(xyz[2],false);
  Mat3 RY=rotationY(xyz[1],false);
  Mat3 RX=rotationX(xyz[0],false);
  Mat3 R=RZ*RY*RX;

  Mat3 dRZ=rotationZ(xyz[2]+M_PI/2,true);
  Mat3 dRY=rotationY(xyz[1]+M_PI/2,true);
  Mat3 dRX=rotationX(xyz[0]+M_PI/2,true);

  dR[2]=dRZ*RY*RX;
  dR[1]=RZ*dRY*RX;
  dR[0]=RZ*RY*dRX;

  ddR[2][0]=dRZ*RY*dRX;
  ddR[2][1]=dRZ*dRY*RX;
  ddR[2][2]=-rotationZ(xyz[2],true)*RY*RX;

  ddR[1][0]=RZ*dRY*dRX;
  ddR[1][1]=-RZ*rotationY(xyz[1],true)*RX;
  ddR[1][2]=ddR[2][1];

  ddR[0][0]=-RZ*RY*rotationX(xyz[0],true);
  ddR[0][1]=ddR[1][0];
  ddR[0][2]=ddR[2][0];
  return R;
}
Mat3 rotation(Vec3 xyz)
{
  return
    rotationZ(xyz[2],false)*
    rotationY(xyz[1],false)*
    rotationX(xyz[0],false);
}
Mat3 cross(Vec3 v)
{
  Mat3 ret;
  ret.setZero();
  ret(0,1)=-v[2];
  ret(0,2)=v[1];
  ret(1,2)=-v[0];
  ret(1,0)=v[2];
  ret(2,0)=-v[1];
  ret(2,1)=v[0];
  return ret;
}
Vec3 invCross(Mat3 r)
{
  return Vec3(r(2,1),r(0,2),r(1,0));
}
void debugRotationDeriv()
{
  Mat3 dR[3],ddR[3][3];
  Mat3 dRTmp[3],ddRTmp[3][3];
  Vec3 xyz=Vec3::Random();
  Mat3 R=rotationDeriv(xyz,dR,ddR);

#define DELTA 1E-8f
  for(sizeType d=0; d<3; d++) {
    Vec3 tmp=xyz+Vec3::Unit(d)*DELTA;
    Mat3 RTmp=rotationDeriv(tmp,dRTmp,ddRTmp);

    INFOV("dR Val: %f, Err: %f!",dR[d].norm(),(dR[d]-(RTmp-R)/DELTA).norm())
    INFOV("ddR[0] Val: %f, Err: %f!",ddR[d][0].norm(),(ddR[d][0]-(dRTmp[0]-dR[0])/DELTA).norm())
    INFOV("ddR[1] Val: %f, Err: %f!",ddR[d][1].norm(),(ddR[d][1]-(dRTmp[1]-dR[1])/DELTA).norm())
    INFOV("ddR[2] Val: %f, Err: %f!",ddR[d][2].norm(),(ddR[d][2]-(dRTmp[2]-dR[2])/DELTA).norm())
  }
}

//sparse matrix
void check(const vector<DTEntry>& I)
{
  for(sizeType i=0; i<(sizeType)I.size()-1; i++)
    ASSERT(I[i] < I[i+1])
  }
void check(const vector<pair<sizeType,Mat4> >& I)
{
  for(sizeType i=0; i<(sizeType)I.size()-1; i++)
    ASSERT(I[i].first < I[i+1].first)
  }
DTEntry::DTEntry():_Dq(Mat4::Zero()),_DqDt(Mat4::Zero()),_col(-1) {}
DTEntry::DTEntry(const Mat4& Dq,const Mat4& DqDt,sizeType col):_Dq(Dq),_DqDt(DqDt),_col(col) {}
bool DTEntry::operator<(const DTEntry& other) const
{
  return _col < other._col;
}
bool DTEntry::operator==(const DTEntry& other) const
{
  return _col == other._col;
}
void DTEntry::merge(const vector<DTEntry>& I,const vector<DTEntry>& J,vector<DTEntry>& out)
{
  check(I);
  check(J);

  out.clear();
  sizeType i=0,j=0;
  while(i<(sizeType)I.size() && j<(sizeType)J.size()) {
    if(I[i] < J[j])
      out.push_back(I[i++]);
    else if(J[j] < I[i])
      out.push_back(J[j++]);
    else ASSERT_MSG(false,"Parent/child cannot share same degree of freedom!")
    }
  while(i<(sizeType)I.size())
    out.push_back(I[i++]);
  while(j<(sizeType)J.size())
    out.push_back(J[j++]);

  check(out);
}
void DTEntry::merge(vector<pair<sizeType,Mat4> >& J,vector<pair<sizeType,Mat4> >& out)
{
  vector<pair<sizeType,Mat4> > I=out;
  check(I);
  check(J);

  out.clear();
  sizeType i=0,j=0;
  while(i<(sizeType)I.size() && j<(sizeType)J.size()) {
    if(I[i].first < J[j].first)
      out.push_back(I[i++]);
    else if(J[j].first < I[i].first)
      out.push_back(J[j++]);
    else {
      out.push_back(make_pair(I[i].first,I[i].second+J[j].second));
      i++;
      j++;
    }
  }
  while(i<(sizeType)I.size())
    out.push_back(I[i++]);
  while(j<(sizeType)J.size())
    out.push_back(J[j++]);

  check(out);
  vector<pair<sizeType,Mat4> > other;
  J.swap(other);
}
void DTTransfer::computeJ(const Mat6& M)
{
  //omega generalized V
  Mat3 cOmega=_DTDt.block<3,3>(0,0)*_T.block<3,3>(0,0).transpose();
  _vel.block<3,1>(0,0)=_DTDt.block<3,1>(0,3);
  _vel.block<3,1>(3,0)=invCross(cOmega);

  //compute Mg
  Mat6 TR=Mat6::Zero(),DTR=Mat6::Zero();
  TR.block<3,3>(0,0)=_T.block<3,3>(0,0);
  TR.block<3,3>(3,3)=_T.block<3,3>(0,0);
  _Mg=TR*(M*TR.transpose());

  //compute [w]Mg+Mg\dot{T}^c
  DTR.block<3,3>(0,0)=_DTDt.block<3,3>(0,0);
  DTR.block<3,3>(3,3)=_DTDt.block<3,3>(0,0);
  _cwMg.setZero();
  _cwMg.block<6,3>(0,3)=(DTR*M*TR.transpose()).block<6,3>(0,3);

  //compute J/DJDt
  Mat3 DwDq;
  for(sizeType i=0; i<(sizeType)_entry.size(); i++) {
    DTEntry& e=_entry[i];
    e._J.block<3,1>(0,0)=e._Dq.block<3,1>(0,3);
    DwDq=e._Dq.block<3,3>(0,0)*_T.block<3,3>(0,0).transpose();
    e._J.block<3,1>(3,0)=invCross(DwDq);

    e._DJDt.block<3,1>(0,0)=e._DqDt.block<3,1>(0,3);
    DwDq=e._DqDt.block<3,3>(0,0)*_T.block<3,3>(0,0).transpose()+
         e._Dq.block<3,3>(0,0)*_DTDt.block<3,3>(0,0).transpose();
    e._DJDt.block<3,1>(3,0)=invCross(DwDq);
  }
}
void DTTransfer::computeDMgDq(const Mat6& M)
{
  Mat6 TR=Mat6::Zero(),DTRDq=Mat6::Zero();
  TR.block<3,3>(0,0)=_T.block<3,3>(0,0);
  TR.block<3,3>(3,3)=_T.block<3,3>(0,0);
  for(sizeType i=0; i<(sizeType)_entry.size(); i++) {
    DTEntry& e=_entry[i];
    DTRDq.block<3,3>(0,0)=e._Dq.block<3,3>(0,0);
    DTRDq.block<3,3>(3,3)=e._Dq.block<3,3>(0,0);

    e._DMgDq=DTRDq*M*TR.transpose();
    e._DMgDq=(e._DMgDq+e._DMgDq.transpose()).eval();
  }
}

PRJ_END
